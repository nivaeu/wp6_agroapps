#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 23 11:28:20 2022

@author: ggous
"""

import os
import numpy as np
import pandas as pd
import pickle
import seaborn as sns
import mlflow
import mlflow.xgboost
import xgboost as xgb
import logging
import matplotlib.pyplot as plt
# -- IF USE GPU  - #
# try:
#     from sklearn.utils import safe_indexing
# except ImportError:
#     from sklearn.utils import _safe_indexing
from mlflow.models.signature import infer_signature
from collections import Counter
from xgboost import plot_importance
from sklearn.metrics import classification_report, f1_score
from sklearn.model_selection import StratifiedKFold
from imblearn.over_sampling import BorderlineSMOTE
from imblearn.under_sampling import RandomUnderSampler
from imblearn.pipeline import Pipeline
from yellowbrick.classifier import ClassificationReport
from tune_sklearn import TuneSearchCV
from params import SEED, EARLY_STOP, NFOLDS, EVAL_METRIC, USE_GPU, NB_TRIALS, \
    xgb_params, NB_CPUS, DEBUG, MLFLOW, logger, RAY_TUNER, SEARCH_OPTIMIZATION

if RAY_TUNER:
    import ray
    #ray.shutdown()
    if not ray.is_initialized():
        ray.init(num_cpus=NB_CPUS)
        #os.environ["TUNE_MAX_PENDING_TRIALS_PG"] = "1"
        #os.environ["TUNE_DISABLE_AUTO_CALLBACK_LOGGERS"] = "1"
    
#plt.style.reload_library()
sns.set()
#plt.style.use(['scientific','no-latex']) # plt.style.use(['science','no-latex'])
    
# def xgb_f1(y, t, threshold=0.5):
#     t = t.get_label()
#     y_pred = (y > threshold).astype(int)
#     y_pred = np.argmax(y_pred, axis=1)
#     return 'f1',f1_score(t, y_pred, average='micro')
                
def xgb_model(x_train, 
              y_train,
              x_val,
              y_val,
              features,
              classes):
    '''
    

    Parameters
    ----------
    x_train : numpy array
        Train X input data.
    y_train : numpy array
        Train y label data.
    x_val : numpy array
        Validation X data.
    y_val : numpy array
        Validation y label data.
    features : List
        Feature names.
    classes : List
        Classes.

    Returns
    -------
    xgb_final : Xgboost model
        Xgboost model.

    '''
    nb_classes = len(np.unique(classes))
    kfold = StratifiedKFold(n_splits=NFOLDS, shuffle=True, random_state=SEED)
    result = []
    fold = 1
    if DEBUG:
        fig, ax = plt.subplots(NFOLDS, len(EVAL_METRIC), sharex=True)#, figsize=(12,12))
        fig.tight_layout()
        #fig.suptitle('Training losses')
    for i, (train_idx, val_idx) in enumerate(kfold.split(x_train, y_train)):
        x_train_, y_train_ = x_train[train_idx, :], y_train[train_idx]
        x_val_, y_val_ = x_train[val_idx, :], y_train[val_idx]
        
        # -------------------- SMOTE -------------------------------------------- #
        # Creating a sampling strategy for SMOTE
        # Because we have high imbalance dataset, we don't just want to 
        # create the same number of cases for each class with the majority class
        # That's why we choose a ratio of oversampling for the minority classes 
        # and a ratio of undersampling for the majority classes
        # Count every class
        counter = Counter(y_train_)
        # sort counter by highest values
        sorted_counter = {k: b for k, b in sorted(
            counter.items(), key=lambda element: element[1], reverse=True)}
        # select a ratio for minority classes 
        # If value is between 1000-2000 or below 1000 then oversample
        # 10% of the highest class count
        # If value is between 2000-35000, then oversample 15%
        # if value is > 10000 , then under sample 30%
        strategy_dict = {}
        # since dict is sorted the max value  is the first element
        max_val = list(sorted_counter.values())[0]
        for (key, value) in sorted_counter.items():
            if (value >= 100) and (value <= 500) or (value < 100):
                strategy_dict[key] = int((max_val * 0.5)) + value
            elif (value > 3000) and (value <= 5000):
                strategy_dict[key] = value - int((max_val * 0.2))
            elif (value > 5000) and (value <= 8000):
                strategy_dict[key] = value - int((max_val * 0.35))  
            elif (value > 8000):
                strategy_dict[key] = value - int((max_val * 0.4))  
            else:
                strategy_dict[key] = value
            
        # plot 
        # pd.DataFrame(strategy_dict, index=[0]).plot(kind = 'bar')
        # plt.show()
       
        # break dict to oversample and undersample
        # keep the values that changed from the initial (counter) dict
        # either up or down (place the values that haven't changed to the under dict)
        over = {}
        under = {}
        for k, v in strategy_dict.items():
            if v > counter[k]:
                over[k] = v
            else:
                under[k] = counter[k]

        oversample = BorderlineSMOTE(sampling_strategy=over)
        undersample = RandomUnderSampler(sampling_strategy=under)
        steps = [('o', oversample), ('u', undersample)]
        pipeline = Pipeline(steps=steps)
        
        X_balanced, y_balanced = pipeline.fit_resample(x_train_, y_train_)
        # ----------------------------------------------------------------------- #
        if DEBUG:
            print('\n-----------------------------------------------------\n')
            counter =  Counter(y_balanced)
            print('Count cases during SMOTE, fold # {0}\n'.format(fold))
            for key, value in counter.items():
                print('Class {0}: \t {1}'.format(key, value))
            fig2, ax2 = plt.subplots()
            pd.DataFrame(counter, index=[0]).plot(kind = 'bar', ax=ax2)
            plt.title('Count classes on fold# {0}'.format(fold))
            print('\n-----------------------------------------------------\n')
    
        if RAY_TUNER:
            # Initialize classifier
            xgb_class = xgb.XGBClassifier(objective ='multi:softprob',
                                          #tree_method = "gpu_hist",
                                          num_class=nb_classes,
                                          #disable_default_eval_metric=True,
                                          use_label_encoder=False,
                                          #eval_metric=EVAL_METRIC, 
                                          seed=SEED,
                                          enable_categorical=False)

            model = TuneSearchCV(
                xgb_class,
                param_distributions=xgb_params,
                n_trials=NB_TRIALS,
                max_iters=15,
                #return_train_score=True,
                search_optimization=SEARCH_OPTIMIZATION,
                early_stopping=True,
                scoring='f1_micro',
                n_jobs=NB_CPUS,
                name='Ray tune',
                verbose=0,
                local_dir='./ray_results',
                use_gpu=USE_GPU,
                )
        else:
            # Use this just for a quick run!
            model = xgb.XGBClassifier(objective ='multi:softprob',
                                      #tree_method = "gpu_hist",
                                      num_class=nb_classes,
                                      #disable_default_eval_metric=True,
                                      use_label_encoder=False,
                                      #eval_metric=EVAL_METRIC, 
                                      seed=SEED,
                                      enable_categorical=False,
                                      learning_rate=0.1,
                                      n_estimators=40) 
                    
        # Train the model
        history = model.fit(X_balanced,
                            y_balanced,
                            eval_metric=EVAL_METRIC,
                            early_stopping_rounds=EARLY_STOP,
                            eval_set=[(X_balanced, y_balanced), (x_val_, y_val_)])
 
        model.class_counts_ = nb_classes

        #history.classes_ # order of classes the same as order in predict_proba
        # get best model
        if RAY_TUNER:
            best_model = history.best_estimator_
        else:
            best_model = history
        # evaluate model on val set
        y_pred = best_model.predict(x_val_)
                
        classif_report = classification_report(y_val_,
                                               y_pred,
                                               #np.argmax(y_pred, axis=1), 
                                               target_names=classes,
                                               output_dict=True)
        
        classif_report_df = pd.DataFrame(classif_report)#.transpose()
        classif_report_df.to_csv('./saved_output/' +
                                 'Classification Report fold #_' + str(fold) + '.csv', index=True )
        
        if DEBUG:
            plt.figure(figsize=(20, 18))
            
            sns_plot = sns.heatmap(classif_report_df.iloc[:-1, :].T, annot=True, cmap='BrBG')
            
            plt.title("Classification report fold # {0}".format(fold))
            plt.tight_layout()
            sns_plot.figure.savefig('./saved_output/Classification Report fold #_{0}.png'.format(fold))
       
        if MLFLOW:
            mlflow.log_figure(sns_plot.figure, 'Classification Report fold #_{0}.png'.format(fold))
          
        # report
        if RAY_TUNER:
            results_eval = history.best_estimator_.evals_result()
        else:
            results_eval = history.evals_result()
            
        epochs = len(results_eval['validation_0']['mlogloss'])
        x_axis = range(0, epochs)
        
        if DEBUG:
            # plot log loss
            ax[i][0].plot(x_axis, results_eval['validation_0']['mlogloss'], label='Train')
            ax[i][0].plot(x_axis, results_eval['validation_1']['mlogloss'], label='Test')
            ax[i][0].set_ylabel('Log Loss')
            ax[i][0].set_title('XGBoost Log Loss')
            
            # plot merror
            ax[i][1].plot(x_axis, results_eval['validation_0']['merror'], label='Train')
            ax[i][1].plot(x_axis, results_eval['validation_1']['merror'], label='Test')
            ax[i][1].set_ylabel('Error')
            ax[i][1].set_title('XGBoost Classification Error')
            ax[i][1].legend()
        
            fig.savefig('./saved_output/train_losses.png')
            
        if MLFLOW:
            mlflow.log_figure(fig, 'train_losses.png')
        # save results
        if RAY_TUNER:
            pickle.dump(best_model, open('./saved_output/' +\
                                         'fold_' + str(fold) + '.dat', "wb"))
            result.append(('fold_' + str(fold), history.best_score_, history.best_params_))
        fold += 1
    
    if DEBUG:
        plt.show()
    else:
        plt.close()
    
    # Select the best model
    if RAY_TUNER:
        models = pd.DataFrame(result, columns=('model', 'score', 'params'))
        # index where the best model lies
        idx_best = models[['score']].idxmax()
        # load best model
        best_filename = models.iloc[idx_best, 0]
        best_model = pickle.load(open('./saved_output/' +\
                                      best_filename.values[0] + '.dat', "rb"))
        ray.shutdown()
    else:
        best_model = model
        
    # scores
    fig_viz, ax_viz = plt.subplots()
    visualizer = ClassificationReport(best_model,
                                      classes=classes,
                                      support=True,
                                      is_fitted=True,
                                      cmap="BrBG")
    visualizer.score(x_val, y_val)
    visualizer.show(outpath='./saved_output/ClassificationReport_val.png',
                    clear_figure=True)

    if MLFLOW:
        mlflow.log_figure(fig_viz,  'ClassificationReport_val.png')
     
    # Now that we are happy with our folds metrics, we can train the model
    # with the whole data
    # Load best params
    best_params = best_model.get_params()
    # save best params
    with open('./saved_output/best_params.pkl', 'wb') as f:
        pickle.dump(best_params, f)
        
    if MLFLOW:
        mlflow.log_params(best_params)
    #logger.info('Best params: {0}'.format(best_params))
    # Now, that we have the best params, we are going 
    # to train a new model with all data available
    X = np.vstack((x_train, x_val))
    y = np.hstack((y_train, y_val))
    # Apply SMOTE
    oversample = BorderlineSMOTE()
    X_balanced, y_balanced = oversample.fit_resample(X, y)
    # Initialize classifier
    xgb_final = xgb.XGBClassifier(**best_params)
    # Train model
    xgb_final.fit(X_balanced,
                  y_balanced)
              
    # plot importance
    xgb_final.get_booster().feature_names = features
    ax_xgb = plot_importance(xgb_final.get_booster(), max_num_features = 10)
    if DEBUG:
        ax_xgb.close()
    ax_xgb.figure.savefig('./saved_output/feature_importance.png')
    if MLFLOW:
        #signature = infer_signature(X_train, xgb_final.predict(X_test))
        mlflow.xgboost.log_model(xgb_final, "xgboost")#, signature=signature)
        mlflow.log_figure(ax_xgb.figure, 'feature_importance.png')   
    
    # save final model
    xgb_final.save_model('./saved_output/' + 'trained_model.json') 
    return xgb_final