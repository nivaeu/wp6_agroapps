## SETUP

Create a directory for the program and run "python setup.py install --user"
In train directory (Data/train) you should have your training data and in
test directory (Data/test) you should place the new data that you want to predict.
The subdirectories (csvs/) in the above folders contain the multiple csvs 
which are joined in order to create one training/testing csv dataset.

## TRAINING

Directions in order to train the model.

1. python train.py 
   ===============

    This script is the main script where the training takes place.
    The results are saved in the saved_output folder.

2. change parameters
   =================
   In the params.py script, you can change some parameters
   in order to train the model. Please, note, which parameters you can
   change and which not.
   
   The parameters you can change are:
   
   a) NB_CPUS
      =======
      The number of cpus when you use TuneSearchCV (bohb search).
      This only exists if you set RAY_TUNER to True.
      
   b) OUTLIER
      =======
      Choose if you want to apply outlier detection on input data
      as a preprocessing step before training.
    
   c) RAY_TUNER
      =========
      Use ray tune TuneSearchCV with Bayesian and Hyperopt search.
      for finding the optimal parameters.
      If it is false, it will train a NON optimized model!
      Use False, only for a quick run!
    
   d) MLFLOW
      ======
      Use this if you want to keep a log for params and metrics 
      every time you want to train a model.
      (You have to install and setup mlflow first!)

   e) TEST 
      ====
      Percent of data to keep as final test set
      
   f) VAL
      ===
      Percent of data to keep as validation set
      
   g) NFOLDS
      ======
      Numer of folds for cross validation.
      Typically, between 3 and 5.
      Increasing this, will increase the training time!
      
   h) FILENAME
      ========
      The name of the csv file to load for training 
      (note, without the 'csv' extension

   i) FILENAME_TEST
      =============
      The name of the csv file to load for making predictions 
      (note, without the 'csv' extension)
      
   j) NB_COUNTS_DISMISS
      =================
      When you have a dataset with multiple classes and each class
      has different counts (nb of class cases) , then dismiss
      those that have very few counts
      
   k) NB_TRIALS
      =========
      Tuner nb of trials (the bigger the trials the better the
      results but the slower the performance and requires more disk space)
      
   l) INFERENCE
      =========
      Set this to True only when you want to make predictions on new dataset.
      Must be False in order to train!
      
3. Your input data (data used for training) must be a csv file which is inside
   the Data/train folder.

4. The training model that is saved, is the trained_model.json file in 
   the saved_output/ folder

##  INFERENCE

Directions in order to predict on a new dataset (this only predicts, it does NOT train the model)

1. Ensure that you have the test_set.csv in the folder /Data/test

2. Run python inference.py after you have set the FILENAME_TEST and
   INFERENCE=True from the params.py

3. The results are saved in "/predicted_results/Predicted results.csv"



## MLFLOW setup

(First, install postgres and mlflow for your distribution)

1. sudo -u postgres -i
2. $ psql -d mlflow_db -U mlflow_user
3. Edit /etc/postgresql/12/main/pg_hba.conf :

```
    # Database administrative login by Unix domain socket
    local   all             postgres                                peer

    # TYPE  DATABASE        USER            ADDRESS                 METHOD

    # "local" is for Unix domain socket connections only
    local   all             all                                     peer
    # IPv4 local connections:
    host    all             all             127.0.0.1/32            trust
    # IPv6 local connections:
    host    all             all             ::1/128                 md5
    # Allow replication connections from localhost, by a user with the
    # replication privilege.
    local   replication     all                                     peer
    host    replication     all             127.0.0.1/32            md5
    host    replication     all             ::1/128                 md5
```

4. Start mlflow server

    Replace (file:/home/GG/Models/OPEKEPE/Classification/) with your local installation
    folder (where the model code exists)
    
    mlflow server --backend-store-uri postgresql://mlflow_user@localhost/mlflow_db   
                  --default-artifact-root file:/home/GG/Models/OPEKEPE/Classification/artifact_root           
                  --host 0.0.0.0     
                  --port 5000
               
5. Go to http://localhost:5000
