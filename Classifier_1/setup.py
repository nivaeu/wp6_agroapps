#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  7 14:19:51 2022

@author: ggous
"""

from setuptools import setup, find_packages


setup(
    name = 'OPEKEPE_Classification',
    packages = find_packages(),
)
