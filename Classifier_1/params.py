#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 22 11:21:12 2022

@author: ggous
"""

import logging
from ray import tune
       
# =========================================================================== #
# ===================== PARAMETERS THAN YOU CAN CHANGE ====================== #
# =========================================================================== #
# The name of the file to apply training
FILENAME = 'df_train'
# The name of the file to predict on
FILENAME_TEST = 'test_set'
# Used in n_jobs for bayesian search
NB_CPUS = 4 
# choose if you want to apply outlier detection on input data
# as a preprocessing step before training
OUTLIER = False
# use ray tune TuneSearchCV with Bayesian search
# if it is false, it will train a NON optimized model!
# use False, only for a quick run!
RAY_TUNER = True
# Tuner nb of trials (the bigger the trials the better the
# results but the slower the performance and requires more disk space)
NB_TRIALS = 40
# log params and metrics (You have to install and setup mlflow!)
MLFLOW = False

TEST = 0.1 # percent of data to keep as final test set
VAL = 0.2  # percent of data to keep as validation set
NFOLDS = 3 # Number of folds in cross validation (typically 3 or 5)
# When you have a dataset with multiple classes and each class
# has different counts (nb of class cases) , then dismiss
# those that have very few counts. For this particular
# training data set, was around 50
NB_COUNTS_DISMISS = 50
# When you want to predict on new data set it to True
# Else, in order to train , must be False
INFERENCE = False
# =========================================================================== #


# =========================================================================== #
# ===================== DO NOT TOUCH! ======================================= #
# =========================================================================== #
# create a logger
logger = logging.getLogger('OPEKEPE_Classification_logger')

DEBUG = False

if DEBUG:
    SEED = 123
else:
    SEED = None

# Use gpu for tune search. Needs to have a GPU and install 
# appropriate packages (CUDAtoolkit etc..)
USE_GPU = False

EARLY_STOP = 10 # xgboost fit
# Choose between ['bohb', 'hyperopt', 'bayesian', 'random']
# Note, that you must install the above packages!
# (for 'bayesian' you need skopt package)
# (for 'hohb' you need hpbandster and ConfigSpace packages)
# (for 'hyperopt' you need hyperopt package)
SEARCH_OPTIMIZATION = 'bohb' 
# eval metrics fo xgboost
EVAL_METRIC = ["merror", "mlogloss"] 

# params for xgb model
xgb_params = {
    'n_estimators': tune.randint(10, 80),
    'reg_alpha': tune.loguniform(0.1, 100),
    'booster': tune.choice(['gbtree', 'gblinear']),
    'colsample_bylevel': tune.uniform(0.05, 0.5), 
    'colsample_bytree': tune.uniform(0.05, 0.5), 
    'learning_rate': tune.uniform(0.001, 0.4),  
    'reg_lambda': tune.loguniform(0.1, 100),  
    'subsample': tune.uniform(0.2, 0.7), 
    "max_depth": tune.randint(1, 10),
    "min_child_weight": tune.choice([1, 2, 3]),
    "eta": tune.loguniform(1e-4, 1e-1),
    }
