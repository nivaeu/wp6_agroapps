#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 22 11:19:43 2022

@author: ggous
"""

import os
import glob
import numpy as np
import pandas as pd
import pickle
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import LabelEncoder
from params import SEED, TEST, VAL, OUTLIER, DEBUG, \
    NB_COUNTS_DISMISS, FILENAME, FILENAME_TEST

pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', None)
               
#os.environ["OMP_NUM_THREADS"] = "4" 
    
def join_csvs(path, INFERENCE):
    all_files = sorted(glob.glob(os.path.join(path , "*.csv")))

    df_list = []

    for filename in all_files:
        df = pd.read_csv(filename)
        df = df.replace(-99999.0, np.nan)
        df = df.dropna()
        df_list.append(df)
        
    a = pd.concat([df_list[0], df_list[1]],axis=1, sort=False)
    a = a.loc[:,~a.columns.duplicated()]

    b = pd.concat([df_list[2], df_list[3]],axis=1, sort=False)
    b = b.loc[:,~b.columns.duplicated()]

    df_final = pd.concat([a, b, df_list[4]],axis=1)
    df_final = df_final.loc[:,~df_final.columns.duplicated()]

    if INFERENCE:
        df_final.to_csv('./Data/test/' + FILENAME_TEST + '.csv', index=False)
    else:
        df_final.to_csv('./Data/train/' + FILENAME + '.csv', index=False)
    
def preprocess(df, INFERENCE):
    '''
    

    Parameters
    ----------
    df : pandas dataframe
        Input dataframe to preprocess

    Returns
    -------
    x_train_sc : pandas dataframe
        Scaled X input train data.
    y_train : numpy array
        Label encoded train labels array.
    x_val_sc : pandas dataframe
        Scaled X input validation data.
    y_val : numpy array
        Label encoded validation labels array.
    x_test_sc : pandas dataframe
        Scaled X input test data.
    y_test : numpy array
        Label encoded test labels array.
    nb_classes : Int scalar
        Number of unique classes
    classes : List
        Unique classes.
    

    '''
    if not INFERENCE:

        if not OUTLIER:
            # drop nan values
            df.dropna(inplace=True)
            # Drop columns not to train
            df.drop(['group_code'], axis=1, inplace=True)
                
            if DEBUG:
                df = df.sample(frac=0.5, random_state=SEED)
                
            # count number of cases for different classes
            df['group_desc'].value_counts().plot(kind="bar")    
            # merge classes together
            # classes_merge_list1 = ['fallowland', 'lowland']
            # classes_merge_list2 = ['Potatoes autumn', 'Potatoes spring', 'Potatoes summer']
            # classes_merge_list3 = ['horticultural fall', 'horticultural fall spring', \
            #                         'horticultural spring', 'horticultural summer']
            # classes_merge_list4 = ['vetch fall', 'vetch fall spring']
            # replace with the lists above
            # df['group_desc'] = df['group_desc'].replace('fallowland', 'lowland')
            # df['group_desc'] = df['group_desc'].replace([
            #     'Potatoes autumn', 'Potatoes spring'], 'Potatoes summer')
            # df['group_desc'] = df['group_desc'].replace(['horticultural fall',\
            #     'horticultural fall spring', 'horticultural spring'], 'horticultural summer')
            # df['group_desc'] = df['group_desc'].replace('vetch fall', 'vetch fall spring')
            
            # keep only >= NB_COUNTS_DISMISS class counts
            class_counts = df['group_desc'].value_counts().mask(df['group_desc'].value_counts() <= NB_COUNTS_DISMISS)
            
            class_names_to_keep = class_counts.dropna()
            class_names_to_keep = class_names_to_keep.index.tolist()
            # apply class names to keep in df
            df = df.loc[df['group_desc'].isin(class_names_to_keep)]
                        
        # keep a test set aside
        test = df.sample(frac=TEST)
        x_test = test.drop(['group_desc', 'eda_id'], axis=1)
        y_test = test['group_desc'].values
    
        # rest of data
        rest = df.loc[~df.index.isin(test.index)]
        features = rest.drop(['group_desc', 'eda_id'], axis=1)
        labels = rest['group_desc'].values
            
        # number of classes and classes
        nb_classes = len(np.unique(labels))
        classes = np.unique(labels).tolist()
        
        x_train, x_val, y_train, y_val = train_test_split(
            features,
            labels,
            stratify=labels,
            test_size = VAL,
            random_state = SEED)
        
        # encode string class values as integers
        label_encoder = LabelEncoder()
        y_train = label_encoder.fit_transform(y_train)#.astype(str))
        y_val = label_encoder.transform(y_val)#.astype(str))
        y_test = label_encoder.transform(y_test)#.astype(str))
    
        # save the mapping of integers to classes names
        # predictions follow the same order as the column labels
        # classes_mapping = label_encoder.classes_ # use with predict_proba
        # np.save('./saved_output/classes_mapping',classes_mapping) #use with predict_proba
        #classes_mapping = dict(zip(y_train, label_encoder.inverse_transform(y_train)))
        #with open('./saved_output/classes_mapping.pkl', 'wb') as f:
        #    pickle.dump(classes_mapping, f)
        # save label encoder
        with open('./saved_output/classes_mapping.pkl', 'wb') as f:
            pickle.dump(label_encoder, f)
            
        scaler = StandardScaler()
        x_train_sc = pd.DataFrame(scaler.fit_transform(x_train), index=x_train.index, columns=x_train.columns)
        x_val_sc = pd.DataFrame(scaler.transform(x_val), index=x_val.index, columns=x_val.columns)
        x_test_sc = scaler.transform(x_test)
        # save scaler
        pickle.dump(scaler, open("./saved_output/scaler", "wb"))
            
        return x_train_sc, y_train, x_val_sc, y_val, x_test_sc, y_test, \
            nb_classes, classes
      
    else:
        # drop nan values
        df.dropna(inplace=True)
        # Keep the object_id
        object_id = df['eda_id']
        # load scaler
        scaler = pickle.load(open("./saved_output/scaler", "rb"))
        # transform test data
        x_test =  pd.DataFrame(scaler.transform(df.drop('eda_id', axis=1)),
                               index=df.drop('eda_id', axis=1).index,
                               columns=df.drop('eda_id', axis=1).columns)
        return x_test, object_id
            
