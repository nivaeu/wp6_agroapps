#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 26 12:41:52 2022

@author: ggous
"""

import numpy as np
import pandas as pd
import psutil
from joblib import Parallel, delayed
import seaborn as sns
from sklearn.svm import OneClassSVM
from params import SEED, DEBUG, NB_COUNTS_DISMISS

sns.set()
        
def make_dataframes(df, the_class):
    '''
    Makes a dataframe for each class.

    Parameters
    ----------
    df : pandas dataframe
        Dataframe that contains all classes.
    the_class : String
        Class name.

    Returns
    -------
    all_dfs : Python dict
        Dictionary that contains dataframes for each class.

    '''
    all_dfs = {}
    df_train = df[df['group_desc'] == the_class]
    numeric_only_data_cols = df_train.select_dtypes(include=np.number).columns.difference(['group_desc'])
    numeric_only_data = df_train[numeric_only_data_cols]
    # split to features and labels
    X_all = numeric_only_data.values#.loc[:, numeric_only_data.columns != 'group_desc'].values
    y_all = df_train['group_desc'].values
   
    svm = OneClassSVM(gamma='auto', nu=0.05)
    svm.fit(X_all)
    pred = svm.predict(X_all)
   
    # select all rows that are not outliers
    mask = pred != -1
    x , y = X_all[mask, :], y_all[mask]
   
    # going back to dataframe from array
    orig_columns = numeric_only_data.loc[:, 
                                         numeric_only_data.columns!='group_desc'].columns
   
    all_dfs[the_class] = pd.DataFrame(x, columns=orig_columns)

    # filter original OBJECTID values from df to new df
    tmp = df_train.merge(all_dfs[the_class], on='eda_id', how='inner')
    all_dfs[the_class]['eda_id'] = tmp['eda_id']
    all_dfs[the_class]['group_desc'] = y
    return all_dfs
    
def outlier(df):
    '''
    Outlier detection

    Parameters
    ----------
    df : pandas dataframe
         A dataframe on which to apply outlier detection

    Returns
    -------
    final_df : pandas dataframe
        The final dataframe with outliers removed

    '''
    # drop nan values
    df.dropna(inplace=True)
    # drop columns not to train
    df.drop(['group_code'], axis=1, inplace=True)
    #print(df.head())

    if DEBUG:
        df = df.sample(frac=0.3, random_state=SEED)

    # count number of cases for different classes
    df['group_desc'].value_counts().plot(kind="bar")    
    # merge classes together
    # classes_merge_list1 = ['fallowland', 'lowland']
    # classes_merge_list2 = ['Potatoes autumn', 'Potatoes spring', 'Potatoes summer']
    # classes_merge_list3 = ['horticultural fall', 'horticultural fall spring', \
    #                        'horticultural spring', 'horticultural summer']
    # classes_merge_list4 = ['vetch fall', 'vetch fall spring']
    # replace with the lists above
    # df['group_desc'] = df['group_desc'].replace('fallowland', 'lowland')
    # df['group_desc'] = df['group_desc'].replace([
    #     'Potatoes autumn', 'Potatoes spring'], 'Potatoes summer')
    # df['group_desc'] = df['group_desc'].replace(['horticultural fall',\
    #     'horticultural fall spring', 'horticultural spring'], 'horticultural summer')
    # df['group_desc'] = df['group_desc'].replace('vetch fall', 'vetch fall spring')
        
    # keep only >= NB_COUNTS_DISMISS class counts
    class_counts = df['group_desc'].value_counts().mask(df['group_desc'].value_counts() <= NB_COUNTS_DISMISS)
    
    class_names_to_keep = class_counts.dropna()
    class_names_to_keep = class_names_to_keep.index.tolist()
    # apply class names to keep in df
    df = df.loc[df['group_desc'].isin(class_names_to_keep)]
    
    classes = np.unique(df['group_desc'])
    
    cpu_count = len(psutil.Process().cpu_affinity())
    
    # Train for one class only leaving out the rest
    all_dfs = {
        key: value
        for parallel in Parallel(n_jobs=cpu_count)(
            delayed(make_dataframes)(df, i) for i in classes
        )
        for key, value in parallel.items()
    }
    final_df = pd.concat(all_dfs.values(), ignore_index=True)
    # keep only >= 100 class counts
    class_counts = final_df['group_desc'].value_counts().mask(final_df['group_desc'].value_counts() <= 100)
    class_names_to_keep = class_counts.dropna()
    class_names_to_keep = class_names_to_keep.index.tolist()
    # apply class names to keep in df
    final_df = final_df.loc[final_df['group_desc'].isin(class_names_to_keep)]
    return final_df