#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 15:20:46 2022

@author: ggous
"""

from main import train
from params import FILENAME
from pathlib import Path

if __name__=="__main__":
    # Create the appropriate directories
    Path("saved_output/").mkdir(parents=True, exist_ok=True)
    Path("predicted_results/").mkdir(parents=True, exist_ok=True)
    Path("Data/train").mkdir(parents=True, exist_ok=True)
    Path("Data/test").mkdir(parents=True, exist_ok=True)
    Path("Data/train/csvs").mkdir(parents=True, exist_ok=True)
    Path("Data/test/csvs").mkdir(parents=True, exist_ok=True)
    Path("logs/").mkdir(parents=True, exist_ok=True)

    train(FILENAME)