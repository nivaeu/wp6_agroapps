#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  6 15:34:38 2022

@author: ggous
"""

from pathlib import Path
import numpy as np
import xgboost as xgb
import pandas as pd
import pickle
from utils import preprocess, join_csvs
from params import FILENAME_TEST
from yellowbrick.classifier import ClassificationReport

# Create the appropriate directories
Path("saved_output/").mkdir(parents=True, exist_ok=True)
Path("predicted_results/").mkdir(parents=True, exist_ok=True)
Path("Data/train").mkdir(parents=True, exist_ok=True)
Path("Data/test").mkdir(parents=True, exist_ok=True)
Path("Data/train/csvs").mkdir(parents=True, exist_ok=True)
Path("Data/test/csvs").mkdir(parents=True, exist_ok=True)
Path("logs/").mkdir(parents=True, exist_ok=True)

# join mutliple csvs
path = './Data/test/csvs/'
INFERENCE = True
join_csvs(path, INFERENCE)

# load data
df = pd.read_csv('./Data/test/' + FILENAME_TEST + '.csv')

# load model
model = xgb.XGBClassifier()
model.load_model('./saved_output/trained_model.json')
# load classes mapping
# use np.load with predict_proba
# classes_mapping = np.load('./saved_output/classes_mapping.npy', allow_pickle=True)
with open('./saved_output/classes_mapping.pkl', 'rb') as f:
    classes_mapping = pickle.load(f)

# preprocess test data
x_test, object_id = preprocess(df.drop(['group_code', 'group_desc'], axis=1), INFERENCE)    
# make predictions on test data
preds = model.predict(x_test)
pred_probs = model.predict_proba(x_test)
#max_prob_indices = np.argmax(pred_probs, axis=1)


# create a dataframe
results = object_id.to_frame()
# use this if you use dict for classes mapping
# results['Predicted class'] = preds
#results = results.replace({"Predicted class": classes_mapping})

# apply the classes names to predictions
results['Predicted class'] = classes_mapping.inverse_transform(preds)
results["Probality"] = pd.DataFrame(pred_probs).max(axis=1) * 100

# save results
results.to_csv('./predicted_results/' +
               'Predicted results.csv', index=True)
