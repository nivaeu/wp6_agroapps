#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 20 15:11:49 2022

@author: ggous
"""

import mlflow
import numpy as np
import datetime
import logging
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from yellowbrick.classifier import ClassificationReport, ConfusionMatrix
from pathlib import Path
from utils import preprocess, join_csvs
from model_xgb import xgb_model
from outlier import outlier
from params import OUTLIER, DEBUG, MLFLOW, logger, RAY_TUNER, FILENAME

if MLFLOW:
    mlflow.set_tracking_uri('http://0.0.0.0:5000')
    mlflow.set_experiment("OPEKEPE_Classification")
   
# set log level
logger.setLevel(logging.DEBUG if DEBUG else logging.INFO)
# Create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# define file handler and format
# file_handler = logging.StreamHandler(sys.stderr)
file_handler = logging.FileHandler(f"logs/log_{datetime.datetime.now():%d-%m-%Y_%H_%M}.txt")
formatter = logging.Formatter("%(asctime)-9s -- %(levelname)-12s -- \
    %(filename)-20s -> %(funcName)-19s line: %(lineno)-9s %(message)s")
file_handler.setFormatter(formatter)
# Add formatter to ch
ch.setFormatter(formatter)
# Add ch to logger
logger.addHandler(ch)
# add file handler to logger
logger.addHandler(file_handler)

sns.set_theme(style='white')


if RAY_TUNER:
    Path("ray_results/").mkdir(parents=True, exist_ok=True)

if MLFLOW:
    # Create directory
    Path("artifact_root/").mkdir(parents=True, exist_ok=True)
            
    # Define some strings to use as runname in mlflow        
    if OUTLIER:
        outlier_txt = 'OUTLIER'
    else:
        outlier_txt = 'NO OUTLIER'
        
    if RAY_TUNER:
        tuner_txt = 'RAY_TUNER'
    else:
        tuner_txt = 'NO RAY_TUNER'
                
    if FILENAME == 'df_train_initial':
        runname = str(outlier_txt + ', '  + tuner_txt + ', Use Initial Scheme')
    else:
        runname = str(outlier_txt + ', '  + tuner_txt)
          
def train(filename):
    # join multiple csvs
    path = './Data/train/csvs/'
    INFERENCE = False
    join_csvs(path, INFERENCE)
   
    # Load dataset
    df_ = pd.read_csv('./Data/train/' + filename + '.csv')

    if OUTLIER:
        # apply outlier detection
        df = outlier(df_)
        
        # preprocess dataset
        x_train, y_train, x_val, y_val, x_test, y_test, \
            nb_classes, classes = preprocess(df)
    else:
        # preprocess dataset
        x_train, y_train, x_val, y_val, x_test, y_test, \
            nb_classes, classes = preprocess(df_, INFERENCE)
        
    if MLFLOW:
        #mlflow.xgboost.autolog(silent=True)
        with mlflow.start_run(run_name=runname) as run:
            # Obtaining the ID of this run
            run_id = run.info.run_id
            # run model    
            model = xgb_model(x_train.values,
                              y_train,
                              x_val.values,
                              y_val,
                              x_train.columns.tolist(),
                              classes)
            
            model.class_counts_ = nb_classes
            fig, ax1 = plt.subplots()
            visualizer = ClassificationReport(model,
                                              classes=classes,
                                              support=True,
                                              is_fitted=True,
                                              cmap="BrBG")
            visualizer.score(x_test, y_test)  
            visualizer.show(outpath='./saved_output/ClassificationReport_test_set.png',
                            clear_figure=True)
            
            mlflow.log_figure(fig, 'ClassificationReport_test_set.png')
            
            classif_report = pd.DataFrame(visualizer.scores_)#.transpose()
            classif_report.to_csv('./saved_output/' +
                                     'Classification Report #test_set.csv', index=True )
            
            fig2, ax2 = plt.subplots()
            cm = ConfusionMatrix(model,
                                 classes=classes, 
                                 is_fitted=True,
                                 cmap="BrBG")
            cm.score(x_test, y_test)
            cm.show(outpath='./saved_output/ConfusionMatrix_test_set.png',
                    clear_figure=True)
            
            mlflow.log_figure(fig2, 'ConfusionMatrix_test_set.png')
            
            conf_matrix = pd.DataFrame(cm.confusion_matrix_, index = classes, columns=classes)
            
            #Plotting the confusion matrix
            plt.figure(figsize=(20, 18))
            sns_plot = sns.heatmap(conf_matrix, annot=True, cmap="BrBG",fmt="d")
            plt.title("Confusion Matrix on test data")
            plt.tight_layout()
            plt.ylabel("True Class"), 
            plt.xlabel("Predicted Class")
            if DEBUG:
                plt.show()
            else:
                plt.close()
            sns_plot.figure.savefig('./saved_output/confusion_m_test_set.png')
            mlflow.log_figure(sns_plot.figure, 'confusion_m_test_set.png')       
                
        mlflow.end_run()
    
    else:
        # run model
        model = xgb_model(x_train.values,
                          y_train,
                          x_val.values,
                          y_val,
                          x_train.columns.tolist(),
                          classes)
        
        model.class_counts_ = nb_classes
        visualizer = ClassificationReport(model,
                                          classes=classes,
                                          support=True,
                                          is_fitted=True,
                                          cmap="BrBG")
        visualizer.score(x_test, y_test)  
        visualizer.show(outpath='./saved_output/ClassificationReport_test_set.png',
                        clear_figure=True)
            
        #ax.get_xticks()
        #ax.get_yticks()
        classif_report = pd.DataFrame(visualizer.scores_)#.transpose()
        classif_report.to_csv('./saved_output/' +
                              'Classification Report #test_set.csv', index=True )
        
        cm = ConfusionMatrix(model,
                             classes=classes, 
                             is_fitted=True,
                             cmap="BrBG")
        cm.score(x_test, y_test)
        cm.show(outpath='./saved_output/ConfusionMatrix_test_set.png',
                clear_figure=True)
        
        conf_matrix = pd.DataFrame(cm.confusion_matrix_, index = classes, columns=classes)
        
        #Plotting the confusion matrix
        plt.figure(figsize=(20, 18))
        sns_plot = sns.heatmap(conf_matrix, annot=True, cmap="BrBG",fmt="d")
        plt.title("Confusion Matrix on test data")
        plt.tight_layout()
        plt.ylabel("True Class"), 
        plt.xlabel("Predicted Class")
        if DEBUG:
            plt.show()
        else:
            plt.close()
        sns_plot.figure.savefig('./saved_output/confusion_m_test_set.png')