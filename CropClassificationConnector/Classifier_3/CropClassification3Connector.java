package eu.niva.orchestrator.sdk;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.io.BufferedReader;
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.ArrayList
import java.util.List;

import org.locationtech.jts.io.WKTWriter;

// Classifier 3 (fallow land)
// https://gitlab.com/nivaeu/wp6_agroapps/-/tree/main/Classifier_2_3/Opekepe_CL2_3

public class CropClassification3Connector implements Connector {

    @Override
    public Object compute(MonitoringParams monitoringParams, Map<String, Object> processorParams) throws Exception {

        String countryZone = (String) processorParams.get("zone");
        // CL3 working directory
        String workingDir = (String) processorParams.get("workingDir");
        // CL3 model: random_forest or xgboost
        String model = (String) processorParams.get("model");
        // CL3 config
        String config = (String) processorParams.get("config");
        // output filename
        String output = (String) processorParams.get("output");

        WKTWriter writer = new WKTWriter();
        String geometryWKT = writer.write(monitoringParams.getGeometry());
        OffsetDateTime referenceDate = monitoringParams.getPointInTime();

        String commandLine = "CL3-predict";
        String commandLineArguments = "--model " + model + " --config " + config + " --output " + output + " --workingDir " + workingDir;

        // example :
        // CL3-predict --model <random_forest or xgboost> --output_filename <predictions filename (.csv)> --config_file <path_to_config_file (.json) --working_dir <path_to_working_directory>

        // execute classifier 3
        String command = executeSysCommands(new String[]{commandLine}, commandLineArguments);
        // read results
        // e.g. cl3_predictions.csv
        String resultsFile = working_dir + output;
        List<AlgorithmResult> results = readResultsFromCSV(resultsFile);

        return results;
    }

    public static class AlgorithmResult {
        // for CL3, the classification is a binary classification
        // CT_pred_1 = 1 is a positive classification (fallowland),
        // CT_pred_1 = 0 is a negative classification (other)

        // EDA_ID is the ID field with same name in the input dataset
        public String EDA_ID;
        // predicted crop code 1
        public Integer CT_pred_1;
        // probability 1
        public Double CT_conf_1;
        // predicted crop code 2
        public Integer CT_pred_2;
        // probability 2
        public Double CT_conf_2;
        // declared crop (code)
        public Integer CT_decl;
        // add if needed
        // public String algorithmRawOutput

        public AlgorithmResult(String EDA_ID, Integer CT_pred_1, Double CT_conf_1, Integer CT_pred_2, Double CT_conf_2, Integer CT_decl) {
            this.EDA_ID = EDA_ID;
            this.CT_pred_1 = CT_pred_1;
            this.CT_conf_1 = CT_conf_1;
            this.CT_pred_2 = CT_pred_2;
            this.CT_conf_2 = CT_conf_2;
            this.CT_decl = CT_decl;
        }
    }

    private static List<AlgorithmResult> readResultsFromCSV(String fileName) {
        List<AlgorithmResult> results = new ArrayList<AlgorithmResult>();

        Path pathToFile = Paths.get(fileName);

        // create an instance of BufferedReader
        try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.US_ASCII)) { // read the first line from the text file
            String line = br.readLine(); // loop until all lines are read
            while (line != null) {
                String[] attributes = line.split(",");
                AlgorithmResult result = createResult(attributes); // adding result into ArrayList
                results.add(result);
                line = br.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        return results;
    }

    private static AlgorithmResult createResult(String[] metadata) {
        String EDA_ID = metadata[0];
        Integer CT_pred_1 = Integer.parseInt(metadata[1]);
        Double CT_conf_1 = Double.parseDouble(metadata[2]);
        Integer CT_pred_2 = Integer.parseInt(metadata[3]);
        Double CT_conf_2 = Double.parseDouble(metadata[4]);
        Integer CT_decl = Integer.parseInt(metadata[5]);

        return new AlgorithmResult(EDA_ID, CT_pred_1, CT_conf_1, CT_pred_2, CT_conf_2, CT_decl);
    }

    public static Process executeSysCommands(String... commands, String currentDirectory, String logFilePath) {
        Process process;
        ProcessBuilder pb = new ProcessBuilder(commands);
        // Set current directory
        pb.directory(new File(currentDirectory));
        // Log errors in specified log file.
        pb.redirectError(new File(logFilePath));
        try {
            process = pb.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return process;
    }

}