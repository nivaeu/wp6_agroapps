package eu.niva.orchestrator.sdk;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.locationtech.jts.io.WKTWriter;

import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.crs.DefaultGeographicCRS;

public class CropClassification1Connector implements Connector {

    @Override
    public Object compute(MonitoringParams monitoringParams, Map<String, Object> processorParams) throws Exception {
        /*
         * Run Classifier #1: Crop classification
         */
        String countryZone = (String) processorParams.get("zone");
        String jobName = (String) processorParams.get("jobName");
        String aoi = (String) processorParams.get("aoi");
        String folder = (String) processorParams.get("folder");
        String samplingFactor = (String) processorParams.get("samplingFactor");
        String lpis = (String) processorParams.get("lpis");
        String pa_min = (String) processorParams.get("pa_min");

        // Classifier 1 specific system call parameters
        //
        // example : python3 process.py -j INTERNAL_TEST_2 -a /home/eouser/eo-tools/data/IMATHIA_2020_SEN4CAP/VECTOR_DATA_TESTING/AOI.geojson --pa_min=50 --sampling_factor=0.2 -p /home/eouser/eo-tools/data/IMATHIA_2020_SEN4CAP/VECTOR_DATA_TESTING/Imathia2020_LPIS.shp -f /home/eouser/eo-tools/data/IMATHIA_2020_SEN4CAP/

        String commandLine = "python3";
        String commandLineArguments = "process.py -j " + jobName + " -a " + aoi + " --pa_min=" + pa_min + " --sampling_factor=" + sampling_factor + " -p " + lpis + " -f " + folder;

        WKTWriter writer = new WKTWriter();
        String geometryWKT = writer.write(monitoringParams.getGeometry());
        OffsetDateTime referenceDate = monitoringParams.getPointInTime();

        String apiCommand = executeSysCommands(new String[]{commandLine}, commandLineArguments);
        // read results from folder with fixed filename
        String resultsFile = folder + "/SHAPES/predicted_results.shp";

        // List of results
        ArrayList<AlgorithmResult> results = new ArrayList<AlgorithmResult>();
        AlgorithmResult result;

        // Use GeoTools to read geofile and extract the result
        // send all results from shapefile, crops and classifications
        // to the NIVA Orchestrator
        File file = new File(resultsFile);

        try {
            Map<String, String> connect = new HashMap();
            connect.put("url", file.toURI().toString());

            DataStore dataStore = DataStoreFinder.getDataStore(connect);
            String[] typeNames = dataStore.getTypeNames();
            String typeName = typeNames[0];

            FeatureSource featureSource = dataStore.getFeatureSource(typeName);
            FeatureCollection collection = featureSource.getFeatures();
            FeatureIterator iterator = collection.features();


            try {
                while (iterator.hasNext()) {
                    Feature feature = iterator.next();
                    GeometryAttribute sourceGeometry = feature.getDefaultGeometryProperty();
                    // populate shape file results
                    // get confidence per crop
                    // read all results from shapefile
                    List attributes = feature.getAttributes();
                    // add the EDA_ID of the crop
                    result.EDA_ID = attributes.get(0).toString();
                    // add the crop type
                    result.cropType = attributes.get(1).toString();
                    // add the confidence percentage
                    result.confidence = attributes.get(2).toDouble();
                    // add to raw result list
                    result.algorithmRawOutput += feature.toString();
                }
            } finally {
                iterator.close();
            }
            results.add(result);

        } catch (Throwable e) {
            throw new RuntimeException(e);
        }


        return results;
    }

    public static class AlgorithmResult {
        // the EDA_ID is the unique identifier of the feature
        public String EDA_ID;
        // the type of crop
        public String cropType;
        // probability of crop type (percentage)
        public Double confidence;
        public String algorithmRawOutput;
    }

    public static Process executeSysCommands(String... commands, String currentDirectory, String logFilePath) {
        Process process = null;
        ProcessBuilder pb = new ProcessBuilder(commands);
        // Set current directory
        pb.directory(new File(currentDirectory));
        // Log errors in specified log file.
        pb.redirectError(new File(logFilePath));
        try {
            process = pb.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return process;
    }

}
