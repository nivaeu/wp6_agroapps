package eu.niva.orchestrator.sdk;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.HashMap;

import org.locationtech.jts.io.WKTWriter;

import org.geotools.coverage.grid.GridCoordinates2D;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.coverage.grid.GridGeometry2D;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.geotools.geometry.DirectPosition2D;

public class Classification4Connector implements Connector {

    private static GridCoverage2D grid;

    @Override
    public Object compute(MonitoringParams monitoringParams, Map<String, Object> processorParams) throws Exception {
        /*
         * Run Classifier #1: Crop classification
         */
        String countryZone = (String) processorParams.get("zone");
        String jobName = (String) processorParams.get("jobName");
        String aoi = (String) processorParams.get("aoi");
        String folder = (String) processorParams.get("folder");

        // Classifier 4 specific system call parameters
        //
        // example : python3 inference.py

        String commandLine = "python3";
        String commandLineArguments = "inference.py";

        WKTWriter writer = new WKTWriter();
        String geometryWKT = writer.write(monitoringParams.getGeometry());
        OffsetDateTime referenceDate = monitoringParams.getPointInTime();

        String command = executeSysCommands(new String[]{commandLine}, commandLineArguments);
        // read results from folder with fixed filename
        // wp6_agroapps/Classifier_4_Segmentation/reconstructed_mask/predicted_mask.tif
        String resultsFile = folder + "reconstructed_mask/predicted_mask.tif";

        // storage for output GeoTIFF
        AlgorithmResult result = null;

        // Use GeoTools to read geofile and extract the result
        // read raster and send result to NIVA Orchestrator
        File tiffFile = new File(resultsFile);
        // use GeoTiffReader tools to read the GeoTIFF file
        GeoTiffReader reader = new GeoTiffReader(tiffFile);
        // read the raster
        grid = reader.read(null);
        // store in AlgorithmResult
        result.raster = grid.getRenderedImage().getData();

        return result;
    }

    public static class AlgorithmResult {
        // classifier #4 is a raster file (GeoTIFF)
        // with MASK values x, y, mask
        // see Deliverable D5.6 for extra information
        Raster raster;
    }

    public static Process executeSysCommands(String... commands, String currentDirectory, String logFilePath) {
        Process process;
        ProcessBuilder pb = new ProcessBuilder(commands);
        // Set current directory
        pb.directory(new File(currentDirectory));
        // Log errors in specified log file.
        pb.redirectError(new File(logFilePath));
        try {
            process = pb.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return process;
    }

}
