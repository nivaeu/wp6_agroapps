# CT4

## Docker version

Build and run the image : 

```
docker build -t cl4 .
docker run -it cl4
```


## SETUP

Create a folder and run:

```sh
python setup.py install --user
```

OR  use `pip install` witn a python virtual environment:

```sh
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt

```

## INFERENCE

For prediction, you must have in the PATH_IMG_TO_PREDICT
folder the image you want to make prediction on
(for example, the 22E182N_NDVI_LAYERSTACK.tif) 

Run:

```sh
python3 -m venv env
source env/bin/activate
python inference.py
```

You can see the results in the PATH_RECONSTRUCTED_MASK folder

## TRAINING

Run `python train.py`

If you want to change training parameters, you go to
config.py and chnage the following parameters.

1) IMG_HEIGHT: The height of the image
2) IMG_WIDTH: The width of the image
3) CHANNELS: 3 for color images and 1 fro gray

Note, that you must convert tif files to png first!
(currently tensorflow generator does not support tif)

4) FILTERS: The number of the filters in CNN network
            The higher the number, the higher the memory requirements
            
5) DROPOUT: The dropout layer

6) EPOCHS: How many epochs you want to run 

7) OPTIMIZER: The optimizer you want to use

8) AUG: If you want or not to apply augmentations

9) BATCH_SIZE: The batch size

10) BATCH_NORM: If you want to use batch normalization or not       

11) TRAIN_SPLIT: The percentage of training data

12) VAL_SPLIT: The percentage of validation data

13) TEST_SPLIT: The percentage of test data

14) NB_PLOTS_TRAIN: How many plots from train data set

15)  NB_PLOTS_TEST: How many plots from test data set

16)  NB_PLOTS_FINAL: How many plots from FINAL unknown test data set

