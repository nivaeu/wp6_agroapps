#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 16:26:05 2022

@author: ggous
"""

import tensorflow as tf
import os
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint,\
    ReduceLROnPlateau, LearningRateScheduler, CSVLogger
from tensorflow.keras.layers import Input, Conv2D, Activation, Conv2DTranspose,\
    Concatenate, Dropout,  BatchNormalization, MaxPooling2D, SpatialDropout2D
from PIL import Image
from config import opt
from attention import AttnGatingBlock

# gpu setup
gpus = tf.config.experimental.list_physical_devices('GPU')
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)
        
   
def conv2d_block(input_tensor,
                 filters,
                 kernel_size=3):
        '''
        Convolution 2D block builder

        Parameters
        ----------
        input_tensor : tensor
            Input dimensions.
        filters : integer
            Filters to be used at each conv layer.
        kernel_size : integer, optional
            Kernel size at each conv layer. The default is 3.

        Returns
        -------
        x : tensor
            Layer.

        '''
        # first layer
        x = Conv2D(filters=filters,
                   kernel_size=(kernel_size, kernel_size),
                   kernel_initializer='he_normal',
                   padding='same')(input_tensor)
        if opt.BATCH_NORM:
            x = BatchNormalization()(x)
        x = Activation('relu')(x)
        #x = LeakyReLU(alpha=0.1)(x)
        #x = Dropout(0.2)(x)

        # second layer
        x = Conv2D(filters=filters,
                   kernel_size=(kernel_size, kernel_size),
                   kernel_initializer='he_normal',
                   padding='same')(x)
        if opt.BATCH_NORM:
            x = BatchNormalization()(x)
        x = Activation('relu')(x)
        #x = LeakyReLU(alpha=0.1)(x)
        #x = Dropout(0.2)(x)

        return x
    
def build_model(height,
                width,
                channels,
                filters,
                batch_norm,
                nb_classes):
    '''
    Build Unet model

    Parameters
    ----------
    height : integer scalar
        Image height.
    width : integer scalar
        Image width.
    channels : integer scalar
        Number of channels (3 for RGB, 1 for grayscale).
    filters : integer scalar
        Nb of filters for convolution layers.
    batch_norm : boolean
        Use or not batch normalization.
    nb_classes : integer scalar
        Number of classes to predict.

    Returns
    -------
    model : tensorflow model (untrained)
        The model architecture.

    '''
    inputs = Input((height, width, channels))
   
    c1 = conv2d_block(inputs, filters=filters * 1, kernel_size=3)
    p1 = MaxPooling2D((2, 2))(c1)
    #p1 = Dropout(0.1)(p1)

    c2 = conv2d_block(p1, filters=filters * 2, kernel_size=3)
    p2 = MaxPooling2D((2, 2))(c2)
    #p2 = Dropout(0.1)(p2)

    c3 = conv2d_block(p2,filters=filters * 4, kernel_size=3)
    p3 = MaxPooling2D((2, 2))(c3)
    #p3 = Dropout(0.1)(p3)

    c4 = conv2d_block(p3, filters=filters * 8, kernel_size=3)
    p4 = MaxPooling2D((2, 2))(c4)
    #p4 = Dropout(0.1)(p4)

    c5 = conv2d_block(p4, filters=filters * 16, kernel_size=3)
    p5 = MaxPooling2D((2, 2))(c5)
    #p5 = Dropout(0.2)(p5)
    
    c6 = conv2d_block(p5, filters=filters * 32, kernel_size=3)

    att0 = AttnGatingBlock(c5, c6, filters * 32)  

    u6 = Conv2DTranspose(filters * 16, (3, 3), strides=(2, 2),
                         padding='same')(c6)
    u6 = Concatenate()([u6, att0])
    u6 = Dropout(opt.DROPOUT)(u6)
    c7 = conv2d_block(u6, filters=filters * 16, kernel_size=3)

    att1 = AttnGatingBlock(c4, c7, filters * 16)
    
    u7 = Conv2DTranspose(filters * 8, (3, 3), strides=(2, 2),
                          padding='same')(c7)
    u7 = Concatenate()([u7, att1])
    #u7 = Dropout(opt.DROPOUT)(u7)
    c8 = conv2d_block(u7, filters=filters * 8, kernel_size=3)

    att2 = AttnGatingBlock(c3, c8, filters * 8)
    
    u8 = Conv2DTranspose(filters * 4, (3, 3), strides=(2, 2), 
                          padding='same')(c8)
    u8 = Concatenate()([u8, att2])
    #u8 = Dropout(opt.DROPOUT)(u8)
    c9 = conv2d_block(u8, filters=filters * 4, kernel_size=3)

    att3 = AttnGatingBlock(c2, c9, filters * 4)
     
    u9 = Conv2DTranspose(filters * 2, (3, 3), strides=(2, 2), 
                          padding='same')(c9)
    u9 = Concatenate()([u9, att3])
    #u9 = Dropout(opt.DROPOUT)(u9)
    c9 = conv2d_block(u9, filters=filters * 2, kernel_size=3)
    
    u10 = Conv2DTranspose(filters * 1, (3, 3), strides=(2, 2),
                          padding='same')(c9)
    u10 = Concatenate()([u10, c1])
    u10 = Dropout(opt.DROPOUT)(u10)
    c10 = conv2d_block(u10, filters=filters * 1, kernel_size=3)
    
    c10 = SpatialDropout2D(0.4)(c10)
    
    outputs = Conv2D(1, (1, 1), activation='sigmoid') (c10)
    model = Model(inputs=[inputs], outputs=[outputs])
    return model

def predict(input_data, model):
    '''
    Make predictions

    Parameters
    ----------
    input_data : Image
        Image in order to make predictions.
    model : tensorflow model
        Tensorflow trained model.

    Returns
    -------
    mask : numpy array
        The masks.

    '''
    pred = model.predict(input_data)[0]
    mask = tf.where(pred > 0.5, 255, 0)
    return mask

def lrfn(epoch):
    '''
    Learning rate scheduler

    Parameters
    ----------
    epoch : integer
        Number of epochs.

    Returns
    -------
    float
        Learning rate.

    '''
    start_lr = opt.START_LR
    min_lr = opt.MIN_LR
    max_lr = opt.MAX_LR
    rampup_epochs = opt.RAMPUP_EPOCHS
    sustain_epochs = opt.SUSTAIN_EPOCHS
    exp_decay = opt.EXP_DECAY
    
    if epoch < rampup_epochs:
        return (max_lr - start_lr)/rampup_epochs * epoch + start_lr
    elif epoch < rampup_epochs + sustain_epochs:
        return max_lr
    else:
        return (max_lr - min_lr) * exp_decay ** \
            (epoch-rampup_epochs-sustain_epochs) + min_lr
                
def callback():
        '''
        Various callbacks to be used

        Parameters
        ----------
        fold : integer
            Fold number.

        Returns
        -------
        cb : list
            Holds all the callbacks.

        '''
        cb = []
        checkpoint = ModelCheckpoint(opt.SAVE_PATH + 'model.h5',
                                     save_best_only=True,
                                     mode='auto',
                                     monitor='val_loss',
                                     save_weights_only=False,
                                     verbose=1)
        cb.append(checkpoint)       
        
        lr_callback = LearningRateScheduler(lrfn, verbose=True)
        stop = EarlyStopping(monitor="val_loss",
                             mode='auto',
                             patience=6,
                             verbose=1,
                             restore_best_weights=True)
        cb.append(lr_callback)
        cb.append(stop)
        return cb
    
def load_model_custom(model_path,
               custom_objects=None):
        '''
        Loads a model

        Parameters
        ----------
        model_path : string
            The path of the model.
        custom_objects : Python dictionary, optional
            If custom metric and/or loss was used,
            define it.
            The default is None.

        Returns
        -------
        keras model
            The model.

        '''
        model = load_model(model_path, custom_objects=custom_objects)
        return model
    
