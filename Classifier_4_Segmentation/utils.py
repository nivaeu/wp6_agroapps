#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 15:21:30 2022

@author: ggous
"""

import albumentations as A
import tensorflow as tf
#import tensorflow.io as tfio
import cv2
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.utils import image_dataset_from_directory
from config import opt

def transforms(image, mask):    
    '''
    Make albumentations (transformations)

    Parameters
    ----------
    image : image
        Image.
    mask : image
        Mask.

    Returns
    -------
    Compose albumentation
        A set of transformations.

    '''
    augmentations = A.Compose(
    [
       A.HorizontalFlip(p =1), 
       A.VerticalFlip(p =1), 
       A.RandomGamma(p=1.), 
       A.Rotate(p=1),
       A.OpticalDistortion(p=1),
       A.GridDistortion(p=1),
       A.ElasticTransform(p=1),
       A.Affine(p=1)
       
       ])
    return augmentations(image=image, mask=mask)

def aug_fn(image, mask):
    data = {"image":image,
            'mask': mask}
    aug_data = transforms(**data)
    aug_img = aug_data["image"]
    aug_mask = aug_data["mask"]
    return aug_img, aug_mask

def process_data(image, mask):
    aug_img, aug_mask = tf.numpy_function(func=aug_fn, 
                                          inp=[image, mask],
                                          Tout=[tf.float32, tf.float32])
    #aug_img.set_shape([None, 256,256,3])
    #aug_mask.set_shape([None, 256,256,1])
    return aug_img, aug_mask

def resize_and_rescale(image, mask):
    mask = tf.where(mask == 1, 0, 255)
    
    image = tf.image.resize(image, 
                            (opt.IMG_HEIGHT, opt.IMG_WIDTH),
                            preserve_aspect_ratio=True)
    mask = tf.image.resize(mask, (opt.IMG_HEIGHT, opt.IMG_WIDTH), 
                           preserve_aspect_ratio=True)
    #image = tf.cast(image, tf.float32)
    #tfio.experimental.image.decode_tiff(image)
    #tfio.experimental.image.decode_tiff(mask)
    image /= 255.0
    #mask -= 1
    mask /= 255.0
    #mask = tf.cast(tf.where(mask > 128, 1, 0), tf.float32)
    return image, mask

def add_sample_weights(image, label):
  # The weights for each class, with the constraint that:
  #     sum(class_weights) == 1.0
  class_weights = tf.constant([1.0, 2.0])
  class_weights = class_weights/tf.reduce_sum(class_weights)

  # Create an image of `sample_weights` by using the label at each pixel as an 
  # index into the `class weights` .
  sample_weights = tf.gather(class_weights, indices=tf.cast(label, tf.int32))

  return image, label, sample_weights
    
def prepare(ds, shuffle=False, augment=False):
    
    # split dataset to images and masks
    #images, masks = tf.data.Dataset.get_single_element(ds.batch(len(ds)))

    # Resize and rescale 
    ds = ds.map(resize_and_rescale, num_parallel_calls=opt.AUTOTUNE)
    
    # Use data augmentation only on the training set
    # When using albumentations you must first augment and then shuffle (batch them)
    # because albumentations process one image/mask at a time
    if augment:
      ds = ds.map(process_data, num_parallel_calls=opt.AUTOTUNE)
      
    if shuffle:
        ds = ds.shuffle(buffer_size=opt.BUFFER_SIZE)
        #ds = ds.map(add_sample_weights)
    
    # Batch dataset
    ds = ds.batch(opt.BATCH_SIZE)
    return ds.prefetch(buffer_size=opt.AUTOTUNE)


def split_train_test_val(ds,
                         seed,
                         train_split=opt.TRAIN_SPLIT, 
                         val_split=opt.VAL_SPLIT, 
                         test_split=opt.TEST_SPLIT,
                         shuffle=True, 
                         shuffle_size=opt.BUFFER_SIZE):
    '''
    Splits a tensorflow dataset to train, val and test sets.

    Parameters
    ----------
    ds : tensorflow dataset
        Tensorflow dataset.
    seed : integer scalar
        The seed used for reproducibility.
    train_split : float scalar, optional
        Train size in %. The default is opt.TRAIN_SPLIT.
    val_split : float scalar, optional
        Validation size in %. The default is opt.VAL_SPLIT.
    test_split : float scalar, optional
        Test size in %. The default is opt.TEST_SPLIT.
    shuffle : boolean, optional
        Shuffle dataset. The default is True.
    shuffle_size : integer scalar, optional
        The buffer size for tensroflow dataset. The default is opt.BUFFER_SIZE.

    Returns
    -------
    train_ds : tensorflow dataset
        Train set.
    val_ds : tensorflow dataset
        Validation set.
    test_ds : tensorflow dataset
        Test set.

    '''
                              
    assert (train_split + test_split + val_split) == 1
    
    ds_size = len(ds)
    if shuffle:
        # Specify seed to always have the same split distribution between runs
        ds = ds.shuffle(shuffle_size, 
                        reshuffle_each_iteration=False,
                        seed=seed)
    
    train_size = int(train_split * ds_size)
    val_size = int(val_split * ds_size)
    
    train_ds = ds.take(train_size)    
    val_ds = ds.skip(train_size).take(val_size)
    test_ds = ds.skip(train_size).skip(val_size)
    
    return train_ds, val_ds, test_ds

def data_gen(path_imgs, path_masks, seed):
    
    images = image_dataset_from_directory(
        path_imgs,
        batch_size=None,
        shuffle=False, 
        label_mode=None)
    
    masks = image_dataset_from_directory(
        path_masks,
        batch_size=None,
        shuffle=False,
        label_mode=None,
        color_mode='grayscale')
    
    dataset = tf.data.Dataset.zip((images, masks))
        
    train_ds, val_ds, test_ds = split_train_test_val(dataset,
                                                     opt.SEED,
                                                     train_split=opt.TRAIN_SPLIT, 
                                                     val_split=opt.VAL_SPLIT, 
                                                     test_split=opt.TEST_SPLIT,
                                                     shuffle=True, 
                                                     shuffle_size=opt.BUFFER_SIZE)
   
    #train_steps = len(train_ds)
    #val_steps = len(val_ds)
    
    train_ds = prepare(train_ds, shuffle=True, augment=opt.AUG)
    val_ds = prepare(val_ds)
    test_ds = prepare(test_ds)
    
    return train_ds, val_ds, test_ds
       
    
def GradCam(model, img, layer_name=None):
    '''
    Applies grad-cam in order to create activation maps.

    Parameters
    ----------
    model : keras model
        model.
    img : image
        The image to use.
    layer_name : string
        Keras layer in the model (usually the last conv layer).

    Returns
    -------
    None.

    '''
    img = img[-1, :, :]
    # get the last layer name
    if layer_name is None:
        layer_name = model.layers[-1].name
    grad_model = tf.keras.models.Model([model.inputs],
                                       [model.get_layer(layer_name).output,
                                        model.output])
    with tf.GradientTape() as tape:
        conv_outputs, predictions = grad_model(np.array([img]))
        loss = predictions#[:, 0]
    
    output = conv_outputs[0]
    grads = tape.gradient(loss, conv_outputs)[0]
    
    gate_f = tf.cast(output > 0, 'float32')
    gate_r = tf.cast(grads > 0, 'float32')
    guided_grads = tf.cast(output > 0, 'float32') * tf.cast(grads > 0,
                                                            'float32') * grads
    weights = tf.reduce_mean(guided_grads, axis=(0, 1))
    cam = np.ones(output.shape[0: 2], dtype = np.float32)
    for i, w in enumerate(weights):
        cam += w * output[:, :, i]
    # resize to model's training size
    cam = cv2.resize(cam.numpy(), 
                     (opt.IMG_HEIGHT,
                      opt.IMG_WIDTH))
    
    cam = np.maximum(cam, 0)
    heatmap = (cam - cam.min()) / (cam.max() - cam.min())
    
    cam = cv2.applyColorMap(np.uint8(255*heatmap), cv2.COLORMAP_JET)
    # resize to original size
    output_image = cv2.addWeighted(
        cv2.cvtColor(img.astype('uint8'),
        cv2.COLOR_RGB2BGR),
        0.5,
        cam,
        1,
        0)
    # resize to original size
    output_image = cv2.resize(output_image,
                             (opt.IMG_HEIGHT,
                              opt.IMG_WIDTH))
    plt.imshow(output_image)
    
