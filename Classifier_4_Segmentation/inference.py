#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 27 14:39:23 2022

@author: ggous
"""

import tensorflow as tf
import os
import glob
import rasterio
import subprocess
import numpy as np
import multiprocessing
from model import load_model_custom
from config import opt
from metrics import jaccard_coef, jaccard_distance
from PIL import Image
import json
from multiprocessing import Pool
from itertools import chain

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
# in newer versions replace 'PROJ_LIB' with 'PROJ_DATA'
# os.environ['PROJ_LIB'] = os.environ["CONDA_PREFIX"] + '/share/proj'

PRELOAD = ['__main__']
multiprocessing.set_forkserver_preload(PRELOAD)
multiprocessing.set_start_method('forkserver', force=True) #spawn

cpu_count = multiprocessing.cpu_count() 

if len(os.sched_getaffinity(0)) < cpu_count:
    try:
        os.sched_setaffinity(0, range(cpu_count))
    except OSError:
        print('Could not set affinity')

nb_procs = len(os.sched_getaffinity(0)) - 2

# Load the image to predict
img_to_predict = glob.glob(os.path.join(opt.PATH_IMG_TO_PREDICT , "*.tif"))[0]
img = np.array(Image.open(img_to_predict))[:, :, :3] # leave channel 4 out (alpha channel) or load with cv2
  
# check if image shape is divisible by nb or processors
# in order to be able to split the array in chunks
# start with the nb of procs decreasing by one
# if it is not divisible, use 1 nb of procs
for i in range(nb_procs, 0, -1):
    if (img.shape[0] % i) == 0:
        nb_procs = i
        break
    else:
        nb_procs = 1
 
print('Using', nb_procs, 'processes for the pool')
                            
dependencies = {
              'jaccard_coef': jaccard_coef,
              'jaccard_distance': jaccard_distance
        }
 
model = load_model_custom(opt.SAVE_PATH + 'model.h5',
                          custom_objects=dependencies)
     

def resize_and_rescale(image):
    image = tf.image.resize(image, 
                            (opt.IMG_HEIGHT, opt.IMG_WIDTH),
                            preserve_aspect_ratio=True)
    image /= 255.0
    return image
    
def prepare(ds):
    # Resize and rescale 
    ds = ds.map(resize_and_rescale)
    return ds

def predictions(data):
    results = []
    for el in data:
        pred = model.predict(el[np.newaxis, :, :, :], verbose=2)[0]
        mask = tf.where(pred[:, :, -1] > 0.5, 255, 0)
        results.append(mask)
        
    return results

def make_tensors(data):
    tens_data = tf.data.Dataset.from_tensor_slices(data)
    tens_data = prepare(tens_data)
    return tens_data

def parallelize_predictions(fn, data, workers):

    pool = Pool(processes=workers)
    # result = pool.map(fn, np.array_split(list(data.as_numpy_iterator()), workers))
    result = pool.map(fn, data.batch(np.ceil(len(data) / workers)))
    pool.close()
    return list(result)

def extract_image_tiles(size, im):
    '''
    Tiles a Square Image to square tiles 
    
    Parameters:
    -----------
    size: Int
          The size of the tiles (height OR width)
    im : PIL image  
        The image
          
    Returns:
    --------
    Tile of images size = size
    '''
    im = im[:, :, :3] # leave channel 4 out (alpha channel) or load with cv2
    w = h = size
    idxs = [(i, (i + h), j, (j + w)) for i in range(0, im.shape[0], h) for j in range(0, im.shape[1], w)]
    tiles_asarrays = []
    for k, (i_start, i_end, j_start, j_end) in enumerate(idxs):
        tile = im[i_start:i_end, j_start:j_end, ...]
        if tile.shape[:2] != (h, w):
            tile_ = tile
            tile_size = (h, w) if tile.ndim == 2 else (h, w, tile.shape[2])
            tile = np.zeros(tile_size, dtype=tile.dtype)
            tile[:tile_.shape[0], :tile_.shape[1], ...] = tile_
        tiles_asarrays.append(tile)
    return np.array(idxs), np.array(tiles_asarrays)

def main():
       
    idxs, tiles = extract_image_tiles(opt.IMG_HEIGHT, img)
    prep = make_tensors(tiles)
    
    masks = parallelize_predictions(predictions, prep, workers=nb_procs)
    masks_flatten = list(chain.from_iterable(masks))

    reconstructed = np.zeros((img.shape[0], img.shape[1]), dtype=np.uint8)
    
    for tile, (y_start, y_end, x_start, x_end) in zip(np.array(masks_flatten), idxs):
        y_end = min(y_end, img.shape[0])
        x_end = min(x_end, img.shape[1])
        reconstructed[y_start:y_end, x_start:x_end] = tile[:(y_end - y_start), :(x_end - x_start)]
        
    return reconstructed
    
    
if __name__=="__main__":
    reconstructed = main()
        
    final = Image.fromarray(reconstructed)
    final.save(opt.PATH_RECONSTRUCTED_MASK + 'predicted_mask.png')
    
    # first, we need to extract projection info from input image
    # pred_img = gdal.Open(img_to_predict)
    # proj_system = osr.SpatialReference(wkt=pred_img.GetProjection()).GetAttrValue('geogcs')
    
    with rasterio.open(img_to_predict) as src:
        crs = src.crs
    
    epsg = crs.data['init'].upper()
    
    # info = gdal.Info(img_to_predict, format='json')
    cmd = ['gdalinfo', '-json', img_to_predict]
    result = subprocess.run(cmd,  stdout=subprocess.PIPE)
    info = json.loads(result.stdout)
    
    with open (opt.PATH_RECONSTRUCTED_MASK + 'predicted_mask.wld', "w") as f:
        f.write(str(info['geoTransform'][1]))
        f.write("\n")
        f.write(str(info['geoTransform'][4]))
        f.write("\n")
        f.write(str(info['geoTransform'][2]))
        f.write("\n")
        f.write(str(info['geoTransform'][5]))
        f.write("\n")
        f.write(str(info['geoTransform'][0] + 0.5 * info['geoTransform'][1] + 0.5 * info['geoTransform'][2]))
        f.write("\n")
        f.write(str(info['geoTransform'][3] + 0.5 * info['geoTransform'][4] + 0.5 * info['geoTransform'][5]))
        f.close()
        
    os.chdir(opt.PATH_RECONSTRUCTED_MASK + '/')
    os.system("gdal_translate -of Gtiff -co COMPRESS=LZW -a_srs EPSG:32634 predicted_mask.png predicted_mask.tif")    
        
    # remove wld file
    os.remove('predicted_mask.wld')
    # remove png image
    os.remove('predicted_mask.png')
