#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  4 09:50:58 2022

@author: ggous
"""

from setuptools import setup, find_packages
from pathlib import Path

# Create the appropriate directories
Path("saved_output/").mkdir(parents=True, exist_ok=True)
Path("reconstructed_mask/").mkdir(parents=True, exist_ok=True)
Path("predicted_masks/").mkdir(parents=True, exist_ok=True)
Path("image_to_predict/").mkdir(parents=True, exist_ok=True)
Path("tiles/").mkdir(parents=True, exist_ok=True)
Path("data/masks/train/").mkdir(parents=True, exist_ok=True)
Path("data/images/train/").mkdir(parents=True, exist_ok=True)

setup(
    name = 'OPEKEPE_Segmentation',
    packages = find_packages(),
)
