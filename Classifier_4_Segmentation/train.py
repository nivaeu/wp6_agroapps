#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 12:03:43 2022

@author: ggous
"""

from model import build_model, predict, callback
from config import opt, LOSS, METRIC
from utils import data_gen, GradCam
from plots import plot_history, plot_train_ds, plot_test_ds
from pathlib import Path
from contextlib import redirect_stdout
  
# create paths if not exist
Path(opt.SAVE_PATH).mkdir(parents=True, exist_ok=True)
Path(opt.PATH_IMGS).mkdir(parents=True, exist_ok=True)
Path(opt.PATH_MASKS).mkdir(parents=True, exist_ok=True)
Path(opt.PATH_IMGS_TEST).mkdir(parents=True, exist_ok=True)
Path(opt.PATH_IMG_TO_PREDICT).mkdir(parents=True, exist_ok=True)
Path(opt.PATH_PRED_MASKS).mkdir(parents=True, exist_ok=True)
Path(opt.PATH_RECONSTRUCTED_MASK).mkdir(parents=True, exist_ok=True)

  
model = build_model(opt.IMG_HEIGHT, 
                    opt.IMG_WIDTH,
                    opt.CHANNELS,
                    opt.FILTERS,
                    opt.BATCH_NORM,
                    opt.NB_CLASSES)

model.compile(optimizer=opt.OPTIMIZER,
              loss=LOSS,
              metrics=METRIC)

train_ds, val_ds, test_ds = data_gen(
    opt.PATH_IMGS,
    opt.PATH_MASKS,
    opt.SEED)
    
history = model.fit(train_ds,
                    validation_data=val_ds,
                    batch_size=opt.BATCH_SIZE,
                    callbacks=callback(),
                    epochs=opt.EPOCHS)

epochs = len(history.history['loss'])
plot_history(range(1, epochs + 1),
              history.history['loss'],
              'loss',
              history.history['val_loss'],
              'val_loss',
              'Epochs',
              '',
              'Train and validation losses',
              'loss')

plot_history(range(1, epochs + 1),
              history.history['jaccard_coef'],
              'jaccard_coef',
              history.history['val_jaccard_coef'],
              'val_jaccard_coef',
              'Epochs',
              '',
              'Train and validation losses jaccard coefs',
              'jaccard_coef')
        
plot_train_ds(opt.NB_PLOTS_TRAIN,
              train_ds,
              history.model)

# Evaluate test set
test_loss, test_jaccard_coef = history.model.evaluate(test_ds)
print('------------------------------------\n')
with open(opt.SAVE_PATH + 'evaluation.txt', 'w') as f:
    with redirect_stdout(f):
        print(' Evaluation on test data\n')
        print('loss: {0} \t jaccard coef: {1}\n'.format(round(test_loss, 3), round(test_jaccard_coef, 3)))
    
print(' Evaluation on test data\n')
print('loss: {0} \t jaccard coef: {1}\n'.format(round(test_loss, 3), round(test_jaccard_coef, 3)))
preds = predict(test_ds, history.model)
nb_plots = opt.NB_PLOTS_TEST
plot_test_ds(nb_plots, test_ds, history.model)
