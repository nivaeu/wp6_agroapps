#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 12 13:06:05 2022

@author: ggous
"""

import tensorflow as tf
import matplotlib.pyplot as plt
from config import opt

def plot_history(x,
                 y1,
                 y1_legend,
                 y2,
                 y2_legend,
                 xlabel,
                 ylabel,
                 title,
                 fig_name):
        '''
        Line plot x vs y

        Args:
        ----
        x: x values. Numpy array of shape (n, )
        y1: y1 values. Numpy array of shape (n, )
        y1_legend: legend for y1 (String)
        y2: y2 values. Numpy array of shape (n, )
        y2_legend: legend for y2 (String)
        xlabel: x axis label (String)
        ylabel: y axis label (String)
        title: Plot title (String)
        fig_name: figure name (String)
        '''
        fig, ax = plt.subplots(figsize=(22, 16))
        ax.set(xlabel=xlabel,
               ylabel=ylabel,
               title=title)
        ax.plot(x, y1, c='blue')
        ax.plot(x, y2, c='green')
        ax.legend([y1_legend, y2_legend],loc='best')
        plt.savefig('{}'.format(opt.SAVE_PATH) + fig_name + '.png', dpi=100)
        plt.show()
        
def plot_preds_masks(orig_img, orig_mask, preds):
    '''
    Plot original, predictions and masked images

    Parameters
    ----------
    ids : string list
        image ids.
    orig_img : numpy array
        original image.
    orig_mask : numpy array
        original mask.
    preds : numpy array
        predictions.
   
    Returns
    -------
    None.

    '''
    titles = ['Image', 'Image Mask', 'Predicted Mask']
    if isinstance(orig_img, list):
        n_rows = len(orig_img)
        fig, ax = plt.subplots(n_rows, 3, figsize=(26, 20))
        for idx, _ in enumerate(range(n_rows)):
            #preds[idx] = np.where(preds[idx] > 0.5, 255, 0)
            for k, kind in enumerate(['original_img', 'original_mask', 'pred_mask']):
                if kind == 'original_img':
                    img = orig_img[idx][-1, :, :]
                elif kind == 'original_mask':
                    img = orig_mask[idx][-1, :, :]
                elif kind == 'pred_mask':
                    mask_pred = preds[idx]
                    #mask_pred = np.where(preds[idx] > 0.5, 255, 0)
                    img = mask_pred#np.where(mask_pred > 0.5, 255, 0)
                    
                ax[idx, k].imshow(img)
                ax[0, k].set_title(titles[k], fontsize=20)
    else:
        #preds = np.where(preds > 0.5, 255, 0)
        n_rows = 1
        fig, ax = plt.subplots(n_rows, 3, figsize=(26, 20))
        for k, kind in enumerate(['original_img', 'original_mask', 'pred_mask']):
            if kind == 'original_img':
                img = orig_img#[-1, :, :]
            elif kind == 'original_mask':
                img = orig_mask#[-1, :, :]
            elif kind == 'pred_mask':
                mask_pred = preds
                img = mask_pred
         
            ax[k].imshow(img)
            ax[k].set_title(titles[k], fontsize=20)
           
    fig.tight_layout()
    plt.show()
    
def plot_img_mask(image, mask, predictions=False):
    '''
    Plot image and mask
    
    Parameters
    ----------
    image : opencv image
        image 
    mask : opencv mask 
        mask
    
    Returns
    -------
    None.
    '''
    if predictions:
        mask = mask 
    else:
        mask = mask#[-1, :, :]
        
    #mask = np.where(mask > 0.5, 255, 0)
    plt.figure(figsize=(15, 15))

    # Image
    plt.subplot(1, 3, 1)
    plt.imshow(image)#[-1, :, :])
    plt.title("Image", fontsize=16)

    # Mask
    plt.subplot(1, 3, 2)
    plt.imshow(mask)
    if predictions:
        plt.title("Predicted Mask", fontsize=16)
    else:
        plt.title("Image Mask", fontsize=16)

    # Image + Mask
    plt.subplot(1, 3, 3)
    plt.imshow(image[-1, :, :])
    plt.imshow(mask, alpha=0.5, cmap='plasma')
    if predictions:
        plt.title("Image + Predicted Mask", fontsize=16)
    else:
        plt.title("Image + Mask", fontsize=16)
    
def plot_test_ds(nb_plots,
                 test_ds,
                 model,
                 known_test_set=True):
    '''
    Plots from the test set

    Parameters
    ----------
    nb_plots : integer scalar
        How many plots (test data images) you want.
    test_ds : tensorflow dataset
        Test dataset.
    model : tensorflow model
        Tensorflow trained model.
    known_test_set : boolean
        True if we are looking for the evaluation
        of known test data set

    Returns
    -------
    None.

    '''
    if known_test_set:
        fig, axs = plt.subplots(nb_plots, 3, figsize=(20, nb_plots * 6 ), constrained_layout=True)
        for ax, element in zip(axs, test_ds.take(nb_plots)):
            image, y_true = element
            prediction = model.predict(image)[0]
            prediction = tf.where(prediction > 0.5, 255, 0)
            ax[0].set_title('Original image', fontsize=20)
            ax[0].imshow(image[0])
            ax[1].set_title('Original mask', fontsize=20)
            ax[1].imshow(tf.where(y_true[0] > 0.5, 255, 0))
            ax[2].set_title('Predicted mask', fontsize=20)
            ax[2].imshow(prediction)
        
        plt.savefig('{}'.format(opt.SAVE_PATH) + 'test_set_known.png', dpi=100)

    else:
        fig, axs = plt.subplots(nb_plots, 2, figsize=(20, nb_plots * 6 ), constrained_layout=True)
        for ax, element in zip(axs, test_ds.unbatch().take(nb_plots)):
            image = element
            prediction = model.predict(tf.expand_dims(image, axis=0))[0]
            prediction = tf.where(prediction > 0.5, 255, 0)
            ax[0].set_title('Original image', fontsize=20)
            ax[0].imshow(image)
            ax[1].set_title('Predicted mask', fontsize=20)
            ax[1].imshow(prediction)
        
        plt.savefig('{}'.format(opt.SAVE_PATH) + 'test_set.png', dpi=100)
            
     
def plot_train_ds(nb_plots,
                  train_ds,
                  model):
    '''
    Plots from the train set

    Parameters
    ----------
    nb_plots : integer scalar
        How many plots (test data images) you want.
    train_ds : tensorflow dataset
        Train dataset.
    model : tensorflow model
        Tensorflow trained model.

    Returns
    -------
    None.

    '''
    fig, axs = plt.subplots(nb_plots, 5, figsize=(22, nb_plots * 7 ), constrained_layout=True)
    for ax, element in zip(axs, train_ds.take(nb_plots)):
        image, y_true = element
        #image, y_true, weights = element
        prediction = model.predict(image)[0]
        prediction = tf.where(prediction > 0.5, 255, 0)
        
        ax[0].set_title('Original image', fontsize=20)
        ax[0].imshow(image[0])
        
        ax[1].set_title('Original mask', fontsize=20)
        ax[1].imshow(tf.where(y_true[0] > 0.5, 255, 0))
        
        ax[2].set_title("Original image + Mask", fontsize=16)
        ax[2].imshow(image[0])
        ax[2].imshow(tf.where(y_true[0] > 0.5, 255, 0), alpha=0.3, cmap='plasma')
        
        ax[3].set_title('Predicted mask', fontsize=20)
        ax[3].imshow(prediction)
        
        ax[4].set_title("Original Image + Predicted mask", fontsize=16)
        ax[4].imshow(image[0])
        ax[4].imshow(prediction, alpha=0.3, cmap='plasma')
    
    plt.savefig('{}'.format(opt.SAVE_PATH) + 'train_set.png', dpi=100)
