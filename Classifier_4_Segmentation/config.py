#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 10 15:54:56 2022

@author: ggous
"""

import tensorflow as tf
import warnings
from metrics import jaccard_coef, jaccard_distance

LOSS = jaccard_distance
METRIC = [jaccard_coef]
 
class DefaultConfig():
    
    model = 'Unet'
    tag = model
    # paths to train data
    PATH_IMGS = ('data/images/train/')
    PATH_MASKS = ('data/masks/train/')
    
    # path to test data (inference mode)
    PATH_IMGS_TEST = ('tiles/')
    
    # path to image we want to make the prediction
    PATH_IMG_TO_PREDICT = ('image_to_predict/')
    
    # path to save predicted masks
    PATH_PRED_MASKS = ('predicted_masks/')
    
    # path to save reconstructed predicted mask
    PATH_RECONSTRUCTED_MASK = ('reconstructed_mask/')
    
    # path to save outputs
    SAVE_PATH = 'saved_outputs/'
        
    DEBUG = False
    AUTOTUNE = tf.data.experimental.AUTOTUNE
    BUFFER_SIZE = 1000
    
    if DEBUG:
        SEED = 123
    else:
        SEED = None
        
    IMG_HEIGHT = 32
    IMG_WIDTH = 32
    CHANNELS = 3
    
    FILTERS = 16
    NB_CLASSES = 1
    DROPOUT = 0.3
    
    EPOCHS = 40
    OPTIMIZER = tf.keras.optimizers.Adam(learning_rate=0.001)
    AUG = True # Apply augmentations        
    BATCH_SIZE = 32
    BATCH_NORM = True
    
    TRAIN_SPLIT = 0.8 
    VAL_SPLIT = 0.1 
    TEST_SPLIT = 0.1
    
    # Model Learning rate scheduler
    START_LR = 0.0001
    MIN_LR = 0.0005
    MAX_LR = 0.001
    RAMPUP_EPOCHS = 4
    SUSTAIN_EPOCHS = 4
    EXP_DECAY = 0.7

    NB_PLOTS_TRAIN = 10 # How many plots from train data set
    NB_PLOTS_TEST = 10 # How many plots from test data set
    NB_PLOTS_FINAL = 2 # How many plots from FINAL unknown test data set
    

def parse(self, kwargs):
    '''
    update parameters
    '''
    for k,v in kwargs.items():
        if not hasattr(self,k):
            warnings.warn("Warning: opt has not attribut %s" %k)
        setattr(self, k, v)

DefaultConfig.parse = parse
opt = DefaultConfig()
