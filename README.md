# Tools for WP6 from Agroapps

## Deliverables

Agroapps Deliverables folder

## Classifier #1

Folders `Classifier_1` and `Classifier_1_scripts` contain the code to run the first classifier

## Classifier #2 and #3

Folder `Classifier_2_3` contains
**Classifier 2** (PlanetFusion for small parcels) and **Classifier 3** (fallow land)

## Classifier #4

Inside folder `Classifier_4_Segmentation` you can find the machine learning process of the classifier #4

## CropClassificationConnector

Folder with Connector for the NIVA orchestrator SDK

## EO_Team docker tools (eo-toos)

Docker image with tools to manipulate/edit/correct project related data files and large datasets

`docker pull agroapps/eo-tools:0.3.0`

**Tools :**
ESA SNAP/SAGA GIS/Orfeo Toolbox/GDAL/R/python3 etc.

The container for eo-tools is prebuilt in `Docker hub`:

https://hub.docker.com/r/agroapps/eo-tools
