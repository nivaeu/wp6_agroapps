DEBIAN Operational system


ESA_SNAP_MAIN_VERSION_8=8
ESA_SNAP_MAIN_VERSION_7=7
ESA_SNAP_MAIN_VERSION_6=6

SAGA_MINOR_VERSION=7.7.1

SEN2COR_VERSION=2.9.0

OTB_VERSION=7.4.0


    software-properties-common \
    wget \
    python3-pip \
    cron \
    dos2unix=7.3.4-3

    g++ \
    make \
    automake \
    libtool

    libwxgtk3.0-dev \
    libtiff5-dev \
    libgdal-dev \
    libproj-dev \
    libexpat-dev \
    wx-common \
    libogdi3.2-dev \
    unixodbc-dev \
    libhpdf-dev \
    libsvm-dev

    gdal-bin \
    python3-gdal

    libgfortran3


    r-base \
    default-jre \
    default-jdk \
    libudunits2-dev && \


        libx11-6 \
        libxext6 \
        libxau6 \
        libxxf86vm1 \
        libxdmcp6 \
        libdrm2 \
        libexpat-dev  \
        libexpat1-dev \
        cmake \
        build-essential \
        libgsl-dev \
        libglu1-mesa-dev \
        freeglut3-dev \
        mesa-common-dev \

install.packages("zoo", version = "1.8-8", Ncpus = 4)
install.packages("dplyr", version = "1.0.5", Ncpus = 4)
install.packages("mlr", version = "2.19.0", Ncpus = 4)
install.packages("tidyverse", version = "1.3.1", Ncpus = 4)
install.packages("parallelMap", version = "1.4", Ncpus = 4)
install.packages("randomForest", version = "4.6-14", Ncpus = 4)
install.packages("randomForestSRC", version = "2.12.1", Ncpus = 4)
install.packages("FSelector", Ncpus = 4)
install.packages("FSelectorRcpp", Ncpus = 4)
install.packages("care", Ncpus = 4)
install.packages("praznik", Ncpus = 4)
install.packages("raster", version = "3.4-5", Ncpus = 4)
install.packages("sp", version = "1.4-5", Ncpus = 4)
install.packages("rgdal", version = "1.5-21", Ncpus = 4)
install.packages("tiff", version = "0.1-8", Ncpus = 4)
install.packages("maptools", version = "3", Ncpus = 4)
install.packages("signal", version = "0.7-6", Ncpus = 4)
install.packages("timeSeries", version = "3062.1", Ncpus = 4)
install.packages("readxl", version = "1.3.1", Ncpus = 4)
install.packages("rgeos", version = "0.5-2", Ncpus = 4)
install.packages("data.table", version = "1.12.8", Ncpus = 4)
install.packages("sjlabelled", version = "1.1.3", Ncpus = 4)
install.packages("magrittr", version = "2.0.1", Ncpus = 4)
install.packages("e1071", version = "1.7-3", Ncpus = 4)
install.packages("Hmisc", version = "4.4-0", Ncpus = 4)
install.packages("caret", version = "6.0-86", Ncpus = 4)
install.packages("kernlab", version = "0.9-29", Ncpus = 4)
install.packages("Metrics", version = "0.1.4", Ncpus = 4)
install.packages("reshape2", version = "1.4.3", Ncpus = 4)
install.packages("Ckmeans.1d.dp", version = "4.3.2", Ncpus = 4)
install.packages("doParallel", version = "1.0.15", Ncpus = 4)
install.packages("sf", version = "1.3.3", Ncpus = 4)



click==7.1.2
psycopg2==2.8.6
Shapely==1.7.0
sentinelsat==0.13
sentinelhub==3.4.0
botocore==1.21.62

