# -*- coding: utf-8 -*-
import csv
import datetime
import os
from typing import Dict, Generator, Tuple

from osgeo import gdal, ogr
from pyarrow import ipc

from niva_classifiers.dataclasses import Parcel


def bounding_box_from_gdal_dataset(dataset: gdal.Dataset) -> Tuple[int, int, int, int]:
    gt = dataset.GetGeoTransform()
    xmin, ymax = gt[0], gt[3]
    xres, yres = gt[1], gt[5]

    nx = dataset.RasterXSize
    ny = dataset.RasterYSize
    xmax = xmin + nx * xres
    ymin = ymax + ny * yres

    return xmin, ymin, xmax, ymax


def bbox_to_polygon(bbox: Tuple[int, int, int, int]) -> ogr.Geometry:
    wkt = f"POLYGON(({bbox[0]} {bbox[1]}, {bbox[2]} {bbox[1]}, {bbox[2]} {bbox[3]}, \
{bbox[0]} {bbox[3]}, {bbox[0]} {bbox[1]}))"
    geom = ogr.CreateGeometryFromWkt(wkt)
    print(wkt)
    return geom


def convert_shp_to_parcels(
    path: str,
    f_client_id: str,
    f_crop_code: str,
    f_catch_crop_code=None,
    f_rfv_code=None,
) -> Generator[Parcel, None, None]:
    """


    Parameters
    ----------
    path : str
        Path to input Shapefile with parcels.
    f_client_id : str
        Field name containing unique object identifier.
    f_crop_code : str
        Field name containing integer crop code.
    f_catch_crop_code : TYPE, optional
        Field name containing integer catch crop code. The default is None.

    Yields
    ------
    Generator[Parcel]
        Parcel objects.

    """

    ds = ogr.Open(path)
    lyr = ds.GetLayer()
    srs = lyr.GetSpatialRef()
    epsg = srs.GetAttrValue("AUTHORITY", 1)
    n = len(lyr)
    for x, feat in enumerate(lyr):
        client_id = feat.GetField(f_client_id)
        crop_code = feat.GetField(f_crop_code)
        if f_catch_crop_code:
            catch_crop_code = feat.GetField(f_catch_crop_code)
        else:
            catch_crop_code = None

        if f_rfv_code:
            rfv_code = feat.GetField(f_rfv_code)
        else:
            rfv_code = None

        geom_wkt = feat.GetGeometryRef().ExportToWkt()

        parcel = Parcel(
            parcel_id=-1,
            client_id=client_id,
            crop_code=crop_code,
            catch_crop_code=catch_crop_code,
            crop_code_groundtruth=rfv_code,
            geom_wkt=geom_wkt,
            epsg=epsg,
        )
        yield parcel

        if x % 10000 == 0:
            print(x, "/", n)
    ds.Destroy()


def ipc_to_csv(input_ipc: str, output_csv: str):
    """
    Converts .ipc (Apache Arrow) file to CSV file

    Parameters
    ----------
    input_ipc : str
        Path to input .ipc file.
    output_csv : str
        Path to output .csv file.

    Returns
    -------
    None.

    """

    reader = ipc.open_file(input_ipc)
    with open(output_csv, "w") as file:
        writer = csv.writer(file, quoting=csv.QUOTE_MINIMAL)
        writer.writerow(reader.schema.names)
        n_batches = reader.num_record_batches
        for i in range(n_batches):
            print(i, "/", n_batches)
            b = reader.get_batch(i)
            writer.writerows(map(list, zip(*b)))


def list_planetfusion_images_by_tile(
    planetfusion_dir: str,
) -> Dict[str, Dict[str, str]]:

    files = {}
    for tile in os.listdir(planetfusion_dir):
        files[tile] = {}
        imagedir = os.path.join(planetfusion_dir, tile, "PF-SR")
        for f in os.listdir(imagedir):
            if f.endswith("tif"):
                files[tile][f] = os.path.join(imagedir, f)

    return files


def generate_planetfusion_selection(startdate, enddate, step):
    season = (enddate - startdate).days
    filenames = [
        f"{(startdate + datetime.timedelta(days=days_off)).strftime('%Y-%m-%d')}.tif"
        for days_off in range(0, season, step)
    ]
    return filenames
