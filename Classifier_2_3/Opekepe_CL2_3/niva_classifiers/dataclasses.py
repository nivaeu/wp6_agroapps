# -*- coding: utf-8 -*-
"""
Created on Thu Oct 13 11:35:54 2022

@author: Cornelis
"""

from dataclasses import dataclass


@dataclass
class Parcel:
    parcel_id: int
    client_id: str
    geom_wkt: str
    epsg: int
    crop_code: int
    catch_crop_code: int = None
    crop_code_groundtruth: int = None
