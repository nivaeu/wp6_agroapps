
delete from %(schema)s.observation_type;
delete from %(schema)s.stats_method;
delete from %(schema)s.marker;
delete from %(schema)s.sensor;
delete from %(schema)s.product_type;
delete from %(schema)s.foi_status;
delete from %(schema)s.delivery_type;

insert into %(schema)s.delivery_type (type)
VALUES 
	('RVO_eventmarkers'),
	('RVO_cropmarkers'),
	('DAA_eventmarkers');

insert into %(schema)s.foi_status (status_id, status)
VALUES
	(1, 'active'),
	(2, 'suspicious'),
	(3, 'inactive');
	
insert into %(schema)s.product_type (type)
VALUES
	('CIR'),
	('RGB'),
	('NDVI');

insert into %(schema)s.sensor (sensor_id, sensor_name)
VALUES
	(1, 'Sentinel-1'),
	(2, 'Sentinel-2'),
	(3, 'Landsat-8'),
	(4, 'SuperView'),
        (5, 'PlanetFusion');

insert into %(schema)s.marker (marker_name, description, marker_type)
VALUES
	('MarkerNDTIPloughing', 'blabla', 'marker'),
	('MarkerNDVIVegetationPresence','blabla','marker'),
	('MowingDetector','blabla','detector'),
	('MowingDetector_DAA', 'blabla', 'detector'),
	('CropClassifier_interpolated', 'blabla', 'classifier');
	
insert into %(schema)s.stats_method (name)
VALUES
	('original_geometry'),
	('inner_buffer_geometry');

insert into %(schema)s.event_type (event_type_id, type)
VALUES
	(100, 'UNATTAINABLE-signal_too_noisy'),
	(101, 'UNATTAINABLE-too_few_orbits'),
	(102, 'UNATTAINABLE-insufficient_data'),
	(103, 'UNATTAINABLE-crop_class_too_small'),
	(104, 'UNATTAINABLE-no_detection'),
	(1, 'VegetationPresence'),
	(2, 'VegetationAbsence'),
	(3, 'Tillage'),
	(4, 'Mowing');


insert into %(schema)s.observation_type (observation_type_id, indicator, observation_metrics, stats_method)
VALUES
	(1, 'blue', ARRAY['mean','stdev'], 'original_geometry'),
	(2, 'green', ARRAY['mean','stdev'], 'original_geometry'),
	(3, 'red', ARRAY['mean','stdev'], 'original_geometry'),
	(4, 'NIR', ARRAY['mean','stdev'], 'original_geometry'),
	(5, 'SWIR1', ARRAY['mean','stdev'], 'original_geometry'),
	(6, 'rededge1', ARRAY['mean','stdev'], 'original_geometry'),
	(7, 'rededge2', ARRAY['mean','stdev'], 'original_geometry'),
	(8, 'rededge3', ARRAY['mean','stdev'], 'original_geometry'),
	(9, 'SWIR2', ARRAY['mean','stdev'], 'original_geometry'),

	(10, 'amp_asc_vv', ARRAY['mean','stdev'], 'original_geometry'),
	(11, 'amp_asc_vh', ARRAY['mean','stdev'], 'original_geometry'),
	(12, 'amp_des_vv', ARRAY['mean','stdev'], 'original_geometry'),
	(13, 'amp_des_vh', ARRAY['mean','stdev'], 'original_geometry'),
	(14, 'coh_asc_vv', ARRAY['mean','stdev'], 'original_geometry'),
	(15, 'coh_asc_vh', ARRAY['mean','stdev'], 'original_geometry'),
	(16, 'coh_des_vv', ARRAY['mean','stdev'], 'original_geometry'),
	(17, 'coh_des_vh', ARRAY['mean','stdev'], 'original_geometry'),

	(20, 'brightness', ARRAY['mean','stdev'], 'original_geometry'),
	(21, 'NDVI', ARRAY['mean','stdev'], 'original_geometry'),
	(22, 'NDWI', ARRAY['mean','stdev'], 'original_geometry'),
	(23, 'NDTI', ARRAY['mean','stdev'], 'original_geometry'),
	(24, 'NDII', ARRAY['mean','stdev'], 'original_geometry'),
	(25, 'NDI7', ARRAY['mean','stdev'], 'original_geometry'),


	(101, 'blue', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(102, 'green', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(103, 'red', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(104, 'NIR', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(105, 'SWIR1', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(106, 'rededge1', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(107, 'rededge2', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(108, 'rededge3', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(109, 'SWIR2', ARRAY['mean','stdev'], 'inner_buffer_geometry'),

	(110, 'amp_asc_vv', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(111, 'amp_asc_vh', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(112, 'amp_des_vv', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(113, 'amp_des_vh', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(114, 'coh_asc_vv', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(115, 'coh_asc_vh', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(116, 'coh_des_vv', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(117, 'coh_des_vh', ARRAY['mean','stdev'], 'inner_buffer_geometry'),

	(120, 'brightness', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(121, 'NDVI', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(122, 'NDWI', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(123, 'NDTI', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(124, 'NDII', ARRAY['mean','stdev'], 'inner_buffer_geometry'),
	(125, 'NDI7', ARRAY['mean','stdev'], 'inner_buffer_geometry');
