
CREATE EXTENSION IF NOT EXISTS postgis;

CREATE SCHEMA IF NOT EXISTS %(schema)s;

CREATE OR REPLACE FUNCTION %(schema)s.set_relative_orbit() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	
	    IF NEW.image_name LIKE 'S1A%%GRDH%%' THEN
		 		NEW.relative_orbit =  mod((substring(NEW.image_name,28,6)::int -73),175)+1;
		 
		ELSIF NEW.image_name LIKE 'S1B%%GRDH%%' THEN
		 		NEW.relative_orbit =  mod((substring(NEW.image_name,28,6)::int -27),175)+1;
				
		ELSIF NEW.image_name LIKE 'S1A%%SLC%%' THEN
		 		NEW.relative_orbit =  mod((substring(NEW.image_name,27,6)::int -73),175)+1;
				
		ELSIF NEW.image_name LIKE 'S1B%%SLC%%' THEN
		 		NEW.relative_orbit =  mod((substring(NEW.image_name,27,6)::int -27),175)+1;
		END IF;

	RETURN NEW;
END;
$$;

-- delivery info tables

CREATE TABLE IF NOT EXISTS %(schema)s.delivery_type (
    type text NOT NULL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS %(schema)s.delivery (
    delivery_id uuid NOT NULL PRIMARY KEY,
    dt_started timestamp without time zone NOT NULL,
    dt_completed timestamp without time zone,
    filenames text[],
    dirname text,
    delivery_type text NOT NULL REFERENCES %(schema)s.delivery_type(type),
    delivery_client_id text
);


-- crop tables

CREATE TABLE IF NOT EXISTS %(schema)s.superclass (
    class_id serial PRIMARY KEY NOT NULL,
    class_type text NOT NULL,
    class_name text NOT NULL,
    class_description text,
    valid_from timestamp without time zone NOT NULL,
    valid_to timestamp without time zone,
    class_code integer
);
CREATE UNIQUE INDEX IF NOT EXISTS superclass_idx ON %(schema)s.superclass USING btree (class_code, valid_from);

CREATE TABLE IF NOT EXISTS %(schema)s.crop (
    crop_id serial NOT NULL PRIMARY KEY,
    crop_code integer NOT NULL,
    superclass_id_early integer NOT NULL REFERENCES %(schema)s.superclass(class_id),
    superclass_id_late integer NOT NULL REFERENCES %(schema)s.superclass(class_id),
    crop_name text NOT NULL,
    valid_from timestamp without time zone NOT NULL,
    valid_to timestamp without time zone
);
CREATE UNIQUE INDEX IF NOT EXISTS crop_idx ON %(schema)s.crop USING btree (valid_from, crop_code);

-- image tables

CREATE TABLE IF NOT EXISTS %(schema)s.aoi (
    aoi_name text NOT NULL PRIMARY KEY,
    project text,
    geom geometry(MultiPolygon,28992),
    start_date timestamp without time zone,
    end_date timestamp without time zone
);

CREATE TABLE IF NOT EXISTS %(schema)s.sensor (
    sensor_id smallint NOT NULL PRIMARY KEY,
    sensor_name text NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS %(schema)s.tile (
    tile_id serial PRIMARY KEY NOT NULL,
    sensor_id smallint NOT NULL REFERENCES %(schema)s.sensor(sensor_id),
    geom geometry(Polygon,4326) NOT NULL,
    tile_name text NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS tile_idx ON %(schema)s.tile USING btree (tile_name, sensor_id);

CREATE TABLE IF NOT EXISTS %(schema)s.image (
    image_name text PRIMARY KEY NOT NULL,
    sensor_id smallint NOT NULL REFERENCES %(schema)s.sensor(sensor_id),
    date date NOT NULL,
    "time" time without time zone NOT NULL,
    relative_orbit integer,
    tile_id integer references %(schema)s.tile(tile_id),
    aoi_name text REFERENCES %(schema)s.aoi(aoi_name),
    geom geometry(MultiPolygon, 28992) NOT NULL
);

CREATE TABLE IF NOT EXISTS %(schema)s.image_file (
    image_file_id serial PRIMARY KEY NOT NULL,
    filename text NOT NULL UNIQUE,
    image_name text NOT NULL references %(schema)s.image(image_name) ON UPDATE CASCADE ON DELETE CASCADE,
    resolution real NOT NULL,
    projection integer NOT NULL,
    source text,
    location_offline text,
    location_online text,
    geom geometry(MultiPolygon, 28992) NOT NULL
);

CREATE TABLE IF NOT EXISTS %(schema)s.product_type (
    type text PRIMARY KEY NOT NULL
);

CREATE TABLE IF NOT EXISTS %(schema)s.product (
    product_id serial PRIMARY KEY NOT NULL,
    image_file_id integer NOT NULL REFERENCES %(schema)s.image_file(image_file_id) ON UPDATE CASCADE ON DELETE CASCADE,
    product_type text NOT NULL REFERENCES %(schema)s.product_type(type),
    bands integer[] NOT NULL,
    pixel_depth integer NOT NULL
);


-- object of interest tables

CREATE TABLE IF NOT EXISTS %(schema)s.delivered_parcel (
    client_id text NOT NULL PRIMARY KEY,
    functioneel_id text
);

CREATE TABLE IF NOT EXISTS %(schema)s.parcel (
    parcel_id serial PRIMARY KEY NOT NULL,
    client_id text NOT NULL REFERENCES %(schema)s.delivered_parcel(client_id),
    zone_number integer,
    crop_code integer NOT NULL,
    catch_crop_code integer,
    crop_code_groundtruth integer,
    geom geometry(MultiPolygon)
);

CREATE TABLE IF NOT EXISTS %(schema)s.foi_status (
    status_id smallint PRIMARY KEY NOT NULL,
    status text NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS %(schema)s.foi (
    foi_id bigserial PRIMARY KEY NOT NULL,
    crop_class_found_id integer REFERENCES %(schema)s.superclass(class_id),
    valid_start timestamp without time zone,
    geom geometry(MultiPolygon) NOT NULL,
    status_id smallint REFERENCES %(schema)s.foi_status(status_id),
    valid_end timestamp without time zone,
    marker_version_id uuid,
    num_pixels_within_inner_buffer int,
    num_pixels_centroid int,
    parent_id bigint references %(schema)s.foi(foi_id)
);

CREATE TABLE IF NOT EXISTS %(schema)s.segment (
    segment_id bigserial PRIMARY KEY NOT NULL,
    foi_id bigint NOT NULL REFERENCES %(schema)s.foi(foi_id),
    image_file_id integer NOT NULL REFERENCES %(schema)s.image_file(image_file_id),
    geom geometry(Polygon) NOT NULL,
    valid_end timestamp without time zone,
    valid_start timestamp without time zone
);


CREATE TABLE IF NOT EXISTS %(schema)s.stats_method (
    name text NOT NULL PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS %(schema)s.parcel_foi (
    parcel_id bigint NOT NULL REFERENCES %(schema)s.parcel(parcel_id),
    foi_id bigint NOT NULL REFERENCES %(schema)s.foi(foi_id) ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS %(schema)s.foi_tile (
    foi_id bigint NOT NULL references %(schema)s.foi(foi_id),
    tile_id integer NOT NULL references %(schema)s.tile(tile_id),
    pixel_count_initialgeom integer,
    pixel_count_buffer integer
);

CREATE TABLE IF NOT EXISTS %(schema)s.foi_delivery (
    delivery_id uuid NOT NULL REFERENCES %(schema)s.delivery(delivery_id),
    foi_id bigint NOT NULL REFERENCES %(schema)s.foi(foi_id)
);

CREATE TABLE IF NOT EXISTS %(schema)s.selection (
    selection_id serial PRIMARY KEY NOT NULL,
    description text NOT NULL,
    query text,
    dt_created timestamp DEFAULT now(),
    foi_count bigint
);
    
CREATE TABLE IF NOT EXISTS %(schema)s.foi_selection(
    selection_id integer NOT NULL REFERENCES %(schema)s.selection(selection_id),
    foi_id bigint NOT NULL REFERENCES %(schema)s.foi(foi_id),
    dt_added timestamp DEFAULT now(),
    CONSTRAINT no_duplicates UNIQUE (selection_id, foi_id)
);

-- observations

CREATE TABLE IF NOT EXISTS %(schema)s.observation_type (
    observation_type_id smallint PRIMARY KEY NOT NULL,
    indicator text NOT NULL,
    observation_metrics text[] NOT NULL,
    stats_method text NOT NULL REFERENCES %(schema)s.stats_method(name)
);
CREATE UNIQUE INDEX IF NOT EXISTS observation_type_idx ON %(schema)s.observation_type USING btree (indicator, stats_method);

CREATE TABLE IF NOT EXISTS %(schema)s.observation (
    observation_id bigserial PRIMARY KEY NOT NULL,
    foi_id bigint NOT NULL REFERENCES %(schema)s.foi(foi_id),
    segment_id bigint REFERENCES %(schema)s.segment(segment_id),
    image_file_id integer NOT NULL REFERENCES %(schema)s.image_file(image_file_id),
    observation_type_id smallint NOT NULL REFERENCES %(schema)s.observation_type(observation_type_id),
    value double precision[] NOT NULL,
    dt_added timestamp without time zone DEFAULT now(),
    constraint obs_uq unique (image_file_id, foi_id, observation_type_id, segment_id)
);

CREATE TABLE IF NOT EXISTS %(schema)s.observation_interpolated (
    observation_interpolated_id bigserial PRIMARY KEY NOT NULL,
    foi_id bigint NOT NULL REFERENCES %(schema)s.foi(foi_id),
    segment_id bigint REFERENCES %(schema)s.segment(segment_id),
    sensor_id smallint NOT NULL REFERENCES %(schema)s.sensor(sensor_id),
    date date NOT NULL,
    relative_orbit integer,
    observation_type_id smallint NOT NULL REFERENCES %(schema)s.observation_type(observation_type_id),
    value double precision NOT NULL,
    nn_distance_days integer DEFAULT 0,
    dt_added timestamp without time zone DEFAULT now(),
    constraint obs_int_uq unique (sensor_id, observation_type_id, foi_id, date, relative_orbit)
);

-- algorithm tables
CREATE TABLE IF NOT EXISTS %(schema)s.event_type (
    event_type_id smallint NOT NULL PRIMARY KEY,
    type text NOT NULL
);

CREATE TABLE IF NOT EXISTS %(schema)s.marker (
    marker_name text PRIMARY KEY NOT NULL,
    description text,
    marker_type text NOT NULL,
    event_type_id smallint REFERENCES %(schema)s.event_type(event_type_id)
);

CREATE TABLE IF NOT EXISTS %(schema)s.marker_input (
    marker_name text NOT NULL REFERENCES %(schema)s.marker(marker_name),
    observation_type_id smallint NOT NULL REFERENCES %(schema)s.observation_type(observation_type_id)
);

CREATE TABLE IF NOT EXISTS %(schema)s.marker_version (
    marker_version_id uuid PRIMARY KEY NOT NULL,
    version integer[] NOT NULL,
    dt_added timestamp without time zone,
    description text NOT NULL,
    marker_name text NOT NULL REFERENCES %(schema)s.marker(marker_name) ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS %(schema)s.marker_run (
    run_id serial PRIMARY KEY NOT NULL,
    marker_version_id uuid NOT NULL REFERENCES %(schema)s.marker_version(marker_version_id),
    obs_window_start timestamp without time zone NOT NULL,
    obs_window_end timestamp without time zone NOT NULL,
    delivery_id uuid REFERENCES %(schema)s.delivery(delivery_id),
    marker_parameters text NOT NULL,
    run_name text NOT NULL UNIQUE,
    selection_id integer REFERENCES %(schema)s.selection(selection_id),
    run_client_id text
);

CREATE TABLE IF NOT EXISTS %(schema)s.image_file_run_selection (
    run_id integer NOT NULL REFERENCES %(schema)s.marker_run(run_id) ON UPDATE CASCADE ON DELETE CASCADE,
    image_file_id integer NOT NULL REFERENCES %(schema)s.image_file(image_file_id)
);

-- groundtruth tables
CREATE TABLE IF NOT EXISTS %(schema)s.groundtruth_collection (
    collection_id serial PRIMARY KEY NOT NULL,
    collection_type text NOT NULL,
    analysis_period_start date NOT NULL,
    analysis_period_end date NOT NULL,
    description text
);

CREATE TABLE IF NOT EXISTS %(schema)s.groundtruth (
    groundtruth_id uuid PRIMARY KEY NOT NULL,
    foi_id bigint NOT NULL REFERENCES %(schema)s.foi(foi_id),
    event_start timestamp without time zone,
    event_end timestamp without time zone,
    event_type_id smallint NOT NULL REFERENCES %(schema)s.event_type(event_type_id),
    event_present text NOT NULL,
    crop_id integer REFERENCES %(schema)s.crop(crop_id),
    collection_id integer NOT NULL REFERENCES %(schema)s.groundtruth_collection(collection_id),
    usability smallint NOT NULL
);

CREATE TABLE IF NOT EXISTS %(schema)s.groundtruth_delivery (
    groundtruth_id uuid NOT NULL REFERENCES %(schema)s.groundtruth(groundtruth_id),
    delivery_id uuid NOT NULL REFERENCES %(schema)s.delivery(delivery_id)
);


-- result tables

CREATE TABLE IF NOT EXISTS %(schema)s.marker_result (
    result_id bigserial PRIMARY KEY NOT NULL,
    foi_id bigint NOT NULL REFERENCES %(schema)s.foi(foi_id),
    event_type_id smallint NOT NULL REFERENCES %(schema)s.event_type(event_type_id),
    run_id integer NOT NULL REFERENCES %(schema)s.marker_run(run_id),
    event_start timestamp without time zone,
    event_end timestamp without time zone,
    latest_obs_timestamp timestamp without time zone NOT NULL,
    probability real NOT NULL,
    segment_id bigint REFERENCES %(schema)s.segment(segment_id),
    predicted_class_id integer REFERENCES %(schema)s.superclass(class_id),
    declared_class_id integer REFERENCES %(schema)s.superclass(class_id),
    geom geometry(Polygon)
);


-- CREATE TABLE %(schema)s.classification_probability (
--     probability_id bigserial NOT NULL PRIMARY KEY,
--     result_id bigint NOT NULL REFERENCES %(schema)s.marker_result(result_id),
--     class_id integer NOT NULL REFERENCES %(schema)s.superclass(class_id),
--     probability real NOT NULL
-- );


-- validation tables

CREATE TABLE IF NOT EXISTS %(schema)s.validation_result_event (
    validation_result_id bigserial NOT NULL PRIMARY KEY,
    run_id integer NOT NULL REFERENCES %(schema)s.marker_run(run_id),
    marker_result_id bigint NOT NULL REFERENCES %(schema)s.marker_result(result_id),
    correct boolean,
    days_off integer NOT NULL,
    groundtruth_id uuid[] NOT NULL
);


CREATE TABLE IF NOT EXISTS %(schema)s.validation_result_groundtruth (
    validation_result_id bigserial NOT NULL PRIMARY KEY,
    run_id integer NOT NULL REFERENCES %(schema)s.marker_run(run_id),
    groundtruth_id uuid NOT NULL,
    correct boolean,
    days_off integer NOT NULL,
    marker_result_id bigint[] NOT NULL
);














CREATE INDEX IF NOT EXISTS image_date_idx ON %(schema)s.image USING btree (date);
CREATE INDEX IF NOT EXISTS image_rel_orbit_idx ON %(schema)s.image USING btree (relative_orbit);
CREATE INDEX IF NOT EXISTS image_sensor_idx ON %(schema)s.image USING btree (sensor_id);
CREATE INDEX IF NOT EXISTS image_time_idx ON %(schema)s.image USING btree ("time");

CREATE INDEX IF NOT EXISTS image_file_image_name_idx ON %(schema)s.image_file USING btree (image_name);

CREATE INDEX IF NOT EXISTS marker_event_end_idx ON %(schema)s.marker_result USING btree (event_end);
CREATE INDEX IF NOT EXISTS marker_event_start_idx ON %(schema)s.marker_result USING btree (event_start);
CREATE INDEX IF NOT EXISTS marker_ ON %(schema)s.marker_result USING btree (event_type_id);
CREATE INDEX IF NOT EXISTS marker_foi_id_idx ON %(schema)s.marker_result USING btree (foi_id);
CREATE INDEX IF NOT EXISTS marker_geom_idx ON %(schema)s.marker_result USING gist (geom);
CREATE INDEX IF NOT EXISTS marker_latest_obs_timestamp_idx ON %(schema)s.marker_result USING btree (latest_obs_timestamp);
CREATE INDEX IF NOT EXISTS marker_probability_idx ON %(schema)s.marker_result USING btree (probability);
CREATE INDEX IF NOT EXISTS marker_run_id_idx ON %(schema)s.marker_result USING btree (run_id);
CREATE INDEX IF NOT EXISTS marker_segment_id_idx ON %(schema)s.marker_result USING btree (segment_id);

CREATE INDEX IF NOT EXISTS observation_dt_added_idx ON %(schema)s.observation USING btree (dt_added);
CREATE INDEX IF NOT EXISTS observation_foi_id_idx ON %(schema)s.observation USING btree (foi_id);
CREATE INDEX IF NOT EXISTS observation_image_file_id_idx ON %(schema)s.observation USING btree (image_file_id);
CREATE INDEX IF NOT EXISTS observation_observation_type_id_idx ON %(schema)s.observation USING btree (observation_type_id);
CREATE INDEX IF NOT EXISTS observation_segment_id_idx ON %(schema)s.observation USING btree (segment_id);
CREATE INDEX IF NOT EXISTS observation_value_idx ON %(schema)s.observation USING btree (value);

CREATE INDEX IF NOT EXISTS segment_foi_id_idx ON %(schema)s.segment USING btree (foi_id);
CREATE INDEX IF NOT EXISTS segment_geom_idx ON %(schema)s.segment USING gist (geom);
CREATE INDEX IF NOT EXISTS segment_image_file_id_idx ON %(schema)s.segment USING btree (image_file_id);




