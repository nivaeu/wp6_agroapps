# -*- coding: utf-8 -*-
import json
import os
from datetime import datetime
from pathlib import Path
from typing import List, Tuple

import geopandas as gpd
import numpy as np
import psycopg2
from cap_framework.foi import FOIWriterDB, ParcelAsFOI
from osgeo import ogr
from psycopg2 import sql
from psycopg2.extensions import AsIs
from psycopg2.extras import execute_values
from pyarrow import ipc
from tqdm import tqdm

from niva_classifiers.dataclasses import Parcel
from niva_classifiers.zonalstats.objects.enums import PixelMode
from niva_classifiers.zonalstats.objects.stats_target import StatsTarget


class Database:
    def __init__(
        self,
        host: str,
        port: int,
        user: str,
        password: str,
        dbname: str,
        schema: str = "neo",
    ):
        """
        Prepare a Database instance. As part of initialization, it is checked
        that we can connect to the database using the given info.

        Parameters
        ----------
        host : str
            host name or IP.
        port : int
            TCP port, usually 5432.
        user : str
            Username, "postgres" usually is superuser.
        password : str
            Self-explanatory.
        dbname : str
            Database name.

        Returns
        -------
        None.

        """
        conn_parameters = {
            "host": host,
            "port": port,
            "user": user,
            "password": password,
            "dbname": dbname,
        }
        dbconfig_path = Path(__file__).parent / "dbconfig.json"

        with open(dbconfig_path, "w") as f:
            json.dump({**conn_parameters, "schema": schema}, f)

        self._verify_host_online(conn_parameters)
        self._connection_parameters = conn_parameters
        self.schema = schema

    @classmethod
    def create_from_db_config(cls, dbconfig_path=None):
        if not dbconfig_path:
            dbconfig_path = Path(__file__).parent / "dbconfig.json"
        else:
            dbconfig_path = Path(dbconfig_path)
        if not dbconfig_path.exists():
            raise ValueError(
                "No previous db config was found! Please first call 'database-prepare'"
            )
        with (dbconfig_path).open("r") as f:
            db_config = json.load(f)
        return cls(
            db_config["host"],
            db_config["port"],
            db_config["user"],
            db_config["password"],
            db_config["dbname"],
            db_config["schema"],
        )

    @staticmethod
    def _verify_host_online(connection_parameters):

        psycopg2.connect(**connection_parameters)

    def connection(self, **kwargs):
        """Returns a psycopg2 Connection instance"""
        return psycopg2.connect(**self._connection_parameters, **kwargs)

    def empty_database(self):
        with self.connection() as conn:
            cursor = conn.cursor()
            cursor.execute(
                f"""
DROP SCHEMA {self.schema} CASCADE;
CREATE SCHEMA {self.schema};
GRANT ALL ON SCHEMA {self.schema} TO %s;
""",
                (AsIs(self._connection_parameters["user"]),),
            )

    def setup(self):
        """Creates PostGIS extension, tables, functions in schema public, fills lookup tables"""
        print("Setting up database...")
        create_tables_str = open(
            os.path.join(os.path.dirname(os.path.abspath(__file__)), "tables_functions.sql")
        ).read()
        fill_tables_str = open(
            os.path.join(os.path.dirname(os.path.abspath(__file__)), "fill_lookup_tables.sql")
        ).read()

        with self.connection() as conn:
            cursor = conn.cursor()
            cursor.execute(create_tables_str, {"schema": AsIs(self.schema)})
            cursor.execute(fill_tables_str, {"schema": AsIs(self.schema)})
        print("Finished!")

    def insert_parcels(self, parcels: List[Parcel], batch_size: int = 10000):
        print("Inserting parcels...")
        base_query_cl = f"INSERT INTO {self.schema}.delivered_parcel (client_id) values %s"
        base_query = f"INSERT INTO {self.schema}.parcel (client_id, crop_code, catch_crop_code, geom) values %s"

        with self.connection() as conn:
            with conn.cursor() as curs:
                for idx in tqdm(
                    range(0, len(parcels), batch_size), total=len(parcels) // batch_size
                ):
                    parcel_batch = parcels[idx : idx + batch_size]

                    batch_flattened = []
                    batch_flattened_cl = []

                    for index, parcel in enumerate(parcel_batch):
                        geom = AsIs(
                            f"ST_Multi(ST_GeomFromText('{parcel.geom_wkt}', {parcel.epsg}))"
                        )
                        batch_flattened.append(
                            (
                                parcel.client_id,
                                parcel.crop_code,
                                parcel.catch_crop_code,
                                geom,
                            )
                        )
                        batch_flattened_cl.append((parcel.client_id,))

                    execute_values(curs, base_query_cl, batch_flattened_cl)
                    execute_values(curs, base_query, batch_flattened)

    def fois_as_statstargets(
        self,
        ref_gt: Tuple,
        foi_ids: List[int] = [],
        fois_within: ogr.Geometry = None,
        pixel_mode: PixelMode = PixelMode.WITHIN,
    ):
        filters = []
        args = {}
        if foi_ids:
            filters += ["foi_id = ANY (%(foi_ids)s)"]
            args["foi_ids"] = foi_ids

        if fois_within is not None:
            filters += ["ST_Within(geom, ST_GeomFromText(%(within_wkt)s, 32634))"]
            args["within_wkt"] = fois_within.ExportToWkt()

        if filters:
            filters = "WHERE " + " AND ".join(filters)
        else:
            filters = ""
        query = f"""SELECT foi_id, st_AsText(geom) as wkt
        FROM {self.schema}.foi {filters}"""
        print(query)
        print(args)
        with self.connection() as conn:
            cursor = conn.cursor(
                "serverside_fois_as_statstargets",
                cursor_factory=psycopg2.extras.NamedTupleCursor,
            )
            cursor.itersize = 5000
            cursor.execute(query, args)

            for foi in cursor:
                geom = ogr.CreateGeometryFromWkt(foi.wkt)
                stats_target = StatsTarget(str(foi.foi_id), geom, ref_gt, pixel_mode)
                yield stats_target

    def store_interpolated_signals(self, signals: List[Tuple]):

        query = f"""INSERT into {self.schema}.observation_interpolated (foi_id, sensor_id, date, observation_type_id, value)
        VALUES %s"""

        with self.connection() as conn:
            cursor = conn.cursor()

            execute_values(cursor, query, signals)

    def get_all_foi_ids(self):
        query = f"SELECT foi_id FROM {self.schema}.foi"
        with self.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(query)
                foi_ids = cur.fetchall()
        return foi_ids

    def get_foi_mapping(self):
        foi_mapping = {k: v[0] for k, v in enumerate(self.get_all_foi_ids())}
        assert len(foi_mapping) > 0, "No fois found in the database!?"
        return foi_mapping

    def get_client_id_foi_id_mapping(self):
        query = """SELECT p.client_id, f.foi_id FROM %(schema)s.foi f
        join %(schema)s.parcel_foi pf on pf.foi_id = f.foi_id
        join %(schema)s.parcel p on p.parcel_id = pf.parcel_id"""

        with self.connection() as conn:
            with conn.cursor() as cur:
                cur.execute(query, {"schema": AsIs(self.schema)})
                mapping = cur.fetchall()
        return dict(mapping)

    def _parse_column_name(self, column_name):
        if not column_name.startswith("XX"):
            return False
        year, month, day, sensor, method, band = column_name[3:].split("_")
        return (
            datetime(year=int(year), month=int(month), day=int(day)),
            sensor,
            method,
            band.lower(),
        )

    def _observation_type_id_mapping(self):
        # Bad and hardcoded but works?
        mapping = {
            "blue": 1,
            "b3": 2,  # green
            "b4": 3,  # red
            "b8": 4,  # NIR
            "b11": 5,  # SWIR1
            "b5": 6,  # Rededge1
            "b6": 7,  # Rededge2
            "b7": 8,  # Rededge3
            "b12": 9,  # SWIR2
            "amp_asc_vv": 10,
            "amp_asc_vh": 11,
            "amp_des_vv": 12,
            "amp_des_vh": 13,
            "coh_asc_vv": 14,
            "coh_asc_vh": 15,
            "coh_des_vv": 16,
            "coh_des_vh": 17,
            "brightness": 20,
            "NDVI": 21,
            "NDWI": 22,
            "NDTI": 23,
            "NDII": 24,
            "NDI7": 25,
        }
        return {str(k).lower(): v for k, v in mapping.items()}

    def import_pyarrow(self, pyarrow_path: str, client_id_field: str):
        print("Importing data from pyarrow file...")
        foi_mapping = self.get_client_id_foi_id_mapping()
        print(list(foi_mapping.items())[:4])
        obs_id_mapping = self._observation_type_id_mapping()

        with ipc.open_file(pyarrow_path) as reader:
            df = reader.read_pandas()

        interpolated_observations = []
        for idx, row in tqdm(df.iterrows(), total=len(df)):
            try:
                foi_id = foi_mapping[str(int(row[client_id_field]))]
            except Exception:
                print("WARNING: no matching client_id found")
                continue
            for k, v in dict(row).items():
                if parsed := self._parse_column_name(k):
                    date, sensor, method, band = parsed
                else:
                    continue
                if method == "dev":
                    continue

                interpolated_observations.append(
                    (
                        foi_id,
                        2,  # sensor_id
                        date,  # date
                        obs_id_mapping[band],  # observation_type_id
                        v,  # value
                    )
                )

        self.store_interpolated_signals(interpolated_observations)
        print("Finished!")
        return len(df)

    def import_parcels(self, shp_file, client_id_field="EDA_ID", crop_code_field="GROUP_CODE"):
        shpfile = gpd.read_file(shp_file)
        epsg = int(shpfile.crs.srs.split(":")[-1])
        vals = [
            Parcel(
                parcel_id=-1,
                client_id=row[client_id_field],
                geom_wkt=row.geometry.wkt,
                epsg=epsg,
                crop_code=row[crop_code_field],
                catch_crop_code=None,
            )
            for _, row in shpfile.iterrows()
        ]
        self.insert_parcels(vals)

        with self.connection() as conn:
            with conn.cursor() as cursor:
                cursor.execute(
                    "select parcel_id from %(schema)s.parcel", {"schema": AsIs(self.schema)}
                )
                parcel_ids = [i for i, in cursor.fetchall()]
            # example workflow creating the simplest FOIs: directly from parcel

            foi_creator = ParcelAsFOI(conn, {"schema": self.schema})
            foi_creator.register(schema=self.schema)

            writer = FOIWriterDB(
                conn, foi_creator, batch_size=100, target_epsg=epsg, schema=self.schema
            )

            foi_creator.create_FOIs(parcel_ids, foi_writers=[writer])

    def get_foi_ids_client_ids_crop_codes(self, query: str):
        """Get foi ids, client_ids and crop codes given a query

        Args:
            query (str): sql query

        Returns:
            numpy arrays: foi_ids, client_ids, crop_codes
        """
        with self.connection() as conn:
            with conn.cursor() as curr:
                curr.execute(sql.SQL(query).format(schema=sql.Identifier(self.schema)))
                result = curr.fetchall()
                foi_ids_crop_codes = np.array(result)
                foi_ids = foi_ids_crop_codes[:, 0]
                client_ids = foi_ids_crop_codes[:, 1]
                crop_codes = foi_ids_crop_codes[:, 2]

                return foi_ids, client_ids, crop_codes

    def get_CL2_fois(self):
        """Get foi ids, client_ids and crop codes for CL2.

        Returns:
            numpy arrays: foi_ids, client_ids, crop_codes
        """
        sql_foi_ids_crop_codes = """
            select f.foi_id, p.client_id, crop_code from {schema}.foi f
            left join {schema}.parcel_foi pf on f.foi_id = pf.foi_id
            left join {schema}.parcel p on p.parcel_id = pf.parcel_id
            where num_pixels_within_inner_buffer = 0
            and f.foi_id in (
                select foi_id from {schema}.observation_interpolated where sensor_id = 5
            )
            order by f.foi_id
        """

        return self.get_foi_ids_client_ids_crop_codes(sql_foi_ids_crop_codes)

    def get_CL3_fois(self):
        """Get foi ids, client_ids and binary crop codes for CL3.

        Returns:
            numpy arrays: foi_ids, client_ids, crop_codes
        """
        sql_foi_ids_crop_codes = """
            select f.foi_id, p.client_id, (case when crop_code = 7 then 1 else 0 end) from {schema}.foi f
            left join {schema}.parcel_foi pf on f.foi_id = pf.foi_id
            left join {schema}.parcel p on p.parcel_id = pf.parcel_id
            where num_pixels_within_inner_buffer > 0
            and f.foi_id in (
                select foi_id from {schema}.observation_interpolated where sensor_id in (1, 2)
            )
            order by f.foi_id
        """

        return self.get_foi_ids_client_ids_crop_codes(sql_foi_ids_crop_codes)
