import argparse
from pathlib import Path

import pandas as pd
from sklearn.model_selection import StratifiedKFold

from niva_classifiers.CL2.model_things import get_params, test, train
from niva_classifiers.database.database import Database


def main(model, params):
    other_params = params.pop("other_params")
    hyper_params = params.pop("hyper_params", {})
    output_path: Path = Path(other_params["working_dir"]) / other_params["output_filename"]
    output_path.unlink(missing_ok=True)  # delete if exists
    model_dir: Path = Path(other_params["working_dir"]) / "models"
    model_dir.mkdir(parents=True, exist_ok=True)
    model_type = model

    db = Database.create_from_db_config()
    foi_ids, client_ids, crop_codes = db.get_CL2_fois()

    with db.connection() as conn:
        cursor = conn.cursor()

        skf = StratifiedKFold(n_splits=other_params["n_splits"])
        for fold, (train_index, val_index) in enumerate(skf.split(foi_ids, crop_codes)):
            print(fold, "TRAIN:", len(train_index), "VALIDATION:", len(val_index))
            X_skf_train, X_skf_val = foi_ids[train_index], foi_ids[val_index]
            _, y_skf_val = crop_codes[train_index], crop_codes[val_index]
            _, client_ids_skf_val = (
                client_ids[train_index],
                client_ids[val_index],
            )

            fold_working_dir = Path(other_params["working_dir"]) / f"fold{fold}"
            fold_working_dir.mkdir(exist_ok=True)
            model_name = f"{model_type}_fold{fold}"

            train(
                cursor,
                X_skf_train,
                fold_working_dir,
                model_dir,
                model_name,
                model_type,
                params,
                db.schema,
                hyper_parameters=hyper_params,
            )
            predictions = test(
                cursor, X_skf_val, fold_working_dir, model_dir, model_name, model_type, db.schema
            )

            X_skf_val = [int(foi_id) for foi_id in X_skf_val]
            for foi_id, y_declared, client_id in zip(X_skf_val, y_skf_val, client_ids_skf_val):
                predictions[foi_id]["CT_decl"] = y_declared
                predictions[foi_id]["EDA_ID"] = client_id

            df = pd.DataFrame(predictions).T
            df.to_csv(
                output_path,
                index=False,
                mode="a",
                header=not output_path.is_file(),
            )


def predict():
    parser = argparse.ArgumentParser("Make predictions for CL2.")
    parser.add_argument(
        "--model",
        type=str,
        required=True,
        help="model to use: random_forest or xgboost",
    )
    parser.add_argument("--output_filename", "-o", required=True)
    parser.add_argument("--config_file", "-c", required=True)
    parser.add_argument("--working_dir", "-w", required=True)
    args = parser.parse_args()
    assert args.model in ["random_forest", "xgboost"]

    predict_params = get_params(args.config_file, args.working_dir, args.output_filename)

    main(args.model, predict_params)
