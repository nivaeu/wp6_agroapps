import argparse
import datetime

import numpy as np
from osgeo import gdal
from zonalstats.objects.enums import BandName

from niva_classifiers.database import Database

# from niva_classifiers.dataclasses import Parcel
from niva_classifiers.utils import (  # convert_shp_to_parcels,
    bbox_to_polygon,
    bounding_box_from_gdal_dataset,
    generate_planetfusion_selection,
    list_planetfusion_images_by_tile,
)


def PF_zonal_stats():

    parser = argparse.ArgumentParser("Run rule-based markers on fallow fields")
    parser.add_argument("--planetfusion_dir", required=True)
    parser.add_argument("--start_date", required=True)
    parser.add_argument("--end_date", required=True)
    parser.add_argument("--interval_days", required=True)
    args = parser.parse_args()

    db = Database.create_from_db_config()
    reference_gt = (528000.0, 3.0, 0.0, 4392000.0, 0.0, -3.0)
    start_date = datetime.datetime.strptime(args.start_date, "%Y-%m-%d")
    end_date = datetime.datetime.strptime(args.end_date, "%Y-%m-%d")

    sensor_id = 5
    bands = {BandName.blue: 1, BandName.green: 2, BandName.red: 3, BandName.nir: 4}

    bands_obs_type_ids = {
        BandName.blue: 1,
        BandName.green: 2,
        BandName.red: 3,
        BandName.nir: 4,
    }

    planetfusion_selection = generate_planetfusion_selection(
        start_date, end_date, int(args.interval_days)
    )

    # then do zonal stats
    band_names = list(bands.keys())
    band_obs_type_ids = [bands_obs_type_ids[band_name] for band_name in band_names]

    image_dic = list_planetfusion_images_by_tile(args.planetfusion_dir)
    for tile, files in image_dic.items():
        print(tile)

        example_filepath = files[planetfusion_selection[0]]
        ds = gdal.Open(example_filepath)
        bbox = bounding_box_from_gdal_dataset(ds)
        ds = None

        bbox_geom = bbox_to_polygon(bbox)

        # retrieve as stats targets
        print("   preparing stats_targets...")
        stats_targets = list(db.fois_as_statstargets(ref_gt=reference_gt, fois_within=bbox_geom))
        print("done")

        print("   retrieving arrays...")
        for filename in planetfusion_selection:
            print("      " + filename)
            filepath = files[filename]

            ds = gdal.Open(filepath)
            raster_gt = ds.GetGeoTransform()

            signals = []
            for stats_target in stats_targets:
                stats_target.retrieve_arrays(filename, ds, raster_gt, bands)

                foi_id = stats_target.geometry_id
                retrieved_array = stats_target.stats[filename]
                date = filename[:-4]
                for obs_type_id, arr in zip(
                    band_obs_type_ids, retrieved_array.get_value_for_bands(band_names)
                ):
                    band_mean = np.mean(arr[stats_target.mask])
                    if band_mean is np.ma.masked:
                        continue
                    signals += [(foi_id, sensor_id, date, obs_type_id, band_mean)]

            print("   storing signals...")
            # print (signals)
            db.store_interpolated_signals(signals)
        print("   done")
