import argparse
import datetime
import os

import psycopg2
from cap_framework.algorithm import AlgorithmRun
from cap_framework.algorithm.settings import AlgorithmSettingsCollection
from cap_framework.algorithm.writer import (  # AlgorithmPlotterLocal,; AlgorithmWriterDB,
    AlgorithmWriterMemory,
)

from niva_classifiers.CL3.utils import (
    get_marker_NDTIPloughing_settings,
    get_marker_NDVIAbsence_settings,
    get_marker_NDVIPresence_settings,
    write_events_to_CSV,
)
from niva_classifiers.database import Database


def run_fallow_markers():

    parser = argparse.ArgumentParser("Run rule-based markers on fallow fields")
    parser.add_argument("--output_dir", required=True)
    parser.add_argument("--start_date", required=True)
    parser.add_argument("--end_date", required=True)
    parser.add_argument("--fallow_crop_code", default=7)
    args = parser.parse_args()

    db = Database.create_from_db_config()
    conn = db.connection()
    cursor = conn.cursor()

    time_range = (
        datetime.datetime.strptime(args.start_date, "%Y-%m-%d"),
        datetime.datetime.strptime(args.end_date, "%Y-%m-%d"),
    )
    output_csvs = [
        os.path.join(args.output_dir, "VegetationPresence.csv"),
        os.path.join(args.output_dir, "VegetationAbsence.csv"),
        os.path.join(args.output_dir, "Tillage.csv"),
    ]

    cursor.execute(
        """select f.foi_id
    from %(schema)s.foi f
    join %(schema)s.parcel_foi pf on pf.foi_id = f.foi_id
    join %(schema)s.parcel p on p.parcel_id = pf.parcel_id
    where p.crop_code = %(fallow_code)s and f.num_pixels_within_inner_buffer > 0
    """,
        {"schema": psycopg2.extensions.AsIs(db.schema), "fallow_code": args.fallow_crop_code},
    )
    fois = [(foi_id, "Grass", None) for (foi_id,) in cursor.fetchall()]

    cursor.execute(
        """select pf.foi_id, p.client_id
from %(schema)s.parcel_foi pf
join %(schema)s.parcel p on p.parcel_id = pf.parcel_id
where pf.foi_id = ANY (%(foi_ids)s)""",
        {
            "schema": psycopg2.extensions.AsIs(db.schema),
            "foi_ids": [foi_id for foi_id, _, __ in fois],
        },
    )
    foi_client_lut = dict(cursor.fetchall())

    marker_settings_pres = get_marker_NDVIPresence_settings(db.schema, time_range, cursor)
    marker_settings_abs = get_marker_NDVIAbsence_settings(db.schema, time_range, cursor)
    marker_settings_tillage = get_marker_NDTIPloughing_settings(db.schema, time_range, cursor)

    for marker_settings, output_csv in zip(
        [marker_settings_pres, marker_settings_abs, marker_settings_tillage], output_csvs
    ):

        algorithm_settings_collection = AlgorithmSettingsCollection(marker_settings)
        # writer = AlgorithmPlotterLocal(schema, conn, time_range, algorithm_settings_collection)
        writer3 = AlgorithmWriterMemory()

        algorithm_run = AlgorithmRun(
            conn, [writer3], fois, algorithm_settings_collection, time_range, db.schema
        )

        algorithm_run.run(metric_based_on_pixel_count=False)
        detections = writer3.get_stored_events()
        client_detections = {
            foi_client_lut[foi_id]: events for foi_id, events in detections.items()
        }
        write_events_to_CSV(output_csv, client_detections, "NewID")
