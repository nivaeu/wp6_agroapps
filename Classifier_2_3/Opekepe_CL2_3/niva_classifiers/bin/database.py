import argparse

from niva_classifiers.database import Database


def prepare_db():
    """
    Creates internal cfg file with the db connection credentials
    and initialises the db
    """
    parser = argparse.ArgumentParser("Prepare the database")
    parser.add_argument("--host", required=True)
    parser.add_argument("--port", required=True)
    parser.add_argument("--user", required=True)
    parser.add_argument("--password", required=True)
    parser.add_argument("--name", required=True)
    parser.add_argument("--schema", default="neo")
    args = parser.parse_args()

    db = Database(
        host=args.host,
        port=args.port,
        user=args.user,
        password=args.password,
        dbname=args.name,
        schema=args.schema,
    )
    db.setup()


def drop_database():
    db = Database.create_from_db_config()
    db.empty_database()


def import_pyarrow():
    parser = argparse.ArgumentParser("Import an .ipc file to the database")
    parser.add_argument("--file", "-f")
    parser.add_argument("--clientid_field", "-i", default="NewID")
    args = parser.parse_args()

    assert args.file.endswith(".ipc"), "Importing from pyarrow expects a .ipc file!"

    db = Database.create_from_db_config()
    db.import_pyarrow(args.file, args.clientid_field)


def import_parcels():
    parser = argparse.ArgumentParser("Import an .ipc file to the database")
    parser.add_argument("--file", "-f")
    parser.add_argument("--cropcode_field", "-c", default="GROUP_CODE")
    parser.add_argument("--clientid_field", "-i", default="EDA_ID")
    args = parser.parse_args()

    assert args.file.endswith(".shp"), "Importing parcels expects a .shp file!"

    db = Database.create_from_db_config()
    db.import_parcels(
        args.file, client_id_field=args.clientid_field, crop_code_field=args.cropcode_field
    )
