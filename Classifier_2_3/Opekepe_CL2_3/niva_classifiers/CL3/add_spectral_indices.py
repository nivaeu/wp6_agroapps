# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 10:25:23 2022

@author: Cornelis
"""

from typing import List

import psycopg2


def compute_add_Sentinel2_NDII(cursor: psycopg2.extensions.cursor, foi_ids: List[int], schema: str):
    query = f"""with sensor (id) as
(select sensor_id from {schema}.sensor where sensor_name = 'Sentinel-2'),
swir1 (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'SWIR1'),
nir (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'NIR'),
ndii (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'NDII')

insert into {schema}.observation_interpolated (foi_id, sensor_id, observation_type_id, date, value)
select
s.foi_id,
(select id from sensor),
(select id from ndii),
s.date,
(n.value - s.value) / NULLIF(n.value + s.value, 0) as ndii
from
(select foi_id, date, value from {schema}.observation_interpolated
 where sensor_id = (select id from sensor)
 and observation_type_id = (select id from swir1)) as s
left join
(select foi_id, date, value from {schema}.observation_interpolated
 where sensor_id = (select id from sensor)
 and observation_type_id = (select id from nir)) as n
on (n.foi_id = s.foi_id and n.date = s.date)
where s.foi_id = ANY (%s);
"""
    cursor.execute(query, (foi_ids,))


def compute_add_Sentinel2_NDTI(cursor: psycopg2.extensions.cursor, foi_ids: List[int], schema: str):
    query = f"""with sensor (id) as
(select sensor_id from {schema}.sensor where sensor_name = 'Sentinel-2'),
swir1 (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'SWIR1'),
swir2 (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'SWIR2'),
ndti (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'NDTI')

insert into {schema}.observation_interpolated (foi_id, sensor_id, observation_type_id, date, value)
select
s1.foi_id,
(select id from sensor),
(select id from ndti),
s1.date,
(s1.value - s2.value) / NULLIF(s1.value + s2.value, 0) as ndti
from
(select foi_id, date, value from {schema}.observation_interpolated
 where sensor_id = (select id from sensor)
 and observation_type_id = (select id from swir1)) as s1
left join
(select foi_id, date, value from {schema}.observation_interpolated
 where sensor_id = (select id from sensor)
 and observation_type_id = (select id from swir2)) as s2
on (s1.foi_id = s2.foi_id and s1.date = s2.date)
where s1.foi_id = ANY (%s)
and s1.value is not null and s2.value is not null and (s1.value + s2.value) != 0;
"""
    cursor.execute(query, (foi_ids,))
