from cap_framework import signal
from cap_framework.algorithm import marker
from cap_framework.algorithm.settings import MarkerSettings


def get_filters_S2_signal(schema, time_range, signal_type):
    filters = {
        "signal_type": signal_type,
        "metric": "mean",
        "schema": schema,
        "sensors": "Sentinel-2",
        "time_range": time_range,
        "object_id_column": "foi_id",
        "stats_method": "original_geometry",
    }
    return filters


def get_marker_NDVIPresence_settings(schema, time_range, cursor):
    filters = get_filters_S2_signal(schema, time_range, "NDVI")
    s = signal.InterpolatedTimeseriesSignal(cursor=cursor, filters=filters)
    m = marker.MarkerNDVIVegetationPresence()
    return MarkerSettings(m, s)


def get_marker_NDVIAbsence_settings(schema, time_range, cursor):
    filters = get_filters_S2_signal(schema, time_range, "NDVI")
    s = signal.InterpolatedTimeseriesSignal(cursor=cursor, filters=filters)
    m = marker.MarkerNDVIVegetationAbsence()
    return MarkerSettings(m, s)


def get_marker_NDTIPloughing_settings(schema, time_range, cursor):
    filters = get_filters_S2_signal(schema, time_range, "NDTI")
    s = signal.InterpolatedTimeseriesSignal(cursor=cursor, filters=filters)
    m = marker.MarkerNDTIPloughing()
    return MarkerSettings(m, s)


def write_events_to_CSV(path, event_dict, id_column_name, separator=","):
    with open(path, "w") as F:
        header = separator.join([id_column_name, "start", "end", "confidence"])
        F.write(header + "\n")
        for object_id, events in event_dict.items():
            if not events:
                F.write(separator.join([str(object_id), "", "", ""]) + "\n")
            for start, end, confidence in events:
                F.write(
                    separator.join([str(object_id), str(start), str(end), str(confidence)]) + "\n"
                )
