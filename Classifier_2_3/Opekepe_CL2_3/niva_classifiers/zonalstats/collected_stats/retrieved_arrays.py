from typing import Dict, Union

import numpy as np

from niva_classifiers.zonalstats.collected_stats.stats_collection_result_base import (
    StatsCollectionResultBase,
)
from niva_classifiers.zonalstats.objects.enums import BandName


class RetrievedArrays(StatsCollectionResultBase):
    __retrieved: Dict[BandName, np.ndarray]

    def __init__(self):
        super(RetrievedArrays, self).__init__()
        self.__retrieved = {}

    def append_band(self, band_name: BandName, array: Union[np.ndarray, None]):
        self.__retrieved[band_name] = array

    @property
    def value(self):
        return list(self.__retrieved.values())

    @property
    def bands(self):
        return list(self.__retrieved.keys())

    def get_value_for_bands(self, bands):
        return [self.__retrieved[band] for band in bands]
