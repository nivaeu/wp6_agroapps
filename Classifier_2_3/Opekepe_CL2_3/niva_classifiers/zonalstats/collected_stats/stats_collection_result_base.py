from abc import ABC, abstractmethod
from typing import List, Union

import numpy as np


class StatsCollectionResultBase(ABC):
    """Idea is to have a range of implemetnations based on this base class"""

    __applied_masks: List[str]

    def __init__(self):
        self.__applied_masks = []

    def append_applied_mask(self, mask: str):
        self.__applied_masks.append(mask)

    @property
    @abstractmethod
    def value(self) -> List[Union[float, np.ndarray]]:  # pragma: no cover
        raise NotImplementedError

    @property
    def applied_masks(self) -> List[str]:
        return self.__applied_masks

    @property
    @abstractmethod
    def bands(self) -> List[str]:  # pragma: no cover
        raise NotImplementedError
