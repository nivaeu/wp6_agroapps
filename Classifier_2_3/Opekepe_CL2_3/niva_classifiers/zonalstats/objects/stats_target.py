from typing import Dict, Tuple

import numpy as np
from osgeo import gdal, ogr

from niva_classifiers.zonalstats.collected_stats.retrieved_arrays import RetrievedArrays
from niva_classifiers.zonalstats.collected_stats.stats_collection_result_base import (
    StatsCollectionResultBase,
)

from .enums import BandName, PixelMode


class StatsTarget:
    """Encapsulates a target geometry + collected stats"""

    target_id: str
    mask: np.ndarray
    gt: Tuple
    stats: Dict[str, StatsCollectionResultBase]

    def __init__(
        self,
        geometry_id: str,
        target_geom: ogr.Geometry,
        ref_gt: Tuple,
        pixel_mode: PixelMode = PixelMode.CENTROID,
    ):
        self.stats = {}
        self.geometry_id = geometry_id

        def ogr_create_single_feature_memory_layer(geometry):
            ds: ogr.DataSource = ogr.GetDriverByName("MEMORY").CreateDataSource("memData")
            tlyr: ogr.Layer = ds.CreateLayer(
                "templayer", geometry.GetSpatialReference(), geometry.GetGeometryType()
            )
            tdefn: ogr.FeatureDefn = tlyr.GetLayerDefn()
            tfeat = ogr.Feature(tdefn)
            tfeat.SetGeometry(geometry)
            tlyr.CreateFeature(tfeat)
            tlyr.SyncToDisk()
            ds.SyncToDisk()
            return ds, tlyr

        xmin, xmax, ymin, ymax = target_geom.GetEnvelope()

        # rounding down and up to ensure all potential pixels are included
        px = int((xmin - ref_gt[0]) / ref_gt[1])
        py = int((ymax - ref_gt[3]) / ref_gt[5])
        nx = int((xmax - ref_gt[0]) / ref_gt[1] - px) + 1
        ny = int((ymin - ref_gt[3]) / ref_gt[5] - py) + 1

        # prepare result geotransform
        self.gt = (
            ref_gt[0] + px * ref_gt[1],
            ref_gt[1],
            0,
            ref_gt[3] + py * ref_gt[5],
            0,
            ref_gt[5],
        )

        # create a temporary raster layer
        target_ds = gdal.GetDriverByName("MEM").Create("", nx, ny, 1, gdal.GDT_Byte)
        # set the mask to entirely opaque (1s):
        target_ds.GetRasterBand(1).Fill(1)
        target_ds.SetGeoTransform(self.gt)

        # create a temporary OGR layer with one feature
        temp_source, temp_geom_layer = ogr_create_single_feature_memory_layer(target_geom)

        # burn 0s into the mask:
        if pixel_mode == PixelMode.TOUCHING:
            options = ["ALL_TOUCHED=TRUE"]
        else:
            options = []
        gdal.RasterizeLayer(target_ds, [1], temp_geom_layer, burn_values=[0], options=options)
        temp_source.Destroy()

        # if WITHIN, mask (burn 1s) again any pixels touching the border by rasterizing the perimeter:
        if pixel_mode == PixelMode.WITHIN:
            temp_source, temp_geomboundary_layer = ogr_create_single_feature_memory_layer(
                target_geom.GetBoundary()
            )
            gdal.RasterizeLayer(
                target_ds,
                [1],
                temp_geomboundary_layer,
                burn_values=[1],
                options=["ALL_TOUCHED=TRUE"],
            )

        self.mask = np.array(target_ds.GetRasterBand(1).ReadAsArray(), dtype=bool)

    def retrieve_arrays(
        self, image_id: str, ds: gdal.Dataset, raster_gt, bands: Dict[BandName, int]
    ):
        def make_window_reader(
            ds: gdal.Dataset,
            raster_gt: Tuple[float],
            window_gt: Tuple[float],
            win_width: int,
            win_height: int,
            mask_nodata: bool = False,
        ):
            """
            Makes a reader that reads pixel values from a raster band for a given window.
            Does padding (left, right, top, bottom) as needed. Padding is done using the
            provided fill value or the band's advertised NoData value if no fill value is
            supplied. Optionally, all padded pixel values are masked and a masked array
            is returned.

            Assumes the geotransforms are from aligned data, same resolution,
            and <mask_gt> is within <ref_gt>

            Parameters:
                ds: source raster dataset
                band: raster band (index) to read from
                raster_gt: GDAL Geotransform (tuple) for raster
                window_gt: GDAL Geotransform (tuple) for window
                win_width: size in x of requested window
                win_height: size in y of requested window
                mask_nodata: whether or not to apply a mask

            Returns:
                Reader that fetches pixel values as a 2D numpy array (width x height)
                from the image or None when the requested window is completely outside of
                the raster dataset bounds. The created reader returns as np.MaskedArray if
                mask_nodata is passed as True.
            """

            win_offset_x = int(round((window_gt[0] - raster_gt[0]) / raster_gt[1]))
            win_offset_y = int(round((window_gt[3] - raster_gt[3]) / raster_gt[5]))
            img_width: int = ds.RasterXSize
            img_height: int = ds.RasterYSize

            # resolve overshoot in offset:
            if win_offset_x > img_width or win_offset_y > img_height:
                return None

            # resolve undershoot in x:
            if win_offset_x < 0:
                array_offset_x = win_offset_x * (-1)
                read_x = win_width - array_offset_x
                win_offset_x = 0
            else:
                array_offset_x = 0
                read_x = win_width

            # resolve undershood in y:
            if win_offset_y < 0:
                array_offset_y = win_offset_y * (-1)
                read_y = win_height - array_offset_y
                win_offset_y = 0
            else:
                array_offset_y = 0
                read_y = win_height

            # resolve overshoot in x:
            if (win_offset_x + read_x) > img_width:
                read_x = img_width - win_offset_x

            # resolve overshoot in y:
            if (win_offset_y + read_y) > img_height:
                read_y = img_height - win_offset_y

            if read_x != win_width or read_y != win_height:

                dt_gdal_to_np = {
                    1: np.int8,
                    2: np.uint16,
                    3: np.int16,
                    4: np.uint32,
                    5: np.int32,
                    6: np.float32,
                    7: np.float64,
                    10: np.complex64,
                    11: np.complex128,
                }

                def padding_reader(band_idx: int, use_as_nodata=None):  # TODO: burn_nodata_value

                    if band_idx > ds.RasterCount:
                        # TODO: add message to output
                        return None
                    band: gdal.Band = ds.GetRasterBand(band_idx)
                    band_nodata = band.GetNoDataValue()
                    if use_as_nodata is None:
                        use_as_nodata = band_nodata
                    else:
                        raise NotImplementedError()  # pragma: no cover

                    data = np.full(
                        (win_height, win_width),
                        0 if use_as_nodata is None else use_as_nodata,
                        dt_gdal_to_np[band.DataType],
                    )
                    retrieved_array = band.ReadAsArray(win_offset_x, win_offset_y, read_x, read_y)
                    if mask_nodata:
                        mask = np.full((win_height, win_width), 1, dtype=bool)
                        mask[
                            array_offset_y : (array_offset_y + read_y),
                            array_offset_x : (array_offset_x + read_x),
                        ] = (
                            (retrieved_array == use_as_nodata) if use_as_nodata is not None else 0
                        )
                    else:
                        raise NotImplementedError()  # pragma: no cover
                    # TODO: if burn_nodata_value is set =>
                    #        then manipulate retrieved_array for elements == use_as_nodata := burn_nodata_value
                    data[
                        array_offset_y : (array_offset_y + read_y),
                        array_offset_x : (array_offset_x + read_x),
                    ] = retrieved_array
                    if mask_nodata:
                        return np.ma.masked_array(data, mask=mask)
                    else:
                        raise NotImplementedError()  # pragma: no cover
                        # return data

                return padding_reader

            else:

                def simple_reader(band_idx: int, use_as_nodata=None):  # TODO: burn_nodata_value

                    if band_idx > ds.RasterCount:
                        # TODO: add message to output
                        return None
                    band: gdal.Band = ds.GetRasterBand(band_idx)
                    band_nodata = band.GetNoDataValue()
                    if use_as_nodata is None:
                        use_as_nodata = band_nodata
                    else:
                        raise NotImplementedError()  # pragma: no cover

                    retrieved_array = band.ReadAsArray(
                        win_offset_x, win_offset_y, win_width, win_height
                    )
                    # TODO: if burn_nodata_value is set =>
                    # then manipulate retrieved_array for elements == use_as_nodata := burn_nodata_value
                    if mask_nodata and use_as_nodata is not None:
                        raise NotImplementedError()  # pragma: no cover
                        # return np.ma.masked_where(retrieved_array == use_as_nodata, retrieved_array)
                    else:
                        return retrieved_array

                return simple_reader

        stats_result = RetrievedArrays()
        height, width = self.mask.shape
        window_reader = make_window_reader(ds, raster_gt, self.gt, width, height, mask_nodata=True)
        if window_reader is None:
            return
        for band_name, band_idx in bands.items():
            data = window_reader(band_idx)
            stats_result.append_band(
                band_name,
                np.ma.masked_array(data, np.ma.getmask(data) | self.mask)
                if data is not None
                else None,
            )
        self.stats[image_id] = stats_result
