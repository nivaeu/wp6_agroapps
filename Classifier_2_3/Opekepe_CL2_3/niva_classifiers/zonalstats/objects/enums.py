from enum import Enum


class PixelMode(Enum):
    CENTROID = 0
    WITHIN = 1
    TOUCHING = 2


class BandName(Enum):
    # TODO: What sensors have which bands? Also, how do we add support for virtual bands?
    # Sentinel-2
    nir = "nir"
    NEOmask_Cloud = "NEOmask_Cloud"
    NEOmask_Water = "NEOmask_Water"
    swir1 = "swir1"
    rededge3 = "rededge3"
    swir2 = "swir2"
    nir_narrow = "nir_narrow"
    blue = "blue"
    NEOmask_Cirrus = "NEOmask_Cirrus"
    rededge2 = "rededge2"
    green = "green"
    rededge1 = "rededge1"
    NEOmask_Snow = "NEOmask_Snow"
    NEOmask_Cloudshadow = "NEOmask_Cloudshadow"
    red = "red"

    # PlanetFusion
    # blue = "blue"
    # green = "green"
    # red = "red"
    # nir = "nir"
    synthetic_perc = "synthetic_perc"
    d_closest_scene = "d_closest_scene"
    cloud_shadow = "cloud_shadow"
    traceability = "traceability"
    No_L8S2_scenes = "No_L8S2_scenes"
    uncertainty_blue = "uncertainty_blue"
    uncertainty_green = "uncertainty_green"
    uncertainty_red = "uncertainty_red"
    uncertainty_nir = "uncertainty_nir"
