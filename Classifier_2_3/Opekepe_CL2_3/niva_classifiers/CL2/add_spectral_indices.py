# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 10:25:23 2022

@author: Cornelis
"""

from typing import List

import psycopg2


def compute_add_PlanetFusion_NDVI(
    cursor: psycopg2.extensions.cursor, foi_ids: List[int], schema: str
):
    query = f"""with sensor (id) as
(select sensor_id from {schema}.sensor where sensor_name = 'PlanetFusion'),
red (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'red'),
nir (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'NIR'),
ndvi (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'NDVI')

insert into {schema}.observation_interpolated (foi_id, sensor_id, observation_type_id, date, value)
select
r.foi_id,
(select id from sensor),
(select id from ndvi),
r.date,
(n.value - r.value) / (r.value + n.value) as ndvi
from
(select foi_id, date, value from {schema}.observation_interpolated
 where sensor_id = (select id from sensor)
 and observation_type_id = (select id from red)) as r
left join
(select foi_id, date, value from {schema}.observation_interpolated
 where sensor_id = (select id from sensor)
 and observation_type_id = (select id from nir)) as n
on (n.foi_id = r.foi_id and n.date = r.date)
where r.foi_id = ANY (%s);
"""
    cursor.execute(query, (foi_ids,))


def compute_add_PlanetFusion_brightness(
    cursor: psycopg2.extensions.cursor, foi_ids: List[int], schema: str
):
    query = f"""with sensor (id) as
(select sensor_id from {schema}.sensor where sensor_name = 'PlanetFusion'),
blue (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'blue'),
green (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'green'),
red (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'red'),
brightness (id) as
(select observation_type_id from {schema}.observation_type
 where stats_method = 'original_geometry' and indicator = 'brightness')

insert into {schema}.observation_interpolated (foi_id, sensor_id, observation_type_id, date, value)
select r.foi_id, (select id from sensor), (select id from brightness),
r.date, r.value + b.value + g.value as brightness
from
(select foi_id, date, value from {schema}.observation_interpolated
 where sensor_id = (select id from sensor)
 and observation_type_id = (select id from red)) as r
left join
(select foi_id, date, value from {schema}.observation_interpolated
 where sensor_id = (select id from sensor)
 and observation_type_id = (select id from green)) as g
on (g.foi_id = r.foi_id and g.date = r.date)
left join
(select foi_id, date, value from {schema}.observation_interpolated
 where sensor_id = (select id from sensor)
 and observation_type_id = (select id from blue)) as b
on (b.foi_id = r.foi_id and b.date = r.date)
where r.foi_id = ANY (%s);
"""
    cursor.execute(query, (foi_ids,))
