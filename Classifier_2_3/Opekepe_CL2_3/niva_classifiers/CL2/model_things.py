import datetime
import json
from pathlib import Path

import numpy as np
from cap_framework.algorithm.classification import CropClassifier, XGBoostCropClassifier
from sklearn.metrics import (
    accuracy_score,
    confusion_matrix,
    fbeta_score,
    precision_score,
    recall_score,
)


def get_params(config_path, working_dir, output_filename):
    with Path(config_path).open("r") as f:
        params = json.load(f)
    params["date_range"][0] = datetime.datetime(
        year=params["date_range"][0][0],
        month=params["date_range"][0][1],
        day=params["date_range"][0][2],
    )
    params["date_range"][1] = datetime.datetime(
        year=params["date_range"][1][0],
        month=params["date_range"][1][1],
        day=params["date_range"][1][2],
    )
    params["other_params"]["working_dir"] = working_dir
    params["other_params"]["output_filename"] = output_filename

    return params


def train(
    cursor,
    foi_ids_array,
    working_dir,
    model_dir,
    model_name,
    model_type,
    parameters,
    schema="public",
    hyper_parameters={},
):
    foi_ids = foi_ids_array.tolist()

    if model_type == "random_forest":
        classifier = CropClassifier(schema=schema)
    elif model_type == "xgboost":
        classifier = XGBoostCropClassifier(hyper_parameters, schema=schema)
    classifier.train_from_database(cursor, foi_ids, working_dir, parameters, verbose=True)
    classifier.save_model(model_name, model_dir)


def test(cursor, foi_ids_array, working_dir, model_dir, model_name, model_type, schema="public"):
    foi_ids = foi_ids_array.tolist()
    if model_type == "random_forest":
        classifier = CropClassifier(schema=schema)
    elif model_type == "xgboost":
        classifier = XGBoostCropClassifier(schema=schema)
    classifier.load_model(model_name, model_dir)

    if model_type == "random_forest":
        pred_classes = classifier.get_model_classes()
    elif model_type == "xgboost":
        le = classifier._label_encoder
        pred_classes = le.inverse_transform(classifier.get_model_classes())

    foi_ids_with_result, pred_proba = classifier.predict_from_database(
        cursor, foi_ids, working_dir, object_level="foi"
    )

    predictions = {}
    for foi_idx in range(pred_proba.shape[0]):
        foi_id = foi_ids_with_result[foi_idx]
        highest_2_proba_idx = np.argsort(pred_proba[foi_idx, :])[::-1][:2]

        CT_pred_1 = pred_classes[highest_2_proba_idx[0]]
        CT_conf_1 = pred_proba[foi_idx, highest_2_proba_idx[0]]
        CT_pred_2 = pred_classes[highest_2_proba_idx[1]]
        CT_conf_2 = pred_proba[foi_idx, highest_2_proba_idx[1]]

        predictions[foi_id] = {}
        predictions[foi_id]["CT_pred_1"] = CT_pred_1
        predictions[foi_id]["CT_conf_1"] = CT_conf_1
        predictions[foi_id]["CT_pred_2"] = CT_pred_2
        predictions[foi_id]["CT_conf_2"] = CT_conf_2

    return predictions


def get_metrics(df):
    accuracy = accuracy_score(df["CT_decl"].astype("int"), df["CT_pred_1"].astype("int"))
    precision = precision_score(df["CT_decl"].astype("int"), df["CT_pred_1"].astype("int"))
    recall = recall_score(df["CT_decl"].astype("int"), df["CT_pred_1"].astype("int"))
    tn, fp, fn, tp = confusion_matrix(
        df["CT_decl"].astype("int"), df["CT_pred_1"].astype("int")
    ).ravel()
    f1 = fbeta_score(df["CT_decl"].astype("int"), df["CT_pred_1"].astype("int"), beta=1.5)

    return accuracy, precision, recall, tn, fp, fn, tp, f1
