from niva_classifiers.database import Database

db_config = {
    "host": "doerian",
    "port": 5432,
    "name": "postgres",
    "password": "postgres",
    "db_name": "A152_niva",
    "schema": "public",
}


def import_pyarrow(db_config):
    # connect to database
    db = Database(
        db_config["host"],
        db_config["port"],
        db_config["name"],
        db_config["password"],
        db_config["db_name"],
        schema=db_config["schema"],
    )
    offset = db.import_pyarrow(
        r"\\bergzuurzak\c\Users\Cornelis\Downloads\wetransfer_sen4cap_mdb_l4a_opt_main_v20211104_20211229_"
        r"20221012t060307-ipc_2022-10-14_1006\SEN4CAP_MDB_L4A_OPT_MAIN_V20211104_20211229_20221012T060307.ipc",
        foi_mapping_offset=0,
    )
    print(offset)
    db.import_pyarrow(
        r"\\bergzuurzak\c\Users\Cornelis\Downloads\wetransfer_sen4cap_mdb_l4a_opt_main_v20211104_20211229_"
        r"20221012t060307-ipc_2022-10-14_1006\SEN4CAP_MDB_L4A_OPT_MAIN_V20211104_20211229_20221012T091040.ipc",
        foi_mapping_offset=offset,
    )


def import_clc(db_config):
    # connect to database
    db = Database(
        db_config["host"],
        db_config["port"],
        db_config["name"],
        db_config["password"],
        db_config["db_name"],
        schema=db_config["schema"],
    )
    db.import_clc(
        r"\\zuurzak\I\99_A152_NIVA\00_LPISparcels\FINAL_LPIS_RFVs_DATASET\LPIS_RFV_WGS84_UTM_Final.shp"
    )


if __name__ == "__main__":
    import_pyarrow(db_config)
