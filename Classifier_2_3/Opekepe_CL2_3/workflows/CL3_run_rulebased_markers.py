# -*- coding: utf-8 -*-
import datetime
import os

import psycopg2
from cap_framework import signal
from cap_framework.algorithm import AlgorithmRun, marker
from cap_framework.algorithm.settings import AlgorithmSettingsCollection, MarkerSettings
from cap_framework.algorithm.writer import (  # AlgorithmPlotterLocal,; AlgorithmWriterDB,
    AlgorithmWriterMemory,
)


def get_filters_S2_signal(schema, time_range, signal_type):
    filters = {
        "signal_type": signal_type,
        "metric": "mean",
        "schema": schema,
        "sensors": "Sentinel-2",
        "time_range": time_range,
        "object_id_column": "foi_id",
        "stats_method": "original_geometry",
    }
    return filters


def get_marker_NDVIPresence_settings(schema, time_range, cursor):
    filters = get_filters_S2_signal(schema, time_range, "NDVI")
    s = signal.InterpolatedTimeseriesSignal(cursor=cursor, filters=filters)
    m = marker.MarkerNDVIVegetationPresence()
    return MarkerSettings(m, s)


def get_marker_NDVIAbsence_settings(schema, time_range, cursor):
    filters = get_filters_S2_signal(schema, time_range, "NDVI")
    s = signal.InterpolatedTimeseriesSignal(cursor=cursor, filters=filters)
    m = marker.MarkerNDVIVegetationAbsence()
    return MarkerSettings(m, s)


def get_marker_NDTIPloughing_settings(schema, time_range, cursor):
    filters = get_filters_S2_signal(schema, time_range, "NDTI")
    s = signal.InterpolatedTimeseriesSignal(cursor=cursor, filters=filters)
    m = marker.MarkerNDTIPloughing()
    return MarkerSettings(m, s)


def write_events_to_CSV(path, event_dict, id_column_name, separator=","):
    with open(path, "w") as F:
        header = separator.join([id_column_name, "start", "end", "confidence"])
        F.write(header + "\n")
        for object_id, events in event_dict.items():
            if not events:
                F.write(separator.join([str(object_id), "", "", ""]) + "\n")
            for start, end, confidence in events:
                F.write(
                    separator.join([str(object_id), str(start), str(end), str(confidence)]) + "\n"
                )


if __name__ == "__main__":

    db_credentials = "host=doerian port=5432 user=postgres password=postgres dbname=A152_niva"
    schema = "public"
    time_range = (datetime.datetime(2021, 3, 1), datetime.datetime(2021, 12, 31))
    output_dir = r"D:\temp"

    output_csvs = [
        os.path.join(output_dir, "VegetationPresence.csv"),
        os.path.join(output_dir, "VegetationAbsence.csv"),
        os.path.join(output_dir, "Tillage.csv"),
    ]

    conn = psycopg2.connect(db_credentials)
    cursor = conn.cursor()

    cursor.execute(
        """select f.foi_id
    from %(schema)s.foi f
    join %(schema)s.parcel_foi pf on pf.foi_id = f.foi_id
    join %(schema)s.parcel p on p.parcel_id = pf.parcel_id
    where p.crop_code = 7 and f.num_pixels_within_inner_buffer > 0
    """,
        {"schema": psycopg2.extensions.AsIs(schema)},
    )
    fois = [(foi_id, "Grass", None) for (foi_id,) in cursor.fetchall()]

    cursor.execute(
        """select pf.foi_id, p.client_id
from %(schema)s.parcel_foi pf
join %(schema)s.parcel p on p.parcel_id = pf.parcel_id
where pf.foi_id = ANY (%(foi_ids)s)""",
        {"schema": psycopg2.extensions.AsIs(schema), "foi_ids": [foi_id for foi_id, _, __ in fois]},
    )
    foi_client_lut = dict(cursor.fetchall())

    marker_settings_pres = get_marker_NDVIPresence_settings(schema, time_range, cursor)
    marker_settings_abs = get_marker_NDVIAbsence_settings(schema, time_range, cursor)
    marker_settings_tillage = get_marker_NDTIPloughing_settings(schema, time_range, cursor)

    for marker_settings, output_csv in zip(
        [marker_settings_pres, marker_settings_abs, marker_settings_tillage], output_csvs
    ):

        algorithm_settings_collection = AlgorithmSettingsCollection(marker_settings)
        # writer = AlgorithmPlotterLocal(schema, conn, time_range, algorithm_settings_collection)
        writer3 = AlgorithmWriterMemory()

        algorithm_run = AlgorithmRun(
            conn, [writer3], fois, algorithm_settings_collection, time_range, schema
        )

        algorithm_run.run(metric_based_on_pixel_count=False)
        detections = writer3.get_stored_events()
        client_detections = {
            foi_client_lut[foi_id]: events for foi_id, events in detections.items()
        }
        write_events_to_CSV(output_csv, client_detections, "EDA_ID")
