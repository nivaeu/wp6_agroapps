# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 10:30:13 2022

@author: Cornelis
"""

import uuid

import psycopg2

from niva_classifiers.CL3.add_spectral_indices import (  # compute_add_Sentinel2_NDTI,
    compute_add_Sentinel2_NDII,
)
from niva_classifiers.database import Database

if __name__ == "__main__":

    # connect to database
    db = Database("doerian", 5432, "postgres", "postgres", "A152_niva")
    batch_size = 1000
    with db.connection() as foi_conn:
        foi_cursor = foi_conn.cursor(
            str(uuid.uuid4()), cursor_factory=psycopg2.extras.NamedTupleCursor
        )
        foi_cursor.itersize = batch_size
        foi_cursor.execute("select foi_id from foi")

        with db.connection() as writer_conn:
            writer_cursor = writer_conn.cursor()

            batch = []
            for x, foi in enumerate(foi_cursor):

                if x % batch_size == 0 and x != 0:
                    print("writing ", x)
                    compute_add_Sentinel2_NDII(writer_cursor, batch)
                    # compute_add_Sentinel2_NDTI(writer_cursor, batch)
                    batch = []
                batch += [foi.foi_id]

            if batch:
                print("writing final batch")
                compute_add_Sentinel2_NDII(writer_cursor, batch)
                # compute_add_Sentinel2_NDTI(writer_cursor, batch)
