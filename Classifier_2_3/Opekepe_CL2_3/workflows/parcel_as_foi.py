# -*- coding: utf-8 -*-
"""
Created on Thu Oct 13 15:32:11 2022

@author: Cornelis
"""

import psycopg2
from cap_framework.foi import FOIWriterDB, ParcelAsFOI

db_conn = psycopg2.connect(
    "dbname=A152_niva host=doerian port=5432 user=postgres password=postgres"
)

cursor = db_conn.cursor()
cursor.execute("select parcel_id from parcel")
parcel_ids = [i for i, in cursor.fetchall()]
# example workflow creating the simplest FOIs: directly from parcel

foi_creator = ParcelAsFOI(db_conn)
foi_creator.register()


writer = FOIWriterDB(db_conn, foi_creator, batch_size=100, target_epsg=32634)

foi_creator.create_FOIs(parcel_ids, foi_writers=[writer])
