# NIVA subcall Greece - Classifier 2 (PlanetFusion for small parcels) and Classifier 3 (fallow land)

## using the package
You can install this repo by running the following command in this repo's root 
directory: `pip install -e ".[dev]"`.
The -e means that any code changes are automatically loaded, with the exception of changes to the setup.cfg or 
pyproject.toml files.

Any new dependencies should be added to the list in setup.cfg

Once installed, you can import and use any code like you would with a regular python package.

## Preparing the database
The package heavily uses a Postgres database with PostGIS extension. It has been tested with Postgres 10, but should work fine with any version >=10. The first time you use the package, a database schema needs to be initialized with a set of standard tables. See commandline tool **prepare-database** below.

Afterwards, data can be imported. This starts with **import-parcels**. For CL2, use any Shapefile containing at least crop (group) codes, geometry in the same CRS as the PlanetFusion data, and a parcel ID column. For CL3, import a Sen4CAP LPIS GeoPackage containing at least a column with crop (group) code and "NewID", the identifier used by Sen4CAP internally.

For CL2, the next step is to collect zonal statistics ("signals") from the PlanetFusion imagery. Use **CL2-collect-signals** for this. 

For CL3, the next step is to import Sen4CAP IPC files. Make sure to import them once only. Use **import-pyarrow** for this.

At this point you are ready to train, predict and run algorithms. For training, see the Jupyter Notebooks. For prediction, see the note below.

## CL2
After installing the package, you can run CL2 prediction with the following command:
```bash
CL2-predict --model <random_forest or xgboost> --output_filename <predictions filename (.csv)> --config_file <path_to_config_file (.json) --working_dir <path_to_working_directory>
```
A template config_file can be found here: [crop_classifier.json](niva_classifiers/CL2/model_configs/crop_classifier.json)

## CL3
After installing the package, you can run CL3 prediction with the following command:
```bash
CL3-predict --model <random_forest or xgboost> --output_filename <predictions filename (.csv)> --config_file <path_to_config_file (.json) --working_dir <path_to_working_directory>
```
A template config_file can be found here: [binary_crop_classifier.json](niva_classifiers/CL3/model_configs/binary_crop_classifier.json)

And the rule-based NDTI tillage detection can be run with:
```bash
CL3-rulebased --output_dir <path_to_existing_directory> --start_date <YYYY-MM-DD> --end_date <YYYY-MM-DD>
```


## Other commandline tools:
| **Command**     | prepare-database  |
|-----------------|---|
| **Description** |  Call the first time you start the docker. Prepares all the tables, and stores the database credentials to be used in any further calls.  |
| **Arguments**   |  --host HOST <br /> --port PORT <br /> --user USER <br /> --password PASSWORD <br /> --name NAME <br /> --schema SCHEMA = “neo” |
| **Example**     |  prepare-database --host doerian --port 5432 --user postgres --password postgres --name A152_niva  |

| **Command**     | import-parcels  |
|-----------------|---|
| **Description** |  Fills the database with parcels from a shapefile   |
| **Arguments**   |  --file -f FILE  |
| **Example**     |  import-parcels -f /input_data/LPIS_RFV_WGS84_UTM_Final.shp   |

| **Command**     | import-pyarrow   |
|-----------------|---|
| **Description** |  Fills the interpolated observations table in the database from data in pyarrow files    |
| **Arguments**   |  --file -f FILE  |
| **Example**     |  import-pyarrow -f /input_data/SEN4CAP_MDB_L4A_OPT_MAIN_V20211104_20211229_20221012T060307.ipc    |

| **Command**     | empty-database   |
|-----------------|---|
| **Description** |  Completely wipes the database schema     |
| **Example**     |  empty-database     |

| **Command**     | CL2-collect-signals  |
|-----------------|---|
| **Description** |  Collects signals from a folder with PlanetFusion data.  |
| **Arguments**   |  --planetfusion_dir PLANETFUSION_DIR <br /> --start_date START_DATE <br /> --end_date END_DATE <br /> --interval_days INTERVAL_DAYS |
| **Example**     |  CL2-collect-signals --planetfusion_dir /input_data/PlanetFusion/34N --start_date 2021-01-01 --end_date 2021-12-31 --interval_days 7 |

