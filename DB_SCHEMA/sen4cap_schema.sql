--
-- PostgreSQL database dump
--

-- Dumped from database version 11.10 (Debian 11.10-1.pgdg90+1)
-- Dumped by pg_dump version 12rc1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;


--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: t_data_type; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.t_data_type AS ENUM (
    'bool',
    'date',
    'float',
    'int',
    'file',
    'directory',
    'string',
    'select'
);


--
-- Name: check_season(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.check_season() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
	IF NOT EXISTS (SELECT id FROM public.season WHERE id != NEW.id AND site_id = NEW.site_id AND enabled = true AND start_date <= NEW.start_date AND end_date >= NEW.end_date) THEN
		RETURN NEW;
	ELSE
		RAISE EXCEPTION 'Nested seasons are not allowed';
	END IF;
END;
$$;


--
-- Name: sp_adduser(character varying, character varying, text, smallint, integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_adduser("userName" character varying, email character varying, pwd text, "roleId" smallint DEFAULT 2, "siteId" integer[] DEFAULT NULL::integer[]) RETURNS smallint
    LANGUAGE sql
    AS $_$INSERT INTO "user"(
            id, login, email, role_id, site_id, password)
    VALUES (
    COALESCE((SELECT MAX(id) FROM public.user) :: integer, 0) + 1, 
    $1, $2, $4, $5,
    crypt($3, gen_salt('md5')));
SELECT id FROM public.user WHERE login = $1;$_$;


--
-- Name: sp_authenticate(character varying, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_authenticate(usr character varying, pwd text) RETURNS TABLE(user_id smallint, site_id integer[], role_id smallint, role_name character varying)
    LANGUAGE sql
    AS $_$SELECT u.id, u.site_id, r.id, r.name
  FROM "user" u
  INNER JOIN role r ON u.role_id=r.id
  WHERE login = $1 AND crypt($2, password) = password
$_$;


--
-- Name: sp_build_archive_path(character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_build_archive_path(_archive_path character varying, _site character varying, _processor character varying, _product_type character varying, _current_path character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
DECLARE return_value VARCHAR;
BEGIN
	
	return_value := regexp_replace(_archive_path, '\{site\}', _site);
	return_value := regexp_replace(return_value, '\{processor\}', _processor);
	return_value := regexp_replace(return_value, '\{product_type\}', _product_type);

	return_value := return_value || '/' || regexp_replace(_current_path, '^.+[/\\]+([^/\\]+)[/\\]+[^/\\]*$', '\1') || '/';
	
	RETURN regexp_replace(return_value, '[/\\]{2,}', '/');

END;
$_$;


--
-- Name: sp_changepassword(smallint, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_changepassword("userId" smallint, "oldPassword" character varying, "newPassword" character varying) RETURNS void
    LANGUAGE sql
    AS $_$UPDATE public.user
	     SET password = crypt($3, gen_salt('md5'))
	     WHERE id = $1 AND password = crypt($2, password)$_$;


--
-- Name: sp_clear_pending_fmask_tiles(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_clear_pending_fmask_tiles() RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
    delete
    from fmask_history
    where status_id = 1; -- processing
end;
$$;


--
-- Name: sp_clear_pending_l1_tiles(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_clear_pending_l1_tiles() RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
    if (select current_setting('transaction_isolation') not ilike 'serializable') then
        raise exception 'Please set the transaction isolation level to serializable.' using errcode = 'UE001';
    end if;

    delete
    from l1_tile_history
    using downloader_history
    where downloader_history.id = l1_tile_history.downloader_history_id
      and l1_tile_history.status_id = 1 -- processing
      and downloader_history.satellite_id in (1, 2); -- sentinel2, landsat8

    update downloader_history
    set status_id = 2 -- downloaded
    where status_id = 7 -- processing
      and not exists (
        select *
        from l1_tile_history
        where status_id = 1 -- processing
    );
end;
$$;


--
-- Name: sp_clear_pending_l1_tiles(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_clear_pending_l1_tiles(_node_id text) RETURNS void
    LANGUAGE plpgsql
    AS $$
		begin
			if (select current_setting('transaction_isolation') not ilike 'serializable') then
				raise exception 'Please set the transaction isolation level to serializable.' using errcode = 'UE001';
			end if;

			delete
			from l1_tile_history
			using downloader_history
			where downloader_history.id = l1_tile_history.downloader_history_id
			  and l1_tile_history.status_id = 1 -- processing
			  and l1_tile_history.node_id = _node_id
			  and downloader_history.satellite_id in (1, 2); -- sentinel2, landsat8

			update downloader_history
			set status_id = 2 -- downloaded
			where status_id = 7 -- processing
			  and not exists (
				select *
				from l1_tile_history
				where status_id = 1 -- processing
			);
		end;
		$$;


--
-- Name: sp_convert_data_type_value(character varying, public.t_data_type); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_convert_data_type_value(_value character varying, _type public.t_data_type) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE boolean_value boolean;
DECLARE date_value date;
DECLARE double_value double precision;
DECLARE bigint_value bigint;
DECLARE varchar_value character varying;
BEGIN

	CASE _type
		WHEN 'float' THEN
			BEGIN
				double_value := _value::double precision;
				RETURN double_value::varchar;
			EXCEPTION WHEN OTHERS THEN
				RETURN 'Invalid float value: '||_value;
			END;
		WHEN 'int' THEN
			BEGIN
				bigint_value := _value::bigint;
				RETURN bigint_value::varchar;
			EXCEPTION WHEN OTHERS THEN
				RETURN 'Invalid integer value: '||_value;
			END;
		ELSE
		    BEGIN
                RETURN _value;
		    END;
	END CASE;

END;
$$;


--
-- Name: sp_dashboard_add_site(character varying, character varying, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_dashboard_add_site(_name character varying, _geog character varying, _enabled boolean) RETURNS smallint
    LANGUAGE plpgsql
    AS $_$
DECLARE _short_name character varying;
DECLARE return_id smallint;
BEGIN

    _short_name := lower(_name);
    _short_name := regexp_replace(_short_name, '\W+', '_', 'g');
    _short_name := regexp_replace(_short_name, '_+', '_', 'g');
    _short_name := regexp_replace(_short_name, '^_', '');
    _short_name := regexp_replace(_short_name, '_$', '');

	INSERT INTO site (name, short_name, geog, enabled)
		VALUES (_name, _short_name, ST_Multi(ST_Force2D(ST_GeometryFromText(_geog))) :: geography, _enabled)
	RETURNING id INTO return_id;

	RETURN return_id;
END;
$_$;


--
-- Name: sp_dashboard_remove_scheduled_task(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_dashboard_remove_scheduled_task(_schedule_id smallint) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 

DELETE FROM scheduled_task WHERE id = _schedule_id;

DELETE FROM scheduled_task_status WHERE task_id = _schedule_id;

END;
$$;


--
-- Name: sp_dashboard_update_scheduled_task(smallint, smallint, smallint, smallint, character varying, json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_dashboard_update_scheduled_task(_schedule_id smallint, _repeat_type smallint, _repeat_after_days smallint, _repeat_on_month_day smallint, _first_run_time character varying, _processor_params json) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 

UPDATE scheduled_task
SET	processor_params = _processor_params,
	repeat_type = _repeat_type,
	repeat_after_days = _repeat_after_days,
	repeat_on_month_day = _repeat_on_month_day,
	first_run_time = _first_run_time
WHERE id = _schedule_id;

UPDATE scheduled_task_status
SET next_schedule =_first_run_time,
	last_scheduled_run = '0',
	last_run_timestamp = '0',
	last_retry_timestamp = '0',
	estimated_next_run_time = '0'
WHERE task_id = _schedule_id;

END;
$$;


--
-- Name: sp_dashboard_update_site(smallint, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_dashboard_update_site(_id smallint, _enabled boolean) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

IF _enabled IS NOT NULL THEN
    UPDATE site
    SET enabled = _enabled AND EXISTS(
                    SELECT *
                    FROM season
                    WHERE season.site_id = _id AND season.enabled)
    WHERE id = _id;
END IF;

END;
$$;


--
-- Name: sp_delete_season(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_delete_season(_season_id smallint) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
    delete from scheduled_task_status where task_id in 
			(select id from scheduled_task where season_id = _season_id);
            
    delete from scheduled_task
    where season_id = _season_id;

    delete from season
    where id = _season_id;

    if not found then
        raise exception 'Invalid season % for site %', _name, _site_id;
    end if;
end;
$$;


--
-- Name: sp_downloader_get_sites(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_downloader_get_sites() RETURNS TABLE(id smallint, name character varying, short_name character varying, geog text[])
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
    RETURN QUERY
        SELECT site.id,
               site.name,
               site.short_name,
               (SELECT array_agg(ST_AsText(ST_MakeValid(ST_SnapToGrid(polys.geog, 0.001))))
                FROM (
                    SELECT (ST_Dump(site.geog :: geometry)).geom AS geog
                ) polys)
        FROM site
	WHERE site.enabled;
END
$$;


--
-- Name: sp_get_all_users(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_all_users() RETURNS TABLE(user_id smallint, user_name character varying, email character varying, role_id smallint, role_name character varying, site_id integer[], site_name text)
    LANGUAGE plpgsql
    AS $$
BEGIN
    RETURN QUERY 
    select u.id as user_id, 
	u.login as user_name,
	u. email,
	u.role_id,
	r.name as role_name, 
	u.site_id,
	(SELECT array_to_string(array_agg(name),', ') from site where id = ANY(u.site_id::int[]) ) as site_name
	--(select array(select name from site where array[id]::integer[] @> u.site_id )) as site_name
	/*CASE WHEN (select array(select name from site where id = ANY(u.site_id::int[])) ) = '{}'
		THEN NULL
	ELSE (select array(select name from site where id = ANY(u.site_id::int[]))) 
	END as site_name	*/
	/*(SELECT json_agg(siteName)
	FROM (SELECT name as siteName from site where id = ANY(u.site_id::int[]) ) as sites) as site_name*/
    FROM "user" u 
    INNER JOIN role r ON u.role_id = r.id;

END;
$$;


--
-- Name: sp_get_categories(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_categories() RETURNS TABLE(id smallint, name character varying, allow_per_site_customization boolean)
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
    RETURN QUERY
        SELECT config_category.id,
               config_category.name,
               config_category.allow_per_site_customization
        FROM config_category
        ORDER BY config_category.display_order;
END
$$;


--
-- Name: sp_get_config_metadata(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_config_metadata() RETURNS TABLE(key character varying, friendly_name character varying, type public.t_data_type, is_advanced boolean, config_category_id smallint)
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
    RETURN QUERY
        SELECT config_metadata.key,
               config_metadata.friendly_name,
               config_metadata.type,
               config_metadata.is_advanced,
               config_metadata.config_category_id
        FROM config_metadata
        ORDER BY config_metadata.is_advanced, config_metadata.key;
END
$$;


--
-- Name: sp_get_dashboard_current_downloads(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_current_downloads(_siteid smallint DEFAULT NULL::smallint) RETURNS TABLE(site character varying, prod_name character varying, sat character varying, prod_date timestamp with time zone)
    LANGUAGE plpgsql
    AS $_$
BEGIN
   RETURN QUERY
   
SELECT site.name, product_name, satellite.satellite_name, product_date
FROM downloader_history
INNER JOIN satellite ON downloader_history.satellite_id = satellite.id
INNER JOIN site ON downloader_history.site_id = site.id AND  status_id =1 AND( $1 IS NULL OR site.id = _siteid)
ORDER BY site.name;

END
$_$;


--
-- Name: sp_get_dashboard_current_job_data(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_current_job_data(page integer DEFAULT 1) RETURNS json
    LANGUAGE plpgsql
    AS $_$DECLARE current_job RECORD;
DECLARE temp_json json;
DECLARE temp_json2 json;
DECLARE oset int;
DECLARE lmt int;
BEGIN
	CREATE TEMP TABLE current_jobs (
		id int,
		processor character varying,
		site  character varying,
		triggered_by character varying,
		triggered_on character varying,
		status_id smallint,
		status character varying,
		tasks_completed int,
		tasks_remaining int,
		current_tasks json,
		actions json
	) ON COMMIT DROP;

	oset := ($1 - 1) * 5;
	lmt := 5;
	-- Get the current jobs
	INSERT INTO current_jobs (
		id, 
		processor,
		site,
		triggered_by,
		triggered_on,
		status_id,
		status,
		tasks_completed,
		tasks_remaining)
	SELECT 
		job.id,
		processor.name,
		site.name,
		CASE job.start_type_id WHEN 1 THEN 'Available Product'  WHEN 2 THEN 'User Request' WHEN 3 THEN 'Scheduler Request' END,
		to_char(job.submit_timestamp, 'YYYY-MM-DD HH:MI:SS'),
		job.status_id,
		activity_status.name,
		(SELECT count(*) FROM task WHERE task.job_id = job.id AND task.status_id IN (6,7,8)),	-- Finished, Cancelled, Error
		(SELECT count(*) FROM task WHERE task.job_id = job.id AND task.status_id NOT IN (6,7,8)) 
	FROM job 
		INNER JOIN processor ON job.processor_id = processor.id 
		INNER JOIN site ON job.site_id = site.id
		INNER JOIN activity_status ON job.status_id = activity_status.id
	WHERE job.status_id NOT IN (6,7,8) -- Finished, Cancelled, Error
	  AND EXISTS(SELECT * FROM task WHERE task.job_id = job.id) -- this is to prevent an empty dashboard display when there are inconsistencies in the job state
		ORDER BY job.id DESC
		OFFSET oset LIMIT lmt;

	-- For each of the current jobs, get the data for their ongoing tasks
	CREATE TEMP TABLE current_tasks (
		current_task_module character varying,
		current_task_steps_completed int,
		current_task_steps_remaining int
	) ON COMMIT DROP;

	FOR current_job IN SELECT * FROM current_jobs ORDER BY id LOOP
		-- Clear the temporary table
		DELETE FROM current_tasks;

		-- Get the current tasks for each job
		INSERT INTO current_tasks (
			current_task_module,
			current_task_steps_completed,
			current_task_steps_remaining
		)
		WITH modules as (SELECT DISTINCT task.module_short_name FROM task WHERE task.job_id = current_job.id)
			SELECT  modules.module_short_name,
				(select count(*) from task where task.job_id = current_job.id and task.module_short_name = modules.module_short_name AND task.status_id = 4),
				(select count(*) from task where task.job_id = current_job.id and task.module_short_name = modules.module_short_name)
			FROM modules
			ORDER BY modules.module_short_name;

			IF current_job.status_id != 5 /*Paused*/ THEN
				temp_json := json_build_array(1,3,4);
			ELSE
				temp_json := json_build_array(2,3,4);
			END IF;

			SELECT array_to_json(array_agg(row_to_json(current_task_details, true))) INTO temp_json2
			FROM (SELECT * FROM current_tasks) current_task_details;

			UPDATE current_jobs 
				SET 	current_tasks = temp_json2,
					actions = temp_json
				WHERE 	current_jobs.id = current_job.id;
		
	END LOOP;

	SELECT array_to_json(array_agg(row_to_json(current_job_details, true))) INTO temp_json
	FROM (SELECT * FROM current_jobs) AS current_job_details;

	temp_json2 := json_build_object('current_jobs', temp_json);

	RETURN temp_json2;

END;$_$;


--
-- Name: sp_get_dashboard_downloader_history(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_downloader_history(_siteid smallint DEFAULT NULL::smallint) RETURNS TABLE(id integer, nr_downloads bigint, nr_downloads_percentage numeric)
    LANGUAGE plpgsql STABLE
    AS $_$
DECLARE total numeric;

                BEGIN

		Select count(dh.id) into total from downloader_history dh WHERE status_id IN (1, 2, 3, 4, 5, 6, 7 ,41) AND (_siteid IS NULL OR site_id = _siteid);

                   IF total<>'0' THEN
			   RETURN QUERY

			    SELECT  1, count(status_id),CASE WHEN total<>'0' THEN round(((count(status_id) * 100)/total)::numeric ,2)
				ELSE 0 END

				FROM downloader_history
				WHERE status_id  = 1 AND ( $1 IS NULL OR site_id = _siteid)
			   UNION
			    SELECT  2, count(status_id),CASE  WHEN total<>'0' THEN round(((count(status_id) * 100)/total)::numeric ,2)
				ELSE 0 END
				FROM downloader_history
				WHERE status_id IN (2, 5, 6, 7) AND ( $1 IS NULL OR site_id = _siteid)
			   UNION
			    SELECT  3, count(status_id),CASE  WHEN total<>'0' THEN round(((count(status_id) * 100)/total)::numeric ,2)
				ELSE 0 END
				FROM downloader_history
				WHERE status_id IN (4,41) AND ( $1 IS NULL OR site_id = _siteid)
			    UNION
			    SELECT  4, count(status_id),CASE  WHEN total<>'0' THEN round(((count(status_id) * 100)/total)::numeric ,2)
				ELSE 0 END
				FROM downloader_history
				WHERE status_id IN (3) AND ( $1 IS NULL OR site_id = _siteid)
			   ORDER BY 1;

		   END IF;
                END
                $_$;


--
-- Name: sp_get_dashboard_job_config(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_job_config(_job_id integer) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE current_processor RECORD;
DECLARE temp_json json;
DECLARE temp_json2 json;
DECLARE return_string text;
BEGIN

	WITH config_params AS (
	SELECT json_build_array(key,value) AS param
	FROM config_job
	WHERE job_id = _job_id)
	SELECT array_to_json(array_agg(config_params.param)) INTO temp_json
	FROM config_params;

	WITH job_input AS (SELECT json_each(parameters) AS tuple FROM job WHERE id = _job_id)
	SELECT array_to_json(array_agg(json_build_array((job_input.tuple).key, (job_input.tuple).value))) INTO temp_json2 FROM job_input;

	return_string := json_build_object('configuration', temp_json, 'input', temp_json2);

	RETURN return_string::json;

END;
$$;


--
-- Name: sp_get_dashboard_job_timeline(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_job_timeline(_job_id integer) RETURNS json
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE res JSON;
BEGIN
    WITH tasks AS (
        SELECT task.id AS id
        FROM task
        WHERE task.job_id = _job_id
    ),
    groups AS (
        SELECT task.module_short_name AS "content",
               task.id AS id
        FROM tasks
        INNER JOIN task ON task.id = tasks.id
    ),
    items AS (
        SELECT step.task_id AS "group",
               "name" AS "title",
               step.start_timestamp AS "start",
               COALESCE(step.end_timestamp, NOW()) AS "end"
        FROM step
        INNER JOIN tasks ON tasks.id = step.task_id
        WHERE step.start_timestamp IS NOT NULL
    ),
    result AS (
        SELECT (SELECT array_agg(groups) FROM groups) AS groups,
               (SELECT array_agg(items) FROM items) AS items
    )
    SELECT row_to_json(result)
    INTO res
    FROM result;

    RETURN res;
END;
$$;


--
-- Name: sp_get_dashboard_landsat_tiles(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_landsat_tiles(site_id smallint) RETURNS json
    LANGUAGE plpgsql
    AS $_$
DECLARE return_string text;
BEGIN
	WITH data(code,geometry) AS (
	     SELECT  DISTINCT 
	     	     WRSD.pr, 
		     ST_AsGeoJSON(WRSD.geog)
	     FROM shape_tiles_l8 WRSD
  	     JOIN site S ON ST_Intersects(S.geog, WRSD.geog)
	     WHERE
		     S.id = $1
        )

	SELECT array_to_json(array_agg(row_to_json(data)),true) INTO return_string FROM data;
	RETURN return_string::json;
END
$_$;


--
-- Name: sp_get_dashboard_processor_scheduled_task(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_processor_scheduled_task(_processor_id smallint) RETURNS TABLE(id smallint, name character varying, site_id smallint, site_name character varying, season_name text, repeat_type smallint, first_run_time character varying, repeat_after_days smallint, repeat_on_month_day smallint, processor_params character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
   RETURN QUERY
	SELECT st.id,
              st.name,
              site.id,
              site.name,
              season.name,
              st.repeat_type,
              st.first_run_time,
              st.repeat_after_days,
              st.repeat_on_month_day,
              st.processor_params
        FROM scheduled_task AS st
        INNER JOIN site on site.id = st.site_id
        LEFT OUTER JOIN season ON season.id = st.season_id
        WHERE st.processor_id = _processor_id
        ORDER BY st.id;


END
$$;


--
-- Name: sp_get_dashboard_processor_statistics(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_processor_statistics() RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE current_processor RECORD;
DECLARE temp_json json;
DECLARE temp_json2 json;
DECLARE temp_json3 json;
DECLARE temp_array json[];
DECLARE temp_array2 json[];
DECLARE return_string text;
BEGIN

	CREATE TEMP TABLE processors (
		id smallint,
		name character varying
		) ON COMMIT DROP;

	-- Get the list of processors to return the resources for
	INSERT INTO processors (id, name)
	SELECT id, short_name
	FROM processor ORDER BY name;

	return_string := '{';

	-- Go through the processors and compute their data
	FOR current_processor IN SELECT * FROM processors ORDER BY name LOOP

		IF return_string != '{' THEN
			return_string := return_string || ',';
		END IF;

		-- First compute the resource averages
		WITH job_resources AS(
		SELECT 
		max(entry_timestamp) AS last_run,
		sum(duration_ms) AS total_duration,
		sum(user_cpu_ms) AS total_user_cpu,
		sum(system_cpu_ms) AS total_system_cpu,
		sum(max_rss_kb) AS total_max_rss,
		sum(max_vm_size_kb) AS total_max_vm_size,
		sum(disk_read_b) AS total_disk_read,
		sum(disk_write_b) AS total_disk_write
		FROM step_resource_log 
		INNER JOIN task ON step_resource_log.task_id = task.id
		INNER JOIN job ON task.job_id = job.id AND job.processor_id = current_processor.id
		GROUP BY job.id)
		SELECT '[' ||
		'["Last Run On","' || to_char(max(last_run), 'YYYY-MM-DD HH:MI:SS') || '"],' ||
		'["Average Duration","' || to_char(avg(total_duration) / 1000 * INTERVAL '1 second', 'HH24:MI:SS.MS') || '"],' ||
		'["Average User CPU","' || to_char(avg(total_user_cpu) / 1000 * INTERVAL '1 second', 'HH24:MI:SS.MS') || '"],' ||
		'["Average System CPU","' || to_char(avg(total_system_cpu) / 1000 * INTERVAL '1 second', 'HH24:MI:SS.MS') || '"],' ||
		'["Average Max RSS","' || round(avg(total_max_rss)::numeric / 1024::numeric, 2)::varchar || ' MB' || '"],' ||
		'["Average Max VM","' || round(avg(total_max_vm_size)::numeric / 1024::numeric, 2)::varchar || ' MB' || '"],' ||
		'["Average Disk Read","' || round(avg(total_disk_read)::numeric / 1048576::numeric, 2)::varchar || ' MB' || '"],' ||
		'["Average Disk Write","' || round(avg(total_disk_write)::numeric / 1048576::numeric, 2)::varchar || ' MB' || '"]' ||
		']' INTO temp_json
		FROM job_resources;

		temp_json := coalesce(temp_json, '[["Last Run On","never"],["Average Duration","00:00:00.000"],["Average User CPU","00:00:00.000"],["Average System CPU","00:00:00.000"],["Average Max RSS","0.00 MB"],["Average Max VM","0.00 MB"],["Average Disk Read","0.00 MB"],["Average Disk Write","0.00 MB"]]');

		-- Next compute the output statistics
		temp_array := array[]::json[];
		SELECT json_build_array('Number of Products', count(*)) INTO temp_json2 FROM product WHERE processor_id = current_processor.id;
		temp_array := array_append(temp_array, temp_json2);

		WITH step_statistics AS(
		SELECT 
		count(*) AS no_of_tiles, 
		sum(duration_ms)/count(*) AS average_duration_per_tile
		FROM step_resource_log 
		INNER JOIN task ON step_resource_log.task_id = task.id
		INNER JOIN job ON task.job_id = job.id AND job.processor_id = current_processor.id
		GROUP BY job.id)
		SELECT array[json_build_array('Average Tiles per Product', coalesce(round(avg(no_of_tiles),2), 0)), json_build_array('Average Duration per Tile', coalesce(to_char(avg(average_duration_per_tile) / 1000 * INTERVAL '1 second', 'HH24:MI:SS.MS'), '00:00:00.000'))]
		INTO temp_array2
		FROM step_statistics;

		temp_array := array_cat(temp_array, temp_array2);
		temp_json2 := array_to_json(temp_array);

		-- Last get the configuration parameters
		WITH config_params AS (
		SELECT json_build_array(
		substring(key from length('processor.' || current_processor.name || '.')+1) || CASE coalesce(config.site_id,0) WHEN 0 THEN '' ELSE '(' || site.short_name || ')' END,
		value) AS param
		FROM config
		LEFT OUTER JOIN site ON config.site_id = site.id
		WHERE config.key ILIKE 'processor.' || current_processor.name || '.%')
		SELECT array_to_json(array_agg(config_params.param)) INTO temp_json3
		FROM config_params;

		-- Update the return json with the computed data
		return_string := return_string || '"' || current_processor.name || '_statistics" :' || json_build_object('resources', temp_json, 'output', temp_json2, 'configuration', temp_json3);
		
	END LOOP;

	return_string := return_string || '}';
	RETURN return_string::json;

END;
$$;


--
-- Name: sp_get_dashboard_processors(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_processors(processor_id smallint DEFAULT NULL::smallint) RETURNS json
    LANGUAGE plpgsql
    AS $_$
DECLARE return_string text;
BEGIN
	WITH data(id,name,description,short_name) AS (
		SELECT 	PROC.id, 
			PROC.name,
			PROC.description, 
			PROC.short_name
  		FROM processor PROC
		WHERE
			($1 IS NULL OR PROC.id = $1)
		ORDER BY PROC.name
	)

	SELECT array_to_json(array_agg(row_to_json(data)),true) INTO return_string FROM data;
	RETURN return_string::json;
END
$_$;


--
-- Name: sp_get_dashboard_product_availability(timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_product_availability(_since_timestamp timestamp with time zone DEFAULT NULL::timestamp with time zone) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE current_product_type RECORD;
DECLARE temp_json json;
DECLARE temp_json2 json;
BEGIN

	IF _since_timestamp IS NULL THEN
		_since_timestamp := '1970-01-01 00:00:00'::timestamp;
	END IF;

	CREATE TEMP TABLE product_types (
		id smallint,
		name character varying,
		products json
		) ON COMMIT DROP;

	-- Get the list of processors to return the resources for
	INSERT INTO product_types (id, name)
	SELECT id, name
	FROM product_type ORDER BY name;

	-- Go through the product types and compute their data
	FOR current_product_type IN SELECT * FROM product_types ORDER BY name LOOP

		WITH products AS (
		SELECT 
			product.full_path AS "key",
			'Site: ' || site.short_name AS "info"
		FROM product
		INNER JOIN site ON product.site_id = site.id
		WHERE created_timestamp >= _since_timestamp
		AND product_type_id = current_product_type.id)
		SELECT array_to_json(array_agg(row_to_json(product_details, true))) INTO temp_json
		FROM (SELECT * FROM products) AS product_details; 

		UPDATE product_types
		SET products = json_build_object('key',current_product_type.name,'_values', temp_json)
		WHERE id = current_product_type.id;
		
	END LOOP;

	SELECT array_to_json(array_agg(product_type_details.products)) INTO temp_json
	FROM (SELECT products FROM product_types) AS product_type_details;

	temp_json2 := json_build_object('products', array_to_json(array[json_build_object('key', 'Products', '_values', temp_json)]));

	RETURN temp_json2;

END;
$$;


--
-- Name: sp_get_dashboard_products(integer[], integer[], smallint, integer[], timestamp with time zone, timestamp with time zone, character varying[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_products(_site_id integer[] DEFAULT NULL::integer[], _product_type_id integer[] DEFAULT NULL::integer[], _season_id smallint DEFAULT NULL::smallint, _satellit_id integer[] DEFAULT NULL::integer[], _since_timestamp timestamp with time zone DEFAULT NULL::timestamp with time zone, _until_timestamp timestamp with time zone DEFAULT NULL::timestamp with time zone, _tiles character varying[] DEFAULT NULL::character varying[]) RETURNS SETOF json
    LANGUAGE plpgsql STABLE
    AS $_$
		DECLARE q text;
		BEGIN
		    q := $sql$
			WITH site_names(id, name, geog, row) AS (
				select id, name, st_astext(geog), row_number() over (order by name)
				from site
			    ),
			    product_type_names(id, name, description, row) AS (
				select id, name, description, row_number() over (order by description)
				from product_type
			    ),
			    data(id, satellite_id, product, product_type_id, product_type,product_type_description,processor,site,full_path,quicklook_image,footprint,created_timestamp, site_coord) AS (
			    SELECT
				P.id,
				P.satellite_id,
				P.name,
				PT.id,
				PT.name,
				PT.description,
				PR.name,
				S.name,
				P.full_path,
				P.quicklook_image,
				P.footprint,
				P.created_timestamp,
				S.geog
			    FROM product P
				JOIN product_type_names PT ON P.product_type_id = PT.id
				JOIN processor PR ON P.processor_id = PR.id
				JOIN site_names S ON P.site_id = S.id
			    WHERE TRUE -- COALESCE(P.is_archived, FALSE) = FALSE
			    AND EXISTS (
				    SELECT * FROM season WHERE season.site_id =P.site_id AND P.created_timestamp BETWEEN season.start_date AND season.end_date + interval '1 day'
		    $sql$;
		    IF $3 IS NOT NULL THEN
			q := q || $sql$
				AND season.id=$3
				$sql$;
		    END IF;

		    q := q || $sql$
			)
		    $sql$;

		    IF $1 IS NOT NULL THEN
			q := q || $sql$
				AND P.site_id = ANY($1)
		    $sql$;
		    END IF;

		    IF $2 IS NOT NULL THEN
			q := q || $sql$
			 	AND P.product_type_id= ANY($2)

				$sql$;
		    END IF;

		IF $5 IS NOT NULL THEN
		q := q || $sql$
			AND P.created_timestamp >= to_timestamp(cast($5 as TEXT),'YYYY-MM-DD HH24:MI:SS')
			$sql$;
		END IF;

		IF $6 IS NOT NULL THEN
		q := q || $sql$
			AND P.created_timestamp <= to_timestamp(cast($6 as TEXT),'YYYY-MM-DD HH24:MI:SS') + interval '1 day'
			$sql$;
		END IF;

		IF $7 IS NOT NULL THEN
		q := q || $sql$
			AND P.tiles <@$7 AND P.tiles!='{}'
			$sql$;
		END IF;


		q := q || $sql$
			ORDER BY S.row, PT.row, P.name
			)
		--         select * from data;
			SELECT array_to_json(array_agg(row_to_json(data)), true) FROM data;
		    $sql$;

		--     raise notice '%', q;

		    RETURN QUERY
		    EXECUTE q
		    USING _site_id, _product_type_id, _season_id, _satellit_id, _since_timestamp, _until_timestamp, _tiles;
		END
		$_$;


--
-- Name: sp_get_dashboard_products_nodes(character varying, integer[], integer[], smallint, integer[], timestamp with time zone, timestamp with time zone, character varying[], boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_products_nodes(_user_name character varying, _site_id integer[] DEFAULT NULL::integer[], _product_type_id integer[] DEFAULT NULL::integer[], _season_id smallint DEFAULT NULL::smallint, _satellit_id integer[] DEFAULT NULL::integer[], _since_timestamp timestamp with time zone DEFAULT NULL::timestamp with time zone, _until_timestamp timestamp with time zone DEFAULT NULL::timestamp with time zone, _tiles character varying[] DEFAULT NULL::character varying[], _get_nodes boolean DEFAULT false) RETURNS SETOF json
    LANGUAGE plpgsql STABLE
    AS $_$
	DECLARE q text;
	BEGIN
	    q := $sql$
	    WITH
		product_type_names(id, name, description, row, is_raster) AS (
		select id, name, description, row_number() over (order by description), is_raster
		from product_type
		-- LPIS products should be excluded
		where name != 'lpis'
		),
	$sql$;

	IF $9 IS TRUE THEN
		q := q || $sql$
	    site_names(id, name, geog, row) AS (
		select s.id, s.name, st_astext(s.geog), row_number() over (order by s.name)
		from site s
		join public.user u on u.login = $1 and (u.role_id = 1 or s.id in (select * from unnest(u.site_id)))
		),
	    data(id, product, footprint, site_coord, product_type_id, satellite_id, is_raster) AS (

		SELECT
		P.id,
		P.name,
		P.footprint,
		S.geog,
		PT.id,
		P.satellite_id,
		PT.is_raster
		$sql$;
	    ELSE
	    q := q || $sql$
	    site_names(id, name,  row) AS (
		select id, name, row_number() over (order by name)
		from site
		),
	      data(id, satellite_id, product_type_id, product_type_description,site, site_id) AS (
		SELECT
		P.id,
		P.satellite_id,
		PT.id,
		PT.description,
		S.name,
		S.id
	     $sql$;
	END IF;

	 q := q || $sql$
		FROM product P
		JOIN product_type_names PT ON P.product_type_id = PT.id
		JOIN processor PR ON P.processor_id = PR.id
		JOIN site_names S ON P.site_id = S.id
		WHERE TRUE -- COALESCE(P.is_archived, FALSE) = FALSE
		AND EXISTS (
		    SELECT * FROM season WHERE season.site_id = P.site_id AND P.created_timestamp BETWEEN season.start_date AND season.end_date + interval '1 day'
	    $sql$;
	    IF $4 IS NOT NULL THEN
	    q := q || $sql$
		AND season.id=$4
		$sql$;
	    END IF;

	    q := q || $sql$
	    )
	    $sql$;
	     raise notice '%', _site_id;raise notice '%', _product_type_id;raise notice '%', _satellit_id;
	    IF $2 IS NOT NULL THEN
	    q := q || $sql$
		AND P.site_id = ANY($2)

	    $sql$;
	    END IF;

	    IF $3 IS NOT NULL THEN
	    q := q || $sql$
		AND P.product_type_id= ANY($3)

		$sql$;
	    END IF;

	IF $6 IS NOT NULL THEN
	q := q || $sql$
	    AND P.created_timestamp >= to_timestamp(cast($6 as TEXT),'YYYY-MM-DD HH24:MI:SS')
	    $sql$;
	END IF;

	IF $7 IS NOT NULL THEN
	q := q || $sql$
	    AND P.created_timestamp <= to_timestamp(cast($7 as TEXT),'YYYY-MM-DD HH24:MI:SS') + interval '1 day'
	    $sql$;
	END IF;

	IF $8 IS NOT NULL THEN
	q := q || $sql$
	    AND P.tiles <@$8 AND P.tiles!='{}'
	    $sql$;
	END IF;

	q := q || $sql$
	    ORDER BY S.row, PT.row, P.name
	    )
	--         select * from data;
	    SELECT array_to_json(array_agg(row_to_json(data)), true) FROM data;
	    $sql$;

	    raise notice '%', q;

	    RETURN QUERY
	    EXECUTE q
	    USING _user_name, _site_id, _product_type_id, _season_id, _satellit_id, _since_timestamp, _until_timestamp, _tiles, _get_nodes;
	END
	$_$;


--
-- Name: sp_get_dashboard_scheduled_processor_task(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_scheduled_processor_task() RETURNS TABLE(processor character varying, site character varying, repeat_type smallint, repeat_on_month_day smallint, priority smallint, first_run_time character varying)
    LANGUAGE plpgsql STABLE
    AS $$

BEGIN 

	RETURN QUERY 

	SELECT processor.name, site.name,scheduled_task.repeat_type,scheduled_task.repeat_on_month_day,scheduled_task.priority,scheduled_task.first_run_time

	FROM site

	JOIN scheduled_task ON site.id = scheduled_task.site_id

	JOIN processor ON scheduled_task.processor_id=processor.id

	ORDER BY site.name;

END

$$;


--
-- Name: sp_get_dashboard_scheduled_processor_tasks(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_scheduled_processor_tasks(processor_id smallint DEFAULT NULL::smallint) RETURNS json
    LANGUAGE plpgsql
    AS $_$

DECLARE return_string text;

BEGIN

	WITH data(proc_name,site_name,repeat_type,repeat_on_month_day,priority,first_run_time) AS (



		SELECT  processor.name, 

			site.name,

			scheduled_task.repeat_type,

			scheduled_task.repeat_on_month_day,

			scheduled_task.priority,

			scheduled_task.first_run_time

		FROM site

			JOIN scheduled_task ON site.id = scheduled_task.site_id

			JOIN processor ON scheduled_task.processor_id=processor.id

		WHERE ($1 IS NULL OR processor.ID = $1)

	)

	SELECT array_to_json(array_agg(row_to_json(data)),true) INTO return_string FROM data;

	RETURN return_string::json;

END

$_$;


--
-- Name: sp_get_dashboard_sentinel_tiles(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_sentinel_tiles(site_id smallint) RETURNS json
    LANGUAGE plpgsql
    AS $_$
DECLARE return_string text;
BEGIN
     	WITH data(code,geometry) AS (
                SELECT
                        STS.tile_id,
                        ST_AsGeoJSON(STS.geog)
                FROM shape_tiles_s2 STS
                JOIN site S ON ST_Intersects(S.geog, STS.geog)
                WHERE
                     	S.id = $1
        )

	SELECT array_to_json(array_agg(row_to_json(data)),true) INTO return_string FROM data;
        RETURN return_string::json;
END
$_$;


--
-- Name: sp_get_dashboard_server_resource_data(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_server_resource_data() RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE current_node RECORD;
DECLARE temp_json json;
DECLARE temp_json2 json;
DECLARE cpu_user_history_json json;
DECLARE cpu_system_history_json json;
DECLARE ram_history_json json;
DECLARE swap_history_json json;
DECLARE load_1min_history_json json;
DECLARE load_5min_history_json json;
DECLARE load_15min_history_json json;

DECLARE since timestamp;
BEGIN

	CREATE TEMP TABLE current_nodes (
		name character varying,
		cpu_user_now smallint,
		cpu_user_history json,
		cpu_system_now smallint,
		cpu_system_history json,
		ram_now real,
		ram_available real,
		ram_unit character varying,
		ram_history json,
		swap_now real,
		swap_available real,
		swap_unit character varying,
		swap_history json,
		disk_used real,
		disk_available real,
		disk_unit character varying,
		load_1min real,
		load_5min real,
		load_15min real,
		load_1min_history json,
		load_5min_history json,
		load_15min_history json
		) ON COMMIT DROP;

	-- Get the list of nodes to return the resources for
	INSERT INTO current_nodes (name)
	SELECT DISTINCT	node_name
	FROM node_resource_log ORDER BY node_resource_log.node_name;

	-- Ensure that default values are set for some of the fields
	UPDATE current_nodes
	SET
		cpu_user_now = 0,
		cpu_system_now = 0,
		ram_now = 0,
		ram_available = 0,
		ram_unit = 'GB',
		swap_now = 0,
		swap_available = 0,
		swap_unit = 'GB',
		disk_used = 0,
		disk_available = 0,
		disk_unit = 'GB',
		load_1min = 0,
		load_5min = 0,
		load_15min = 0;

	-- Go through the nodes and compute their data
	FOR current_node IN SELECT * FROM current_nodes ORDER BY name LOOP

		-- First, get the NOW data
		UPDATE current_nodes
		SET
			cpu_user_now = coalesce(current_node_now.cpu_user,0) / 10,
			cpu_system_now = coalesce(current_node_now.cpu_system,0) / 10,
			ram_now = round(coalesce(current_node_now.mem_used_kb,0)::numeric / 1048576::numeric, 2),	-- Convert to GB
			ram_available = round(coalesce(current_node_now.mem_total_kb,0)::numeric / 1048576::numeric, 2),	-- Convert to GB
			ram_unit = 'GB',
			swap_now = round(coalesce(current_node_now.swap_used_kb,0)::numeric / 1048576::numeric, 2),	-- Convert to GB
			swap_available = round(coalesce(current_node_now.swap_total_kb,0)::numeric / 1048576::numeric, 2),	-- Convert to GB
			swap_unit = 'GB',
			disk_used = round(coalesce(current_node_now.disk_used_bytes,0)::numeric / 1073741824::numeric, 2),	-- Convert to GB
			disk_available = round(coalesce(current_node_now.disk_total_bytes,0)::numeric / 1073741824::numeric, 2),	-- Convert to GB
			disk_unit = 'GB',
			load_1min = coalesce(current_node_now.load_avg_1m,0) / 100,
			load_5min = coalesce(current_node_now.load_avg_5m,0) / 100,
			load_15min = coalesce(current_node_now.load_avg_15m,0) / 100
		FROM (SELECT * FROM node_resource_log WHERE node_resource_log.node_name = current_node.name
		AND timestamp >= now() - '1 minute'::interval
		ORDER BY timestamp DESC LIMIT 1) AS current_node_now
		WHERE current_nodes.name = current_node.name;

		-- The history will be shown since:
		since := now() - '15 minutes'::interval;

		-- Next, get the HISTORY data
		SELECT
			array_to_json(array_agg( json_build_array(extract(epoch from resource_history.timestamp)::bigint * 1000, resource_history.cpu_user / 10))),
			array_to_json(array_agg( json_build_array(extract(epoch from resource_history.timestamp)::bigint * 1000, resource_history.cpu_system / 10))),
			array_to_json(array_agg( json_build_array(extract(epoch from resource_history.timestamp)::bigint * 1000, round(resource_history.mem_used_kb::numeric / 1048576::numeric, 2)))),	-- Convert to GB
			array_to_json(array_agg( json_build_array(extract(epoch from resource_history.timestamp)::bigint * 1000, round(resource_history.swap_used_kb::numeric / 1048576::numeric, 2)))),	-- Convert to GB
			array_to_json(array_agg( json_build_array(extract(epoch from resource_history.timestamp)::bigint * 1000, resource_history.load_avg_1m / 100))),
			array_to_json(array_agg( json_build_array(extract(epoch from resource_history.timestamp)::bigint * 1000, resource_history.load_avg_5m / 100))),
			array_to_json(array_agg( json_build_array(extract(epoch from resource_history.timestamp)::bigint * 1000, resource_history.load_avg_15m / 100)))
		INTO
			cpu_user_history_json,
			cpu_system_history_json,
			ram_history_json,
			swap_history_json,
			load_1min_history_json,
			load_5min_history_json,
			load_15min_history_json
		FROM (
			SELECT
			timestamp,
			cpu_user,
			cpu_system,
			mem_used_kb,
			swap_used_kb,
			load_avg_1m,
			load_avg_5m,
			load_avg_15m
			FROM node_resource_log
			WHERE node_resource_log.node_name = current_node.name
			AND node_resource_log.timestamp >= since
			ORDER BY timestamp DESC) resource_history;

		-- Make sure that there are enough entries in the arrays so that the graph is shown as coming from right to left in the first 15 minutes
		cpu_user_history_json := sp_pad_left_json_history_array(cpu_user_history_json, since, '1 minute');
		cpu_system_history_json := sp_pad_left_json_history_array(cpu_system_history_json, since, '1 minute');
		ram_history_json := sp_pad_left_json_history_array(ram_history_json, since, '1 minute');
		swap_history_json := sp_pad_left_json_history_array(swap_history_json, since, '1 minute');
		load_1min_history_json := sp_pad_left_json_history_array(load_1min_history_json, since, '1 minute');
		load_5min_history_json := sp_pad_left_json_history_array(load_5min_history_json, since, '1 minute');
		load_15min_history_json := sp_pad_left_json_history_array(load_15min_history_json, since, '1 minute');

		-- Make sure that there are entries added in the arrays even if there isn't data up to now
		cpu_user_history_json := sp_pad_right_json_history_array(cpu_user_history_json, since, '1 minute');
		cpu_system_history_json := sp_pad_right_json_history_array(cpu_system_history_json, since, '1 minute');
		ram_history_json := sp_pad_right_json_history_array(ram_history_json, since, '1 minute');
		swap_history_json := sp_pad_right_json_history_array(swap_history_json, since, '1 minute');
		load_1min_history_json := sp_pad_right_json_history_array(load_1min_history_json, since, '1 minute');
		load_5min_history_json := sp_pad_right_json_history_array(load_5min_history_json, since, '1 minute');
		load_15min_history_json := sp_pad_right_json_history_array(load_15min_history_json, since, '1 minute');

		UPDATE current_nodes
		SET
			cpu_user_history = cpu_user_history_json,
			cpu_system_history = cpu_system_history_json,
			ram_history = ram_history_json,
			swap_history = swap_history_json,
			load_1min_history = load_1min_history_json,
			load_5min_history = load_5min_history_json,
			load_15min_history = load_15min_history_json
		WHERE current_nodes.name = current_node.name;

	END LOOP;

	SELECT array_to_json(array_agg(row_to_json(current_nodes_details, true))) INTO temp_json
	FROM (SELECT * FROM current_nodes) AS current_nodes_details;

	temp_json2 := json_build_object('server_resources', temp_json);

	RETURN temp_json2;


END;
$$;


--
-- Name: sp_get_dashboard_sites(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_dashboard_sites(site_id smallint DEFAULT NULL::smallint) RETURNS json
    LANGUAGE plpgsql
    AS $_$
DECLARE return_string text;
BEGIN
	WITH data(id,name,short_name) AS (
		SELECT 	S.id, 
			S.name, 
			S.short_name
  		FROM site S
		WHERE
			($1 IS NULL OR S.id = $1)
		ORDER BY S.name
	)

	SELECT array_to_json(array_agg(row_to_json(data)),true) INTO return_string FROM data;
	RETURN return_string::json;
END
$_$;


--
-- Name: sp_get_estimated_number_of_products(smallint, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_estimated_number_of_products(_siteid smallint DEFAULT NULL::smallint, _sentinel_1 boolean DEFAULT false) RETURNS integer
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE total_1 integer;
DECLARE total_2 integer;
DECLARE total_3 integer DEFAULT 0::integer;
BEGIN
	SELECT COALESCE(SUM(product_count),0)  into total_1 FROM downloader_count
	WHERE (_siteid IS NULL OR site_id = _siteid) AND (site_id,last_updated) IN (SELECT site_id,min(last_updated) FROM downloader_count WHERE (_siteid IS NULL OR site_id = _siteid ) AND product_count >=0 AND satellite_id=1  GROUP BY site_id );

	SELECT COALESCE(SUM(product_count),0)  into total_2 FROM downloader_count
	WHERE (_siteid IS NULL OR site_id = _siteid) AND (site_id,last_updated) IN (SELECT site_id,min(last_updated) FROM downloader_count WHERE (_siteid IS NULL OR site_id = _siteid) AND product_count >=0 AND satellite_id=2 GROUP BY site_id );

	IF(_sentinel_1) THEN
		SELECT COALESCE(SUM(product_count),0)  into total_3 FROM downloader_count
		WHERE (_siteid IS NULL OR site_id = _siteid) AND (site_id,last_updated) IN (SELECT site_id,min(last_updated) FROM downloader_count WHERE (_siteid IS NULL OR site_id = _siteid) AND product_count >=0 AND satellite_id=3 GROUP BY site_id );
	END IF;

	RETURN (total_1 + total_2 + total_3) as nr;
END
$$;


--
-- Name: sp_get_intersecting_tiles(integer, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_intersecting_tiles(_satellite_id integer, _tile_id text) RETURNS TABLE(satellite_id integer, tile_id text)
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE _geog GEOGRAPHY;
BEGIN
    CASE _satellite_id
        WHEN 1 THEN
        BEGIN
            _geog := (SELECT geog
                      FROM shape_tiles_s2
                      WHERE shape_tiles_s2.tile_id = _tile_id);
        END;
        WHEN 2 THEN
        BEGIN
            _geog := (SELECT geog
                      FROM shape_tiles_l8
                      WHERE pr = _tile_id :: INT);
        END;
    END CASE;

    RETURN QUERY
        SELECT 1 AS satellite_id,
               shape_tiles_s2.tile_id :: TEXT
        FROM shape_tiles_s2
        WHERE ST_Intersects(shape_tiles_s2.geog, _geog)
        UNION
        SELECT 2 AS satellite_id,
               lpad(shape_tiles_l8.pr :: TEXT, 6, '0')
        FROM shape_tiles_l8
        WHERE ST_Intersects(shape_tiles_l8.geog, _geog)
        ORDER BY satellite_id, tile_id;
END;
$$;


--
-- Name: sp_get_job_history_custom_page(smallint, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_job_history_custom_page(_siteid smallint DEFAULT NULL::smallint, _page integer DEFAULT 1, _rows_per_page integer DEFAULT 20) RETURNS TABLE(id integer, end_timestamp timestamp with time zone, processor character varying, site character varying, status character varying, start_type character varying)
    LANGUAGE plpgsql STABLE
    AS $_$
BEGIN
    RETURN QUERY
        SELECT J.id, J.end_timestamp, P.name, S.name, AST.name, ST.name
		FROM job J
			JOIN processor P ON J.processor_id = P.id
			JOIN site S ON J.site_id = S.id
			JOIN job_start_type ST ON J.start_type_id = ST.id
			JOIN activity_status AST ON J.status_id = AST.id
		WHERE   $1 IS NULL OR site_id = _siteid
	ORDER BY J.submit_timestamp DESC
	OFFSET ($2 - 1) * _rows_per_page LIMIT _rows_per_page;
END
$_$;


--
-- Name: sp_get_job_output(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_job_output(_job_id integer) RETURNS TABLE(step_name character varying, command text, stdout_text character varying, stderr_text character varying, exit_code integer, execution_status smallint)
    LANGUAGE plpgsql STABLE
    AS $$
begin
    return query
        select step.name,
               array_to_string(array_prepend(config.value :: text, array(select json_array_elements_text(json_extract_path(step.parameters, 'arguments')))), ' ') as command,
               step_resource_log.stdout_text,
               step_resource_log.stderr_text,
               step.exit_code,
               step.status_id
        from task
        inner join step on step.task_id = task.id
        left outer join step_resource_log on step_resource_log.step_name = step.name and step_resource_log.task_id = task.id
        left outer join config on config.site_id is null and config.key = 'executor.module.path.' || task.module_short_name
        where task.job_id = _job_id
        order by step.submit_timestamp;
end;
$$;


--
-- Name: sp_get_job_parameters(integer, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_job_parameters(_job_id integer, _prefix character varying DEFAULT NULL::character varying) RETURNS TABLE(key character varying, value character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY
    SELECT config_job.key,
           config_job.value
    FROM config_job
    WHERE config_job.job_id = _job_id
      AND CASE
              WHEN _prefix IS NOT NULL THEN config_job.key like _prefix || '%'
              ELSE 1 = 1
          END;

END;
$$;


--
-- Name: sp_get_job_steps_for_resume(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_job_steps_for_resume(_job_id integer) RETURNS TABLE(task_id integer, module_short_name character varying, step_name character varying, parameters json)
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT task.id AS task_id, task.module_short_name, step.name AS step_name, step.parameters
FROM task INNER JOIN step ON task.id = step.task_id
WHERE task.job_id = _job_id
AND step.status_id = 5;

END;
$$;


--
-- Name: sp_get_job_tasks_by_status(integer, json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_job_tasks_by_status(_job_id integer, _status_list json) RETURNS TABLE(task_id integer)
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT id FROM task
WHERE job_id = _job_id
AND status_id IN (SELECT value::smallint FROM json_array_elements_text(_status_list));

END;
$$;


--
-- Name: sp_get_l1c_products(smallint, json, json, timestamp with time zone, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_l1c_products(site_id smallint DEFAULT NULL::smallint, sat_ids json DEFAULT NULL::json, status_ids json DEFAULT NULL::json, start_time timestamp with time zone DEFAULT NULL::timestamp with time zone, end_time timestamp with time zone DEFAULT NULL::timestamp with time zone) RETURNS TABLE("ProductId" integer, "Name" character varying, "SiteId" smallint, full_path character varying, "SatelliteId" smallint, "StatusId" smallint, product_date timestamp with time zone, created_timestamp timestamp with time zone)
    LANGUAGE plpgsql STABLE
    AS $_$
DECLARE q text;
BEGIN
    q := $sql$
    WITH site_names(id, name, row) AS (
            select id, name, row_number() over (order by name)
            from site
        ),
        satellite_ids(id, name, row) AS (
            select id, satellite_name, row_number() over (order by satellite_name)
            from satellite
        )
	  	SELECT P.id AS ProductId,
			P.product_name AS Name,
            P.site_id as SiteId,
            P.full_path,
            P.satellite_id as SatelliteId,
            P.status_id as StatusId,
            P.product_date,
            P.created_timestamp
  		FROM downloader_history P
            JOIN satellite_ids SAT ON P.satellite_id = SAT.id
            JOIN site_names S ON P.site_id = S.id
    	WHERE TRUE$sql$;

    IF NULLIF($1, -1) IS NOT NULL THEN
        q := q || $sql$
            AND P.site_id = $1$sql$;
    END IF;
    IF $2 IS NOT NULL THEN
        q := q || $sql$
            AND P.satellite_id IN (SELECT value::smallint FROM json_array_elements_text($2))
        $sql$;
    END IF;
    IF $3 IS NOT NULL THEN
        q := q || $sql$
            AND P.status_id IN (SELECT value::smallint FROM json_array_elements_text($3))
        $sql$;
    END IF;
    IF $4 IS NOT NULL THEN
        q := q || $sql$
            AND P.product_date >= $4$sql$;
    END IF;
    IF $5 IS NOT NULL THEN
        q := q || $sql$
            AND P.product_date <= $5$sql$;
    END IF;
    q := q || $SQL$
        ORDER BY S.row, SAT.row, P.product_name;$SQL$;

    -- raise notice '%', q;
    
	RETURN QUERY
        EXECUTE q
        USING $1, $2, $3, $4, $5;
END
$_$;


--
-- Name: sp_get_last_l2a_product(smallint, character varying, integer, integer, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_last_l2a_product(_site_id smallint, _tile_id character varying, _satellite_id integer, _orbit_id integer, _l1c_date timestamp with time zone) RETURNS TABLE(path character varying, date timestamp with time zone)
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
    RETURN QUERY
        SELECT product.full_path path,
               product.created_timestamp "date"
        FROM product
        WHERE product.site_id = _site_id
          AND product.product_type_id = 1   -- only L2A
          AND product.satellite_id = _satellite_id
          AND product.created_timestamp < _l1c_date
          AND product.tiles @> ARRAY[_tile_id]
          AND (_satellite_id <> 1 -- sentinel2
            OR product.orbit_id = _orbit_id)
        ORDER BY product.created_timestamp DESC
        LIMIT 1;
END;
$$;


--
-- Name: sp_get_new_events(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_new_events() RETURNS TABLE(id integer, type_id smallint, data json, submitted_timestamp timestamp with time zone, processing_started_timestamp timestamp with time zone)
    LANGUAGE plpgsql
    AS $$
BEGIN

	RETURN QUERY SELECT event.id, event.type_id, event.data, event.submitted_timestamp,
	event.processing_started_timestamp
	FROM event WHERE event.processing_completed_timestamp IS NULL;

END;
$$;


--
-- Name: sp_get_parameters(character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_parameters(_prefix character varying DEFAULT NULL::character varying) RETURNS TABLE(key character varying, site_id smallint, value character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT config.key, config.site_id, 
			CASE WHEN _prefix IN ('downloader.summer-season.start', 'downloader.winter-season.start')
				THEN CASE WHEN CAST(left(config.value, 2) AS integer) <= CAST(config2.value AS integer)
						THEN trim(to_char(12 + CAST(left(config.value, 2) AS integer) - CAST(config2.value AS integer), '00'))||'01'
						ELSE trim(to_char(CAST(left(config.value, 2) AS integer) - CAST(config2.value AS integer), '00'))||'01' END
				ELSE config.value
			END AS value
			FROM config, config AS config2
			WHERE config2.key = 'downloader.start.offset'
				AND CASE WHEN _prefix IS NOT NULL THEN config.key like _prefix || '%' ELSE 1 = 1 END
		ORDER BY config.key;

END;
$$;


--
-- Name: sp_get_processors(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_processors() RETURNS TABLE(id smallint, short_name character varying, name character varying)
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
    RETURN QUERY
        SELECT processor.id,
               processor.short_name,
               processor.name
        FROM processor
        ORDER BY processor.id;
END
$$;


--
-- Name: sp_get_product_by_id(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_product_by_id(_id integer) RETURNS TABLE(product_id integer, product_type_id smallint, processor_id smallint, site_id smallint, full_path character varying, created_timestamp timestamp with time zone, inserted_timestamp timestamp with time zone)
    LANGUAGE plpgsql
    AS $$
                BEGIN
                    RETURN QUERY SELECT product.id AS product_id, product.product_type_id, product.processor_id, product.site_id, product.full_path, product.created_timestamp, product.inserted_timestamp 
                    FROM product
                    WHERE product.id = _id;
                END;
                $$;


--
-- Name: sp_get_product_by_name(smallint, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_product_by_name(_site_id smallint, _name character varying) RETURNS TABLE(product_id smallint, product_type_id smallint, processor_id smallint, site_id smallint, full_path character varying, created_timestamp timestamp with time zone, inserted_timestamp timestamp with time zone)
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT product.product_type_id AS product_id, product.product_type_id, product.processor_id, product.site_id, product.full_path, product.created_timestamp, product.inserted_timestamp 
FROM product
WHERE product.site_id = _site_id AND
      product.name = _name;

END;
$$;


--
-- Name: sp_get_product_types(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_product_types() RETURNS TABLE(id smallint, description character varying, name character varying)
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
    RETURN QUERY
        SELECT product_type.id,
               product_type.description,
               product_type.name
        FROM product_type
        ORDER BY product_type.id;
END
$$;


--
-- Name: sp_get_products(smallint, smallint, timestamp with time zone, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_products(site_id smallint DEFAULT NULL::smallint, product_type_id smallint DEFAULT NULL::smallint, start_time timestamp with time zone DEFAULT NULL::timestamp with time zone, end_time timestamp with time zone DEFAULT NULL::timestamp with time zone) RETURNS TABLE("ProductId" integer, "Product" character varying, "ProductType" character varying, "ProductTypeId" smallint, "Processor" character varying, "ProcessorId" smallint, "Site" character varying, "SiteId" smallint, full_path character varying, quicklook_image character varying, footprint polygon, created_timestamp timestamp with time zone, inserted_timestamp timestamp with time zone)
    LANGUAGE plpgsql STABLE
    AS $_$
DECLARE q text;
BEGIN
    q := $sql$
    WITH site_names(id, name, row) AS (
            select id, name, row_number() over (order by name)
            from site
        ),
        product_type_names(id, name, row) AS (
            select id, name, row_number() over (order by name)
            from product_type
        )
	  	SELECT P.id AS ProductId,
			P.name AS Product,
  			PT.name AS ProductType,
  			P.product_type_id AS ProductTypeId,
            PR.name AS Processor,
            P.processor_id AS ProcessorId,
            S.name AS Site,
            P.site_id AS SiteId,
            P.full_path,
            P.quicklook_image,
            P.footprint,
            P.created_timestamp,
            P.inserted_timestamp
  		FROM product P
            JOIN product_type_names PT ON P.product_type_id = PT.id
            JOIN processor PR ON P.processor_id = PR.id
            JOIN site_names S ON P.site_id = S.id
    	WHERE TRUE$sql$;

    IF NULLIF($1, -1) IS NOT NULL THEN
        q := q || $sql$
            AND P.site_id = $1$sql$;
    END IF;
    IF NULLIF($2, -1) IS NOT NULL THEN
        q := q || $sql$
            AND P.product_type_id = $2$sql$;
    END IF;
    IF $3 IS NOT NULL THEN
        q := q || $sql$
            AND P.created_timestamp >= $3$sql$;
    END IF;
    IF $4 IS NOT NULL THEN
        q := q || $sql$
            AND P.created_timestamp <= $4$sql$;
    END IF;
    q := q || $SQL$
        ORDER BY S.row, PT.row, P.name;$SQL$;

    -- raise notice '%', q;
    
	RETURN QUERY
        EXECUTE q
        USING $1, $2, $3, $4;
END
$_$;


--
-- Name: sp_get_products_area(character varying[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_products_area(VARIADIC products character varying[]) RETURNS double precision
    LANGUAGE plpgsql
    AS $_$DECLARE surf double precision;
BEGIN
	surf := 0;
	IF $1 IS NOT NULL THEN
		FOR i IN 1 .. array_upper($1, 1)
		LOOP
			surf := surf + st_area(geog) FROM product WHERE name = $1[i];
		END LOOP;
	END IF;
	RETURN surf;
END$_$;


--
-- Name: sp_get_products_by_inserted_time(smallint, smallint, timestamp with time zone, timestamp with time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_products_by_inserted_time(site_id smallint DEFAULT NULL::smallint, product_type_id smallint DEFAULT NULL::smallint, start_time timestamp with time zone DEFAULT NULL::timestamp with time zone, end_time timestamp with time zone DEFAULT NULL::timestamp with time zone) RETURNS TABLE("ProductId" integer, "Product" character varying, "ProductType" character varying, "ProductTypeId" smallint, "Processor" character varying, "ProcessorId" smallint, "Site" character varying, "SiteId" smallint, full_path character varying, quicklook_image character varying, footprint polygon, created_timestamp timestamp with time zone, inserted_timestamp timestamp with time zone)
    LANGUAGE plpgsql STABLE
    AS $_$
DECLARE q text;
BEGIN
    q := $sql$
    WITH site_names(id, name, row) AS (
            select id, name, row_number() over (order by name)
            from site
        ),
        product_type_names(id, name, row) AS (
            select id, name, row_number() over (order by name)
            from product_type
        )
	  	SELECT P.id AS ProductId,
			P.name AS Product,
  			PT.name AS ProductType,
  			P.product_type_id AS ProductTypeId,
            PR.name AS Processor,
            P.processor_id AS ProcessorId,
            S.name AS Site,
            P.site_id AS SiteId,
            P.full_path,
            P.quicklook_image,
            P.footprint,
            P.created_timestamp,
            P.inserted_timestamp
  		FROM product P
            JOIN product_type_names PT ON P.product_type_id = PT.id
            JOIN processor PR ON P.processor_id = PR.id
            JOIN site_names S ON P.site_id = S.id
    	WHERE TRUE$sql$;

    IF NULLIF($1, -1) IS NOT NULL THEN
        q := q || $sql$
            AND P.site_id = $1$sql$;
    END IF;
    IF NULLIF($2, -1) IS NOT NULL THEN
        q := q || $sql$
            AND P.product_type_id = $2$sql$;
    END IF;
    IF $3 IS NOT NULL THEN
        q := q || $sql$
            AND P.inserted_timestamp >= $3$sql$;
    END IF;
    IF $4 IS NOT NULL THEN
        q := q || $sql$
            AND P.inserted_timestamp <= $4$sql$;
    END IF;
    q := q || $SQL$
        ORDER BY S.row, PT.row, P.name;$SQL$;

    -- raise notice '%', q;
    
	RETURN QUERY
        EXECUTE q
        USING $1, $2, $3, $4;
END
$_$;


--
-- Name: sp_get_products_for_tile(smallint, character varying, smallint, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_products_for_tile(_site_id smallint, _tile_id character varying, _product_type_id smallint, _satellite_id integer, _out_satellite_id integer) RETURNS TABLE(full_path character varying, product_date timestamp with time zone)
    LANGUAGE plpgsql STABLE
    AS $$
DECLARE _geog GEOGRAPHY;
BEGIN
    CASE _satellite_id
        WHEN 1 THEN -- sentinel2
            _geog := (SELECT shape_tiles_s2.geog FROM shape_tiles_s2 WHERE tile_id = _tile_id);
        WHEN 2 THEN -- landsat8
            _geog := (SELECT shape_tiles_l8 FROM shape_tiles_l8 WHERE shape_tiles_l8.pr = _tile_id :: INT);
    END CASE;

    RETURN QUERY
        SELECT product.full_path,
               product.created_timestamp
        FROM product
        WHERE product.site_id = _site_id AND
              product.satellite_id = _out_satellite_id AND
              product.product_type_id = _product_type_id AND  
              ST_Intersects(product.geog, _geog);
END;
$$;


--
-- Name: sp_get_products_sites(integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_products_sites(_site_ids integer[] DEFAULT NULL::integer[]) RETURNS TABLE(id smallint, name character varying, short_name character varying, enabled boolean)
    LANGUAGE plpgsql STABLE
    AS $$
    BEGIN
    RETURN QUERY
        SELECT S.id,
               S.name,
               S.short_name,
               S.enabled
        FROM site S
        WHERE _site_ids IS NULL
           OR S.id = ANY(_site_ids)
          AND EXISTS (SELECT *
                      FROM product P
                      WHERE P.site_id = S.id)
        ORDER BY S.name;
    END
$$;


--
-- Name: sp_get_products_to_archive(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_products_to_archive() RETURNS TABLE(product_id integer, current_path character varying, archive_path character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN

	CREATE TEMP TABLE settings (
		processor_id smallint,
		product_type_id smallint,
		site_id smallint,
		age smallint,
		archive_path varchar
		) ON COMMIT DROP;

	-- Get the configured 2 main archiver settings (max_age and archive_path), split by site, processor and product
	WITH 
	t1 AS
	(SELECT processor.id as processor_id, product_type.id as product_type_id, params.site_id, cast(params.value as int) as age
		FROM sp_get_parameters('archiver') AS params 
		LEFT OUTER JOIN processor ON params.key ILIKE '%' || processor.short_name || '%' 
		LEFT OUTER JOIN product_type ON params.key ILIKE '%' || product_type.name || '%' 
		WHERE params.key ILIKE '%max_age%'), 
	t2 AS 
	(SELECT processor.id as processor_id, product_type.id as product_type_id, params.site_id, params.value as archive_path
		FROM sp_get_parameters('archiver') AS params 
		LEFT OUTER JOIN processor ON params.key ILIKE '%' || processor.short_name || '%' 
		LEFT OUTER JOIN product_type ON params.key ILIKE '%' || product_type.name || '%' 
		WHERE params.key ILIKE '%archive_path%')
	INSERT INTO settings (
		processor_id,
		product_type_id,
		site_id,
		age,
		archive_path)
	SELECT coalesce(t1.processor_id, t2.processor_id) as processor_id,
		coalesce(t1.product_type_id, t2.product_type_id) as product_type_id,
		coalesce(t1.site_id, t2.site_id) as site_id,
		t1.age,
		t2.archive_path
	FROM t1 INNER JOIN t2
	ON (t1.processor_id = t2.processor_id OR t1.processor_id IS NULL OR t2.processor_id IS NULL)
	AND (t1.product_type_id = t2.product_type_id OR t1.product_type_id IS NULL OR t2.product_type_id IS NULL)
	AND (t1.site_id = t2.site_id OR t1.site_id IS NULL OR t2.site_id IS NULL);

	-- Get the products that have not been archived, determine their new path and return them
	RETURN QUERY
	WITH unarchived_products AS
	(SELECT * FROM product WHERE is_archived = false ORDER BY full_path)
	SELECT 
	unarchived_products.id AS product_id,
	regexp_replace(unarchived_products.full_path, '[/\\]{2,}', '/')::varchar AS current_path,
	sp_build_archive_path(settings.archive_path, site.short_name, processor.short_name, product_type.name, unarchived_products.full_path) AS archive_path
	FROM unarchived_products INNER JOIN settings
	ON (unarchived_products.processor_id = settings.processor_id OR settings.processor_id IS NULL)
	AND (unarchived_products.product_type_id = settings.product_type_id OR settings.product_type_id IS NULL)
	AND (unarchived_products.site_id = settings.site_id OR settings.site_id IS NULL)
	INNER JOIN processor ON unarchived_products.processor_id = processor.id
	INNER JOIN product_type ON unarchived_products.product_type_id = product_type.id
	INNER JOIN site ON unarchived_products.site_id = site.id
	ORDER BY unarchived_products.created_timestamp;

END;
$$;


--
-- Name: sp_get_scheduled_processor_task(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_scheduled_processor_task() RETURNS TABLE(processor character varying, site character varying, repeat_type smallint, repeat_on_month_day smallint, priority smallint, first_run_time character varying)
    LANGUAGE plpgsql STABLE
    AS $$

BEGIN 

	RETURN QUERY 

	SELECT processor.name, site.name,scheduled_task.repeat_type,scheduled_task.repeat_on_month_day,scheduled_task.priority,scheduled_task.first_run_time

	FROM site

	JOIN scheduled_task ON site.id = scheduled_task.site_id

	JOIN processor ON scheduled_task.processor_id=processor.id

	WHERE site.enabled

	ORDER BY site.name;

END

$$;


--
-- Name: sp_get_scheduled_task(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_scheduled_task() RETURNS TABLE(processor character varying, site character varying, repeat_type smallint, repeat_on_month_day smallint, priority smallint, first_run_time character varying)
    LANGUAGE plpgsql STABLE
    AS $$

BEGIN 

	RETURN QUERY 

	SELECT processor.name, site.name,scheduled_task.repeat_type,scheduled_task.repeat_on_month_day,scheduled_task.priority,scheduled_task.first_run_time

	FROM site

	JOIN scheduled_task ON site.id = scheduled_task.site_id

	JOIN processor ON scheduled_task.processor_id=processor.id
	
	WHERE site.enabled

	ORDER BY site.name;

END

$$;


--
-- Name: sp_get_scheduled_tasks(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_scheduled_tasks() RETURNS TABLE(id smallint, name character varying, processor_id smallint, site_id smallint, season_id smallint, processor_params character varying, repeat_type smallint, repeat_after_days smallint, repeat_on_month_day smallint, retry_seconds integer, priority smallint, first_run_time character varying, status_id smallint, next_schedule character varying, last_scheduled_run character varying, last_run_timestamp character varying, last_retry_timestamp character varying, estimated_next_run_time character varying)
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
    RETURN QUERY
        SELECT scheduled_task.id,
               scheduled_task.name,
               scheduled_task.processor_id,
               scheduled_task.site_id,
               scheduled_task.season_id,
               scheduled_task.processor_params,
               scheduled_task.repeat_type,
               scheduled_task.repeat_after_days,
               scheduled_task.repeat_on_month_day,
               scheduled_task.retry_seconds,
               scheduled_task.priority,
               scheduled_task.first_run_time,
               scheduled_task_status.id,
               scheduled_task_status.next_schedule,
               scheduled_task_status.last_scheduled_run,
               scheduled_task_status.last_run_timestamp,
               scheduled_task_status.last_retry_timestamp,
               scheduled_task_status.estimated_next_run_time
        FROM scheduled_task
        INNER JOIN scheduled_task_status ON scheduled_task.id = scheduled_task_status.task_id
        INNER JOIN site on site.id = scheduled_task.site_id
        WHERE site.enabled;
END
$$;


--
-- Name: sp_get_season_scheduled_processors(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_season_scheduled_processors(_season_id smallint) RETURNS TABLE(processor_id smallint, processor_name character varying, processor_short_name character varying)
    LANGUAGE plpgsql STABLE
    AS $$
begin
    return query
        select
            processor.id,
            processor.name,
            processor.short_name
        from processor
        where exists(select *
                     from scheduled_task
                     where scheduled_task.season_id = _season_id
                       and scheduled_task.processor_id = processor.id)
        order by processor.short_name;
end;
$$;


--
-- Name: sp_get_site_seasons(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_site_seasons(_site_id smallint) RETURNS TABLE(id smallint, site_id smallint, name text, start_date date, end_date date, mid_date date, enabled boolean)
    LANGUAGE plpgsql STABLE
    AS $$
begin
    if _site_id is null then
        return query
            select
                season.id,
                season.site_id,
                season.name,
                season.start_date,
                season.end_date,
                season.mid_date,
                season.enabled
            from season
            order by season.site_id, season.start_date;
    else
        return query
            select
                season.id,
                season.site_id,
                season.name,
                season.start_date,
                season.end_date,
                season.mid_date,
                season.enabled
            from season
            where season.site_id = _site_id
            order by season.start_date;
    end if;
end;
$$;


--
-- Name: sp_get_site_tiles(smallint, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_site_tiles(_site_id smallint, _satellite_id integer) RETURNS TABLE(tile_id text)
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
    CASE _satellite_id
        WHEN 1 THEN -- sentinel2
            RETURN QUERY
                WITH geog_tiles AS (
                    SELECT array_agg(shape_tiles_s2.tile_id :: text) as tiles
                    FROM shape_tiles_s2
                    INNER JOIN site ON ST_Intersects(site.geog, shape_tiles_s2.geog)
                    WHERE site.id = _site_id
                )
                SELECT unnest(geog_tiles.tiles)
                FROM geog_tiles
                INTERSECT
                SELECT unnest(COALESCE(site_tiles.tiles, geog_tiles.tiles))
                from geog_tiles
                LEFT OUTER JOIN site_tiles ON site_tiles.site_id = _site_id
                                          AND site_tiles.satellite_id = 1; -- landsat8
        WHEN 2 THEN -- landsat8
            RETURN QUERY
                WITH geog_tiles AS (
                    SELECT array_agg(lpad(shape_tiles_l8.pr :: text, 6, '0')) as tiles
                    FROM shape_tiles_l8
                    INNER JOIN site ON ST_Intersects(site.geog, shape_tiles_l8.geog)
                    WHERE site.id = _site_id
                )
                SELECT unnest(geog_tiles.tiles)
                FROM geog_tiles
                INTERSECT
                SELECT unnest(COALESCE(site_tiles.tiles, geog_tiles.tiles))
                from geog_tiles
                LEFT OUTER JOIN site_tiles ON site_tiles.site_id = _site_id
                                          AND site_tiles.satellite_id = 2; -- landsat8
    END CASE;
END;
$$;


--
-- Name: sp_get_sites(integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_sites(_site_id integer[] DEFAULT NULL::integer[]) RETURNS TABLE(id smallint, name character varying, short_name character varying, enabled boolean)
    LANGUAGE plpgsql
    AS $$
BEGIN
   RETURN QUERY
        SELECT site.id,
               site.name,
               site.short_name,
               site.enabled
        FROM site
        WHERE _site_id IS NULL OR site.id = ANY(_site_id)
        ORDER BY site.name;
END
$$;


--
-- Name: sp_get_task_console_outputs(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_task_console_outputs(_task_id integer) RETURNS TABLE(task_id integer, step_name character varying, stdout_text character varying, stderr_text character varying)
    LANGUAGE plpgsql STABLE
    AS $$
BEGIN
    RETURN QUERY
        SELECT step.task_id,
               step.name,
               step_resource_log.stdout_text,
               step_resource_log.stderr_text
         FROM step
         INNER JOIN step_resource_log ON step_resource_log.task_id = step.task_id
                                     AND step_resource_log.step_name = step.name
         WHERE step.task_id = _task_id;
END;
$$;


--
-- Name: sp_get_task_steps_for_start(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_get_task_steps_for_start(_task_id integer) RETURNS TABLE(task_id integer, module_short_name character varying, step_name character varying, parameters json)
    LANGUAGE plpgsql
    AS $$
BEGIN

RETURN QUERY SELECT task.id AS task_id, task.module_short_name, step.name AS step_name, step.parameters
FROM task INNER JOIN step ON task.id = step.task_id
WHERE task.id = _task_id
AND step.status_id = 1;

END;
$$;


--
-- Name: sp_insert_default_scheduled_tasks(smallint, smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_insert_default_scheduled_tasks(_season_id smallint, _processor_id smallint DEFAULT NULL::smallint) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare _site_id site.id%type;
declare _site_name site.short_name%type;
declare _processor_name processor.short_name%type;
declare _season_name season.name%type;
declare _start_date season.start_date%type;
declare _mid_date season.start_date%type;
begin
    select site.short_name
    into _site_name
    from season
    inner join site on site.id = season.site_id
    where season.id = _season_id;

    select processor.short_name
    into _processor_name
    from processor
    where id = _processor_id;

    if not found then
        raise exception 'Invalid season id %', _season_id;
    end if;

    select site_id,
           name,
           start_date,
           mid_date
    into _site_id,
         _season_name,
         _start_date,
         _mid_date
    from season
    where id = _season_id;

	if _processor_id is null or (_processor_id = 2 and _processor_name = 'l3a') then
        perform sp_insert_scheduled_task(
                    _site_name || '_' || _season_name || '_L3A' :: character varying,
                    2,
                    _site_id :: int,
                    _season_id :: int,
                    2::smallint,
                    0::smallint,
                    31::smallint,
                    cast((select date_trunc('month', _start_date) + interval '1 month' - interval '1 day') as character varying),
                    60,
                    1 :: smallint,
                    '{}' :: json);
    end if;

	if _processor_id is null or (_processor_id = 3 and (_processor_name = 'l3b_lai' or _processor_name = 'l3b'))  then
        perform sp_insert_scheduled_task(
                    _site_name || '_' || _season_name || '_L3B' :: character varying,
                    3,
                    _site_id :: int,
                    _season_id :: int,
                    1::smallint,
                    1::smallint,
                    0::smallint,
                    cast((_start_date + 1) as character varying),
                    60,
                    1 :: smallint,
                    '{"general_params":{"product_type":"L3B"}}' :: json);
    end if;

	if _processor_id is null or (_processor_id = 5 and _processor_name = 'l4a') then
        perform sp_insert_scheduled_task(
                    _site_name || '_' || _season_name || '_L4A' :: character varying,
                    5,
                    _site_id :: int,
                    _season_id :: int,
                    2::smallint,
                    0::smallint,
                    31::smallint,
                    cast(_mid_date as character varying),
                    60,
                    1 :: smallint,
                    '{}' :: json);
    end if;

	if _processor_id is null or (_processor_id = 6 and _processor_name = 'l4b') then
        perform sp_insert_scheduled_task(
                    _site_name || '_' || _season_name || '_L4B' :: character varying,
                    6,
                    _site_id :: int,
                    _season_id :: int,
                    2::smallint,
                    0::smallint,
                    31::smallint,
                    cast(_mid_date as character varying),
                    60,
                    1 :: smallint,
                    '{}' :: json);
    end if;
    
  	if _processor_id is null or _processor_name = 's4c_l4a' then
        perform sp_insert_scheduled_task(
                    _site_name || '_' || _season_name || '_S4C_L4A' :: character varying,
                    9,
                    _site_id :: int,
                    _season_id :: int,
                    2::smallint,
                    0::smallint,
                    31::smallint,
                    cast(_mid_date as character varying),
                    60,
                    1 :: smallint,
                    '{}' :: json);
    end if;

  	if _processor_id is null or _processor_name = 's4c_l4b' then
        perform sp_insert_scheduled_task(
                    _site_name || '_' || _season_name || '_S4C_L4B' :: character varying,
                    10,
                    _site_id :: int,
                    _season_id :: int,
                    2::smallint,
                    0::smallint,
                    31::smallint,
                    cast((_start_date + 31) as character varying),
                    60,
                    1 :: smallint,
                    '{}' :: json);
    end if;

  	if _processor_id is null or _processor_name = 's4c_l4c' then
        perform sp_insert_scheduled_task(
                    _site_name || '_' || _season_name || '_S4C_L4C' :: character varying,
                    11,
                    _site_id :: int,
                    _season_id :: int,
                    1::smallint,
                    7::smallint,
                    0::smallint,
                    cast((_start_date + 7) as character varying),
                    60,
                    1 :: smallint,
                    '{}' :: json);
    end if;

    if _processor_id is null or (_processor_id = 14 and _processor_name = 's4c_mdb1')  then
        perform sp_insert_scheduled_task(
                    _site_name || '_' || _season_name || '_S4C_MDB1' :: character varying,
                    14,
                    _site_id :: int,
                    _season_id :: int,
                    1::smallint,
                    1::smallint,
                    0::smallint,
                    cast((_start_date + 1) as character varying),
                    60,
                    1 :: smallint,
                    '{}' :: json);
    end if;

    if _processor_id is not null and _processor_id not in (2, 3, 5, 6, 9, 10, 11, 12, 13, 14) then
        raise exception 'No default jobs defined for processor id %', _processor_id;
    end if;

end;
$$;


--
-- Name: sp_insert_event(integer, json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_insert_event(_type_id integer, _data json) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE _return_id int;
BEGIN

	INSERT INTO event(
	type_id, 
	data)
	VALUES (
	_type_id,
	_data
	) RETURNING id INTO _return_id;

	RETURN _return_id;

END;
$$;


--
-- Name: sp_insert_node_statistics(character varying, smallint, smallint, integer, integer, integer, integer, integer, integer, integer, bigint, bigint, timestamp without time zone); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_insert_node_statistics(_node_name character varying, _cpu_user smallint, _cpu_system smallint, _mem_total_kb integer, _mem_used_kb integer, _swap_total_kb integer, _swap_used_kb integer, _load_avg_1m integer, _load_avg_5m integer, _load_avg_15m integer, _disk_total_bytes bigint, _disk_used_bytes bigint, _timestamp timestamp without time zone) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO node_resource_log(
        node_name,
        cpu_user,
        cpu_system,
        mem_total_kb,
        mem_used_kb,
        swap_total_kb,
        swap_used_kb,
        load_avg_1m,
        load_avg_5m,
        load_avg_15m,
        disk_total_bytes,
        disk_used_bytes,
        "timestamp"
    )
    VALUES (
        _node_name,
        _cpu_user,
        _cpu_system,
        _mem_total_kb,
        _mem_used_kb,
        _swap_total_kb,
        _swap_used_kb,
        _load_avg_1m,
        _load_avg_5m,
        _load_avg_15m,
        _disk_total_bytes,
        _disk_used_bytes,
        now()
    );
END;
$$;


--
-- Name: sp_insert_processor(character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_insert_processor(_name character varying, _description character varying, _short_name character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE _return_id int;
BEGIN

	INSERT INTO processor(
	name,
	description,
	short_name)
	VALUES (
	_name,
	_description,
	_short_name
	) RETURNING id INTO _return_id;

	RETURN _return_id;

END;
$$;


--
-- Name: sp_insert_product(smallint, smallint, integer, smallint, integer, character varying, timestamp with time zone, character varying, character varying, public.geography, integer, json, smallint, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_insert_product(_product_type_id smallint, _processor_id smallint, _satellite_id integer, _site_id smallint, _job_id integer, _full_path character varying, _created_timestamp timestamp with time zone, _name character varying, _quicklook_image character varying, _footprint public.geography, _orbit_id integer, _tiles json, _orbit_type_id smallint DEFAULT NULL::smallint, _downloader_history_id integer DEFAULT NULL::integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE return_id product.id%TYPE;
BEGIN
    UPDATE product
    SET job_id = _job_id,
        full_path = _full_path,
        created_timestamp = _created_timestamp,
        quicklook_image = _quicklook_image,
        footprint = (SELECT '(' || string_agg(REPLACE(replace(ST_AsText(geom) :: text, 'POINT', ''), ' ', ','), ',') || ')'
                     from ST_DumpPoints(ST_Envelope(_footprint :: geometry))
                     WHERE path[2] IN (1, 3)) :: POLYGON,
        geog = _footprint,
        tiles = array(select tile :: character varying from json_array_elements_text(_tiles) tile),
        is_archived = FALSE
    WHERE product_type_id = _product_type_id
      AND processor_id = _processor_id
      AND satellite_id = _satellite_id
      AND site_id = _site_id
      AND COALESCE(orbit_id, 0) = COALESCE(_orbit_id, 0)
      AND "name" = _name
    RETURNING id INTO return_id;

    IF NOT FOUND THEN
        INSERT INTO product(
            product_type_id,
            processor_id,
            satellite_id,
            job_id,
            site_id,
            full_path,
            created_timestamp,
            "name",
            quicklook_image,
            footprint,
            geog,
            orbit_id,
            tiles,
            orbit_type_id,
            downloader_history_id
        )
        VALUES (
            _product_type_id,
            _processor_id,
            _satellite_id,
            _job_id,
            _site_id,
            _full_path,
            COALESCE(_created_timestamp, now()),
            _name,
            _quicklook_image,
            (SELECT '(' || string_agg(REPLACE(replace(ST_AsText(geom) :: text, 'POINT', ''), ' ', ','), ',') || ')'
             from ST_DumpPoints(ST_Envelope(_footprint :: geometry))
             WHERE path[2] IN (1, 3)) :: POLYGON,
             _footprint,
             _orbit_id,
            array(select tile :: character varying from json_array_elements_text(_tiles) tile),
            _orbit_type_id,
            _downloader_history_id
        )
        RETURNING id INTO return_id;
        
        INSERT INTO event(
            type_id,
            data,
            submitted_timestamp)
            VALUES (
            3, -- "ProductAvailable"
            ('{"product_id":' || return_id || '}') :: json,
            now()
        );
    END IF;

	RETURN return_id;
END;
$$;


--
-- Name: sp_insert_product_provenance(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_insert_product_provenance(_product_id integer, _parent_product_id integer, _parent_product_date integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
    insert into product_provenance(product_id, parent_product_id, parent_product_date)
    values (_product_id, _parent_product_id, _parent_product_date);
end;
$$;


--
-- Name: sp_insert_scheduled_task(character varying, integer, integer, integer, smallint, smallint, smallint, character varying, integer, smallint, json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_insert_scheduled_task(_name character varying, _processor_id integer, _site_id integer, _season_id integer, _repeat_type smallint, _repeat_after_days smallint, _repeat_on_month_day smallint, _first_run_time character varying, _retry_seconds integer, _priority smallint, _processor_params json) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE _return_id int;
BEGIN

	INSERT INTO scheduled_task(
		name,
		processor_id,
		site_id,
		season_id,
		repeat_type,
		repeat_after_days,
		repeat_on_month_day,
		first_run_time,
		retry_seconds,
		priority,
		processor_params)
	VALUES (
		_name,
		_processor_id,
		_site_id,
		_season_id,
		_repeat_type,
		_repeat_after_days,
		_repeat_on_month_day,
		_first_run_time,
		_retry_seconds,
		_priority,
		_processor_params
	) RETURNING id INTO _return_id;

	INSERT INTO scheduled_task_status(
		task_id,
		next_schedule,
		last_scheduled_run,
		last_run_timestamp,
		last_retry_timestamp,
		estimated_next_run_time)
	VALUES (
		_return_id,
		_first_run_time,
		'0',
		'0',
		'0',
		'0'
	);

	RETURN _return_id;

END;
$$;


--
-- Name: sp_insert_season(smallint, text, date, date, date, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_insert_season(_site_id smallint, _name text, _start_date date, _end_date date, _mid_date date, _enabled boolean) RETURNS smallint
    LANGUAGE plpgsql
    AS $$
declare _season_id season.id%type;
begin
    insert into season(
        site_id,
        name,
        start_date,
        end_date,
        mid_date,
        enabled
    ) values (
        _site_id,
        _name,
        _start_date,
        _end_date,
        _mid_date,
        _enabled
    )
    returning season.id
    into _season_id;

    return _season_id;
end;
$$;


--
-- Name: sp_insert_update_user(character varying, character varying, smallint, smallint, integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_insert_update_user(_username character varying, _email character varying, _id smallint DEFAULT NULL::smallint, _roleid smallint DEFAULT 2, _siteid integer[] DEFAULT NULL::integer[]) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

    IF _id IS NOT NULL THEN
        UPDATE public.user
	    SET login = _username,
	    email = _email,
	    role_id = _roleid,
	    site_id = _siteid
	    WHERE id = _id ;
    ELSE 
	    INSERT INTO public.user (id, login, email, role_id, site_id)
	    VALUES (COALESCE((SELECT MAX(id) FROM public.user) :: integer, 0) + 1, 
	    _username, _email, _roleid, _siteid);
    END IF;
END;
$$;


--
-- Name: sp_is_product_stale(integer, integer[]); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_is_product_stale(_product_id integer, _parent_products integer[]) RETURNS boolean
    LANGUAGE plpgsql STABLE
    AS $$
begin
    return exists (
        select pid
        from unnest(_parent_products) as pp(pid)
        where not exists (
            select *
            from product_provenance
            where (product_id, parent_product_id) = (_product_id, pid)
        )
    ) or exists (
        select *
        from product_provenance
        where exists (
            select *
            from product
            where product.id = product_provenance.parent_product_id
              and product.created_timestamp >= product_provenance.parent_product_date
        )
    );
end;
$$;


--
-- Name: sp_mark_event_processing_completed(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_event_processing_completed(_event_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE event
	SET processing_completed_timestamp = now()
	WHERE id = _event_id;

END;
$$;


--
-- Name: sp_mark_event_processing_started(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_event_processing_started(_event_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE event
	SET processing_started_timestamp = now()
	WHERE id = _event_id;

END;
$$;


--
-- Name: sp_mark_fmask_l1_tile_done(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_fmask_l1_tile_done(_downloader_history_id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
begin
	if (select current_setting('transaction_isolation') not ilike 'serializable') then
		raise exception 'Please set the transaction isolation level to serializable.' using errcode = 'UE001';
	end if;

    update fmask_history
    set status_id = 3, -- done
        status_timestamp = now(),
        failed_reason = null
    where (downloader_history_id) = (_downloader_history_id);

    return true;
end;
$$;


--
-- Name: sp_mark_fmask_l1_tile_failed(integer, text, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_fmask_l1_tile_failed(_downloader_history_id integer, _reason text, _should_retry boolean) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
begin
	if (select current_setting('transaction_isolation') not ilike 'serializable') then
		raise exception 'Please set the transaction isolation level to serializable.' using errcode = 'UE001';
	end if;

    update fmask_history
    set status_id = 2, -- failed
        status_timestamp = now(),
        retry_count = case _should_retry
            when true then retry_count + 1
            else 3
        end,
        failed_reason = _reason
    where (downloader_history_id) = (_downloader_history_id);

    return true;
end;
$$;


--
-- Name: sp_mark_job_cancelled(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_job_cancelled(_job_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	IF (SELECT current_setting('transaction_isolation') NOT ILIKE 'REPEATABLE READ') THEN
		RAISE EXCEPTION 'The transaction isolation level has not been set to REPEATABLE READ as expected.' USING ERRCODE = 'UE001';
	END IF;

	UPDATE step
	SET status_id = 7, --Cancelled
	status_timestamp = now()
	FROM task
	WHERE task.id = step.task_id AND task.job_id = _job_id
	AND step.status_id NOT IN (6, 8) -- Finished or failed steps can't be cancelled
	AND step.status_id != 7; -- Prevent resetting the status on serialization error retries.

	UPDATE task
	SET status_id = 7, --Cancelled
	status_timestamp = now()
	WHERE job_id = _job_id
	AND status_id NOT IN (6, 8) -- Finished or failed tasks can't be cancelled
	AND status_id != 7; -- Prevent resetting the status on serialization error retries.

	UPDATE job
	SET status_id = 7, --Cancelled
	status_timestamp = now(),
	end_timestamp = now()
	WHERE id = _job_id
    AND status_id NOT IN (6, 7, 8); -- Finished or failed jobs can't be cancelled
END;
$$;


--
-- Name: sp_mark_job_failed(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_job_failed(_job_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
	-- Remaining tasks should be cancelled; the task that has failed has already been marked as failed.
	UPDATE task
	SET status_id = 7, -- Cancelled
	status_timestamp = now()
	WHERE job_id = _job_id
	AND status_id NOT IN (6, 7, 8); -- Finished, cancelled or failed tasks can't be cancelled

	UPDATE job
	SET status_id = 8, -- Error
	status_timestamp = now(),
	end_timestamp = now()
	WHERE id = _job_id;

END;
$$;


--
-- Name: sp_mark_job_finished(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_job_finished(_job_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE job
	SET status_id = 6, --Finished
	status_timestamp = now(),
	end_timestamp = now()
	WHERE id = _job_id; 

END;
$$;


--
-- Name: sp_mark_job_needs_input(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_job_needs_input(_job_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE job
	SET status_id = 3, -- NeedsInput
	status_timestamp = now()
	WHERE id = _job_id;

END;
$$;


--
-- Name: sp_mark_job_paused(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_job_paused(_job_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$

BEGIN

	IF (SELECT current_setting('transaction_isolation') NOT ILIKE 'REPEATABLE READ') THEN
		RAISE EXCEPTION 'The transaction isolation level has not been set to REPEATABLE READ as expected.' USING ERRCODE = 'UE001';
	END IF;

	UPDATE step
	SET status_id = 5, --Paused
	status_timestamp = now()
	FROM task
	WHERE task.id = step.task_id AND task.job_id = _job_id
	AND step.status_id NOT IN (5, 6, 7, 8); -- Finished, cancelled or failed steps can't be paused

	UPDATE task
	SET status_id = 5, --Paused
	status_timestamp = now()
	WHERE job_id = _job_id
	AND status_id NOT IN (5, 6, 7, 8); -- Finished, cancelled or failed tasks can't be paused

	UPDATE job
	SET status_id = 5, --Paused
	status_timestamp = now()
	WHERE id = _job_id
	AND status_id NOT IN (5, 6, 7, 8); -- Finished, cancelled or failed jobs can't be paused

END;
$$;


--
-- Name: sp_mark_job_resumed(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_job_resumed(_job_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE unrunnable_task_ids int[];
DECLARE runnable_task_ids int[];
DECLARE runnable_task_id int;
DECLARE processor_id processor.id%TYPE;
BEGIN

	IF (SELECT current_setting('transaction_isolation') NOT ILIKE 'REPEATABLE READ') THEN
		RAISE EXCEPTION 'The transaction isolation level has not been set to REPEATABLE READ as expected.' USING ERRCODE = 'UE001';
	END IF;

	UPDATE step
	SET status_id = 1, --Submitted
	status_timestamp = now()
	FROM task
	WHERE task.id = step.task_id AND task.job_id = _job_id
	AND step.status_id = 5; --Paused

	-- Get the list of tasks that depended on tasks that have NOT been finished
	SELECT array_agg(task.id) INTO unrunnable_task_ids FROM task
	WHERE task.job_id = _job_id
	  AND task.status_id = 5 --Paused
	  AND EXISTS (SELECT * FROM task AS task2 WHERE task2.id = ANY (task.preceding_task_ids) AND task2.status_id != 6 /*Finished*/ );

	-- Get the list of tasks that depended on tasks that HAVE been finished
	SELECT array_agg(task.id) INTO runnable_task_ids FROM task
    WHERE task.job_id = _job_id
	  AND task.status_id = 5 --Paused
	  AND NOT EXISTS (SELECT * FROM task AS task2 WHERE task2.id = ANY (task.preceding_task_ids) AND task2.status_id != 6 /*Finished*/ );

	-- Update the tasks that CANNOT be started right now
	UPDATE task
	SET status_id = 3, --NeedsInput
	status_timestamp = now()
	WHERE id = ANY (unrunnable_task_ids);

	-- Update the tasks that CAN be started right now
	UPDATE task
	SET status_id = CASE WHEN EXISTS (SELECT * FROM step WHERE step.task_id = task.id AND step.status_id = 6) THEN 4 --Running
			ELSE 1 --Submitted
			END,
	status_timestamp = now()
	WHERE id = ANY (runnable_task_ids);

    processor_id := (SELECT job.processor_id FROM job WHERE id = _job_id);

    IF runnable_task_ids IS NOT NULL THEN
        -- Add events for all the runnable tasks
        FOREACH runnable_task_id IN ARRAY runnable_task_ids
        LOOP
                INSERT INTO event(
                type_id,
                data,
                submitted_timestamp)
                VALUES (
                1, -- TaskRunnable
                ('{"job_id":' || _job_id || ', "processor_id":' || processor_id || ', "task_id":' || runnable_task_id || '}') :: json,
                now());
        END LOOP;
    END IF;

	UPDATE job
	SET status_id = CASE WHEN EXISTS (SELECT * FROM task WHERE task.job_id = job.id AND status_id IN (4,6)) THEN 4 --Running
			ELSE 1 --Submitted
			END,
	status_timestamp = now()
	WHERE id = _job_id
	AND status_id = 5; --Paused

END;
$$;


--
-- Name: sp_mark_l1_tile_done(integer, text, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_l1_tile_done(_downloader_history_id integer, _tile_id text, _cloud_coverage integer, _snow_coverage integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
		begin
			if (select current_setting('transaction_isolation') not ilike 'serializable') then
				raise exception 'Please set the transaction isolation level to serializable.' using errcode = 'UE001';
			end if;

			update l1_tile_history
			set status_id = 3, -- done
				status_timestamp = now(),
				failed_reason = null,
				cloud_coverage = _cloud_coverage,
				snow_coverage = _snow_coverage
			where (downloader_history_id, tile_id) = (_downloader_history_id, _tile_id);

			return sp_update_l1_tile_status(_downloader_history_id);
		end;
		$$;


--
-- Name: sp_mark_l1_tile_failed(integer, text, text, boolean, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_l1_tile_failed(_downloader_history_id integer, _tile_id text, _reason text, _should_retry boolean, _cloud_coverage integer, _snow_coverage integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
		begin
			if (select current_setting('transaction_isolation') not ilike 'serializable') then
				raise exception 'Please set the transaction isolation level to serializable.' using errcode = 'UE001';
			end if;

			update l1_tile_history
			set status_id = 2, -- failed
				status_timestamp = now(),
				retry_count = case _should_retry
					when true then retry_count + 1
					else (
							select
								coalesce(
									(
										select value
										from config
										where key = 'processor.l2a.optical.max-retries'
										and site_id = (
											select site_id
											from downloader_history
											where id = _downloader_history_id)
									), (
										select value
										from config
										where key = 'processor.l2a.optical.max-retries'
										and site_id is null
									)
								) :: int
						) + 1
				end,
				failed_reason = _reason,
				cloud_coverage = _cloud_coverage,
				snow_coverage = _snow_coverage
			where (downloader_history_id, tile_id) = (_downloader_history_id, _tile_id);

			return sp_update_l1_tile_status(_downloader_history_id);
		end;
		$$;


--
-- Name: sp_mark_products_archived(json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_products_archived(_products json) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	CREATE TEMP TABLE products (
		product_id int,
		archive_path varchar) ON COMMIT DROP;

	-- Parse the JSON and fill the temporary table.
	BEGIN
		INSERT INTO products
		SELECT * FROM json_populate_recordset(null::products, _products);
	EXCEPTION WHEN OTHERS THEN
		RAISE EXCEPTION 'JSON did not match expected format or incorrect values were found. Error: %', SQLERRM USING ERRCODE = 'UE001';
	END;

	-- Update the table
	UPDATE product
	SET 
	full_path = products.archive_path,
	is_archived = true,
	archived_timestamp = now()
	FROM products WHERE product.id = products.product_id;

END;
$$;


--
-- Name: sp_mark_step_failed(integer, character varying, character varying, integer, bigint, bigint, bigint, integer, integer, bigint, bigint, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_step_failed(_task_id integer, _step_name character varying, _node_name character varying, _exit_code integer, _user_cpu_ms bigint, _system_cpu_ms bigint, _duration_ms bigint, _max_rss_kb integer, _max_vm_size_kb integer, _disk_read_b bigint, _disk_write_b bigint, _stdout_text character varying, _stderr_text character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE job_id int;
BEGIN

	IF (SELECT current_setting('transaction_isolation') NOT ILIKE 'REPEATABLE READ') THEN
		RAISE EXCEPTION 'The transaction isolation level has not been set to REPEATABLE READ as expected.' USING ERRCODE = 'UE001';
	END IF;

	UPDATE step
	SET status_id = CASE status_id
                        WHEN 1 THEN 8 -- Submitted -> Error
                        WHEN 2 THEN 8 -- PendingStart -> Error
                        WHEN 4 THEN 8 -- Running -> Error
                        ELSE status_id
                    END,
	end_timestamp = now(),
	status_timestamp = CASE status_id
                           WHEN 1 THEN now()
                           WHEN 2 THEN now()
                           WHEN 4 THEN now()
                           ELSE status_timestamp
                       END,
	exit_code = _exit_code
	WHERE name = _step_name AND task_id = _task_id 
	AND status_id != 8; -- Prevent resetting the status on serialization error retries.

	UPDATE task
	SET status_id = CASE status_id
                        WHEN 1 THEN 8 -- Submitted -> Error
                        WHEN 4 THEN 8 -- Running -> Error
                        ELSE status_id
                    END,
	status_timestamp = CASE status_id
                           WHEN 1 THEN now()
                           WHEN 4 THEN now()
                           ELSE status_timestamp
                       END
	WHERE id = _task_id
	AND status_id != 8; -- Prevent resetting the status on serialization error retries.

	-- Make sure the statistics are inserted only once.
	IF NOT EXISTS (SELECT * FROM step_resource_log WHERE step_name = _step_name AND task_id = _task_id) THEN
		INSERT INTO step_resource_log(
		step_name, 
		task_id, 
		node_name, 
		entry_timestamp, 
		duration_ms, 
		user_cpu_ms, 
		system_cpu_ms, 
		max_rss_kb, 
		max_vm_size_kb, 
		disk_read_b, 
		disk_write_b,
		stdout_text,
		stderr_text)
		VALUES (
		_step_name, 
		_task_id, 
		_node_name, 
		now(), 
		_duration_ms, 
		_user_cpu_ms, 
		_system_cpu_ms, 
		_max_rss_kb, 
		_max_vm_size_kb, 
		_disk_read_b, 
		_disk_write_b,
		_stdout_text,
		_stderr_text);
	END IF;
	
	IF EXISTS (SELECT * FROM step WHERE task_id = _task_id AND name = _step_name AND status_id = 8) THEN
		
		SELECT task.job_id INTO job_id FROM task WHERE task.id = _task_id;
	
		INSERT INTO event(
		type_id, 
		data, 
		submitted_timestamp)
		VALUES (
		8, -- StepFailed
		('{"job_id":' || job_id || ',"task_id":' || _task_id || ',"step_name":"' || _step_name || '"}') :: json,
		now()
		);
	END IF;

END;
$$;


--
-- Name: sp_mark_step_finished(integer, character varying, character varying, integer, bigint, bigint, bigint, integer, integer, bigint, bigint, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_step_finished(_task_id integer, _step_name character varying, _node_name character varying, _exit_code integer, _user_cpu_ms bigint, _system_cpu_ms bigint, _duration_ms bigint, _max_rss_kb integer, _max_vm_size_kb integer, _disk_read_b bigint, _disk_write_b bigint, _stdout_text character varying, _stderr_text character varying) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE runnable_task_ids int[];
DECLARE runnable_task_id int;
BEGIN

	IF (SELECT current_setting('transaction_isolation') NOT ILIKE 'REPEATABLE READ') THEN
		RAISE EXCEPTION 'The transaction isolation level has not been set to REPEATABLE READ as expected.' USING ERRCODE = 'UE001';
	END IF;

	UPDATE step
	SET status_id = CASE status_id
                        WHEN 1 THEN 6 -- Submitted -> Finished
                        WHEN 2 THEN 6 -- PendingStart -> Finished
                        WHEN 4 THEN 6 -- Running -> Finished
                        ELSE status_id
                    END,
	end_timestamp = now(),
	status_timestamp = CASE status_id
                           WHEN 1 THEN now()
                           WHEN 2 THEN now()
                           WHEN 4 THEN now()
                           ELSE status_timestamp
                       END,
	exit_code = _exit_code
	WHERE name = _step_name AND task_id = _task_id
	AND status_id != 6; -- Prevent resetting the status on serialization error retries.

	-- Make sure the statistics are inserted only once.
	IF NOT EXISTS (SELECT * FROM step_resource_log WHERE step_name = _step_name AND task_id = _task_id) THEN
		INSERT INTO step_resource_log(
		step_name,
		task_id,
		node_name,
		entry_timestamp,
		duration_ms,
		user_cpu_ms,
		system_cpu_ms,
		max_rss_kb,
		max_vm_size_kb,
		disk_read_b,
		disk_write_b,
		stdout_text,
		stderr_text)
		VALUES (
		_step_name,
		_task_id,
		_node_name,
		now(),
		_duration_ms,
		_user_cpu_ms,
		_system_cpu_ms,
		_max_rss_kb,
		_max_vm_size_kb,
		_disk_read_b,
		_disk_write_b,
		_stdout_text,
		_stderr_text);
	END IF;

	UPDATE task
	SET status_id = CASE status_id
                        WHEN 1 THEN 6 -- Submitted -> Finished
                        WHEN 4 THEN 6 -- Running -> Finished
                        ELSE status_id
                    END,
	status_timestamp = CASE status_id
                           WHEN 1 THEN now()
                           WHEN 4 THEN now()
                           ELSE status_timestamp
                       END
	WHERE id = _task_id
	AND status_id != 6 -- Prevent resetting the status on serialization error retries.
	AND NOT EXISTS (SELECT * FROM step WHERE task_id = _task_id AND status_id != 6); -- Check that all the steps have been finished.

	IF EXISTS (SELECT * FROM task WHERE id = _task_id AND status_id = 6) THEN
		INSERT INTO event(
		type_id,
		data,
		submitted_timestamp)
		SELECT
		2, -- TaskFinished
		('{"job_id":' || job.id || ', "processor_id":' || job.processor_id || ', "site_id":' || job.site_id || ', "task_id":' || _task_id || ', "module_short_name":"' || task.module_short_name || '"}') :: json,
		now()
		FROM job INNER JOIN task ON job.id = task.job_id WHERE task.id = _task_id;

		-- Get the list of tasks that depended on this one and were in status 3 NeedsInput
		WITH tasks_preceded AS (
			SELECT id, preceding_task_ids  FROM task WHERE
			_task_id = ANY(preceding_task_ids)  AND status_id = 3 -- NeedsInput
		)
		-- From the list of tasks determined above, get the list of tasks whose preceding tasks have all been completed and store the ids of the runnable tasks into an array
		SELECT array_agg(tasks_preceded.id) INTO runnable_task_ids FROM tasks_preceded
		WHERE NOT EXISTS (SELECT * FROM task WHERE task.id = ANY (tasks_preceded.preceding_task_ids) AND task.status_id != 6 /*Finished*/ );

		-- Update the tasks that can be run
		UPDATE task SET
			status_id = CASE status_id
                            WHEN 3 THEN 1 -- NeedsInput -> Submitted
                            ELSE status_id
                        END,
			status_timestamp = CASE status_id
                                   WHEN 3 THEN now()
                                   ELSE status_timestamp
                               END
		WHERE task.id = ANY(runnable_task_ids);

        IF runnable_task_ids IS NOT NULL THEN
            -- Add events for all the runnable tasks
            FOREACH runnable_task_id IN ARRAY runnable_task_ids
            LOOP
		    INSERT INTO event(
		    type_id,
		    data,
		    submitted_timestamp)
		    SELECT
		    1, -- TaskRunnable
		    ('{"job_id":' || job_id || ', "processor_id":' || job.processor_id || ', "task_id":' || runnable_task_id || '}') :: json,
		    now()
		    FROM job INNER JOIN task ON job.id = task.job_id WHERE task.id = runnable_task_id;
            END LOOP;
        END IF;

	END IF;

	RETURN EXISTS (SELECT *
	FROM task WHERE id = _task_id AND status_id = 6);

END;
$$;


--
-- Name: sp_mark_step_pending_start(integer, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_step_pending_start(_task_id integer, _step_name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	UPDATE step
	SET status_id = CASE status_id
                        WHEN 1 THEN 2 -- Submitted -> PendingStart
                        ELSE status_id
                    END,
	status_timestamp = CASE status_id
                           WHEN 1 THEN now()
                           ELSE status_timestamp
                       END
	WHERE task_id = _task_id AND name = _step_name;

END;
$$;


--
-- Name: sp_mark_step_started(integer, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_mark_step_started(_task_id integer, _step_name character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
DECLARE return_id int;
BEGIN

	IF (SELECT current_setting('transaction_isolation') NOT ILIKE 'REPEATABLE READ') THEN
		RAISE EXCEPTION 'The transaction isolation level has not been set to REPEATABLE READ as expected.' USING ERRCODE = 'UE001';
	END IF;

	UPDATE step
    SET status_id = CASE status_id
                        WHEN 2 THEN 4 -- PendingStart -> Running
                        ELSE status_id
                    END,
	start_timestamp = now(),
	status_timestamp = CASE status_id
                           WHEN 2 THEN now()
                           ELSE status_timestamp
                       END
	WHERE name = _step_name AND task_id = _task_id
	AND status_id != 4; -- Prevent resetting the status on serialization error retries.

	UPDATE task
	SET status_id = CASE status_id
                        WHEN 1 THEN 4 -- Submitted -> Running
                        ELSE status_id
                    END,
	status_timestamp = CASE status_id
                           WHEN 1 THEN now()
                           ELSE status_timestamp
                       END
	WHERE id = _task_id
	AND status_id != 4; -- Prevent resetting the status on serialization error retries.

	UPDATE job
	SET status_id = CASE job.status_id
                        WHEN 1 THEN 4 -- Submitted -> Running
                        ELSE job.status_id
                    END,
	status_timestamp = CASE job.status_id
                           WHEN 1 THEN now()
                           ELSE job.status_timestamp
                       END
	FROM task WHERE job.id = task.job_id
	AND job.status_id != 4; -- Prevent resetting the status on serialization error retries.

END;
$$;


--
-- Name: sp_pad_left_json_history_array(json, timestamp without time zone, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_pad_left_json_history_array(_history json, _since timestamp without time zone, _interval character varying) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE temp_array json[];
DECLARE temp_json json;
DECLARE previous_timestamp timestamp;
BEGIN

	-- Get the array of timestamp - value json pairs
	SELECT array_agg(history_array.value::json) INTO temp_array FROM (SELECT * FROM json_array_elements(_history)) AS history_array;

	-- If the array is not empty, get the oldes timestamp
	IF temp_array IS NULL OR array_length(temp_array,1) = 0 THEN
		previous_timestamp := now();
	ELSE
		previous_timestamp := TIMESTAMP 'epoch' + (temp_array[1]::json->>0)::bigint / 1000 * INTERVAL '1 second';
	END IF;

	-- Add values to the left of the array until the desired "since" timestamp is reached
	LOOP
		-- Compute the new previous timestamp
		previous_timestamp := previous_timestamp - _interval::interval;

		-- If using the new previous timestamp would take the array beyond the since, break
		IF previous_timestamp < _since THEN
			EXIT;  
		END IF;
		
		temp_json := json_build_array(extract(epoch from previous_timestamp)::bigint * 1000, null);
		temp_array := array_prepend(temp_json, temp_array);
	END LOOP;

	temp_json := array_to_json(temp_array);

	RETURN temp_json;
	
END;
$$;


--
-- Name: sp_pad_right_json_history_array(json, timestamp without time zone, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_pad_right_json_history_array(_history json, _since timestamp without time zone, _interval character varying) RETURNS json
    LANGUAGE plpgsql
    AS $$
DECLARE temp_array json[];
DECLARE temp_json json;
DECLARE previous_timestamp timestamp;
DECLARE to_timestamp timestamp;
BEGIN

	-- Get the array of timestamp - value json pairs
	SELECT array_agg(history_array.value::json) INTO temp_array FROM (SELECT * FROM json_array_elements(_history)) AS history_array;

	-- The previous timestamp always starts from now
	previous_timestamp := now();

	-- If the array is not empty, get the newest timestamp; otherwise use _since as the oldest entry to go to
	IF temp_array IS NULL OR array_length(temp_array,1) = 0 THEN
		to_timestamp := _since;
	ELSE
		to_timestamp := TIMESTAMP 'epoch' + (temp_array[array_length(temp_array, 1)]::json->>0)::bigint / 1000 * INTERVAL '1 second';
	END IF;

	-- Add values to the right of the array until the desired "to" timestamp is reached
	LOOP
		-- Compute the new previous timestamp
		previous_timestamp := previous_timestamp - _interval::interval;

		-- If using the new previous timestamp would take the array beyond the to, or beyond the _since, break. This keeps the array from growing larger than needed.
		IF previous_timestamp < to_timestamp OR previous_timestamp < _since THEN
			EXIT;  
		END IF;
		
		temp_json := json_build_array(extract(epoch from previous_timestamp)::bigint * 1000, null);
		temp_array := array_append(temp_array, temp_json);
	END LOOP;

	temp_json := array_to_json(temp_array);

	RETURN temp_json;
	
END;
$$;


--
-- Name: sp_set_user_password(character varying, character varying, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_set_user_password(user_name character varying, email character varying, pwd text) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
  DECLARE user_id smallint;

  BEGIN 
	SELECT id into user_id FROM "user" WHERE "user".login = $1 AND "user".email = $2;

	IF user_id IS NOT NULL THEN
		IF char_length(trim(pwd))>0 THEN

			UPDATE "user"
			     SET password = crypt($3, gen_salt('md5'))
			     WHERE id = user_id ;--AND password = crypt(user_pwd, password);
			RETURN 1;
		ELSE 
			RETURN 0;
		END IF;
	ELSE RETURN 2;
	END IF;
	
  END;
  $_$;


--
-- Name: sp_split_site(smallint); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_split_site(_site_id smallint) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare _name site.name%type;
declare _short_name site.short_name%type;
declare _geog site.geog%type;
declare _enabled site.enabled%type;
declare _tile_id text;
declare _satellite_id satellite.id%type;
declare _new_site_id site.id%type;
begin
    select name,
           short_name,
           geog,
           enabled
    into _name,
         _short_name,
         _geog,
         _enabled
    from site
    where site.id = _site_id;

    if not found then
        raise exception 'Could not find site id %', _site_id;
    end if;

    for _tile_id,
        _satellite_id
    in
        select tile_id, 1
        from sp_get_site_tiles(_site_id, 1)
        union all
        select tile_id, 2
        from sp_get_site_tiles(_site_id, 2)
    loop
        insert into site(name,
                         short_name,
                         geog,
                         enabled)
        values (
            _name || '_' || _tile_id,
            _short_name || '_' || lower(_tile_id),
            _geog,
            _enabled
        )
        returning id
        into _new_site_id;

        insert into site_tiles(site_id,
                               satellite_id,
                               tiles)
        values (
            _new_site_id,
            _satellite_id,
            array[_tile_id]
        );

        insert into season(site_id,
                           name,
                           start_date,
                           end_date,
                           mid_date,
                           enabled)
        select _new_site_id,
               name,
               start_date,
               end_date,
               mid_date,
               enabled
        from season
        where season.site_id = _site_id;
    end loop;
end;
$$;


--
-- Name: sp_start_fmask_l1_tile_processing(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_start_fmask_l1_tile_processing() RETURNS TABLE(site_id integer, satellite_id smallint, downloader_history_id integer, path text)
    LANGUAGE plpgsql
    AS $$
declare _satellite_id smallint;
declare _downloader_history_id int;
declare _path text;
declare _site_id int;
declare _product_date timestamp;
begin
    if (select current_setting('transaction_isolation') not ilike 'serializable') then
        raise exception 'Please set the transaction isolation level to serializable.' using errcode = 'UE001';
    end if;

    select fmask_history.satellite_id,
           fmask_history.downloader_history_id
    into _satellite_id,
         _downloader_history_id
    from fmask_history
    where status_id = 2 -- failed
      and retry_count < 3
      and status_timestamp < now() - interval '1 day'
    order by status_timestamp
    limit 1;

    if found then
        select downloader_history.product_date,
               downloader_history.full_path,
               downloader_history.site_id
        into _product_date,
             _path,
             _site_id
        from downloader_history
        where id = _downloader_history_id;

        update fmask_history
        set status_id = 1, -- processing
            status_timestamp = now()
        where (fmask_history.downloader_history_id) = (_downloader_history_id);
    else
        select distinct
            downloader_history.satellite_id,
            downloader_history.id,
            downloader_history.product_date,
            downloader_history.full_path,
            downloader_history.site_id
        into _satellite_id,
            _downloader_history_id,
            _product_date,
            _path,
            _site_id
        from downloader_history
        inner join site on site.id = downloader_history.site_id
        where not exists (
            select *
            from fmask_history
            where (fmask_history.satellite_id) = (downloader_history.satellite_id)
              and (status_id = 1 or -- processing
                   retry_count < 3 and status_id = 2 -- failed
              )
              or (fmask_history.downloader_history_id) = (downloader_history.id)
        ) and downloader_history.status_id in (2, 5, 7) -- downloaded, processing
        and site.enabled
        and downloader_history.satellite_id in (1, 2) -- sentinel2, landsat8
        order by satellite_id, product_date
        limit 1;

        if found then
            insert into fmask_history (
                satellite_id,
                downloader_history_id,
                status_id
            ) values (
                _satellite_id,
                _downloader_history_id,
                1 -- processing
            );
        end if;
    end if;

    if _downloader_history_id is not null then
        return query
            select _site_id,
                _satellite_id,
                _downloader_history_id,
                _path;
    end if;
end;
$$;


--
-- Name: sp_start_l1_tile_processing(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_start_l1_tile_processing() RETURNS TABLE(site_id integer, satellite_id smallint, orbit_id integer, tile_id text, downloader_history_id integer, path text, prev_l2a_path text)
    LANGUAGE plpgsql
    AS $$
declare _satellite_id smallint;
declare _orbit_id int;
declare _tile_id text;
declare _downloader_history_id int;
declare _path text;
declare _prev_l2a_path text;
declare _site_id int;
declare _product_date timestamp;
begin
    if (select current_setting('transaction_isolation') not ilike 'serializable') then
        raise exception 'Please set the transaction isolation level to serializable.' using errcode = 'UE001';
    end if;

    create temporary table if not exists site_config(
        key,
        site_id,
        value
    ) as
    select
        keys.key,
        site.id,
        config.value
    from site
    cross join (
        values
            ('processor.l2a.s2.implementation'),
            ('processor.l2a.optical.retry-interval'),
            ('processor.l2a.optical.max-retries'),
            ('processor.l2a.optical.num-workers'),
            ('s2.enabled'),
            ('l8.enabled')
    ) as keys(key)
    cross join lateral (
        select
            coalesce((
                select value
                from config
                where key = keys.key
                and config.site_id = site.id
            ), (
                select value
                from config
                where key = keys.key
                and config.site_id is null
            )) as value
    ) config;

    select l1_tile_history.satellite_id,
           l1_tile_history.orbit_id,
           l1_tile_history.tile_id,
           l1_tile_history.downloader_history_id
    into _satellite_id,
         _orbit_id,
         _tile_id,
         _downloader_history_id
    from l1_tile_history
    inner join downloader_history on downloader_history.id = l1_tile_history.downloader_history_id
    inner join site on site.id = downloader_history.site_id
    cross join lateral (
        select
            (
                select value :: int as max_retries
                from site_config
                where site_config.site_id = downloader_history.site_id
                  and key = 'processor.l2a.optical.max-retries'
            ),
            (
                select value :: interval as retry_interval
                from site_config
                where site_config.site_id = downloader_history.site_id
                  and key = 'processor.l2a.optical.retry-interval'
            ),
            (
                select value :: boolean as s2_enabled
                from site_config
                where site_config.site_id = downloader_history.site_id
                  and key = 's2.enabled'
            ),
            (
                select value :: boolean as l8_enabled
                from site_config
                where site_config.site_id = downloader_history.site_id
                  and key = 'l8.enabled'
            )
    ) config
    where l1_tile_history.status_id = 2 -- failed
      and l1_tile_history.retry_count < config.max_retries
      and l1_tile_history.status_timestamp < now() - config.retry_interval
      and case downloader_history.satellite_id
              when 1 then config.s2_enabled
              when 2 then config.l8_enabled
              else false
      end
      and (
          site.enabled
          or exists (
              select *
              from downloader_history
              where downloader_history.status_id = 2 -- downloaded
                and l1_tile_history.tile_id = any(downloader_history.tiles)
                and l1_tile_history.orbit_id = downloader_history.orbit_id
                and exists (
                    select *
                    from site
                    where site.id = downloader_history.site_id
                      and site.enabled
                )
          )
      )
    order by l1_tile_history.status_timestamp
    limit 1;

    if found then
        select downloader_history.product_date,
               downloader_history.full_path,
               downloader_history.site_id
        into _product_date,
             _path,
             _site_id
        from downloader_history
        where id = _downloader_history_id;

        update l1_tile_history
        set status_id = 1, -- processing
            status_timestamp = now()
        where (l1_tile_history.downloader_history_id, l1_tile_history.tile_id) = (_downloader_history_id, _tile_id);
    else
        select distinct
            downloader_history.satellite_id,
            downloader_history.orbit_id,
            tile_ids.tile_id,
            downloader_history.id,
            downloader_history.product_date,
            downloader_history.full_path,
            downloader_history.site_id
        into _satellite_id,
            _orbit_id,
            _tile_id,
            _downloader_history_id,
            _product_date,
            _path,
            _site_id
        from downloader_history
        inner join site on site.id = downloader_history.site_id
        cross join lateral (
                select unnest(tiles) as tile_id
            ) tile_ids
        cross join lateral (
            select
                (
                    select value as l2a_implementation
                    from site_config
                    where site_config.site_id = downloader_history.site_id
                    and key = 'processor.l2a.s2.implementation'
                ),
                (
                    select value :: int as max_retries
                    from site_config
                    where site_config.site_id = downloader_history.site_id
                    and key = 'processor.l2a.optical.max-retries'
                ),
                (
                    select value :: boolean as s2_enabled
                    from site_config
                    where site_config.site_id = downloader_history.site_id
                    and key = 's2.enabled'
                ),
                (
                    select value :: boolean as l8_enabled
                    from site_config
                    where site_config.site_id = downloader_history.site_id
                    and key = 'l8.enabled'
                )
        ) config
        where (
            config.l2a_implementation = 'sen2cor'
            and downloader_history.satellite_id = 1
            or not exists (
                select *
                from l1_tile_history
                where (l1_tile_history.satellite_id,
                    l1_tile_history.orbit_id,
                    l1_tile_history.tile_id) =
                    (downloader_history.satellite_id,
                    downloader_history.orbit_id,
                    tile_ids.tile_id)
                and (status_id = 1 or -- processing
                    retry_count < config.max_retries and status_id = 2 -- failed
                )
            )
        )
        and not exists (
            select *
            from l1_tile_history
            where (l1_tile_history.downloader_history_id, l1_tile_history.tile_id) = (downloader_history.id, tile_ids.tile_id)
        )
        and downloader_history.status_id in (2, 7) -- downloaded, processing
        and site.enabled
        and downloader_history.satellite_id in (1, 2) -- sentinel2, landsat8
        and case downloader_history.satellite_id
                when 1 then config.s2_enabled
                when 2 then config.l8_enabled
                else false
        end
        order by satellite_id,
                orbit_id,
                tile_id,
                product_date
        limit 1;

        if found then
            insert into l1_tile_history (
                satellite_id,
                orbit_id,
                tile_id,
                downloader_history_id,
                status_id
            ) values (
                _satellite_id,
                _orbit_id,
                _tile_id,
                _downloader_history_id,
                1 -- processing
            );

            update downloader_history
            set status_id = 7 -- processing
            where id = _downloader_history_id;
        end if;
    end if;

    if _downloader_history_id is not null then
        select product.full_path
        into _prev_l2a_path
        from product
        where product.site_id = _site_id
          and product.product_type_id = 1 -- l2a
          and product.satellite_id = _satellite_id
          and product.created_timestamp < _product_date
          and product.tiles :: text[] @> array[_tile_id]
          and (product.satellite_id <> 1 -- sentinel2
               or product.orbit_id = _orbit_id)
        order by created_timestamp desc
        limit 1;

        return query
            select _site_id,
                _satellite_id,
                _orbit_id,
                _tile_id,
                _downloader_history_id,
                _path,
                _prev_l2a_path;
    end if;
end;
$$;


--
-- Name: sp_start_l1_tile_processing(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_start_l1_tile_processing(_node_id text) RETURNS TABLE(site_id integer, satellite_id smallint, orbit_id integer, tile_id text, downloader_history_id integer, path text, prev_l2a_path text)
    LANGUAGE plpgsql
    AS $$
		declare _satellite_id smallint;
		declare _orbit_id int;
		declare _tile_id text;
		declare _downloader_history_id int;
		declare _path text;
		declare _prev_l2a_path text;
		declare _site_id int;
		declare _product_date timestamp;
		begin
			if (select current_setting('transaction_isolation') not ilike 'serializable') then
				raise exception 'Please set the transaction isolation level to serializable.' using errcode = 'UE001';
			end if;

			create temporary table if not exists site_config(
				key,
				site_id,
				value
			) as
			select
				keys.key,
				site.id,
				config.value
			from site
			cross join (
				values
					('processor.l2a.s2.implementation'),
					('processor.l2a.optical.retry-interval'),
					('processor.l2a.optical.max-retries'),
					('processor.l2a.optical.num-workers'),
					('s2.enabled'),
					('l8.enabled')
			) as keys(key)
			cross join lateral (
				select
					coalesce((
						select value
						from config
						where key = keys.key
						and config.site_id = site.id
					), (
						select value
						from config
						where key = keys.key
						and config.site_id is null
					)) as value
			) config;

			select l1_tile_history.satellite_id,
				   l1_tile_history.orbit_id,
				   l1_tile_history.tile_id,
				   l1_tile_history.downloader_history_id
			into _satellite_id,
				 _orbit_id,
				 _tile_id,
				 _downloader_history_id
			from l1_tile_history
			inner join downloader_history on downloader_history.id = l1_tile_history.downloader_history_id
			inner join site on site.id = downloader_history.site_id
			cross join lateral (
				select
					(
						select value :: int as max_retries
						from site_config
						where site_config.site_id = downloader_history.site_id
						  and key = 'processor.l2a.optical.max-retries'
					),
					(
						select value :: interval as retry_interval
						from site_config
						where site_config.site_id = downloader_history.site_id
						  and key = 'processor.l2a.optical.retry-interval'
					),
					(
						select value :: boolean as s2_enabled
						from site_config
						where site_config.site_id = downloader_history.site_id
						  and key = 's2.enabled'
					),
					(
						select value :: boolean as l8_enabled
						from site_config
						where site_config.site_id = downloader_history.site_id
						  and key = 'l8.enabled'
					)
			) config
			where l1_tile_history.status_id = 2 -- failed
			  and l1_tile_history.retry_count < config.max_retries
			  and l1_tile_history.status_timestamp < now() - config.retry_interval
			  and case downloader_history.satellite_id
					  when 1 then config.s2_enabled
					  when 2 then config.l8_enabled
					  else false
			  end
			  and (
				  site.enabled
				  or exists (
					  select *
					  from downloader_history
					  where downloader_history.status_id = 2 -- downloaded
						and l1_tile_history.tile_id = any(downloader_history.tiles)
						and l1_tile_history.orbit_id = downloader_history.orbit_id
						and exists (
							select *
							from site
							where site.id = downloader_history.site_id
							  and site.enabled
						)
				  )
			  )
			order by l1_tile_history.status_timestamp
			limit 1;

			if found then
				select downloader_history.product_date,
					   downloader_history.full_path,
					   downloader_history.site_id
				into _product_date,
					 _path,
					 _site_id
				from downloader_history
				where id = _downloader_history_id;

				update l1_tile_history
				set status_id = 1, -- processing
					status_timestamp = now(),
					node_id = _node_id
				where (l1_tile_history.downloader_history_id, l1_tile_history.tile_id) = (_downloader_history_id, _tile_id);
			else
				select distinct
					downloader_history.satellite_id,
					downloader_history.orbit_id,
					tile_ids.tile_id,
					downloader_history.id,
					downloader_history.product_date,
					downloader_history.full_path,
					downloader_history.site_id
				into _satellite_id,
					_orbit_id,
					_tile_id,
					_downloader_history_id,
					_product_date,
					_path,
					_site_id
				from downloader_history
				inner join site on site.id = downloader_history.site_id
				cross join lateral (
						select unnest(tiles) as tile_id
					) tile_ids
				cross join lateral (
					select
						(
							select value as l2a_implementation
							from site_config
							where site_config.site_id = downloader_history.site_id
							and key = 'processor.l2a.s2.implementation'
						),
						(
							select value :: int as max_retries
							from site_config
							where site_config.site_id = downloader_history.site_id
							and key = 'processor.l2a.optical.max-retries'
						),
						(
							select value :: boolean as s2_enabled
							from site_config
							where site_config.site_id = downloader_history.site_id
							and key = 's2.enabled'
						),
						(
							select value :: boolean as l8_enabled
							from site_config
							where site_config.site_id = downloader_history.site_id
							and key = 'l8.enabled'
						)
				) config
				where (
					config.l2a_implementation = 'sen2cor'
					and downloader_history.satellite_id = 1
					or not exists (
						select *
						from l1_tile_history
						where (l1_tile_history.satellite_id,
							l1_tile_history.orbit_id,
							l1_tile_history.tile_id) =
							(downloader_history.satellite_id,
							downloader_history.orbit_id,
							tile_ids.tile_id)
						and (status_id = 1 or -- processing
							retry_count < config.max_retries and status_id = 2 -- failed
						)
					)
				)
				and not exists (
					select *
					from l1_tile_history
					where (l1_tile_history.downloader_history_id, l1_tile_history.tile_id) = (downloader_history.id, tile_ids.tile_id)
				)
				and downloader_history.status_id in (2, 7) -- downloaded, processing
				and site.enabled
				and downloader_history.satellite_id in (1, 2) -- sentinel2, landsat8
				and case downloader_history.satellite_id
						when 1 then config.s2_enabled
						when 2 then config.l8_enabled
						else false
				end
				order by satellite_id,
						orbit_id,
						tile_id,
						product_date
				limit 1;

				if found then
					insert into l1_tile_history (
						satellite_id,
						orbit_id,
						tile_id,
						downloader_history_id,
						status_id,
						node_id
					) values (
						_satellite_id,
						_orbit_id,
						_tile_id,
						_downloader_history_id,
						1, -- processing
						_node_id
					);

					update downloader_history
					set status_id = 7 -- processing
					where id = _downloader_history_id;
				end if;
			end if;

			if _downloader_history_id is not null then
				select product.full_path
				into _prev_l2a_path
				from product
				where product.site_id = _site_id
				  and product.product_type_id = 1 -- l2a
				  and product.satellite_id = _satellite_id
				  and product.created_timestamp < _product_date
				  and product.tiles :: text[] @> array[_tile_id]
				  and (product.satellite_id <> 1 -- sentinel2
					   or product.orbit_id = _orbit_id)
				order by created_timestamp desc
				limit 1;

				return query
					select _site_id,
						_satellite_id,
						_orbit_id,
						_tile_id,
						_downloader_history_id,
						_path,
						_prev_l2a_path;
			end if;
		end;
		$$;


--
-- Name: sp_submit_job(character varying, character varying, smallint, smallint, smallint, json, json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_submit_job(_name character varying, _description character varying, _processor_id smallint, _site_id smallint, _start_type_id smallint, _parameters json, _configuration json) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE return_id int;
BEGIN

	INSERT INTO job(
	processor_id, 
	site_id, 
	start_type_id, 
	parameters, 
	submit_timestamp, 
	status_id, 
	status_timestamp)
	VALUES (
	_processor_id, 
	_site_id, 
	_start_type_id, 
	_parameters, 
	now(), 
	1, -- Submitted
	now()) RETURNING id INTO return_id;

    WITH configCTE AS (
        SELECT "key",
               "value" AS global_value
        FROM config
        WHERE config.site_id IS NULL
    ),
    configValues AS (
        SELECT "key",
               COALESCE((SELECT "value"
                        FROM config
                        WHERE config.key = configCTE.key
                        AND config.site_id = _site_id), global_value) AS "value"
        FROM configCTE
    )
    INSERT INTO config_job(
        "job_id",
        "key",
        "value")
    SELECT return_id,
           "key",
           "value"
    FROM configValues;

    PERFORM
        sp_upsert_job_parameters(return_id, _configuration);

	INSERT INTO event(
	type_id,
	data,
	submitted_timestamp)
	VALUES (
	7, -- JobSubmitted
	('{"job_id":' || return_id || ', "processor_id":' || _processor_id || ', "site_id":' || _site_id || ', "parameters":' || _parameters || '}') :: json,
	now()
	);

	RETURN return_id;

END;
$$;


--
-- Name: sp_submit_scheduled_tasks_statuses(json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_submit_scheduled_tasks_statuses(_statuses json) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
        UPDATE scheduled_task_status SET
		next_schedule = params.next_schedule,
		last_scheduled_run = params.last_scheduled_run,
		last_run_timestamp = params.last_run_timestamp,
		last_retry_timestamp = params.last_retry_timestamp,
		estimated_next_run_time = params.estimated_next_run_time
		
	FROM  json_populate_recordset(null::scheduled_task_status, _statuses) AS params
	
        WHERE scheduled_task_status.id = params.id;

END;
$$;


--
-- Name: sp_submit_steps(json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_submit_steps(_steps json) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN

	CREATE TEMP TABLE steps (
		task_id INT NOT NULL,
		name character varying NOT NULL,
		parameters json) ON COMMIT DROP;

	-- Parse the JSON and fill the temporary table.
	BEGIN
		INSERT INTO steps
		SELECT * FROM json_populate_recordset(null::steps, _steps);
	EXCEPTION WHEN OTHERS THEN
		RAISE EXCEPTION 'JSON did not match expected format or incorrect values were found. Error: %', SQLERRM USING ERRCODE = 'UE001';
	END;

	INSERT INTO step(
	name, 
	task_id, 
	parameters, 
	submit_timestamp, 
	status_id, 
	status_timestamp)
	SELECT 
	name,
	task_id,
	parameters,
	now(), 
	1, -- Submitted
	now()
	FROM steps;

END;
$$;


--
-- Name: sp_submit_task(integer, character varying, json, json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_submit_task(_job_id integer, _module_short_name character varying, _parameters json, _preceding_task_ids json) RETURNS integer
    LANGUAGE plpgsql
    AS $$
DECLARE return_id int;
DECLARE preceding_task_ids int[];
DECLARE status_id smallint;
DECLARE processor_id processor.id%TYPE;
BEGIN

	BEGIN
		SELECT array_agg(value) INTO  preceding_task_ids FROM json_array_elements(_preceding_task_ids);
	EXCEPTION WHEN OTHERS THEN
		RAISE EXCEPTION '_preceding_task_ids JSON did not match expected format or incorrect values were found. Error: %', SQLERRM USING ERRCODE = 'UE001';
	END;

	IF coalesce(array_length(preceding_task_ids, 1),0) = 0 THEN
		status_id := 1; --Submitted
	ELSE
		status_id := 3; --NeedsInput
	END IF;

        processor_id := (SELECT job.processor_id FROM job WHERE id = _job_id);

	INSERT INTO task(
	job_id,
	module_short_name,
	parameters,
	submit_timestamp,
	status_id,
	status_timestamp,
	preceding_task_ids)
	VALUES (
	_job_id,
	_module_short_name,
	_parameters,
	now(),
	status_id,
	now(),
	preceding_task_ids) RETURNING id INTO return_id;

	IF status_id = 1 THEN  -- Submitted
		INSERT INTO event(
		type_id,
		data,
		submitted_timestamp)
		VALUES (
		1, -- TaskRunnable
		('{"job_id":' || _job_id || ', "processor_id":' || processor_id || ', "task_id":' || return_id || '}') :: json,
		now()
		);
	END IF;

	RETURN return_id;

END;
$$;


--
-- Name: sp_update_datasource(smallint, smallint, boolean, smallint, integer, integer, integer, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_update_datasource(_id smallint, _scope smallint, _enabled boolean, _fetch_mode smallint, _max_retries integer, _retry integer, _max_connections integer, _download_path character varying, _local_root character varying, _username character varying, _password character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
	BEGIN
	IF _id IS NOT NULL THEN
		UPDATE datasource
		SET enabled = _enabled,
			scope = _scope,
			fetch_mode = _fetch_mode,
			max_retries = _max_retries,
			retry_interval_minutes = _retry,
			max_connections = _max_connections,
			download_path = _download_path,
			local_root = _local_root,
			username = _username,
			passwrd = _password
		WHERE id = _id;
	END IF;

	END;
$$;


--
-- Name: sp_update_l1_tile_status(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_update_l1_tile_status(_downloader_history_id integer) RETURNS boolean
    LANGUAGE plpgsql
    AS $$
		begin
			if not exists(
				select unnest(tiles)
				from downloader_history
				where id = _downloader_history_id
				except all
				select tile_id
				from l1_tile_history
				where downloader_history_id = _downloader_history_id
				  and (l1_tile_history.status_id = 3 -- done
					or l1_tile_history.status_id = 2 -- failed
						and l1_tile_history.retry_count >= (
							select
								coalesce(
									(
										select value
										from config
										where key = 'processor.l2a.optical.max-retries'
										and site_id = (
											select site_id
											from downloader_history
											where id = _downloader_history_id)
									), (
										select value
										from config
										where key = 'processor.l2a.optical.max-retries'
										and site_id is null
									)
								) :: int
						)
				  )
			) then
				if exists(
					select *
					from l1_tile_history
					where downloader_history_id = _downloader_history_id
					  and status_id = 3 -- done
				) then
					update downloader_history
					set status_id = 5 -- processed
					where id = _downloader_history_id;
				else
					update downloader_history
					set status_id = 6 -- processing_failed
					where id = _downloader_history_id;
				end if;
				return true;
			else
				return false;
			end if;
		end;
		$$;


--
-- Name: sp_update_season(smallint, smallint, text, date, date, date, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_update_season(_id smallint, _site_id smallint, _name text, _start_date date, _end_date date, _mid_date date, _enabled boolean) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
    update season
    set site_id = coalesce(_site_id, site_id),
        name = coalesce(_name, name),
        start_date = coalesce(_start_date, start_date),
        end_date = coalesce(_end_date, end_date),
        mid_date = coalesce(_mid_date, mid_date),
        enabled = coalesce(_enabled, enabled)
    where id = _id;

    if not found then
        raise exception 'Invalid season % for site %', _name, _site_id;
    end if;
end;
$$;


--
-- Name: sp_upsert_job_parameters(integer, json); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_upsert_job_parameters(_job_id integer, _parameters json) RETURNS TABLE(key character varying, error_message character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN

    CREATE TEMP TABLE params (
        "key" CHARACTER VARYING,
        "value" character varying,
        type t_data_type,
        is_valid_error_message character varying) ON COMMIT DROP;

    -- Parse the JSON and fill the temporary table.
    BEGIN
        INSERT INTO params
        SELECT * FROM json_populate_recordset(null::params, _parameters);
    EXCEPTION WHEN OTHERS THEN
        RAISE EXCEPTION 'JSON did not match expected format or incorrect values were found. Error: %', SQLERRM USING ERRCODE = 'UE001';
    END;

    -- Get the parameter types from the main table.
    UPDATE params 
    SET type = config_metadata.type
    FROM config_metadata
    WHERE config_metadata.key = params.key;

    -- Validate the values against the expected data types.
    UPDATE params
    SET is_valid_error_message = sp_validate_data_type_value(value, type);

    -- Use a proper message for the params not found in the config_metadata table
    UPDATE params
    SET is_valid_error_message = 'Parameter not found in config table.'
    WHERE NOT EXISTS (
                  SELECT *
                  FROM config_metadata
                  WHERE config_metadata.key = params.key);

    -- Update the ones that do exist in the current table and are valid
    UPDATE config_job SET
        "value" = params.value,
        last_updated = now()
    FROM params
    WHERE config_job.job_id = _job_id
      AND config_job.key = params.key
      AND params.is_valid_error_message IS NULL;

    -- Insert the ones that do not exist in the current table, that can be found in the main table and are also valid
    INSERT INTO config_job(
        job_id,
        "key",
        "value")
    SELECT
        _job_id,
        params."key",
        params.value
    FROM params
    INNER JOIN config ON params.key = config.key
    WHERE NOT EXISTS (
                  SELECT *
                  FROM config_job
                  WHERE config_job.job_id = _job_id
                    AND config_job.key = params.key)
      AND params.is_valid_error_message IS NULL;

    -- Report any possible errors
    RETURN QUERY
        SELECT params.key,
               params.is_valid_error_message as error_message
        FROM params
        WHERE params.is_valid_error_message IS NOT NULL;
    
END;
$$;


--
-- Name: sp_upsert_parameter(character varying, character varying, public.t_data_type, boolean, character varying, character varying); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_upsert_parameter(_key character varying, _friendly_name character varying, _data_type public.t_data_type, _is_advanced boolean, _config_category character varying, _default_value character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
-- SELECT sp_upsert_parameter('http-listener.root-path', 'Document Root Path', 'directory', TRUE, 'Dashboard', '/srv/sen2agri-dashboard');
DECLARE category_id config_category.id%TYPE;
BEGIN
    SELECT config_category.id
    FROM config_category
    WHERE config_category.name = _config_category
    INTO category_id;

    IF category_id IS NULL THEN
        INSERT INTO config_category("name", display_order)
        VALUES(_config_category, (SELECT MAX(display_order) + 1
                                  FROM config_category))
        RETURNING config_category.id
        INTO category_id;
    END IF;

    PERFORM sp_upsert_parameters(array_to_json(array_agg(row_to_json(t))), TRUE)
    FROM (SELECT _key AS "key",
                 NULL AS site_id,
                 _friendly_name AS friendly_name,
                 _default_value AS "value",
                 _data_type AS "type",
                 _is_advanced AS is_advanced,
                 category_id AS config_category_id) t;
END;
$$;


--
-- Name: sp_upsert_parameters(json, boolean); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_upsert_parameters(_parameters json, _is_admin boolean) RETURNS TABLE(key character varying, error_message character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN

	CREATE TEMP TABLE params (
		key character varying,
		site_id smallint,
		friendly_name character varying,
		value character varying,
		type t_data_type,
		is_advanced boolean,
		config_category_id smallint,
		is_valid_error_message character varying) ON COMMIT DROP;

	-- Parse the JSON and fill the temporary table.
	BEGIN
		INSERT INTO params
		SELECT * FROM json_populate_recordset(null::params, _parameters);
	EXCEPTION WHEN OTHERS THEN
		RAISE EXCEPTION 'JSON did not match expected format or incorrect values were found. Error: %', SQLERRM USING ERRCODE = 'UE001';
	END;

	-- Get the type from the table for those values that already exist.
	UPDATE params 
	SET type = config_metadata.type
	FROM config_metadata
	WHERE params.key = config_metadata.key AND params.type IS NULL;

	-- Validate the values against the expected data type.
	UPDATE params
	SET is_valid_error_message = sp_validate_data_type_value(value, type);

	-- Make sure that integers and floats are formatted corectly.
	UPDATE params
	SET value = sp_convert_data_type_value(value, type)
	WHERE is_valid_error_message IS NULL;

        IF NOT _is_admin THEN
            -- Make sure not to update advanced parameters if the caller is not an admin
            UPDATE params
            SET is_valid_error_message = 'Only an administrator can update this parameter'
            FROM config_metadata
            WHERE params.key = config_metadata.key and config_metadata.is_advanced;
        END IF;

	-- Update the values for the params that do exist, that are not to be deleted and that are valid
	UPDATE config SET
		key = params.key,
		value = params.value,
		last_updated = now()
	FROM  params
        WHERE config.key = params.key AND config.site_id IS NOT DISTINCT FROM params.site_id AND params.value IS NOT NULL AND params.is_valid_error_message IS NULL;

	-- Update the metadata for the params that do exist, that are not to be deleted and that are valid
        UPDATE config_metadata SET
		friendly_name = coalesce(params.friendly_name, config_metadata.friendly_name),
		type = coalesce(params.type, config_metadata.type),
		is_advanced = coalesce(params.is_advanced, config_metadata.is_advanced),
		config_category_id = coalesce(params.config_category_id, config_metadata.config_category_id)
	FROM  params
        WHERE config_metadata.key = params.key AND params.value IS NOT NULL AND params.is_valid_error_message IS NULL;

	-- Insert the values for the params that do not exist, that are not to be deleted and that are valid
	INSERT INTO config(
		key,
		site_id,
		value)
        SELECT 
		params.key,
		params.site_id,
		params.value		
	FROM params
	WHERE params.key IS NOT NULL AND params.value IS NOT NULL AND params.is_valid_error_message IS NULL AND
	NOT EXISTS (SELECT * FROM config WHERE config.key = params.key AND config.site_id IS NOT DISTINCT FROM params.site_id);

	-- Insert the metadat for the params that do not exist, that are not to be deleted and that are valid
	INSERT INTO config_metadata(
		key,
		friendly_name,
		type,
		is_advanced,
		config_category_id)
        SELECT 
		params.key,
		coalesce(params.friendly_name, ''),
		coalesce(params.type, 'string'),
		coalesce(params.is_advanced, false),
		coalesce(params.config_category_id, 1)
	FROM params
	WHERE params.key IS NOT NULL AND params.value IS NOT NULL AND params.is_valid_error_message IS NULL AND
	NOT EXISTS (SELECT * FROM config_metadata WHERE config_metadata.key = params.key);

	-- Delete the values that receive NULL as value since this is the marked for the delete action
	DELETE FROM config
	WHERE id IN (SELECT config.id FROM config INNER JOIN params ON config.key = params.key AND config.site_id IS NOT DISTINCT FROM params.site_id AND params.value IS NULL);

	-- Report any possible errors
	RETURN QUERY SELECT params.key as key, params.is_valid_error_message as error_message FROM params WHERE params.is_valid_error_message IS NOT NULL;
	
END;
$$;


--
-- Name: sp_validate_data_type_value(character varying, public.t_data_type); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.sp_validate_data_type_value(_value character varying, _type public.t_data_type) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
DECLARE boolean_value boolean;
DECLARE date_value date;
DECLARE double_value double precision;
DECLARE bigint_value bigint;
DECLARE varchar_value character varying;
BEGIN

	CASE _type
		WHEN 'bool' THEN
			BEGIN
				boolean_value := _value::boolean;
			EXCEPTION WHEN OTHERS THEN
				RETURN 'Invalid boolean value: '||_value;
			END;
		WHEN 'date' THEN
			BEGIN
				date_value := _value::date;
			EXCEPTION WHEN OTHERS THEN
				RETURN 'Invalid date value: '||_value;
			END;
		WHEN 'float' THEN
			BEGIN
				double_value := _value::double precision;
			EXCEPTION WHEN OTHERS THEN
				RETURN 'Invalid float value: '||_value;
			END;
		WHEN 'int' THEN
			BEGIN
				bigint_value := _value::bigint;
			EXCEPTION WHEN OTHERS THEN
				RETURN 'Invalid integer value: '||_value;
			END;
		WHEN 'file','directory','string' THEN
			RETURN NULL; -- The _value is already a string.
		ELSE
			RETURN 'Data type validation not implemented';
	END CASE;

	RETURN NULL;

END;
$$;


--
-- Name: activity_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.activity_status (
    id smallint NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description character varying
);


--
-- Name: agricultural_practice; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.agricultural_practice (
    id integer NOT NULL,
    name text NOT NULL
);


--
-- Name: component; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.component (
    id smallint NOT NULL,
    name text NOT NULL
);


--
-- Name: config; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.config (
    id integer NOT NULL,
    key character varying NOT NULL,
    site_id smallint,
    value character varying NOT NULL,
    last_updated timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: config_category; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.config_category (
    id smallint NOT NULL,
    name character varying NOT NULL,
    display_order integer DEFAULT 0 NOT NULL,
    allow_per_site_customization boolean DEFAULT true
);


--
-- Name: config_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.config_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.config_id_seq OWNED BY public.config.id;


--
-- Name: config_job; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.config_job (
    job_id integer NOT NULL,
    key character varying NOT NULL,
    value character varying,
    last_updated timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: config_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.config_metadata (
    key character varying NOT NULL,
    friendly_name character varying DEFAULT ''::character varying NOT NULL,
    type public.t_data_type NOT NULL,
    is_advanced boolean DEFAULT false NOT NULL,
    config_category_id smallint NOT NULL,
    is_site_visible boolean DEFAULT false NOT NULL,
    label character varying,
    "values" json
);


--
-- Name: datasource; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.datasource (
    id smallint NOT NULL,
    satellite_id smallint NOT NULL,
    name character varying(50) NOT NULL,
    scope smallint DEFAULT 3 NOT NULL,
    username character varying(100),
    passwrd character varying(100),
    fetch_mode smallint DEFAULT 1 NOT NULL,
    max_retries integer DEFAULT 3 NOT NULL,
    retry_interval_minutes integer DEFAULT 3600 NOT NULL,
    download_path character varying(255),
    specific_params json,
    max_connections integer DEFAULT 1 NOT NULL,
    local_root character varying(255),
    enabled boolean DEFAULT false NOT NULL,
    site_id smallint,
    secondary_datasource_id integer
);


--
-- Name: datasource_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.datasource_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: datasource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.datasource_id_seq OWNED BY public.datasource.id;


--
-- Name: decl_hmathia2020_2020; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.decl_hmathia2020_2020 (
    objectid numeric(10,0),
    crop integer,
    holding character varying(50),
    parcel character varying(50),
    wkb_geometry public.geometry(MultiPolygon,2100),
    ori_id text NOT NULL,
    ori_hold text NOT NULL,
    ori_crop integer NOT NULL,
    "NewID" integer NOT NULL,
    "HoldID" integer,
    "GeomValid" boolean NOT NULL,
    "Duplic" boolean,
    "Overlap" boolean NOT NULL,
    "Area_meters" real NOT NULL,
    "ShapeInd" real,
    "S1Pix" integer DEFAULT 0 NOT NULL,
    "S2Pix" integer DEFAULT 0 NOT NULL,
    is_deleted boolean DEFAULT false,
    inserted_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    updated_timestamp timestamp with time zone,
    geom_change_ratio real
);


--
-- Name: decl_hmathia2022_2022; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.decl_hmathia2022_2022 (
    co_ownersh numeric(1,0),
    activate_e numeric(1,0),
    is_co_cult numeric(1,0),
    is_seed_cu numeric(1,0),
    parcel character varying(50),
    holding character varying(50),
    crop integer,
    wkb_geometry public.geometry(MultiPolygon,2100),
    ori_id text NOT NULL,
    ori_hold text NOT NULL,
    ori_crop integer NOT NULL,
    "NewID" integer NOT NULL,
    "HoldID" integer,
    "GeomValid" boolean NOT NULL,
    "Duplic" boolean,
    "Overlap" boolean NOT NULL,
    "Area_meters" real NOT NULL,
    "ShapeInd" real,
    "S1Pix" integer DEFAULT 0 NOT NULL,
    "S2Pix" integer DEFAULT 0 NOT NULL,
    is_deleted boolean DEFAULT false,
    inserted_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    updated_timestamp timestamp with time zone,
    geom_change_ratio real
);


--
-- Name: decl_mockzone_2021; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.decl_mockzone_2021 (
    objectid numeric(9,0),
    shape_leng numeric(19,11),
    shape_area numeric(19,11),
    parcel character varying(50),
    holding character varying(50),
    crop integer,
    wkb_geometry public.geometry(MultiPolygon,2100),
    ori_id text NOT NULL,
    ori_hold text NOT NULL,
    ori_crop integer NOT NULL,
    "NewID" integer NOT NULL,
    "HoldID" integer,
    "GeomValid" boolean NOT NULL,
    "Duplic" boolean,
    "Overlap" boolean NOT NULL,
    "Area_meters" real NOT NULL,
    "ShapeInd" real,
    "S1Pix" integer DEFAULT 0 NOT NULL,
    "S2Pix" integer DEFAULT 0 NOT NULL,
    is_deleted boolean DEFAULT false,
    inserted_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    updated_timestamp timestamp with time zone,
    geom_change_ratio real
);


--
-- Name: decl_prefs2021_2021; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.decl_prefs2021_2021 (
    objectid numeric(9,0),
    shape_leng numeric(19,11),
    shape_area numeric(19,11),
    parcel character varying(50),
    holding character varying(50),
    crop integer,
    wkb_geometry public.geometry(MultiPolygon,2100),
    ori_id text NOT NULL,
    ori_hold text NOT NULL,
    ori_crop integer NOT NULL,
    "NewID" integer,
    "HoldID" integer,
    "GeomValid" boolean NOT NULL,
    "Duplic" boolean,
    "Overlap" boolean NOT NULL,
    "Area_meters" real NOT NULL,
    "ShapeInd" real,
    "S1Pix" integer DEFAULT 0 NOT NULL,
    "S2Pix" integer DEFAULT 0 NOT NULL,
    is_deleted boolean DEFAULT false,
    inserted_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    updated_timestamp timestamp with time zone,
    geom_change_ratio real
);


--
-- Name: decl_prefs2021_2021_old; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.decl_prefs2021_2021_old (
    objectid numeric(9,0),
    shape_leng numeric(19,11),
    shape_area numeric(19,11),
    parcel character varying(50),
    holding character varying(50),
    crop integer,
    wkb_geometry public.geometry(MultiPolygon,2100),
    ori_id text NOT NULL,
    ori_hold text NOT NULL,
    ori_crop integer NOT NULL,
    "NewID" integer NOT NULL,
    "HoldID" integer,
    "GeomValid" boolean NOT NULL,
    "Duplic" boolean,
    "Overlap" boolean NOT NULL,
    "Area_meters" real NOT NULL,
    "ShapeInd" real,
    "S1Pix" integer DEFAULT 0 NOT NULL,
    "S2Pix" integer DEFAULT 0 NOT NULL,
    is_deleted boolean DEFAULT false,
    inserted_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    updated_timestamp timestamp with time zone,
    geom_change_ratio real
);


--
-- Name: decl_trik_card2022_2022; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.decl_trik_card2022_2022 (
    co_ownersh numeric(1,0),
    activate_e numeric(1,0),
    is_co_cult numeric(1,0),
    is_seed_cu numeric(1,0),
    parcel character varying(50),
    holding character varying(50),
    crop integer,
    wkb_geometry public.geometry(MultiPolygon,2100),
    ori_id text NOT NULL,
    ori_hold text NOT NULL,
    ori_crop integer NOT NULL,
    "NewID" integer NOT NULL,
    "HoldID" integer,
    "GeomValid" boolean NOT NULL,
    "Duplic" boolean,
    "Overlap" boolean NOT NULL,
    "Area_meters" real NOT NULL,
    "ShapeInd" real,
    "S1Pix" integer DEFAULT 0 NOT NULL,
    "S2Pix" integer DEFAULT 0 NOT NULL,
    is_deleted boolean DEFAULT false,
    inserted_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    updated_timestamp timestamp with time zone,
    geom_change_ratio real
);


--
-- Name: decl_trikcard_2021; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.decl_trikcard_2021 (
    objectid numeric(9,0),
    shape_leng numeric(19,11),
    shape_area numeric(19,11),
    parcel character varying(50),
    holding character varying(50),
    crop integer,
    wkb_geometry public.geometry(MultiPolygon,2100),
    ori_id text NOT NULL,
    ori_hold text NOT NULL,
    ori_crop integer NOT NULL,
    "NewID" integer NOT NULL,
    "HoldID" integer,
    "GeomValid" boolean NOT NULL,
    "Duplic" boolean,
    "Overlap" boolean NOT NULL,
    "Area_meters" real NOT NULL,
    "ShapeInd" real,
    "S1Pix" integer DEFAULT 0 NOT NULL,
    "S2Pix" integer DEFAULT 0 NOT NULL,
    is_deleted boolean DEFAULT false,
    inserted_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    updated_timestamp timestamp with time zone,
    geom_change_ratio real
);


--
-- Name: downloader_count; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.downloader_count (
    site_id smallint NOT NULL,
    satellite_id smallint NOT NULL,
    product_count integer NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    last_updated timestamp with time zone DEFAULT now()
);


--
-- Name: downloader_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.downloader_history (
    id integer NOT NULL,
    site_id smallint NOT NULL,
    satellite_id smallint NOT NULL,
    product_name character varying NOT NULL,
    full_path character varying NOT NULL,
    created_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    status_id smallint NOT NULL,
    no_of_retries smallint DEFAULT 0 NOT NULL,
    product_date timestamp with time zone DEFAULT now() NOT NULL,
    orbit_id integer DEFAULT '-1'::integer NOT NULL,
    orbit_type_id smallint,
    status_reason character varying,
    tiles text[],
    footprint public.geography
);


--
-- Name: downloader_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.downloader_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: downloader_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.downloader_history_id_seq OWNED BY public.downloader_history.id;


--
-- Name: downloader_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.downloader_status (
    id integer NOT NULL,
    status_description character varying NOT NULL
);


--
-- Name: event; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.event (
    id integer NOT NULL,
    type_id smallint NOT NULL,
    data json,
    submitted_timestamp timestamp with time zone DEFAULT now(),
    processing_started_timestamp timestamp with time zone,
    processing_completed_timestamp timestamp with time zone
);


--
-- Name: event_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.event_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.event_id_seq OWNED BY public.event.id;


--
-- Name: event_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.event_type (
    id smallint NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description character varying
);


--
-- Name: fmask_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fmask_history (
    satellite_id smallint NOT NULL,
    downloader_history_id integer NOT NULL,
    status_id integer NOT NULL,
    status_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    retry_count integer DEFAULT 0 NOT NULL,
    failed_reason text
);


--
-- Name: job; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.job (
    id integer NOT NULL,
    processor_id smallint NOT NULL,
    site_id smallint NOT NULL,
    start_type_id smallint NOT NULL,
    parameters json,
    submit_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    start_timestamp timestamp with time zone,
    end_timestamp timestamp with time zone,
    status_id smallint NOT NULL,
    status_timestamp timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: job_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.job_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: job_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.job_id_seq OWNED BY public.job.id;


--
-- Name: job_start_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.job_start_type (
    id smallint NOT NULL,
    name character varying NOT NULL,
    description character varying NOT NULL
);


--
-- Name: l1_tile_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.l1_tile_history (
    satellite_id smallint NOT NULL,
    orbit_id integer NOT NULL,
    tile_id text NOT NULL,
    downloader_history_id integer NOT NULL,
    status_id integer NOT NULL,
    status_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    retry_count integer DEFAULT 0 NOT NULL,
    failed_reason text,
    cloud_coverage integer,
    snow_coverage integer,
    node_id text
);


--
-- Name: l1_tile_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.l1_tile_status (
    id smallint NOT NULL,
    description text NOT NULL
);


--
-- Name: l4c_practices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.l4c_practices (
    site_id smallint NOT NULL,
    year integer NOT NULL,
    practice_short_name character varying NOT NULL,
    country character varying NOT NULL,
    orig_id character varying,
    main_crop character varying NOT NULL,
    veg_start character varying NOT NULL,
    h_start character varying NOT NULL,
    h_end character varying NOT NULL,
    practice character varying NOT NULL,
    p_type character varying NOT NULL,
    p_start character varying NOT NULL,
    p_end character varying NOT NULL
);


--
-- Name: log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.log (
    id integer NOT NULL,
    severity smallint NOT NULL,
    component_id smallint NOT NULL,
    date timestamp without time zone DEFAULT now() NOT NULL,
    message text NOT NULL,
    data json,
    acknowledged_date timestamp without time zone
);


--
-- Name: log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.log_id_seq OWNED BY public.log.id;


--
-- Name: lut_hmathia2020_2020; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lut_hmathia2020_2020 (
    ori_crop integer,
    ctnum integer NOT NULL,
    ct character varying,
    lc integer NOT NULL,
    ctnuml4a integer NOT NULL,
    ctl4a character varying,
    ctnumdiv integer NOT NULL,
    ctdiv integer,
    eaa bit(1) NOT NULL,
    al bit(1) NOT NULL,
    pgrass bit(1) NOT NULL,
    tgrass bit(1) NOT NULL,
    fallow bit(1) NOT NULL,
    cwater bit(1) NOT NULL
);


--
-- Name: lut_hmathia2022_2022; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lut_hmathia2022_2022 (
    ori_crop integer,
    ctnum integer NOT NULL,
    ct character varying,
    lc integer NOT NULL,
    ctnuml4a integer NOT NULL,
    ctl4a character varying,
    ctnumdiv integer NOT NULL,
    ctdiv integer,
    eaa bit(1) NOT NULL,
    al bit(1) NOT NULL,
    pgrass bit(1) NOT NULL,
    tgrass bit(1) NOT NULL,
    fallow bit(1) NOT NULL,
    cwater bit(1) NOT NULL
);


--
-- Name: lut_mockzone_2021; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lut_mockzone_2021 (
    ori_crop integer,
    ctnum integer NOT NULL,
    ct character varying,
    lc integer NOT NULL,
    ctnuml4a integer NOT NULL,
    ctl4a character varying,
    ctnumdiv integer NOT NULL,
    ctdiv integer,
    eaa bit(1) NOT NULL,
    al bit(1) NOT NULL,
    pgrass bit(1) NOT NULL,
    tgrass bit(1) NOT NULL,
    fallow bit(1) NOT NULL,
    cwater bit(1) NOT NULL
);


--
-- Name: lut_prefs2021_2021; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lut_prefs2021_2021 (
    ori_crop integer,
    ctnum integer NOT NULL,
    ct character varying,
    lc integer NOT NULL,
    ctnuml4a integer NOT NULL,
    ctl4a character varying,
    ctnumdiv integer NOT NULL,
    ctdiv integer,
    eaa bit(1) NOT NULL,
    al bit(1) NOT NULL,
    pgrass bit(1) NOT NULL,
    tgrass bit(1) NOT NULL,
    fallow bit(1) NOT NULL,
    cwater bit(1) NOT NULL
);


--
-- Name: lut_trik_card2022_2022; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lut_trik_card2022_2022 (
    ori_crop integer,
    ctnum integer NOT NULL,
    ct character varying,
    lc integer NOT NULL,
    ctnuml4a integer NOT NULL,
    ctl4a character varying,
    ctnumdiv integer NOT NULL,
    ctdiv integer,
    eaa bit(1) NOT NULL,
    al bit(1) NOT NULL,
    pgrass bit(1) NOT NULL,
    tgrass bit(1) NOT NULL,
    fallow bit(1) NOT NULL,
    cwater bit(1) NOT NULL
);


--
-- Name: lut_trikcard_2021; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lut_trikcard_2021 (
    ori_crop integer,
    ctnum integer NOT NULL,
    ct character varying,
    lc integer NOT NULL,
    ctnuml4a integer NOT NULL,
    ctl4a character varying,
    ctnumdiv integer NOT NULL,
    ctdiv integer,
    eaa bit(1) NOT NULL,
    al bit(1) NOT NULL,
    pgrass bit(1) NOT NULL,
    tgrass bit(1) NOT NULL,
    fallow bit(1) NOT NULL,
    cwater bit(1) NOT NULL
);


--
-- Name: meta; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.meta (
    version text NOT NULL
);


--
-- Name: module; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.module (
    short_name character varying NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description character varying
);


--
-- Name: node_resource_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.node_resource_log (
    node_name character varying NOT NULL,
    cpu_user smallint NOT NULL,
    cpu_system smallint NOT NULL,
    mem_total_kb integer NOT NULL,
    mem_used_kb integer NOT NULL,
    swap_total_kb integer NOT NULL,
    swap_used_kb integer NOT NULL,
    load_avg_1m integer NOT NULL,
    load_avg_5m integer NOT NULL,
    load_avg_15m integer NOT NULL,
    disk_total_bytes bigint NOT NULL,
    disk_used_bytes bigint NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);


--
-- Name: orbit_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.orbit_type (
    id smallint NOT NULL,
    description text NOT NULL
);


--
-- Name: pd_ap_staging_4995_na; Type: TABLE; Schema: public; Owner: -
--

CREATE UNLOGGED TABLE public.pd_ap_staging_4995_na (
    ogc_fid integer NOT NULL,
    newid character varying,
    "20211030_m1" character varying,
    "20211030_m2" character varying,
    "20211030_m3" character varying,
    "20211030_m4" character varying,
    "20211030_m5" character varying
);


--
-- Name: pd_ap_staging_4995_na_ogc_fid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pd_ap_staging_4995_na_ogc_fid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pd_ap_staging_4995_na_ogc_fid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pd_ap_staging_4995_na_ogc_fid_seq OWNED BY public.pd_ap_staging_4995_na.ogc_fid;


--
-- Name: pd_ap_staging_4998_na; Type: TABLE; Schema: public; Owner: -
--

CREATE UNLOGGED TABLE public.pd_ap_staging_4998_na (
    ogc_fid integer NOT NULL,
    newid character varying,
    "20211030_m1" character varying,
    "20211030_m2" character varying,
    "20211030_m3" character varying,
    "20211030_m4" character varying,
    "20211030_m5" character varying
);


--
-- Name: pd_ap_staging_4998_na_ogc_fid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pd_ap_staging_4998_na_ogc_fid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pd_ap_staging_4998_na_ogc_fid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pd_ap_staging_4998_na_ogc_fid_seq OWNED BY public.pd_ap_staging_4998_na.ogc_fid;


--
-- Name: pd_ap_staging_5000_na; Type: TABLE; Schema: public; Owner: -
--

CREATE UNLOGGED TABLE public.pd_ap_staging_5000_na (
    ogc_fid integer NOT NULL,
    newid character varying,
    "20211030_m1" character varying,
    "20211030_m2" character varying,
    "20211030_m3" character varying,
    "20211030_m4" character varying,
    "20211030_m5" character varying
);


--
-- Name: pd_ap_staging_5000_na_ogc_fid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.pd_ap_staging_5000_na_ogc_fid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: pd_ap_staging_5000_na_ogc_fid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.pd_ap_staging_5000_na_ogc_fid_seq OWNED BY public.pd_ap_staging_5000_na.ogc_fid;


--
-- Name: processor; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.processor (
    id smallint NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description character varying,
    short_name character varying,
    label character varying
);


--
-- Name: product; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.product (
    id integer NOT NULL,
    product_type_id smallint NOT NULL,
    processor_id smallint NOT NULL,
    site_id smallint NOT NULL,
    full_path character varying NOT NULL,
    created_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    inserted_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    is_archived boolean DEFAULT false,
    archived_timestamp timestamp with time zone,
    name character varying(512),
    quicklook_image character varying(512),
    footprint polygon,
    job_id integer,
    geog public.geography,
    satellite_id integer,
    orbit_id integer,
    orbit_type_id smallint,
    tiles character varying[] NOT NULL,
    downloader_history_id integer
);


--
-- Name: product_details_l4a; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.product_details_l4a (
    product_id integer NOT NULL,
    "NewID" integer NOT NULL,
    "CT_decl" integer,
    "CT_pred_1" integer,
    "CT_conf_1" real,
    "CT_pred_2" integer,
    "CT_conf_2" real
);


--
-- Name: product_details_l4c; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.product_details_l4c (
    product_id integer NOT NULL,
    "NewID" integer NOT NULL,
    practice_id integer NOT NULL,
    orig_id text NOT NULL,
    country text NOT NULL,
    year integer NOT NULL,
    main_crop text NOT NULL,
    veg_start text NOT NULL,
    h_start text NOT NULL,
    h_end text NOT NULL,
    practice text NOT NULL,
    p_type text NOT NULL,
    p_start text NOT NULL,
    p_end text NOT NULL,
    l_week text NOT NULL,
    m1 text NOT NULL,
    m2 text NOT NULL,
    m3 text NOT NULL,
    m4 text NOT NULL,
    m5 text NOT NULL,
    h_week text NOT NULL,
    h_w_start text NOT NULL,
    h_w_end text NOT NULL,
    h_w_s1 text NOT NULL,
    m6 text NOT NULL,
    m7 text NOT NULL,
    m8 text NOT NULL,
    m9 text NOT NULL,
    m10 text NOT NULL,
    c_index text NOT NULL,
    s1_pix text NOT NULL,
    s1_gaps text NOT NULL,
    h_s1_gaps text NOT NULL,
    p_s1_gaps text NOT NULL,
    h_w_s1_gaps text,
    h_quality text,
    c_quality text
);


--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: product_provenance; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.product_provenance (
    product_id integer NOT NULL,
    parent_product_id integer NOT NULL,
    parent_product_date timestamp with time zone NOT NULL
);


--
-- Name: product_stats; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.product_stats (
    product_id integer NOT NULL,
    min_value real NOT NULL,
    max_value real NOT NULL,
    mean_value real NOT NULL,
    std_dev real NOT NULL,
    histogram integer[]
);


--
-- Name: product_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.product_type (
    id smallint NOT NULL,
    name character varying DEFAULT ''::character varying NOT NULL,
    description character varying,
    is_raster boolean DEFAULT true NOT NULL
);


--
-- Name: role; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.role (
    id smallint NOT NULL,
    name character varying(10) NOT NULL,
    description character varying(50) NOT NULL
);


--
-- Name: satellite; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.satellite (
    id integer NOT NULL,
    satellite_name character varying NOT NULL
);


--
-- Name: scheduled_task; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.scheduled_task (
    id smallint NOT NULL,
    name character varying NOT NULL,
    processor_id smallint NOT NULL,
    site_id smallint NOT NULL,
    season_id smallint NOT NULL,
    processor_params character varying,
    repeat_type smallint,
    repeat_after_days smallint,
    repeat_on_month_day smallint,
    retry_seconds integer,
    priority smallint,
    first_run_time character varying
);


--
-- Name: scheduled_task_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.scheduled_task_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scheduled_task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.scheduled_task_id_seq OWNED BY public.scheduled_task.id;


--
-- Name: scheduled_task_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.scheduled_task_status (
    id smallint NOT NULL,
    task_id smallint NOT NULL,
    next_schedule character varying,
    last_scheduled_run character varying,
    last_run_timestamp character varying,
    last_retry_timestamp character varying,
    estimated_next_run_time character varying
);


--
-- Name: scheduled_task_status_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.scheduled_task_status_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: scheduled_task_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.scheduled_task_status_id_seq OWNED BY public.scheduled_task_status.id;


--
-- Name: season; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.season (
    id smallint NOT NULL,
    site_id smallint NOT NULL,
    name text NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    mid_date date NOT NULL,
    enabled boolean NOT NULL
);


--
-- Name: season_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.season_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: season_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.season_id_seq OWNED BY public.season.id;


--
-- Name: season_site_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.season_site_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: season_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.season_site_id_seq OWNED BY public.season.site_id;


--
-- Name: severity; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.severity (
    id smallint NOT NULL,
    name text NOT NULL
);


--
-- Name: shape_tiles_l8; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.shape_tiles_l8 (
    gid integer NOT NULL,
    area numeric,
    perimeter numeric,
    pr_ double precision,
    pr_id double precision,
    rings_ok integer,
    rings_nok integer,
    wrspr double precision,
    pr integer,
    path smallint,
    "row" smallint,
    mode character varying(1),
    dayclass smallint,
    sequence integer,
    geog public.geography(MultiPolygon,4326)
);


--
-- Name: shape_tiles_l8_gid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.shape_tiles_l8_gid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shape_tiles_l8_gid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.shape_tiles_l8_gid_seq OWNED BY public.shape_tiles_l8.gid;


--
-- Name: shape_tiles_s2; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.shape_tiles_s2 (
    tile_id character(5) NOT NULL,
    geom public.geometry NOT NULL,
    geog public.geography NOT NULL,
    epsg_code integer NOT NULL
);


--
-- Name: site; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.site (
    id smallint NOT NULL,
    name character varying NOT NULL,
    short_name character varying,
    geog public.geography(MultiPolygon,4326) NOT NULL,
    enabled boolean NOT NULL
);


--
-- Name: site_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.site_id_seq
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.site_id_seq OWNED BY public.site.id;


--
-- Name: site_tiles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.site_tiles (
    site_id smallint NOT NULL,
    satellite_id integer NOT NULL,
    tiles text[]
);


--
-- Name: step; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.step (
    name character varying NOT NULL,
    task_id integer NOT NULL,
    parameters json,
    submit_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    start_timestamp timestamp with time zone,
    end_timestamp timestamp with time zone,
    exit_code integer,
    status_id smallint NOT NULL,
    status_timestamp timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: step_resource_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.step_resource_log (
    step_name character varying NOT NULL,
    task_id integer NOT NULL,
    node_name character varying NOT NULL,
    entry_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    duration_ms bigint,
    user_cpu_ms bigint,
    system_cpu_ms bigint,
    max_rss_kb integer,
    max_vm_size_kb integer,
    disk_read_b bigint,
    disk_write_b bigint,
    stdout_text character varying NOT NULL,
    stderr_text character varying NOT NULL
);


--
-- Name: task; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.task (
    id integer NOT NULL,
    job_id integer NOT NULL,
    module_short_name character varying NOT NULL,
    parameters json,
    submit_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    start_timestamp timestamp with time zone,
    end_timestamp timestamp with time zone,
    status_id smallint NOT NULL,
    status_timestamp timestamp with time zone DEFAULT now() NOT NULL,
    preceding_task_ids integer[]
);


--
-- Name: task_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.task_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: task_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.task_id_seq OWNED BY public.task.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."user" (
    id smallint NOT NULL,
    login character varying(50) NOT NULL,
    email character varying(100) NOT NULL,
    role_id smallint NOT NULL,
    site_id integer[],
    password text
);


--
-- Name: v_get_processors; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.v_get_processors AS
 SELECT processor.id,
    processor.name,
    processor.description,
    processor.short_name,
    processor.label
   FROM public.processor;


--
-- Name: v_get_product_types; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.v_get_product_types AS
 SELECT product_type.id,
    product_type.name,
    product_type.description,
    product_type.is_raster
   FROM public.product_type;


--
-- Name: config id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config ALTER COLUMN id SET DEFAULT nextval('public.config_id_seq'::regclass);


--
-- Name: datasource id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datasource ALTER COLUMN id SET DEFAULT nextval('public.datasource_id_seq'::regclass);


--
-- Name: downloader_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.downloader_history ALTER COLUMN id SET DEFAULT nextval('public.downloader_history_id_seq'::regclass);


--
-- Name: event id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event ALTER COLUMN id SET DEFAULT nextval('public.event_id_seq'::regclass);


--
-- Name: job id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.job ALTER COLUMN id SET DEFAULT nextval('public.job_id_seq'::regclass);


--
-- Name: log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log ALTER COLUMN id SET DEFAULT nextval('public.log_id_seq'::regclass);


--
-- Name: pd_ap_staging_4995_na ogc_fid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pd_ap_staging_4995_na ALTER COLUMN ogc_fid SET DEFAULT nextval('public.pd_ap_staging_4995_na_ogc_fid_seq'::regclass);


--
-- Name: pd_ap_staging_4998_na ogc_fid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pd_ap_staging_4998_na ALTER COLUMN ogc_fid SET DEFAULT nextval('public.pd_ap_staging_4998_na_ogc_fid_seq'::regclass);


--
-- Name: pd_ap_staging_5000_na ogc_fid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pd_ap_staging_5000_na ALTER COLUMN ogc_fid SET DEFAULT nextval('public.pd_ap_staging_5000_na_ogc_fid_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Name: scheduled_task id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_task ALTER COLUMN id SET DEFAULT nextval('public.scheduled_task_id_seq'::regclass);


--
-- Name: scheduled_task_status id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_task_status ALTER COLUMN id SET DEFAULT nextval('public.scheduled_task_status_id_seq'::regclass);


--
-- Name: season id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.season ALTER COLUMN id SET DEFAULT nextval('public.season_id_seq'::regclass);


--
-- Name: season site_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.season ALTER COLUMN site_id SET DEFAULT nextval('public.season_site_id_seq'::regclass);


--
-- Name: shape_tiles_l8 gid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.shape_tiles_l8 ALTER COLUMN gid SET DEFAULT nextval('public.shape_tiles_l8_gid_seq'::regclass);


--
-- Name: site id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.site ALTER COLUMN id SET DEFAULT nextval('public.site_id_seq'::regclass);


--
-- Name: task id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.task ALTER COLUMN id SET DEFAULT nextval('public.task_id_seq'::regclass);


--
-- Name: activity_status activity_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.activity_status
    ADD CONSTRAINT activity_status_pkey PRIMARY KEY (id);


--
-- Name: agricultural_practice agricultural_practice_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.agricultural_practice
    ADD CONSTRAINT agricultural_practice_pkey PRIMARY KEY (id);


--
-- Name: component component_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT component_pkey PRIMARY KEY (id);


--
-- Name: config_category config_category_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config_category
    ADD CONSTRAINT config_category_pkey PRIMARY KEY (id);


--
-- Name: config_job config_job_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config_job
    ADD CONSTRAINT config_job_pkey PRIMARY KEY (job_id, key);


--
-- Name: config_metadata config_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config_metadata
    ADD CONSTRAINT config_metadata_pkey PRIMARY KEY (key);


--
-- Name: config config_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.config
    ADD CONSTRAINT config_pkey PRIMARY KEY (id);


--
-- Name: decl_hmathia2020_2020 decl_hmathia2020_2020_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.decl_hmathia2020_2020
    ADD CONSTRAINT decl_hmathia2020_2020_pkey PRIMARY KEY ("NewID");


--
-- Name: decl_hmathia2022_2022 decl_hmathia2022_2022_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.decl_hmathia2022_2022
    ADD CONSTRAINT decl_hmathia2022_2022_pkey PRIMARY KEY ("NewID");


--
-- Name: decl_mockzone_2021 decl_mockzone_2021_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.decl_mockzone_2021
    ADD CONSTRAINT decl_mockzone_2021_pkey PRIMARY KEY ("NewID");


--
-- Name: decl_prefs2021_2021_old decl_prefs2021_2021_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.decl_prefs2021_2021_old
    ADD CONSTRAINT decl_prefs2021_2021_pkey PRIMARY KEY ("NewID");


--
-- Name: decl_trik_card2022_2022 decl_trik_card2022_2022_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.decl_trik_card2022_2022
    ADD CONSTRAINT decl_trik_card2022_2022_pkey PRIMARY KEY ("NewID");


--
-- Name: decl_trikcard_2021 decl_trikcard_2021_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.decl_trikcard_2021
    ADD CONSTRAINT decl_trikcard_2021_pkey PRIMARY KEY ("NewID");


--
-- Name: downloader_history downloader_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.downloader_history
    ADD CONSTRAINT downloader_history_pkey PRIMARY KEY (id);


--
-- Name: downloader_status downloader_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.downloader_status
    ADD CONSTRAINT downloader_status_pkey PRIMARY KEY (id);


--
-- Name: event event_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);


--
-- Name: event_type event_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.event_type
    ADD CONSTRAINT event_type_pkey PRIMARY KEY (id);


--
-- Name: fmask_history fmask_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fmask_history
    ADD CONSTRAINT fmask_history_pkey PRIMARY KEY (downloader_history_id);


--
-- Name: job job_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.job
    ADD CONSTRAINT job_pkey PRIMARY KEY (id);


--
-- Name: job_start_type job_start_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.job_start_type
    ADD CONSTRAINT job_start_type_pkey PRIMARY KEY (id);


--
-- Name: l1_tile_history l1_tile_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.l1_tile_history
    ADD CONSTRAINT l1_tile_history_pkey PRIMARY KEY (downloader_history_id, tile_id);


--
-- Name: l1_tile_status l1_tile_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.l1_tile_status
    ADD CONSTRAINT l1_tile_status_pkey PRIMARY KEY (id);


--
-- Name: log log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log
    ADD CONSTRAINT log_pkey PRIMARY KEY (id);


--
-- Name: lut_hmathia2020_2020 lut_hmathia2020_2020_ori_crop_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_hmathia2020_2020
    ADD CONSTRAINT lut_hmathia2020_2020_ori_crop_key UNIQUE (ori_crop);


--
-- Name: lut_hmathia2020_2020 lut_hmathia2020_2020_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_hmathia2020_2020
    ADD CONSTRAINT lut_hmathia2020_2020_pkey PRIMARY KEY (ctnum);


--
-- Name: lut_hmathia2022_2022 lut_hmathia2022_2022_ori_crop_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_hmathia2022_2022
    ADD CONSTRAINT lut_hmathia2022_2022_ori_crop_key UNIQUE (ori_crop);


--
-- Name: lut_hmathia2022_2022 lut_hmathia2022_2022_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_hmathia2022_2022
    ADD CONSTRAINT lut_hmathia2022_2022_pkey PRIMARY KEY (ctnum);


--
-- Name: lut_mockzone_2021 lut_mockzone_2021_ori_crop_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_mockzone_2021
    ADD CONSTRAINT lut_mockzone_2021_ori_crop_key UNIQUE (ori_crop);


--
-- Name: lut_mockzone_2021 lut_mockzone_2021_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_mockzone_2021
    ADD CONSTRAINT lut_mockzone_2021_pkey PRIMARY KEY (ctnum);


--
-- Name: lut_prefs2021_2021 lut_prefs2021_2021_ori_crop_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_prefs2021_2021
    ADD CONSTRAINT lut_prefs2021_2021_ori_crop_key UNIQUE (ori_crop);


--
-- Name: lut_prefs2021_2021 lut_prefs2021_2021_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_prefs2021_2021
    ADD CONSTRAINT lut_prefs2021_2021_pkey PRIMARY KEY (ctnum);


--
-- Name: lut_trik_card2022_2022 lut_trik_card2022_2022_ori_crop_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_trik_card2022_2022
    ADD CONSTRAINT lut_trik_card2022_2022_ori_crop_key UNIQUE (ori_crop);


--
-- Name: lut_trik_card2022_2022 lut_trik_card2022_2022_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_trik_card2022_2022
    ADD CONSTRAINT lut_trik_card2022_2022_pkey PRIMARY KEY (ctnum);


--
-- Name: lut_trikcard_2021 lut_trikcard_2021_ori_crop_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_trikcard_2021
    ADD CONSTRAINT lut_trikcard_2021_ori_crop_key UNIQUE (ori_crop);


--
-- Name: lut_trikcard_2021 lut_trikcard_2021_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lut_trikcard_2021
    ADD CONSTRAINT lut_trikcard_2021_pkey PRIMARY KEY (ctnum);


--
-- Name: module module_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.module
    ADD CONSTRAINT module_pkey PRIMARY KEY (short_name);


--
-- Name: orbit_type orbit_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.orbit_type
    ADD CONSTRAINT orbit_type_pkey PRIMARY KEY (id);


--
-- Name: pd_ap_staging_4995_na pd_ap_staging_4995_na_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pd_ap_staging_4995_na
    ADD CONSTRAINT pd_ap_staging_4995_na_pkey PRIMARY KEY (ogc_fid);


--
-- Name: pd_ap_staging_4998_na pd_ap_staging_4998_na_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pd_ap_staging_4998_na
    ADD CONSTRAINT pd_ap_staging_4998_na_pkey PRIMARY KEY (ogc_fid);


--
-- Name: pd_ap_staging_5000_na pd_ap_staging_5000_na_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.pd_ap_staging_5000_na
    ADD CONSTRAINT pd_ap_staging_5000_na_pkey PRIMARY KEY (ogc_fid);


--
-- Name: datasource pk_datasource; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datasource
    ADD CONSTRAINT pk_datasource PRIMARY KEY (id);


--
-- Name: downloader_count pk_donwloader_count; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.downloader_count
    ADD CONSTRAINT pk_donwloader_count PRIMARY KEY (site_id, satellite_id, start_date, end_date);


--
-- Name: product_stats pk_product_details; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_stats
    ADD CONSTRAINT pk_product_details PRIMARY KEY (product_id);


--
-- Name: processor processor_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.processor
    ADD CONSTRAINT processor_pkey PRIMARY KEY (id);


--
-- Name: product_details_l4a product_details_l4a_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_details_l4a
    ADD CONSTRAINT product_details_l4a_pkey PRIMARY KEY (product_id, "NewID");


--
-- Name: product_details_l4c product_details_l4c_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_details_l4c
    ADD CONSTRAINT product_details_l4c_pkey PRIMARY KEY (product_id, "NewID");


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: product_provenance product_provenance_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_provenance
    ADD CONSTRAINT product_provenance_pkey PRIMARY KEY (product_id, parent_product_id);


--
-- Name: product_type product_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_type
    ADD CONSTRAINT product_type_pkey PRIMARY KEY (id);


--
-- Name: role role_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_name_key UNIQUE (name);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: satellite satellite_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.satellite
    ADD CONSTRAINT satellite_pkey PRIMARY KEY (id);


--
-- Name: scheduled_task scheduled_task_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_task
    ADD CONSTRAINT scheduled_task_pkey PRIMARY KEY (id);


--
-- Name: scheduled_task_status scheduled_task_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_task_status
    ADD CONSTRAINT scheduled_task_status_pkey PRIMARY KEY (id);


--
-- Name: season season_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.season
    ADD CONSTRAINT season_pkey PRIMARY KEY (id);


--
-- Name: season season_site_id_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.season
    ADD CONSTRAINT season_site_id_name_key UNIQUE (site_id, name);


--
-- Name: severity severity_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.severity
    ADD CONSTRAINT severity_pkey PRIMARY KEY (id);


--
-- Name: shape_tiles_l8 shape_tiles_l8_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.shape_tiles_l8
    ADD CONSTRAINT shape_tiles_l8_pkey PRIMARY KEY (gid);


--
-- Name: shape_tiles_s2 shape_tiles_s2_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.shape_tiles_s2
    ADD CONSTRAINT shape_tiles_s2_pkey PRIMARY KEY (tile_id);


--
-- Name: site site_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.site
    ADD CONSTRAINT site_pkey PRIMARY KEY (id);


--
-- Name: step step_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.step
    ADD CONSTRAINT step_pkey PRIMARY KEY (name, task_id);


--
-- Name: step_resource_log step_resource_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.step_resource_log
    ADD CONSTRAINT step_resource_log_pkey PRIMARY KEY (step_name, task_id);


--
-- Name: task task_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id);


--
-- Name: user user_login_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_login_key UNIQUE (login);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: fki_fk_downloader_count_satellite; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fk_downloader_count_satellite ON public.downloader_count USING btree (satellite_id);


--
-- Name: fki_fk_downloader_count_site; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fk_downloader_count_site ON public.downloader_count USING btree (site_id);


--
-- Name: idx_l4c_practices; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX idx_l4c_practices ON public.l4c_practices USING btree (site_id, year, practice_short_name);


--
-- Name: ix_config_key_site_id; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX ix_config_key_site_id ON public.config USING btree (key, COALESCE((site_id)::integer, '-1'::integer));


--
-- Name: ix_decl_hmathia2020_2020_wkb_geometry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_decl_hmathia2020_2020_wkb_geometry ON public.decl_hmathia2020_2020 USING gist (wkb_geometry);


--
-- Name: ix_decl_hmathia2022_2022_wkb_geometry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_decl_hmathia2022_2022_wkb_geometry ON public.decl_hmathia2022_2022 USING gist (wkb_geometry);


--
-- Name: ix_decl_mockzone_2021_wkb_geometry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_decl_mockzone_2021_wkb_geometry ON public.decl_mockzone_2021 USING gist (wkb_geometry);


--
-- Name: ix_decl_prefs2021_2021_wkb_geometry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_decl_prefs2021_2021_wkb_geometry ON public.decl_prefs2021_2021 USING gist (wkb_geometry);


--
-- Name: ix_decl_trik_card2022_2022_wkb_geometry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_decl_trik_card2022_2022_wkb_geometry ON public.decl_trik_card2022_2022 USING gist (wkb_geometry);


--
-- Name: ix_decl_trikcard_2021_wkb_geometry; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_decl_trikcard_2021_wkb_geometry ON public.decl_trikcard_2021 USING gist (wkb_geometry);


--
-- Name: ix_downloader_history_product_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_downloader_history_product_name ON public.downloader_history USING btree (product_name);


--
-- Name: ix_downloader_history_site_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_downloader_history_site_id ON public.downloader_history USING btree (site_id);


--
-- Name: ix_node_resource_log_timestamp; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_node_resource_log_timestamp ON public.node_resource_log USING btree ("timestamp" DESC);


--
-- Name: ix_product_created_timestamp; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_product_created_timestamp ON public.product USING btree (created_timestamp);


--
-- Name: ix_product_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_product_name ON public.product USING btree (name);


--
-- Name: ix_season_site_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_season_site_id ON public.season USING btree (site_id);


--
-- Name: ix_shape_tiles_l8_geog; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_shape_tiles_l8_geog ON public.shape_tiles_l8 USING gist (geog);


--
-- Name: ix_shape_tiles_l8_path_row; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_shape_tiles_l8_path_row ON public.shape_tiles_l8 USING btree (path, "row");


--
-- Name: ix_shape_tiles_s2_geog; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_shape_tiles_s2_geog ON public.shape_tiles_s2 USING gist (geog);


--
-- Name: ix_step_task_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_step_task_id ON public.step USING btree (task_id);


--
-- Name: ix_task_job_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ix_task_job_id ON public.task USING btree (job_id);


--
-- Name: season check_season_dates; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER check_season_dates BEFORE INSERT OR UPDATE ON public.season FOR EACH ROW EXECUTE PROCEDURE public.check_season();


--
-- Name: datasource datasource_satellite_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datasource
    ADD CONSTRAINT datasource_satellite_id_fkey FOREIGN KEY (satellite_id) REFERENCES public.satellite(id);


--
-- Name: datasource datasource_site_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datasource
    ADD CONSTRAINT datasource_site_id_fkey FOREIGN KEY (site_id) REFERENCES public.site(id);


--
-- Name: downloader_history downloader_history_satellite_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.downloader_history
    ADD CONSTRAINT downloader_history_satellite_id_fkey FOREIGN KEY (satellite_id) REFERENCES public.satellite(id);


--
-- Name: downloader_history downloader_history_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.downloader_history
    ADD CONSTRAINT downloader_history_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.downloader_status(id);


--
-- Name: datasource fk_datasource_datasource; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.datasource
    ADD CONSTRAINT fk_datasource_datasource FOREIGN KEY (secondary_datasource_id) REFERENCES public.datasource(id);


--
-- Name: downloader_count fk_downloader_count_satellite; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.downloader_count
    ADD CONSTRAINT fk_downloader_count_satellite FOREIGN KEY (satellite_id) REFERENCES public.satellite(id);


--
-- Name: downloader_count fk_downloader_count_site; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.downloader_count
    ADD CONSTRAINT fk_downloader_count_site FOREIGN KEY (site_id) REFERENCES public.site(id);


--
-- Name: log fk_log_component; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log
    ADD CONSTRAINT fk_log_component FOREIGN KEY (component_id) REFERENCES public.component(id);


--
-- Name: log fk_log_severity; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.log
    ADD CONSTRAINT fk_log_severity FOREIGN KEY (severity) REFERENCES public.severity(id);


--
-- Name: product_stats fk_product; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_stats
    ADD CONSTRAINT fk_product FOREIGN KEY (product_id) REFERENCES public.product(id);


--
-- Name: downloader_history fk_product_orbit_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.downloader_history
    ADD CONSTRAINT fk_product_orbit_type FOREIGN KEY (orbit_type_id) REFERENCES public.orbit_type(id);


--
-- Name: product fk_product_orbit_type; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT fk_product_orbit_type FOREIGN KEY (orbit_type_id) REFERENCES public.orbit_type(id);


--
-- Name: product_provenance fk_product_provenance_parent_product_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_provenance
    ADD CONSTRAINT fk_product_provenance_parent_product_id FOREIGN KEY (parent_product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: product_provenance fk_product_provenance_product_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_provenance
    ADD CONSTRAINT fk_product_provenance_product_id FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: scheduled_task fk_scheduled_task_season; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_task
    ADD CONSTRAINT fk_scheduled_task_season FOREIGN KEY (season_id) REFERENCES public.season(id) ON DELETE CASCADE;


--
-- Name: scheduled_task_status fk_scheduled_task_status_scheduled_task; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.scheduled_task_status
    ADD CONSTRAINT fk_scheduled_task_status_scheduled_task FOREIGN KEY (task_id) REFERENCES public.scheduled_task(id) ON DELETE CASCADE;


--
-- Name: season fk_season_site; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.season
    ADD CONSTRAINT fk_season_site FOREIGN KEY (site_id) REFERENCES public.site(id);


--
-- Name: site_tiles fk_site_tiles_satellite; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.site_tiles
    ADD CONSTRAINT fk_site_tiles_satellite FOREIGN KEY (satellite_id) REFERENCES public.satellite(id);


--
-- Name: site_tiles fk_site_tiles_site; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.site_tiles
    ADD CONSTRAINT fk_site_tiles_site FOREIGN KEY (site_id) REFERENCES public.site(id);


--
-- Name: fmask_history fmask_history_downloader_history_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fmask_history
    ADD CONSTRAINT fmask_history_downloader_history_id_fkey FOREIGN KEY (downloader_history_id) REFERENCES public.downloader_history(id);


--
-- Name: fmask_history fmask_history_satellite_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fmask_history
    ADD CONSTRAINT fmask_history_satellite_id_fkey FOREIGN KEY (satellite_id) REFERENCES public.satellite(id);


--
-- Name: fmask_history fmask_history_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fmask_history
    ADD CONSTRAINT fmask_history_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.l1_tile_status(id);


--
-- Name: l1_tile_history l1_tile_history_downloader_history_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.l1_tile_history
    ADD CONSTRAINT l1_tile_history_downloader_history_id_fkey FOREIGN KEY (downloader_history_id) REFERENCES public.downloader_history(id);


--
-- Name: l1_tile_history l1_tile_history_satellite_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.l1_tile_history
    ADD CONSTRAINT l1_tile_history_satellite_id_fkey FOREIGN KEY (satellite_id) REFERENCES public.satellite(id);


--
-- Name: l1_tile_history l1_tile_history_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.l1_tile_history
    ADD CONSTRAINT l1_tile_history_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.l1_tile_status(id);


--
-- Name: product_details_l4a product_details_l4a_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_details_l4a
    ADD CONSTRAINT product_details_l4a_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: product_details_l4c product_details_l4c_practice_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_details_l4c
    ADD CONSTRAINT product_details_l4c_practice_id_fkey FOREIGN KEY (practice_id) REFERENCES public.agricultural_practice(id);


--
-- Name: product_details_l4c product_details_l4c_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.product_details_l4c
    ADD CONSTRAINT product_details_l4c_product_id_fkey FOREIGN KEY (product_id) REFERENCES public.product(id) ON DELETE CASCADE;


--
-- Name: user user_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_fk FOREIGN KEY (role_id) REFERENCES public.role(id);


--
-- PostgreSQL database dump complete
--

