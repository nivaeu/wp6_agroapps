import psycopg2
from dotenv import load_dotenv
import os
import argparse
import subprocess
import shutil


def run(process, msg='\n'):
    print(msg, "Running: >", process)
    output = subprocess.run(process, shell=True)
    print(output)


def exitp(exit_code, error_msg):
    print("ERROR:", error_msg)
    exit(exit_code)


def create_dirs(dirs, parent_dir):
    for dir in dirs:
        path = os.path.join(parent_dir, dir)
        if not os.path.isdir(path):
            os.mkdir(path)


print("OPEKEPE Classifier#1 Runner v1.0.2\n")

# Load environment variables
print("Loading environment variables...")
load_dotenv()

db_host = os.environ["db_host"]
db_port = os.environ["db_port"]
db_database = os.environ["db_database"]
db_user = os.environ["db_user"]
db_password = os.environ["db_password"]
db_schema = os.environ["db_schema"]
data_folder = os.environ["data_folder"]


# command line arguments
parser = argparse.ArgumentParser(
    prog='',
    description='Prepare and execute toolchains for Classifier #1 ',
    epilog='Text at the bottom of help')

parser.add_argument('-j', '--job_name', required=True)
parser.add_argument('-p', '--parcel_shp',
                    help='Path to parcel shapefile', required=True)
parser.add_argument(
    '-a', '--aoi', help="AOIs file in GeoJSON format", required=True)
parser.add_argument(
    '-m', '--pa_min', help='Minimum parcels number to be considered', required=True)
parser.add_argument('-s', '--sampling_factor',
                    help='Sampling factor', required=True)
parser.add_argument('-d', '--data_folder', help='Data folder (where results will be stored in a job_name directory)',
                    default='./data/', required=False)
parser.add_argument('-f', '--sen4cap_job_folder',
                    help='Full path to SEN4CAP job folder (e.g. /mnt/archive/hmathia2021)', required=True)

# get arguments
args = parser.parse_args()

job_name = args.job_name
job_name = job_name.lower()
if job_name is None or job_name == '':
    exitp(1, "Job name cannot be empty")

parcels_shp = args.parcel_shp
if job_name is None or job_name == '' or not os.path.isfile(parcels_shp):
    exitp(
        2, f"Parcel shapefile {parcels_shp} cannot be empty or does not exist")

aois = args.aoi
if aois is None or job_name == '' or not os.path.isfile(aois):
    exitp(3, f"AOIs file {aois} cannot be empty or does not exist")

data_folder = args.data_folder
if data_folder is None or data_folder == '' or not os.path.isdir(data_folder):
    exitp(4, f"Data folder {data_folder} does not exist")

sen4cap_job_folder = args.sen4cap_job_folder
if sen4cap_job_folder is None or sen4cap_job_folder == '' or not os.path.isdir(sen4cap_job_folder):
    exitp(5, f"SEN4CAP job folder {sen4cap_job_folder} does not exist")

pa_min = args.pa_min
sampling_factor = args.sampling_factor

# create JOB folder inside data folder
working_dir = os.path.join(data_folder, job_name, '')
if not os.path.isdir(working_dir):
    os.mkdir(working_dir)
if not os.path.isdir(working_dir):
    exitp(4, f"Working folder {working_dir} does not exist")


create_dirs(['RAW_NDVI_L3B', 'L3_C', 'LOGS', 'SHAPES', 'TABLES'], working_dir)
create_dirs(['NDVI', 'NDVI_S2_Mosaic', 'NDVI_S2_QM_Mosaic'],
            working_dir+'/RAW_NDVI_L3B')
create_dirs(['GapFilled_NDVI',  '1D',  'GLCM_ENTROPY',
            'GLCM_HOMOGENITY', 'GLCM_VARIANCE'], working_dir+'/L3_C')

SEN2AGRI_PREFIX = "S2AGRI_L3B_SNDVI"
NDVI_DIR = os.path.abspath(f'{working_dir}/RAW_NDVI_L3B/NDVI/')
# find . -type f -iname ${SEN2AGRI_PREFIX}*.TIF -exec cp {} ${NDVI_DIR} \;
run('cd '+sen4cap_job_folder+' && find . -type f -name ' +
    SEN2AGRI_PREFIX+'*.TIF -exec cp {} '+NDVI_DIR+' \;')

# before database creation
con = None
try:
    con = psycopg2.connect(host=db_host,
                           port=db_port,
                           database=db_database,
                           user=db_user,
                           password=db_password
                           )
    if con is not None:
        con.autocommit = True
        cur = con.cursor()
        cur.execute("SELECT datname FROM pg_database;")
        list_database = cur.fetchall()
        new_db_database = f'{db_database}_{job_name}'
        if (new_db_database,) in list_database:
            print(f"Database {new_db_database} already exists")
            db_database = new_db_database
        else:
            SQL_CREATE_DB = f"create database {new_db_database};"
            cur.execute(SQL_CREATE_DB)
            print(f"Database {new_db_database} created")
            con.close()
            db_database = new_db_database
    else:
        print(f"Database {db_database} not found")
        exit(1)

except:
    print(f"Database {db_database} not connected")
    exit(1)

# create connection string
connection_string = f"host={db_host} port={db_port} dbname={db_database} user={db_user} password={db_password}"
# must be the new database with db_name + job_name
print(f"### Connecting to database : {connection_string}")

# Build Connection:
connection = psycopg2.connect(host=db_host,
                              port=db_port,
                              database=db_database,
                              user=db_user,
                              password=db_password
                              )
CREATE_SCHEMA = f"CREATE EXTENSION postgis; CREATE SCHEMA IF NOT EXISTS {db_schema};"
DROP_TABLE_PARCELS = "drop table if exists opekepe_spatial.parcels;"
DROP_TABLE_AOI = "drop table if exists opekepe_spatial.aois;"

DROP_TABLES = """
drop table if exists opekepe_spatial.buffer;
drop table if exists opekepe_spatial.subset_final;
drop table if exists opekepe_spatial.pixminfilter0;
drop table if exists opekepe_spatial.pixminfilter1;
drop table if exists opekepe_spatial.crop_type_count;
drop table if exists opekepe_spatial.pixminfilter1_croptypeselection;
drop table if exists opekepe_spatial.cropselect1;
drop table if exists opekepe_spatial.pixbestfilter0;
drop table if exists opekepe_spatial.pixbestfilter1;
drop table if exists opekepe_spatial.rfv_positive;
drop table if exists opekepe_spatial.rfv0;
drop table if exists opekepe_spatial.rfv0_stratified;
drop table if exists opekepe_spatial.rfv0_minus_stratified;
drop table if exists opekepe_spatial.training;
drop table if exists opekepe_spatial.prediction;
drop table if exists opekepe_spatial.parcels;
drop table if exists opekepe_spatial.aois;
"""


# Create Schema and Drop Tables:
with connection:
    with connection.cursor() as cursor:
        cursor.execute(CREATE_SCHEMA)
        cursor.execute(DROP_TABLE_PARCELS)
        # cursor.execute(DROP_TABLE_AOI)
        cursor.execute(DROP_TABLES)
        print(f"Connected to database {db_database}:{db_port}/{db_schema}")

#
# export
#

print("Job name: ", job_name)
print("Minimum parcels number: ", pa_min)
print("Sampling factor: ", sampling_factor)
print("Parcel shapefile: ", parcels_shp)
print("AOIs file: ", aois)
print("Working folder: ", working_dir)

import_shape = f"ogr2ogr -append -update -progress -f \"PostgreSQL\" PG:\"{connection_string}\" {parcels_shp} -nln opekepe_spatial.parcels -lco GEOMETRY_NAME=shape -nlt MULTIPOLYGON"
run(import_shape)

import_aois = f"ogr2ogr -append -update -progress -f \"PostgreSQL\" PG:\"{connection_string}\" {aois} -nln opekepe_spatial.aois -lco GEOMETRY_NAME=shape -nlt MULTIPOLYGON"
run(import_aois)

# execute SQL commands
SQL_EXE = f"""
/* Buffering, St_contains, Elongation */

/* 5m buffer */
CREATE TABLE opekepe_spatial.buffer AS 
SELECT ST_Buffer(shape, -5, 'endcap=square join=mitre') FROM opekepe_spatial.parcels;
CREATE INDEX idx_buffer ON opekepe_spatial.buffer USING GIST (st_buffer);

/* St_contains */
CREATE TABLE opekepe_spatial.subsetBuffered AS
SELECT parcels.ogc_fid, parcels.objectid, parcels.eda_id, parcels.group_code, parcels.group_desc, parcels.rfv_crop, parcels.rfv_group, parcels.fallow_det, parcels.fallow_per, buffer.st_buffer
FROM opekepe_spatial.parcels, opekepe_spatial.buffer
WHERE ST_Contains(parcels.shape, buffer.st_buffer);

CREATE INDEX idx_subset ON opekepe_spatial.subsetBuffered USING GIST (st_buffer);

/*Calculate area of polygonal geometry in square meters and ha & shape length */
CREATE TABLE opekepe_spatial.subsetBuffered_CALC AS
SELECT subsetBuffered.*, ST_Area(st_buffer) AS SHAPE_AREA, ST_Area(st_buffer)/1000 AS AREA_HA, ST_Perimeter(st_buffer) AS SHAPE_LENG FROM opekepe_spatial.subsetBuffered;

/*Calculate elongation and store in a final table*/ 
CREATE TABLE opekepe_spatial.Subset_Final AS
SELECT subsetBuffered_CALC.*, shape_area/shape_leng AS ELONGATION FROM opekepe_spatial.subsetBuffered_CALC;

CREATE INDEX idx_subsetfinal ON opekepe_spatial.Subset_Final USING GIST (st_buffer);

DROP TABLE opekepe_spatial.subsetBuffered;
DROP TABLE opekepe_spatial.subsetBuffered_CALC;

/* MLM AND pixMin filtering */ 
ALTER TABLE opekepe_spatial.subset_final ADD mlm float, ADD pixMinFilter INTEGER;

UPDATE opekepe_spatial.subset_final SET mlm = 0.008538265*subset_final.shape_area+0.057656360*subset_final.elongation+0.540621148;

--1
UPDATE opekepe_spatial.subset_final SET pixMinFilter = 1 WHERE subset_final.mlm >= 5;
--0
UPDATE opekepe_spatial.subset_final SET pixMinFilter = 0 WHERE subset_final.mlm < 5;



/* Keep pixMinFilter = 0 in a table for Cornelis */
CREATE TABLE opekepe_spatial.pixMinFilter0 AS
SELECT * FROM opekepe_spatial.subset_final 
WHERE pixMinFilter = 0;

ALTER TABLE opekepe_spatial.pixMinFilter0 ADD rating varchar(254);
UPDATE opekepe_spatial.pixMinFilter0 SET rating = 'non-assessed' WHERE pixMinFilter0.pixMinFilter = 0;

/* Keep pixMinFilter = 1 */ 
CREATE TABLE opekepe_spatial.pixMinFilter1 AS
SELECT * FROM opekepe_spatial.subset_final 
WHERE pixMinFilter = 1;

/* Count εγγραφών ανά group_desc στο pixMinFilter1 */ 
CREATE TABLE opekepe_spatial.crop_type_count AS 
SELECT group_desc, COUNT(*) FROM opekepe_spatial.pixMinFilter1 GROUP BY group_desc;

/* Count threshold - CROP SELECT - Calculate to pa_min/sampling factor */ 
--Έστω ότι pa_min = 50 kai sampling factor = 20% ή 0.2 
ALTER TABLE opekepe_spatial.crop_type_count ADD crop_select INTEGER;

UPDATE opekepe_spatial.crop_type_count SET crop_select = 1 WHERE crop_type_count.count >= {pa_min}/{sampling_factor};
UPDATE opekepe_spatial.crop_type_count SET crop_select = 0 WHERE crop_type_count.count < {pa_min}/{sampling_factor};

/* Join crop_type_count_select with pixMinFilter1 */
CREATE TABLE opekepe_spatial.pixMinFilter1_croptypeselection AS 
SELECT DISTINCT pixMinFilter1.ogc_fid, pixMinFilter1.objectid, pixMinFilter1.eda_id,  pixMinFilter1.group_code, pixMinFilter1.rfv_crop, pixMinFilter1.rfv_group, pixMinFilter1.fallow_det, pixMinFilter1.fallow_per, pixMinFilter1.st_buffer, pixMinFilter1.shape_area, pixMinFilter1.area_ha, pixMinFilter1.shape_leng, pixMinFilter1.elongation, pixMinFilter1.mlm, pixMinFilter1.pixminfilter, crop_type_count.*
FROM opekepe_spatial.pixMinFilter1
LEFT JOIN opekepe_spatial.crop_type_count
ON pixMinFilter1.group_desc = crop_type_count.group_desc;

/* Keep crop_select = 1 */
CREATE TABLE opekepe_spatial.cropselect1 AS
SELECT * FROM opekepe_spatial.pixMinFilter1_croptypeselection 
WHERE crop_select = 1;

/* pixBEST filtering on cropselect1 */
ALTER TABLE opekepe_spatial.cropselect1 ADD pixBestFilter INTEGER;

--1
UPDATE opekepe_spatial.cropselect1 SET pixBestFilter = 1 WHERE cropselect1.mlm >= 12;
--0
UPDATE opekepe_spatial.cropselect1 SET pixBestFilter = 0 WHERE cropselect1.mlm < 12;

/* Keep pixBestFilter = 0 in a table */
CREATE TABLE opekepe_spatial.pixbestfilter0 AS
SELECT * FROM opekepe_spatial.cropselect1 
WHERE pixBestFilter = 0;

/* Keep pixBestFilter = 1 in a table */
CREATE TABLE opekepe_spatial.pixbestfilter1 AS
SELECT * FROM opekepe_spatial.cropselect1 
WHERE pixBestFilter = 1;

/* 'Update' crop type count in pixbestfilter1 */ 
CREATE TABLE opekepe_spatial.pixbestfilter1_ctcount AS 
SELECT group_desc, COUNT(*) FROM opekepe_spatial.pixbestfilter1 GROUP BY group_desc;

UPDATE opekepe_spatial.pixbestfilter1
SET count = pixbestfilter1_ctcount.count 
FROM opekepe_spatial.pixbestfilter1_ctcount
WHERE pixbestfilter1.group_desc = pixbestfilter1_ctcount.group_desc;

DROP TABLE opekepe_spatial.pixbestfilter1_ctcount;

/* RFV TRUE στο pixbestfilter1 */

--CASE WHEN RFV = TRUE (1)
CREATE TABLE opekepe_spatial.rfv1 AS 
SELECT * FROM opekepe_spatial.pixbestfilter1
WHERE rfv_crop IS NOT NULL OR fallow_det IS NOT NULL; 

--Create rfv_positive 
CREATE TABLE opekepe_spatial.rfv_positive AS
SELECT * FROM opekepe_spatial.rfv1 
WHERE group_desc = rfv_group OR fallow_det IS NOT NULL;

DROP TABLE opekepe_spatial.rfv1;

--Update crop type count in rfv_positive 
CREATE TABLE opekepe_spatial.rfv_positive_ctcount AS 
SELECT group_desc, COUNT(*) FROM opekepe_spatial.rfv_positive GROUP BY group_desc;

UPDATE opekepe_spatial.rfv_positive
SET count = rfv_positive_ctcount.count 
FROM opekepe_spatial.rfv_positive_ctcount
WHERE rfv_positive.group_desc = rfv_positive_ctcount.group_desc;

DROP TABLE opekepe_spatial.rfv_positive_ctcount;


--CASE WHEN RFV =! TRUE (0) 
CREATE TABLE opekepe_spatial.rfv0 AS 
SELECT * FROM opekepe_spatial.pixbestfilter1
WHERE rfv_crop IS NULL AND fallow_det IS NULL; 

--Update crop type count in rfv0
CREATE TABLE opekepe_spatial.rfv0_ctcount AS 
SELECT group_desc, COUNT(*) FROM opekepe_spatial.rfv0 GROUP BY group_desc;

UPDATE opekepe_spatial.rfv0
SET count = rfv0_ctcount.count 
FROM opekepe_spatial.rfv0_ctcount
WHERE rfv0.group_desc = rfv0_ctcount.group_desc;

DROP TABLE opekepe_spatial.rfv0_ctcount;

--Stratified sampling ston rfv0 
CREATE TABLE opekepe_spatial.rfv0_stratified AS 
SELECT s.*
FROM (SELECT s.*,
             row_number() OVER (partition by group_code order by random()) AS seqnum,
			 count(*) OVER (partition by group_code) AS cnt
      FROM opekepe_spatial.rfv0 s
     ) s
where seqnum <= round(cnt * {sampling_factor}, 0);


--Oti den pairnei to stratified 
CREATE TABLE opekepe_spatial.rfv0_minus_stratified AS
SELECT * 
FROM opekepe_spatial.rfv0 
WHERE  eda_id NOT IN (
   SELECT eda_id  -- DISTINCT is optional
   FROM opekepe_spatial.rfv0_stratified
   );
   


/* RFV ERASE ston pixbestfilter0 */
DELETE FROM opekepe_spatial.pixbestfilter0
WHERE (rfv_crop IS NOT NULL OR fallow_det IS NOT NULL) AND (group_desc <> rfv_group);



/* Training dataset */
-- Merge rfv_positive and rfv0_stratified
SELECT * INTO opekepe_spatial.training 
FROM opekepe_spatial.rfv_positive NATURAL FULL OUTER JOIN opekepe_spatial.rfv0_stratified;

--Update crop type count in training 
CREATE TABLE opekepe_spatial.training_ctcount AS 
SELECT group_desc, COUNT(*) FROM opekepe_spatial.training GROUP BY group_desc;

UPDATE opekepe_spatial.training
SET count = training_ctcount.count 
FROM opekepe_spatial.training_ctcount
WHERE training.group_desc = training_ctcount.group_desc;

DROP TABLE opekepe_spatial.training_ctcount;

--Add final crop count filtering on training 
ALTER TABLE opekepe_spatial.training ADD final_count_selection INTEGER;
--0 where count < pa_min/samplingfactor
UPDATE opekepe_spatial.training SET final_count_selection = 0 WHERE training.count < {pa_min}/{sampling_factor};
--Delete rows where final_count_selection = 0
DELETE FROM opekepe_spatial.training
WHERE final_count_selection = 0;


--Prediction dataset ###
--Merge rfv_positive + rfv0_stratified + rfv0_minus_stratified + pixbestfilter0 cleanedUp
SELECT * INTO opekepe_spatial.prediction FROM opekepe_spatial.rfv_positive
NATURAL FULL OUTER JOIN opekepe_spatial.rfv0_stratified
NATURAL FULL OUTER JOIN opekepe_spatial.rfv0_minus_stratified
NATURAL FULL OUTER JOIN opekepe_spatial.pixbestfilter0;

--Update crop type count in prediction 
CREATE TABLE opekepe_spatial.prediction_ctcount AS 
SELECT group_desc, COUNT(*) FROM opekepe_spatial.prediction GROUP BY group_desc;

UPDATE opekepe_spatial.prediction
SET count = prediction_ctcount.count 
FROM opekepe_spatial.prediction_ctcount
WHERE prediction.group_desc = prediction_ctcount.group_desc;

DROP TABLE opekepe_spatial.prediction_ctcount;

--Add final crop count filtering on prediction 
ALTER TABLE opekepe_spatial.prediction ADD final_count_selection INTEGER;
--0 where count < pa_min/samplingfactor
UPDATE opekepe_spatial.prediction SET final_count_selection = 0 WHERE prediction.count < {pa_min}/{sampling_factor};
--Delete rows where final_count_selection = 0
DELETE FROM opekepe_spatial.prediction
WHERE final_count_selection = 0;

/*Export */ 
ALTER TABLE opekepe_spatial.training
ALTER COLUMN eda_id TYPE integer;

ALTER TABLE opekepe_spatial.training
DROP COLUMN ogc_fid,
DROP COLUMN objectid,
DROP COLUMN rfv_crop,
DROP COLUMN rfv_group,
DROP COLUMN fallow_det,
DROP COLUMN fallow_per,
DROP COLUMN shape_area,
DROP COLUMN area_ha,
DROP COLUMN shape_leng,
DROP COLUMN elongation,
DROP COLUMN mlm,
DROP COLUMN pixminfilter,
DROP COLUMN count, 
DROP COLUMN crop_select,
DROP COLUMN pixbestfilter,
DROP COLUMN seqnum,
DROP COLUMN cnt,
DROP COLUMN final_count_selection; 

ALTER TABLE opekepe_spatial.prediction
ALTER COLUMN eda_id TYPE integer;

ALTER TABLE opekepe_spatial.prediction
DROP COLUMN ogc_fid,
DROP COLUMN objectid,
DROP COLUMN rfv_crop,
DROP COLUMN rfv_group,
DROP COLUMN fallow_det,
DROP COLUMN fallow_per,
DROP COLUMN shape_area,
DROP COLUMN area_ha,
DROP COLUMN shape_leng,
DROP COLUMN elongation,
DROP COLUMN mlm,
DROP COLUMN pixminfilter,
DROP COLUMN count, 
DROP COLUMN crop_select,
DROP COLUMN pixbestfilter,
DROP COLUMN seqnum,
DROP COLUMN cnt,
DROP COLUMN final_count_selection; 
"""

# execute sql commands
with connection:
    with connection.cursor() as cursor:
        cursor.execute(SQL_EXE)
        print(f"SQL commands executed successfully")


# Export training and prediction datasets
export_training = f"ogr2ogr -f \"ESRI Shapefile\" -t_srs EPSG:4326 \"{working_dir}SHAPES/training.shp\" PG:\"{connection_string}\" -sql \"SELECT * FROM opekepe_spatial.training\" -lco GEOMETRY_NAME=shape -nlt POLYGON -lco ENCODING=UTF-8"
run(export_training)

export_prediction = f"ogr2ogr -f \"ESRI Shapefile\" -t_srs EPSG:4326 \"{working_dir}SHAPES/prediction.shp\" PG:\"{connection_string}\" -sql \"SELECT * FROM opekepe_spatial.prediction\" -lco GEOMETRY_NAME=shape -nlt POLYGON -lco ENCODING=UTF-8"
run(export_prediction)

shape_file = f"{working_dir}SHAPES/AOI.geojson"
export_aoi = f"ogr2ogr -f \"GeoJSON\" -t_srs EPSG:4326 \"{shape_file}\" PG:\"{connection_string}\" -sql \"SELECT * FROM opekepe_spatial.AOIs\" -lco GEOMETRY_NAME=shape -nlt POLYGON -lco ENCODING=UTF-8"
run(export_aoi, f"Exporting shape file {shape_file}")

if not os.path.isfile(shape_file):
    exitp(
        5, f"Parcel shape file {shape_file} cannot be empty or does not exist")

# --------------------------------

shutil.copy2("NIVA_eoDataProcess.sh", working_dir)

run(
    f'cd docker && docker-compose up -d && docker-compose run -e JOB_NAME={job_name} --rm eo-tools /bin/bash -c "data/{job_name}/NIVA_eoDataProcess.sh"')


print("-------------------------------- NIVA eoDataProcess --------------------------------")

# copy csv files from TABLES folder
prediction_files = [
    'ZonalStatistics_1D_prediction_',
    'ZonalStatistics_GLCM_HOMOGENITY_prediction_',
    'ZonalStatistics_GapFilled_NDVI_prediction_',
    'ZonalStatistics_GLCM_ENTROPY_prediction_',
    'ZonalStatistics_GLCM_VARIANCE_prediction_'
]

train_files = [
    'ZonalStatistics_1D_training_',
    'ZonalStatistics_GLCM_HOMOGENITY_training_',
    'ZonalStatistics_GapFilled_NDVI_training_',
    'ZonalStatistics_GLCM_ENTROPY_training_',
    'ZonalStatistics_GLCM_VARIANCE_training_'
]

destination_train_dir = '../Classifier_1/Data/train/csvs/'
destination_test_dir = '../Classifier_1/Data/test/csvs/'

for file in train_files:
    shutil.copy2(f"{working_dir}/TABLES/{file+job_name+'.csv'}",
                 destination_train_dir)

for file in prediction_files:
    shutil.copy2(f"{working_dir}/TABLES/{file+job_name+'.csv'}",
                 destination_test_dir)

# cd to classifier #1 and execute classifier
run('cd ../Classifier_1/ && python3 train.py && python3 inference.py')

results_file = "../Classifier_1/predicted_results/Predicted results.csv"
if os.path.isfile(results_file):
    shutil.copy2(results_file, f"{working_dir}/TABLES/")

# Import predicted results.csv to DB
cmd1 = f'ogr2ogr -append -update -progress -f "PostgreSQL" PG:"{connection_string}"  "{working_dir}/TABLES/Predicted results.csv" -nln opekepe_spatial.predicted_results_csv -nlt NONE'
run(cmd1)

SQL_SH = """
-- Tidy up table and change data types
ALTER TABLE opekepe_spatial.predicted_results_csv
ALTER COLUMN eda_id TYPE float USING eda_id::double precision;

ALTER TABLE opekepe_spatial.predicted_results_csv
RENAME COLUMN probality TO probability;

ALTER TABLE opekepe_spatial.predicted_results_csv
ALTER COLUMN probability TYPE float USING probability::double precision;

-- Merge predicted_results with parcels 
CREATE TABLE opekepe_spatial.predicted_results AS 
SELECT DISTINCT predicted_results_csv.*, parcels.shape
FROM opekepe_spatial.predicted_results_csv
LEFT JOIN opekepe_spatial.parcels
ON predicted_results_csv.eda_id = parcels.eda_id;

ALTER TABLE opekepe_spatial.predicted_results 
DROP COLUMN ogc_fid,
DROP COLUMN field_1;

ALTER TABLE opekepe_spatial.predicted_results
ALTER COLUMN eda_id TYPE integer;
"""

with connection:
    with connection.cursor() as cursor:
        cursor.execute(SQL_SH)
        print(f"SQL for exporting results executed successfully")

# Export predicted_results.shp
cmd2 = f'ogr2ogr -f "ESRI Shapefile" -t_srs EPSG:4326 "{working_dir}/SHAPES/predicted_results.shp" PG:"{connection_string}" -sql "SELECT * FROM opekepe_spatial.predicted_results" -lco GEOMETRY_NAME=shape -nlt POLYGON -lco ENCODING=UTF-8'
run(cmd2)

exit(0)
