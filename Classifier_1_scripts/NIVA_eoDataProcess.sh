#!/bin/bash
#

############################################
# User Configuration
############################################

#PROCESSING SCRIPTS

##ESA SNAP Graphs (GPT)
###GPT_GLCM_Texture.xml

##SAGA Toolchains
###SAGA_CMD_Mosaic_QM_Export_Clip_GCS_WGS84.xml (NEW) -Done
###SAGA_CMD_Index_QM_Percentage.xml
###SAGA_CMD_LayerStack_int16.xml		(NEW) -Done
###SAGA_CMD_GapFill_NDVI_BV_Correction.xml
###SAGA_CMD_Script_Zonal_Statistics.xml

##Python Scripts
###mosaicing.py (uses saga_toolchain)

##R Scripts
###Savitzky_Golay_1D_Filter_CMD.r 
###Split_NDVIts_LayerStack.r 

##Shell Scripts
###GDALINFO.sh
###QM_Index.sh (saga_toolchain)
###CreateStack.sh (saga_toolchain)
###Batch_GLCM.sh (esa-snap GPT)
###Zonal_Statistics_NESTED.sh (saga_toolchain)


############################################
# Command line handling
############################################
# first parameter is Sen4Cap parent folder definition
# sen4capParent=
# second parameter is the job name into which the code runs
# nivaJob="$2"
############################################
# Main processing
############################################

# export JOB_NAME=${nivaJob}
export SCRIPT_FOLDER=/eo-tools/user-scripts/
export JOB_FOLDER=/eo-tools/data/${JOB_NAME}

#Import sen2agri L3B NDVI raster layers from Sen4Cap job folder 

echo @@@@@@@@@@@@@@@@@@@@@ COPY NDVI LAYERS FROM SEN4CAP @@@@@@@@@@@@@@@@@@@@@

export NDVI_DIR=${JOB_FOLDER}/RAW_NDVI_L3B/NDVI/
export SEN2AGRI_PREFIX="S2AGRI_L3B_SNDVI"

# cd ${sen4capParent}
# find . -type f -name ${SEN2AGRI_PREFIX}*.TIF -exec cp {} ${NDVI_DIR} \;

#NDVI Mosaicking - Create Input/Output date lists

echo @@@@@@@@@@@@@@@@@@@@@ CREATE NDVI MOSAICS @@@@@@@@@@@@@@@@@@@@@

export PYTHON_SCRIPT_FOLDER=/custom-toolbox/
export SHAPE_FOLDER="${JOB_FOLDER}/SHAPES"
export AOI_SHAPEFILE="${SHAPE_FOLDER}/AOI.geojson"

cd ${PYTHON_SCRIPT_FOLDER}
./mosaicing.py --product-prefix "${SEN2AGRI_PREFIX}" --raster-root-dir "${NDVI_DIR}" --toolchain SAGA_CMD_Mosaic_QM_Export_Clip_GCS_WGS84 --geojson "${AOI_SHAPEFILE}"

export MOSAIC_DIR=${JOB_FOLDER}/RAW_NDVI_L3B/NDVI_S2_Mosaic/
export QM_MOSAIC_DIR=${JOB_FOLDER}/RAW_NDVI_L3B/NDVI_S2_QM_Mosaic/
export NDVI_ts_DIR=${JOB_FOLDER}/L3_C/GapFilled_NDVI/

cd ${NDVI_DIR}/${SEN2AGRI_PREFIX}_Mosaic
find . -type f -name '*.tif' -exec mv {} ${MOSAIC_DIR} \;
rm -d ${NDVI_DIR}/${SEN2AGRI_PREFIX}_Mosaic

cd ${NDVI_DIR}/${SEN2AGRI_PREFIX}_QM_Mosaic
find . -type f -name '*.tif' -exec mv {} ${QM_MOSAIC_DIR} \;
rm -d ${NDVI_DIR}/${SEN2AGRI_PREFIX}_QM_Mosaic

cd ${NDVI_DIR}
dos2unix InputDates.txt
dos2unix OutputDates.txt
mv InputDates.txt OutputDates.txt ${NDVI_ts_DIR} 

export LOG_DIR=${JOB_FOLDER}/LOGS

cd ${SCRIPT_FOLDER}
./GDALINFO.sh ${MOSAIC_DIR} 2>&1 | tee gdalinfo_NDVI_Mosaic.log
./GDALINFO.sh ${QM_MOSAIC_DIR} 2>&1 | tee gdalinfo_NDVI_QM_Mosaic.log

mv gdalinfo_NDVI_Mosaic.log gdalinfo_NDVI_QM_Mosaic.log ${LOG_DIR}

#Create Quality Mask Percentage Index as % of masked pixels of Mosaicked Layers 

echo @@@@@@@@@@@@@@@@@@@@@ CREATE QUALITY MASK PERCENTAGE INDEX @@@@@@@@@@@@@@@@@@@@@

./QM_Index.sh ${QM_MOSAIC_DIR}

mv ${QM_MOSAIC_DIR}/QM_Percentage_Index.tif ${LOG_DIR}

#Create NDVI and QM LayerStacks 

echo @@@@@@@@@@@@@@@@@@@@@ CREATE NDVI AND QM LAYERSTACKS @@@@@@@@@@@@@@@@@@@@@

./CreateStack.sh ${MOSAIC_DIR} ${QM_MOSAIC_DIR} ${NDVI_ts_DIR} 

#Process NDVI for temporal interpolation - gap fill at 10 day composites

echo @@@@@@@@@@@@@@@@@@@@@ CREATE GAP FILLED NDVI @@@@@@@@@@@@@@@@@@@@@

cd ${NDVI_ts_DIR}

otbcli_ImageTimeSeriesGapFilling -in NDVI_LAYERSTACK.img -mask QM_LAYERSTACK.img -out NDVI_S2_Mosaic_GapFill.tif -comp 1 -it spline -id InputDates.txt -od OutputDates.txt -ram 8000

gdalinfo NDVI_S2_Mosaic_GapFill.tif -stats 2>&1 | tee gdalinfo_NDVI_S2_Mosaic_GapFill.log
mv gdalinfo_NDVI_S2_Mosaic_GapFill.log  ${LOG_DIR}

#Correct GapFilled NDVI for OutofBound values

echo @@@@@@@@@@@@@@@@@@@@@ PROCESS GAP FILLED NDVI @@@@@@@@@@@@@@@@@@@@@

cd ${NDVI_ts_DIR} 
saga_cmd toolchains SAGA_CMD_GapFill_NDVI_BV_Correction -GapFilled_NDVI="NDVI_S2_Mosaic_GapFill.tif" -GapFilled_NDVI_BV_COR="NDVI_S2_Mosaic_GapFill_BV.tif"

gdalinfo NDVI_S2_Mosaic_GapFill_BV.tif -stats 2>&1 | tee gdalinfo_NDVI_S2_Mosaic_GapFill_BV.log
mv gdalinfo_NDVI_S2_Mosaic_GapFill_BV.log  ${LOG_DIR}

rm NDVI_S2_Mosaic_GapFill.tif

#Compute  Fist Derivative (1D) on NDVI Timeseries

echo @@@@@@@@@@@@@@@@@@@@@ COMPUTE NDVI FIRST DERIVATIVE @@@@@@@@@@@@@@@@@@@@@

cd ${SCRIPT_FOLDER}

##Install R package dependency - library(doParallel)
R -e "install.packages('snow')"

export WorkingDirectory=${NDVI_ts_DIR} 
export InterpolatedLayerstack=NDVI_S2_Mosaic_GapFill_BV.tif	
export StartDate=$(head -n 1 ${NDVI_ts_DIR}/OutputDates.txt)	
export EndDate=$(tail -n 1 ${NDVI_ts_DIR}/OutputDates.txt)		
export TimeIntervalDays=10										

Rscript --vanilla Savitzky_Golay_1D_Filter_CMD.r "${WorkingDirectory}" "${InterpolatedLayerstack}" "${StartDate}" "${EndDate}" "${TimeIntervalDays}"

#Split NDVI GapFilled LayerStack into separate layers 

echo @@@@@@@@@@@@@@@@@@@@@ SPLIT NDVI LAYERSTACK @@@@@@@@@@@@@@@@@@@@@

cd ${NDVI_ts_DIR} 
mkdir NDVI_SL
export NDVI_TIME=${NDVI_ts_DIR}NDVI_SL

cd ${SCRIPT_FOLDER}

Rscript --vanilla Split_NDVIts_LayerStack.r "${NDVI_ts_DIR}"

# Rename NDVI Layers

echo @@@@@@@@@@@@@@@@@@@@@ RENAME NDVI LAYERS @@@@@@@@@@@@@@@@@@@@@

cd ${NDVI_TIME}
ls -1 "$PWD/"*.tif 2>&1 | tee NDVI_OldName.txt
cat ${NDVI_ts_DIR}/OutputDates.txt |awk -v MAIN_PATH=${NDVI_TIME}/NDVI_S2_Mosaic_GapFill_ '{print MAIN_PATH$0".tif"}' |tee NDVI_NewName.txt
paste -d' ' NDVI_OldName.txt NDVI_NewName.txt |tee NDVI_Rename.txt
xargs -a NDVI_Rename.txt -n 2 mv

cd ${NDVI_TIME}
find . -type f -name '*.tif' -exec mv {} ${NDVI_ts_DIR} \;
find . -type f -name '*.txt' -exec mv {} ${NDVI_ts_DIR} \;

rm -d ${NDVI_TIME}	

cd ..			
rm NDVI_S2_Mosaic_GapFill_BV.tif	

#Rename NDVI 1D Layers

echo @@@@@@@@@@@@@@@@@@@@@ RENAME NDVI FIRST DERIVATIVE LAYERS @@@@@@@@@@@@@@@@@@@@@

cd ${JOB_FOLDER}/L3_C/1D/
ls -1 "$PWD/"*.tif 2>&1 | tee 1D_OldName.txt
cat ${NDVI_ts_DIR}/OutputDates.txt |awk -v MAIN_PATH=${JOB_FOLDER}/L3_C/1D/NDVI_1st_Derivative_ '{print MAIN_PATH$0".tif"}' |tee 1D_NewName.txt
paste -d' ' 1D_OldName.txt 1D_NewName.txt |tee 1D_Rename.txt
xargs -a 1D_Rename.txt -n 2 mv


#Compute GLCM Image Texture from NDVI Timeseries

echo @@@@@@@@@@@@@@@@@@@@@ COMPUTE GLCM IMAGE TEXTURE FROM NDVI LAYERS @@@@@@@@@@@@@@@@@@@@@

cd ${SCRIPT_FOLDER}

export GLCM_GRAPHS=${SCRIPT_FOLDER}/GPT_GLCM_Texture.xml
export READ=${NDVI_ts_DIR}

export OUTPUT_ENTROPY=${JOB_FOLDER}/L3_C/GLCM_ENTROPY/
export OUTPUT_VARIANCE=${JOB_FOLDER}/L3_C/GLCM_VARIANCE/
export OUTPUT_HOMOGENITY=${JOB_FOLDER}/L3_C/GLCM_HOMOGENITY/

./Batch_GLCM.sh \
 ${GLCM_GRAPHS} \
 ${READ} \
 ${OUTPUT_ENTROPY} \
 ${OUTPUT_VARIANCE} \
 ${OUTPUT_HOMOGENITY}
 
#Compute Zonal Statistics on Parcel Geometries

echo @@@@@@@@@@@@@@@@@@@@@ COMPUTE ZONAL STATISTICS ON PARCELS @@@@@@@@@@@@@@@@@@@@@

export SOURCE_VAR="GapFilled_NDVI 1D GLCM_ENTROPY GLCM_HOMOGENITY GLCM_VARIANCE"
export SHAPEFILE="training prediction"     
export JOB=${JOB_NAME}

export SHAPE_FOLDER=${JOB_FOLDER}/SHAPES/	
export TABLE_FOLDER=${JOB_FOLDER}/TABLES/

 cd ${SCRIPT_FOLDER}

./Zonal_Statistics_NESTED.sh "${SOURCE_VAR}" "${SHAPEFILE}" "${SHAPE_FOLDER}" "${TABLE_FOLDER}" "${JOB}" "${JOB_FOLDER}"

echo @@@@@@@@@@@@@@@@@@@@@ EO PROCESS COMPLETE @@@@@@@@@@@@@@@@@@@@@
