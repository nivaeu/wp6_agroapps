#!/bin/bash

# Build the EO tools image

cd docker/eo-tools
docker build -t agroapps/eo-tools:0.3.0 .
