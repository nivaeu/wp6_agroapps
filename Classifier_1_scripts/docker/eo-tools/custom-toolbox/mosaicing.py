#!/usr/bin/python3


import click
import re
import subprocess

from os import walk, path, mkdir


def _classified_rasters_in_dates(raster_root_dir: str, product_prefix: str) -> dict:

    rasters = {}
    for root_dir, dirs, files in walk(raster_root_dir):
        for file in files:
            if not file.startswith(product_prefix):
                continue
            date_re = re.search(r"\d{8}(?=T)", file)
            if date_re:
                filepath = f"{root_dir}/{file}"
                if date_re.group() not in rasters:
                    rasters[date_re.group()] = [filepath]
                else:
                    rasters[date_re.group()].append(filepath)

    return rasters


def stich(
    rasters: list, toolchain: str, geojson: str, output_product: str, output_mask: str
):

    cmd = [
        "saga_cmd",
        "toolchains",
        toolchain,
        f"-INPUT_GRIDLIST={';'.join(rasters)}",
        f"-SHAPE={geojson}",
        f"-Mosaic_Export={output_product}",
        f"-Mosaic_QM_Export={output_mask}",
    ]

    try:
        subprocess.run(cmd, check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        result = True
    except subprocess.CalledProcessError as e:
        print(str(e))
        result = False

    return result


def _calculate_output_dates(mosaicked_dates):

    first_date = mosaicked_dates[0]
    first_year = int(first_date[:4])
    first_month = int(first_date[4:6])

    last_date = mosaicked_dates[-1]
    last_year = int(last_date[:4])
    last_month = int(last_date[4:6])

    output_dates = []
    for year in range(first_year, last_year + 1):
        for month in range(1, 12 + 1):
            if year == first_year and month <= first_month:
                continue
            if year == last_year and month >= last_month:
                continue

            last_month_day = 30 if month != 2 else 28
            output_dates = output_dates + [
                f"{str(year).zfill(2)}{str(month).zfill(2)}10",
                f"{str(year).zfill(2)}{str(month).zfill(2)}20",
                f"{str(year).zfill(2)}{str(month).zfill(2)}{str(last_month_day).zfill(2)}",
            ]

    return output_dates


@click.command()
@click.option(
    "--product-prefix",
    type=str,
    required=True,
    help="Selects only rasters that have this prefix (ex. NDVI). This will be the prefix of the resulting files.",
)
@click.option(
    "--toolchain",
    required=True,
    help="The SAGA toolchain to be used. Its xml must be in /usr/local/share/saga/toolchains",
)
@click.option(
    "--raster-root-dir",
    type=click.Path(exists=True),
    required=True,
    help="The path to the raster root directory.",
)
@click.option(
    "--geojson",
    type=click.Path(exists=True),
    required=True,
    help="The path to the resulting AOI geojson.",
)
def mosaic(product_prefix, toolchain, raster_root_dir, geojson):
    """
    Takes a path to a directory with raster files and merges them based on their dates.
    Ex. Usage: /custom-toolbox/mosaicing.py --product-prefix NDVI --toolchain SAGA_CMD_Mosaic_QM_Export_Clip --raster-root-dir /myProducts --geojson /myProducts/AOI.geojson
    Also, it creates twi txt files in the raster-root-dir:
        1. A list of the 8 digit dates (YYYYMMDD) of the mosaicked products
        2. A list of 8 digit dates (YYYYMMDD) of the 10th 20th and 30th (or 28th for Feb) day of each month
           for the period between the first and last month of the mosaicked products. This list does not
           include dates in the first or last month
    """
    product_rasters_in_dates = _classified_rasters_in_dates(
        raster_root_dir, product_prefix
    )
    print(product_rasters_in_dates)

    mosaic_dir = f"{raster_root_dir}/{product_prefix}_Mosaic"
    if not path.exists(mosaic_dir):
        mkdir(mosaic_dir)

    mask_dir = f"{raster_root_dir}/{product_prefix}_QM_Mosaic"
    if not path.exists(mask_dir):
        mkdir(mask_dir)

    for date, rasters in product_rasters_in_dates.items():
        output_product = f"{mosaic_dir}/{product_prefix}_Mosaic_{date}.tif"
        output_mask = f"{mask_dir}/{product_prefix}_QM_Mosaic_{date}.tif"
        stich(rasters, toolchain, geojson, output_product, output_mask)
        print(f"Outputing product in: {output_product}")
        print(f"Outputing mask in: {output_mask}")

    # Makes sure the dates are ordered before processing them
    ordered_dates = sorted(product_rasters_in_dates.keys())
    with open(
        f"{raster_root_dir}/InputDates.txt", "w", encoding="utf8"
    ) as input_stream:
        input_stream.write("\n".join(ordered_dates))

    with open(
        f"{raster_root_dir}/OutputDates.txt", "w", encoding="utf8"
    ) as output_stream:
        output_stream.write("\n".join(_calculate_output_dates(ordered_dates)))


if __name__ == "__main__":
    mosaic()
