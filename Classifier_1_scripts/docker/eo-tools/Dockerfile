FROM ubuntu:bionic

ARG EO_HOME_DIR=/eo-tools
ARG DEBIAN_FRONTEND=noninteractive

# versions of ESA SNAP
# see https://step.esa.int/main/download/snap-download/ for available versions
ARG ESA_SNAP_MAIN_VERSION_8=8
ARG ESA_SNAP_MAIN_VERSION_7=7
ARG ESA_SNAP_MAIN_VERSION_6=6

# version of SAGA-GIS
# see https://sourceforge.net/projects/saga-gis/files/ for available versions
ARG SAGA_MAJOR_VERSION=7
ARG SAGA_MINOR_VERSION=7.7.1

# version of ESA Sen2Cor
# see https://step.esa.int/main/third-party-plugins-2/sen2cor/ for available versions
ARG SEN2COR_VERSION=2.9.0
ARG SEN2COR_VERSION_ALT=02.09.00

# version of Orfeo ToolBox
ARG OTB_VERSION=7.4.0
ARG OTB_VERSION_SHORT=7.4

# set environment variables
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# update apt
RUN apt-get update

# install basic packages
RUN apt-get update && \
    apt-get install -y \
    software-properties-common \
    wget \
    python3-pip \
    cron \
    dos2unix=7.3.4-3

# install compilation and configuration tools
RUN apt-get install -y \
    g++ \
    make \
    automake \
    libtool

# install prequisities (for SAGA)
RUN apt-get install -y \
    libwxgtk3.0-dev \
    libtiff5-dev \
    libgdal-dev \
    libproj-dev \
    libexpat-dev \
    wx-common \
    libogdi3.2-dev \
    unixodbc-dev \
    libhpdf-dev \
    libsvm-dev

# install GDAL
RUN apt-get install -y \
    gdal-bin \
    python3-gdal

# install Fortran libraries (needed for running a graph that creates the SAR indexes)
RUN apt-get install -y \
    libgfortran3

# clean up
RUN apt-get clean

# install ESA-SNAP and upgrade
# Dependencies
RUN apt-get install -y \
    fonts-dejavu \
    fontconfig \
    procps

# the libraries will be installed in /usr/local/snap/
# Attempting to upgrade snap results in hanging which is a known snap error
# https://senbox.atlassian.net/wiki/spaces/SNAP/pages/30539785/Update+SNAP+from+the+command+line
# A workaround is parse the upgrade output until the line updates=0 appears and then kill the process
# Killing the process results in a non 0 exit code which causes the build to fail.
# To override the fail exit code `|| true` is used since the failing process is in the middle of a chain.
# the symlink points to the -> gpt8
RUN _TMPDIR=$(mktemp -d) && \
    cd $_TMPDIR && \
    wget http://step.esa.int/downloads/${ESA_SNAP_MAIN_VERSION_8}.0/installers/esa-snap_all_unix_${ESA_SNAP_MAIN_VERSION_8}_0.sh && \
    chmod +x esa-snap_all_unix_${ESA_SNAP_MAIN_VERSION_8}_0.sh && \
    ./esa-snap_all_unix_${ESA_SNAP_MAIN_VERSION_8}_0.sh -q && \
    mv /usr/local/snap /usr/local/snap-${ESA_SNAP_MAIN_VERSION_8} && \
    /usr/local/snap-${ESA_SNAP_MAIN_VERSION_8}/bin/snap --nosplash --nogui --modules --update-all 2>&1 | while read -r line; do     echo "$line";     [ "$line" = "updates=0" ] && sleep 2 && pkill -TERM -f "/jre/bin/java"; done || true && \
    ln -s /usr/local/snap-${ESA_SNAP_MAIN_VERSION_8}/bin/gpt /usr/bin/gpt${ESA_SNAP_MAIN_VERSION_8} && \
    cd && \
    rm -rf $_TMPDIR

# the symlink points to the -> gpt7
RUN _TMPDIR=$(mktemp -d) && \
    cd $_TMPDIR && \
    wget http://step.esa.int/downloads/${ESA_SNAP_MAIN_VERSION_7}.0/installers/esa-snap_all_unix_${ESA_SNAP_MAIN_VERSION_7}_0.sh && \
    chmod +x esa-snap_all_unix_${ESA_SNAP_MAIN_VERSION_7}_0.sh && \
    ./esa-snap_all_unix_${ESA_SNAP_MAIN_VERSION_7}_0.sh -q && \
    mv /usr/local/snap /usr/local/snap-${ESA_SNAP_MAIN_VERSION_7} && \
    /usr/local/snap-${ESA_SNAP_MAIN_VERSION_7}/bin/snap --nosplash --nogui --modules --update-all 2>&1 | while read -r line; do     echo "$line";     [ "$line" = "updates=0" ] && sleep 2 && pkill -TERM -f "/jre/bin/java"; done || true && \
    ln -s /usr/local/snap-${ESA_SNAP_MAIN_VERSION_7}/bin/gpt /usr/bin/gpt${ESA_SNAP_MAIN_VERSION_7} && \
    cd && \
    rm -rf $_TMPDIR

# the symlink points to the -> gpt6
RUN _TMPDIR=$(mktemp -d) && \
    cd $_TMPDIR && \
    wget http://step.esa.int/downloads/${ESA_SNAP_MAIN_VERSION_6}.0/installers/esa-snap_all_unix_${ESA_SNAP_MAIN_VERSION_6}_0.sh && \
    chmod +x esa-snap_all_unix_${ESA_SNAP_MAIN_VERSION_6}_0.sh && \
    ./esa-snap_all_unix_${ESA_SNAP_MAIN_VERSION_6}_0.sh -q && \
    mv /usr/local/snap /usr/local/snap-${ESA_SNAP_MAIN_VERSION_6} && \
    /usr/local/snap-${ESA_SNAP_MAIN_VERSION_6}/bin/snap --nosplash --nogui --modules --update-all 2>&1 | while read -r line; do     echo "$line";     [ "$line" = "updates=0" ] && sleep 2 && pkill -TERM -f "/jre/bin/java"; done || true && \
    ln -s /usr/local/snap-${ESA_SNAP_MAIN_VERSION_6}/bin/gpt /usr/bin/gpt${ESA_SNAP_MAIN_VERSION_6} && \
    cd && \
    rm -rf $_TMPDIR

# compile SAGA
# the libraries will be installed in /usr/local/lib/saga
RUN _TMPDIR=$(mktemp -d) && \
    cd $_TMPDIR && \
    wget -O saga-$SAGA_MINOR_VERSION.tar.gz "https://sourceforge.net/projects/saga-gis/files/SAGA - $SAGA_MAJOR_VERSION/SAGA - $SAGA_MINOR_VERSION/saga-$SAGA_MINOR_VERSION.tar.gz/download" && \
    tar xvzf saga-$SAGA_MINOR_VERSION.tar.gz && \
    cd saga-$SAGA_MINOR_VERSION/ && \
    autoreconf -fi && \
    ./configure && \
    make -j$(nproc) && \
    make install && \
    ldconfig && \
    cd && \
    rm -rf $_TMPDIR

# install Sen2Cor
# the libraries will be installed in /usr/local/sen2cor/
RUN _TMPDIR=$(mktemp -d) && \
    cd $_TMPDIR && \
    wget http://step.esa.int/thirdparties/sen2cor/${SEN2COR_VERSION}/Sen2Cor-${SEN2COR_VERSION_ALT}-Linux64.run && \
    chmod +x Sen2Cor-${SEN2COR_VERSION_ALT}-Linux64.run && \
    ./Sen2Cor-${SEN2COR_VERSION_ALT}-Linux64.run --target /usr/local/sen2cor --quiet && \
    ln -s /usr/local/sen2cor/bin/L2A_Process /usr/bin/L2A_Process && \
    cd && \
    rm -rf $_TMPDIR

# install python requirements
ADD requirements.txt /
RUN pip3 install --upgrade pip && \
    pip3 install -r /requirements.txt
RUN rm /requirements.txt

# setup ESA SNAP's home folder
# when ESA SNAP runs as www-data user, it stores data in /var/www/.snap
# so we need to make sure that this folder is accessible and writable by www-data
# and also add a configuration file to it
ADD --chown=www-data:www-data snap.properties /var/www/.snap/etc/


# install user packages
RUN apt-get install -y \
    vim \
    nano \
    tmux \
    htop

# Install R and R packages
RUN apt-get update && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9 && \
    add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran35/" && \
    apt-get -y update && \
    apt-get -y install \
    r-base \
    default-jre \
    default-jdk \
    libudunits2-dev && \
    R CMD javareconf

ADD dependencies.r /
RUN Rscript /dependencies.r
RUN rm /dependencies.r

# Install OTB
RUN apt-get -y update && \
    apt-get -y install \
        libx11-6 \
        libxext6 \
        libxau6 \
        libxxf86vm1 \
        libxdmcp6 \
        libdrm2 \
        libexpat-dev  \
        libexpat1-dev \
        cmake \
        build-essential \
        libgsl-dev \
        libglu1-mesa-dev \
        freeglut3-dev \
        mesa-common-dev \
        git

RUN wget https://www.orfeo-toolbox.org/packages/OTB-${OTB_VERSION}-Linux64.run && \
    chmod +x ./OTB-${OTB_VERSION}-Linux64.run && \
    ./OTB-${OTB_VERSION}-Linux64.run  && \
    rm ./OTB-${OTB_VERSION}-Linux64.run && \
    cd ./OTB-${OTB_VERSION}-Linux64 && \
    /bin/bash -c "source otbenv.profile" && \
    ctest -S share/otb/swig/build_wrapping.cmake -VV && \
    cd / && \
    git clone https://gitlab.orfeo-toolbox.org/jinglada/phenotb.git && \
    cd /phenotb && \
    mkdir build && cd build && \
    cmake \
        -D OTB_DIR=/OTB-${OTB_VERSION}-Linux64/lib/cmake/OTB-${OTB_VERSION_SHORT} \
        -D OTB_BUILD_MODULE_AS_STANDALONE=ON \
        -D CMAKE_INSTALL_PREFIX=/phenotb/install \
        -D CMAKE_INSTALL_RPATH=/phenotb/install/lib \
        -D CMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE ../  && \
    make install

ENV PATH="/OTB-${OTB_VERSION}-Linux64/bin:${PATH}"
ENV OTB_APPLICATION_PATH="/phenotb/install/lib:${OTB_APPLICATION_PATH}"
ENV PATH="/phenotb/install/bin:${PATH}"

WORKDIR ${EO_HOME_DIR}
