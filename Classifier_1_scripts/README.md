# opekepe scripts

## processing flow για την αυτοματοποίηση του CL1 στο NIVA


```
OPEKEPE Classifier#1 Runner v1.0.2

Loading environment variables...
usage: [-h] -j JOB_NAME -p PARCEL_SHP -a AOI -m PA_MIN -s SAMPLING_FACTOR [-d DATA_FOLDER] -f SEN4CAP_JOB_FOLDER

Prepare and execute toolchains for Classifier #1

options:
  -h, --help            show this help message and exit
  -j JOB_NAME, --job_name JOB_NAME
  -p PARCEL_SHP, --parcel_shp PARCEL_SHP
                        Path to parcel shapefile
  -a AOI, --aoi AOI     AOIs file in GeoJSON format
  -m PA_MIN, --pa_min PA_MIN
                        Minimum parcels number to be considered
  -s SAMPLING_FACTOR, --sampling_factor SAMPLING_FACTOR
                        Sampling factor
  -d DATA_FOLDER, --data_folder DATA_FOLDER
                        Data folder (where results will be stored in a job_name directory)
  -f SEN4CAP_JOB_FOLDER, --sen4cap_job_folder SEN4CAP_JOB_FOLDER
                        Full path to SEN4CAP job folder (e.g. /mnt/archive/hmathia2021)

```

example test run:

```
cd docker
docker-compose up -d --remove-orphans

cd ..
python3 process.py -j INTERNAL_TEST_2 -a /home/eouser/eo-tools/data/IMATHIA_2020_SEN4CAP/VECTOR_DATA_TESTING/AOI.geojson --pa_min=50 --sampling_factor=0.2 -p /home/eouser/eo-tools/data/IMATHIA_2020_SEN4CAP/VECTOR_DATA_TESTING/Imathia2020_LPIS.shp -f /home/eouser/eo-tools/data/IMATHIA_2020_SEN4CAP/

```

## INSTALL

Required libraries/tools

```sh
apt install gdal-bin
```

install psycopg2 and dotenv with apt or pip

```sh
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

or

```sh
sudo apt install python3-psycopg2
sudo apt install python3-dotenv
````

## EO-tools docker image

use docker image

```sh
docker pull agroapps/eo-tools:0.3.0
```
