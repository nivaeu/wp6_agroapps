#!/bin/bash

# Bring the docker stack up

cd docker
docker-compose down
docker-compose up -d
