#!/bin/bash
#

############################################
# User Configuration
############################################

# adapt this path to your needs
	#gptPath="/?????/gpt.sh"

	#gptPath="/usr/local/snap-6/bin/gpt.sh"

	#gptPath=$(which gpt)

############################################
# Command line handling
############################################

# first parameter is toolchain identifier
toolchain="$1"

# use second parameter for path to source products
sourceDirectory="$2"

# use third parameter for path to target products
targetDirectory="$3"

# the fourth parameter is a file prefix for the target product name, typically indicating the type of processing
targetFilePrefix="$4"

# The fifth parameter is basename of the polygon vector accross which we compute zonal stats
zonalVector="$5"

############################################
# Main processing
############################################

# Constant

OUTPUT_EXTENSION="csv"

# Create the target directory
mkdir -p "${targetDirectory}"

for RasterFilePath in $(ls -1d "${sourceDirectory}"/*.tif); do # For each directory in a dir list inside source directory do ...# 

  #absoluteRasterFilePath=$(cd ${RasterFilePath} && pwd -P) #Change directory to the input directory and print working directory path #

  sourceRaster=${RasterFilePath}
  
  VectorFile="${sourceDirectory}/${zonalVector}"
  
  RasterFile=$(basename $RasterFilePath) #Extract input name with .tif extension, form absolute path#
  RasterFileStem=${RasterFile%.*} #Extract input name without any extension#

  targetFile="${targetDirectory}/${targetFilePrefix}_${RasterFileStem}.${OUTPUT_EXTENSION}"
  
  saga_cmd toolchains "${toolchain}" \
    -Raster_Layer_GRIDLIST="${sourceRaster}" \
    -Feature_Zones_POLYGONS="${VectorFile}" \
    -ZONAL_STATS_TAB="${targetFile}"

    
  done
