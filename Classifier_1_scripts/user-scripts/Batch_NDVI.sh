#!/bin/bash
#

############################################
# User Configuration
############################################

# adapt this path to your needs
	#gptPath="/?????/gpt.sh"

	#gptPath="/usr/local/snap-6/bin/gpt.sh"

	#gptPath=$(which gpt)

############################################
# Command line handling
############################################

# first parameter is a path to the graph xml
graphXmlPath="$1"

# second parameter spatial subset wkt
wkt="$2"

# use third parameter for path to source products
sourceDirectory="$3"

# use fourth parameter for path to target products
targetDirectory="$4"

# the fifth parameter is a file prefix for the target product name, typically indicating the type of processing
targetFilePrefix="$5"


############################################
# Main processing
############################################

# Constant
XML_HEADER="MTD_MSIL2A.xml"
OUTPUT_EXTENSION="tif"

# Create the target directory
mkdir -p "${targetDirectory}"

for safeFilePath in $(ls -1d "${sourceDirectory}"/S2*.SAFE); do # For each directory in a dir list inside source directory do ...# 

  absoluteSafePath=$(cd ${safeFilePath} && pwd -P) #Change directory to the SAFE directory and print working directory path #

  sourceFile=${absoluteSafePath}/${XML_HEADER}
  
  safeFile=$(basename $absoluteSafePath) #Extract SAFE folder name with .SAFE extension, form absolute path#
  safeFileStem=${safeFile%.*} #Extract SAFE folder name without any extension#

  targetFile="${targetDirectory}/${targetFilePrefix}_${safeFileStem}.${OUTPUT_EXTENSION}"
  
  gpt8 "${graphXmlPath}" \
    -q 16 \
    -x \
    -c 80G \
    -Pwkt="${wkt}" \
    -POutput_NDVI="${targetFile}" \
    -PInput="${sourceFile}"
    
  done
