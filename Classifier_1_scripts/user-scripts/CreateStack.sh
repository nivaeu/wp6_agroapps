#!/bin/bash
#

############################################
# Command line handling
############################################

# first parameter is the folder path from which to read the S2_Mosaic stack list
sourceFolder1="$1"   #/eo-tools/data/downloads/NDVI/NDVI_S2_Mosaic

# second parameter is the folder path from which to read the S2_QM_Mosaic stack list
sourceFolder2="$2"	 #/eo-tools/data/downloads/NDVI/NDVI_S2_QM_Mosaic/

# third parameter folder path to place target products
targetDirectory="$3" #/eo-tools/data/downloads/NDVI/GapFilled_NDVI

############################################
# Main processing
############################################


#Create the target directory
mkdir -p "${targetDirectory}"

#Create StackLists. List in " " quotes all files with a specific extension (.tif) separated by a specific delimiter (;)

cd $sourceFolder1
stack1=$(ls -1 "$PWD/"*.tif | tr '\n' ';' | sed -e '$a\' | sed 's/.$//' | sed 's/$/"/' | sed 's/^/"/')

cd $sourceFolder2
stack2=$(ls -1 "$PWD/"*.tif | tr '\n' ';' | sed -e '$a\' | sed 's/.$//' | sed 's/$/"/' | sed 's/^/"/')

#Target Products

targetFile1="${targetDirectory}/NDVI_LAYERSTACK.img"
targetFile2="${targetDirectory}/QM_LAYERSTACK.img"


#Run Toolchain

  saga_cmd toolchains SAGA_CMD_LayerStack_int16 \
    -INDEX_GRIDLIST="${stack1}" \
    -QM_GRIDLIST="${stack2}" \
    -INDEX_LAYERSTACK="${targetFile1}" \
	-QM_LAYERSTACK="${targetFile2}"
	
#Remove core dumped files

cd $sourceFolder2
rm core