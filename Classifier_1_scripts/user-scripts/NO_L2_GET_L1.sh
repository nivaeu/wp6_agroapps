#!/bin/bash

if [[ -z $1 ]]; then
    echo "Missing argument"
    usage
    exit 1
fi

cd ../data/downloads/
mkdir L1Products
mv /eo-tools/user-scripts/IS_EMPTY.log /eo-tools/data/downloads/L1Products
cd L1Products
#while reading IS_EMPTY.log for each line do define it as path
#run from L1 folder
products_file=$1
while read line; do

#Path=/eodata/Sentinel-2/MSI/L2A/2018/08/25/S2A_MSIL2A_20180825T093031_N0208_R136_T34TDS_20180825T120941.SAFE
Path=$line

	#/eodata/Sentinel-2/MSI/L2A/2018/08/25/S2A_MSIL2A_20180825T093031_N0208_R136_T34TDS_20180825T120941.SAFE
DateSubFolder=${Path:0:38}
L1DateSubFolder=${DateSubFolder/L2A/'L1C'}
TileTag=${Path:71:11}

echo $L1DateSubFolder
echo $TileTag
find $L1DateSubFolder -maxdepth 1 -name "*$TileTag*"  -exec cp -r {} /eo-tools/data/downloads/L1Products \; #also copied user scripts

#find . -maxdepth 1 -name "*$tag*" -print
done < $products_file

#Split L1 Products into subfolders (15) to run them on separate containers
cd ..
mkdir 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15
cd L1Products
x=("../01" "../02" "../03" "../04" "../05" "../06" "../07" "../08" "../09" "../10" "../11" "../12" "../13" "../14" "../15")
c=0
for f in *
do
    mv "$f" "${x[c]}"
    c=$(( (c+1)%15 ))
done












