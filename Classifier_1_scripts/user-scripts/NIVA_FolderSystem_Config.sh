#!/bin/bash
#

############################################
# User Configuration
############################################

############################################
# Command line handling
############################################

# first parameter is Sen4Cap parent folder definition
sen4capParent="$1"

# second parameter is Sen4Cap job name definition
sen4capJob="$2"

# third parameter is NIVA job name definition
nivaJob="$3"

############################################
# Main processing
############################################

#Set Sen4Cap parent folder
##	export SEN4CAP_PARENT_FOLDER=/mnt/archive/
 export SEN4CAP_PARENT_FOLDER=${sen4capParent}
#Set Sen4Cap job name
 export SEN4CAP_JOB=${sen4capJob}
#Create Folder Structrure
## Define where our Job folder will be placed
export JOB_NAME=${nivaJob}
export JOB_FOLDER=${SEN4CAP_PARENT_FOLDER}${JOB_NAME}
mkdir "${JOB_FOLDER}"
##Create Data SubFolders
cd ${JOB_FOLDER}
mkdir RAW_NDVI_L3B L3_C LOGS SHAPES TABLES
##Create Products SubFolders inside Data
cd ${JOB_FOLDER}/RAW_NDVI_L3B
mkdir NDVI NDVI_S2_Mosaic NDVI_S2_QM_Mosaic

cd ${JOB_FOLDER}/L3_C
mkdir GapFilled_NDVI 1D GLCM_ENTROPY GLCM_HOMOGENITY GLCM_VARIANCE
