#!/bin/bash
#

############################################
# Command line handling
############################################

# first parameter is the folder path from which to read the S2_Mosaic stack list
workingFolder="$1"   #/eo-tools/data/downloads/NDVI/NDVI_S2_QM_Mosaic/


############################################
# Main processing
############################################

#Create StackList. List in " " quotes all files with a specific extension (.tif) separated by a specific delimiter (;)

cd $workingFolder
stack1=$(ls -1 "$PWD/"*.tif | tr '\n' ';' | sed -e '$a\' | sed 's/.$//' | sed 's/$/"/' | sed 's/^/"/')


#Target Products

targetFile="${workingFolder}/QM_Percentage_Index.tif"



#Run Toolchain

  saga_cmd toolchains SAGA_CMD_Index_QM_Percentage \
    -QM_GRIDLIST="${stack1}" \
	-QM_PERCENTAGE="${targetFile}"
