#!/bin/bash
#
############################################
# Command line handling
############################################

# first parameter is the path to source products
sourceDirectory="$1"


############################################
# Main processing
############################################

for safeFilePath in $(ls -1d "${sourceDirectory}"/*MSIL1C*.SAFE); do # For each directory in a dir list inside source directory do ...# 

  absoluteSafePath=$(cd ${safeFilePath} && pwd -P) #Change directory to the SAFE directory and print working directory path #
  echo "PROCESSING L1 to L2 OF" $safeFilePath
  L2A_Process ${absoluteSafePath}
    
done

#Change directory to wd
cd "${sourceDirectory}"

#Remove All L1 Data *.SAFE Folders recursively 
find . -type d -name '*MSIL1C*.SAFE' -exec rm -r {} +