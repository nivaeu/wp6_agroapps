#Task : Filter NDVI Timeseries with Savitzky-Golay 1D & 2D
#Timeseries is gap-filled with spline interpolation, and spans from 2019-09-30 to 2020-07-31 with a 5 day sampling interval

library(raster)
library(sp)
library(rgdal)
library(tiff)
library(ggplot2)
library(maptools)
library (zoo)
library (signal)
library(timeSeries)
library(doParallel)
library(dplyr)

T1<-Sys.time()
#Set Working Directory
setwd("/eo-tools/data/downloads/NDVI/GapFilled_NDVI")

#Define Raster Stack
S2ts=brick("NDVI_S2_Mosaic_GapFill.tif")
names(S2ts)
#plotRGB(S2ts, r=7,g=5,b=14,scale=800,stretch = "lin")

#Extract Date Information
Starting_Date<-"2018-09-01" # Use "YYYY-MM-DD" format
Starting_DOY<-strftime(as.Date(Starting_Date), format = "%j")
Starting_Year<-format(as.Date(Starting_Date),format="%Y")

Ending_Date<-"2019-11-19" # Use "YYYY-MM-DD" format
Ending_DOY<-strftime(as.Date(Ending_Date), format = "%j")
Ending_Year<-format(as.Date(Ending_Date),format="%Y")

#Timeseries Parameters
Aggregation.Days<-6	#How many days in interval. Default value - 6d = 6"

Start.Year<-Starting_Year
Start.DOY<-Starting_DOY
Start.Int<-round(as.numeric(Start.DOY)/as.numeric(Aggregation.Days)) #On which aggregation interval do we start#

End.Year<-Ending_Year
End.DOY<-Ending_DOY
End.Int<-round(as.numeric(End.DOY)/as.numeric(Aggregation.Days)) #On which aggregation interval do we end#

Freq=round(365/as.numeric(Aggregation.Days)) #How many aggregation intervals over a year#

#Create Filtering Functions 
Filter1D <- function(x) {
v=as.vector(x)
z=substituteNA(v, type="mean") # from package timeSeries

	#Create Timeseries for 1D filtering

	#	start=c("Starting Year","Which 6-day comp"),
	#	end=c("Ending Year","Which 6-day comp"),
	#	frequency=How many 6-d in a year"


		#frequency=365/6 days=61

			#Start: Which 6-day comp in the series?
			#Starting Date =2020-11-01 ---> DOY="2020-11-01"-DATE(YEAR("2020-11-01"),1,1)+1=306
			#ROUND(306/6 days)=51 ,ie the 51th 6-d comp int he year (2020)

			#End : Which 6-day comp in the series?
			#Ending Date=2021-08-25  ---> DOY="2021-08-25"-DATE(YEAR("2021-08-25"),1,1)+1=237
			#ROUND(237/6 days)=39 ,ie the 40th 6-d comp in τhe year (2021)

#NDVI.ts2 = ts(z, start=c(as.numeric(Start.Year),as.numeric(Start.Int)), end=c(as.numeric(End.Year),as.numeric(End.Int)), frequency=as.numeric(Freq))#
NDVI.ts2 = ts(z, start=c(2015,41), end=c(2016,54), frequency=61)

	#1st Derivative Savitzky-Golay
		# p=filter order (polynomial order, specified as a positive integer. p must be smaller than n. If p = n – 1, the filter produces no smoothing.If p<n+1 smoothing occurs)
		# n=filter length (must be positive odd integer)
		# m=return the m-th derivative of the filter coefficients
		# ts=time scaling factor (1/nr of days in composites)
#x=sgolayfilt(NDVI.ts2, p=3, n=as.numeric(w),m=2,ts=1/as.numeric(Aggregation.Days))  }
 x=sgolayfilt(NDVI.ts2, p=2, n=3,m=1,ts=1/6)  }
 
 
 Filter2D <- function(x) {
v=as.vector(x)
z=substituteNA(v, type="mean") # from package timeSeries

	#Create Timeseries for 2D filtering

	#	start=c("Starting Year","Which 6-day comp"),
	#	end=c("Ending Year","Which 6-day comp"),
	#	frequency=How many 6-d in a year"


		#frequency=365/6 days=61

			#Start: Which 6-day comp in the series?
			#Starting Date =2020-11-01 ---> DOY="2020-11-01"-DATE(YEAR("2020-11-01"),1,1)+1=306
			#ROUND(306/6 days)=51 ,ie the 51th 6-d comp int he year (2020)

			#End : Which 5-day comp in the series?
			#Ending Date=2021-08-25  ---> DOY="2021-08-25"-DATE(YEAR("2021-08-25"),1,1)+1=237
			#ROUND(237/6 days)=39 ,ie the 40th 6-d comp in τhe year (2021)

#NDVI.ts2 = ts(z, start=c(as.numeric(Start.Year),as.numeric(Start.Int)), end=c(as.numeric(End.Year),as.numeric(End.Int)), frequency=as.numeric(Freq))#
NDVI.ts2 = ts(z, start=c(2015,41), end=c(2016,54), frequency=61)


	#2nd Derivative Savitzky-Golay
		# p=filter order (polynomial order, specified as a positive integer. p must be smaller than n. If p = n – 1, the filter produces no smoothing.If p<n+1 smoothing occurs)
		# n=filter length (must be positive odd integer)
		# m=return the m-th derivative of the filter coefficients
		# ts=time scaling factor (1/nr of days in composites)
 #x=sgolayfilt(NDVI.ts2, p=3, n=as.numeric(w),m=2,ts=1/as.numeric(Aggregation.Days))  }
 x=sgolayfilt(NDVI.ts2, p=2, n=3,m=2,ts=1/6)  }
 
 
#Apply Savitzky-Golay filters to Raster Stack as Grid Calculator
UseCores <- detectCores() -1
print("UsedCores =")
print(UseCores)
registerDoParallel() 
#NDVI.1D <- calc(S2ts, Filter1D, progress='text') #raster calculation process
print(rasterOptions())
rasterOptions(todisk = TRUE,chunksize=5e10, maxmemory=10e10, memfrac=0.8)
print(rasterOptions())
start.time <- Sys.time()
beginCluster(UseCores)	
NDVI.1Dv2 <- clusterR(S2ts,calc,args=list(fun=Filter1D), progress='text')
endCluster()
end.time <- Sys.time()
exec.time<-difftime(end.time,start.time)
print(exec.time)
#NDVI.2D <- calc(S2ts, Filter2D, progress='text') #raster calculation process

start.time <- Sys.time()
beginCluster(UseCores)	
NDVI.2Dv2 <- clusterR(S2ts,calc,args=list(fun=Filter2D), progress='text')
endCluster()
end.time <- Sys.time()
exec.time<-difftime(end.time,start.time)
print(exec.time)
#Write Raster in parallel processing
		#writeRaster(NDVI.1Dv2 , "./1D/NDVI_1st_Derivative.tif", format="GTiff", bylayer=TRUE)
		#
		#writeRaster(NDVI.2Dv2 , "./2D/NDVI_2nd_Derivative.tif", format="GTiff", bylayer=TRUE)
		
		
cl <- makeCluster(UseCores)
registerDoParallel(cl)


tmp1 <- foreach(i = 1:nlayers(NDVI.1Dv2)) %dopar% 
{  
  r <- raster::raster(NDVI.1Dv2, i)
  raster::writeRaster(r, 
                      filename = paste0("./1D/NDVI_1st_Derivative_", formatC(i, width = 2, flag = "0")),NAflag=-99999, format="GTiff",overwrite=TRUE)
  rm(r)
}

tmp2 <- foreach(i = 1:nlayers(NDVI.2Dv2)) %dopar% 
{  
  r <- raster::raster(NDVI.2Dv2, i)
  raster::writeRaster(r, 
                      filename = paste0("./2D/NDVI_2nd_Derivative_", formatC(i, width = 2, flag = "0")),NAflag=-99999, format="GTiff",overwrite=TRUE)
  rm(r)
}
stopCluster(cl)   

T2<-Sys.time()
XT<-difftime(T2,T1)
print(XT)




