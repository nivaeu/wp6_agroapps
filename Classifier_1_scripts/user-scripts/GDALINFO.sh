#!/bin/bash
#

############################################
# Command line handling
############################################
# first parameter is a path to source products
sourceDirectory="$1"

############################################
# Main processing
############################################
for RasterFilePath in $(ls -1d "${sourceDirectory}"/*.tif); do # For each directory in a dir list inside source directory do ...# 

  sourceRaster=${RasterFilePath}
  
  gdalinfo ${sourceRaster} -stats

  done
