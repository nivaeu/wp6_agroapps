#!/bin/bash
#
############################################
## User Configuration
############################################

##set SAGAcmdPath="c#\Users\adimitrakos\Desktop\GIS-RS_Software\saga-7.2.0_x64\saga_cmd.exe"
##set toolchain="SAGA_CMD_CLIP_LAYERSTACK_MASK_PATCHES"

############################################
## Command line handling
############################################


sourceDirectory="$1"  

sourceImage="$2"	

targetImageFolder="/eo-tools/data/grasslands/TRAIN_DATA/SR_IMAGE_PATCHES"			#"/eo-tools/data/grasslands/TRAIN_DATA/NDVI_IMAGE_PATCHES"

targetMaskFolder="/eo-tools/data/grasslands/TRAIN_DATA/MASK_PATCHES"

targetImagePrefix="SR"					#"NDVI"

targetMaskPrefix="MASK"

############################################
## Main processing
############################################


for F in $(ls -1d "${sourceDirectory}"/*.gpkg); do

sourceFileFullPath=${F}
sourceFile=$(basename $F)								
sourceFileStem=${sourceFile%.*}

targetImage=${targetImageFolder}/${targetImagePrefix}_${sourceFileStem}.tif
targetMask=${targetMaskFolder}/${targetMaskPrefix}_${sourceFileStem}.tif

echo ${targetImage}
echo ${targetMask}

saga_cmd toolchains SAGA_CMD_CLIP_LAYERSTACK_MASK_PATCHES \
 -LAYERSTACK="${sourceImage}" \
 -VECTOR_MASK="${sourceFileFullPath}" \
 -NDVI_CLIPPED="${targetImage}" \
 -RASTER_LABLES="${targetMask}"


done

