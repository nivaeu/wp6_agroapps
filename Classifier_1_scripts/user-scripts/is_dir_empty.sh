#!/bin/bash

if [[ -z $1 ]]; then
    echo "Missing argument"
    usage
    exit 1
fi

if [[ -f $1 ]]; then
    products_file=$1

    while read product_path; do
        if [ -d "$product_path" ]
        then
            if [ "$(ls -l $product_path | wc -l)" -gt 4 ]; then
            echo "$product_path is NOT Empty"
            else
            echo "$product_path is Empty"
            fi
        else
            echo "Directory $product_path not found."
        fi
    done < $products_file

fi

