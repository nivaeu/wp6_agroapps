#!/bin/bash
#

############################################
# User Configuration
############################################

# adapt this path to your needs
	#gptPath="/?????/gpt.sh"

	#gptPath="/usr/local/snap-6/bin/gpt.sh"

	#gptPath=$(which gpt)

############################################
# Command line handling
############################################

# first parameter is a path to the graph xml
graphXmlPath="$1"

# second parameter for path to source products
sourceDirectory="$2"

# use third parameter for path to ENTROPY target products
targetDirectory1="$3"

# use fourth parameter for path to VARIANCE target products
targetDirectory2="$4"

# use fifth parameter for path to HOMOGENITY target products
targetDirectory3="$5"


############################################
# Main processing
############################################

# Constant
targetFilePrefix1="ENT"
targetFilePrefix2="VAR"
targetFilePrefix3="HOM"

# Create the target directories
mkdir -p "${targetDirectory1}"
mkdir -p "${targetDirectory2}"
mkdir -p "${targetDirectory3}"


for RasterFilePath in $(ls -1d "${sourceDirectory}"/*.tif); do # For each directory in a dir list inside source directory do ...# 

  #absolutePath=$(cd ${RasterFilePath} && pwd -P) #Change directory to the SAFE directory and print working directory path #

  sourceRaster=${RasterFilePath}
  RasterFile=$(basename $RasterFilePath) #Extract input name with .tif extension, form absolute path#
  
  #RasterFile=$(basename $absoluteSafePath) #Extract SAFE folder name with .SAFE extension, form absolute path#
  #safeFileStem=${safeFile%.*} #Extract SAFE folder name without any extension#

  targetFile1="${targetDirectory1}/${targetFilePrefix1}_${RasterFile}"
  targetFile2="${targetDirectory2}/${targetFilePrefix2}_${RasterFile}"
  targetFile3="${targetDirectory3}/${targetFilePrefix3}_${RasterFile}"
  
  gpt8 "${graphXmlPath}" \
    -q 16 \
    -x \
    -c 80G \
    -POutput_ENT="${targetFile1}" \
	-POutput_VAR="${targetFile2}" \
	-POutput_HOM="${targetFile3}" \
    -PInput="${sourceRaster}"
    
  done
