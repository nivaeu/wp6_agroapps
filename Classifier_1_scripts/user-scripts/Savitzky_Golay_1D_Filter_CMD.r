#Task : Filter NDVI Timeseries with Savitzky-Golay 1D & 2D
#Timeseries is gap-filled with spline interpolation, and spans from 2019-09-30 to 2020-07-31 with a 5 day sampling interval
################################################

#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly=TRUE)
T1<-Sys.time()

library(raster)
library(sp)
library(rgdal)
library(tiff)
library(ggplot2)
library(maptools)
library (zoo)
library (signal)
library(timeSeries)
library(doParallel)
library(dplyr)
#Set Working Directory
setwd(args[1])

#Define Raster Stack
S2ts=brick(args[2])
names(S2ts)
#plotRGB(S2ts, r=7,g=5,b=14,scale=800,stretch = "lin")

#Extract Date Information
Start<-args[3] # Use "YYYYMMDD" format
Starting_Date<-as.Date(Start,"%Y%m%d")
Starting_DOY<-strftime(as.Date(Starting_Date), format = "%j")
Starting_Year<-format(as.Date(Starting_Date),format="%Y")

End<-args[4] # Use "YYYYMMDD" format
Ending_Date<-as.Date(End,"%Y%m%d")
Ending_DOY<-strftime(as.Date(Ending_Date), format = "%j")
Ending_Year<-format(as.Date(Ending_Date),format="%Y")

#Timeseries Parameters
Aggregation.Days<-args[5]	#How many days in interval. Default value - 10d = 10"

Start.Year<-Starting_Year
Start.DOY<-Starting_DOY
Start.Int<-round(as.numeric(Start.DOY)/as.numeric(Aggregation.Days)) #On which aggregation interval do we start#

End.Year<-Ending_Year
End.DOY<-Ending_DOY
End.Int<-round(as.numeric(End.DOY)/as.numeric(Aggregation.Days)) #On which aggregation interval do we end#

Freq=round(365/as.numeric(Aggregation.Days)) #How many aggregation intervals over a year#
#Function Parameter Objects
START<-c(as.numeric(Start.Year),Start.Int)
END<-c(as.numeric(End.Year),End.Int)
FREQUENCY<-Freq
AGGREGATION<-Aggregation.Days
#Create Filtering Functions 
Filter1D <- function(x) {
  v=as.vector(x)
  z=substituteNA(v, type="mean") # from package timeSeries
  
  NDVI.ts2 = ts(z, start=START, end=END, frequency=FREQUENCY) #Create TimeSeries Object
  
	#1st Derivative Savitzky-Golay
	# p=filter order (polynomial order, specified as a positive integer. p must be smaller than n. If p = n – 1, the filter produces no smoothing.If p<n+1 smoothing occurs)
	# n=filter length (must be positive odd integer)
	# m=return the m-th derivative of the filter coefficients
	# ts=time scaling factor (1/nr of days in composites)
  x=sgolayfilt(NDVI.ts2, p=2, n=3,m=1,ts=(1/as.numeric(AGGREGATION)))  }
  
#Apply Savitzky-Golay filters to Raster Stack as Grid Calculator, with Cluster Parallelization
UseCores <- detectCores() -1
print("UsedCores =")
print(UseCores)
registerDoParallel() 

print(rasterOptions())
rasterOptions(todisk = TRUE,chunksize=1e+08, maxmemory=2e10, memfrac=0.6)
print(rasterOptions())

start.time <- Sys.time()
beginCluster(UseCores)	
cl<-getCluster()
clusterExport(cl,"START")
clusterExport(cl,"END")
clusterExport(cl,"FREQUENCY")
clusterExport(cl,"AGGREGATION")

NDVI.1Dv2 <- clusterR(S2ts,calc,args=list(fun=Filter1D), progress='text')

endCluster()

end.time <- Sys.time()
exec.time<-difftime(end.time,start.time)
print(exec.time)

#Write Raster in parallel processing (On a parallel folder : From wd /L3_C/GapFilled_NDVI/ to /L3_C/1D/)
		#writeRaster(NDVI.1Dv2 , "../1D/NDVI_1st_Derivative.tif", format="GTiff", bylayer=TRUE) - Folder /1D/ already created
				
cl <- makeCluster(UseCores)
registerDoParallel(cl)

tmp1 <- foreach(i = 1:nlayers(NDVI.1Dv2)) %dopar% 
{  
  r <- raster::raster(NDVI.1Dv2, i)
  raster::writeRaster(r, 
                      filename = paste0("../1D/NDVI_1st_Derivative_", formatC(i, width = 2, flag = "0")),NAflag=-99999, format="GTiff",overwrite=TRUE)
  rm(r)
}
stopCluster(cl)   

T2<-Sys.time()
XT<-difftime(T2,T1)
print(XT)

