#!/bin/bash
#

############################################
# Command line handling
############################################


# first parameter is Feature Variable from L3_C to take zonal statistics from :GapFilled_NDVI,1D,GLCM_ENTROPY,GLCM_HOMOGENITY,GLCM_VARIANCE
sourceVar="$1"

# 2nd parameter is the type of LPIS shapefile subset to take zonal statistics with: training, prediction (training_merged, prediction_merged for optional DB Schema)
shapefile="$2"

# third parameter is the folder path from which to read the shapefiles : /eo-tools/data/downloads/IMATHIA/SHAPES/
shapeFolder="$3"

# fourth parameter is the folder path where zonal tables will be written : /eo-tools/data/downloads/IMATHIA/TABLES/
targetFolder="$4"

# fifth  parameter is the name of the job
jobName="$5"
# sixth  parameter is the name of the job
jobFolder="$6"
############################################
# Main processing
############################################

varSeq="${sourceVar}"
shapeSeq="${shapefile}"
JOB_FOLDER="${jobFolder}"

for i in ${varSeq}
do
    for j in ${shapeSeq}
    do 
	
sourceFolder=${JOB_FOLDER}/L3_C/${i}/
cd ${sourceFolder}
rasterList=$(ls -1 "$PWD/"*.tif | tr '\n' ';' | sed -e '$a\' | sed 's/.$//' | sed 's/$/"/' | sed 's/^/"/')

#Create Shapefile path
PolygonFile="${shapeFolder}${j}.shp"

#Target Table Product
targetFile="${targetFolder}ZonalStatistics_${i}_${j}_${jobName}.csv"


#Modify Toolchain as far as what statistics we want

#Run Toolchain

	saga_cmd toolchains SAGA_CMD_Script_Zonal_Statistics \
	-Raster_Layer_GRIDLIST="${rasterList}" \
	-Feature_Zones_POLYGONS="${PolygonFile}" \
	-ZONAL_STATS_TAB="${targetFile}"
	
    done
done
	
	
	
       







	


