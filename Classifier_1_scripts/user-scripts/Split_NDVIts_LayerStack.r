#!/usr/bin/env Rscript
args <- commandArgs(trailingOnly=TRUE)

library(raster)
library(sp)
library(rgdal)
library(tiff)
library(maptools)
library(timeSeries)
library(doParallel)
library(dplyr)

T1<-Sys.time()
#Set Working Directory
setwd(args[1])

#Define Raster Stack
S2ts=brick("NDVI_S2_Mosaic_GapFill_BV.tif")
names(S2ts)
#plotRGB(S2ts, r=7,g=5,b=14,scale=800,stretch = "lin")


#Write Raster in parallel processing
		
		
UseCores <- detectCores() -1
print("UsedCores =")
print(UseCores)		
cl <- makeCluster(UseCores)
registerDoParallel(cl)


tmp1 <- foreach(i = 1:nlayers(S2ts)) %dopar% 
{  
  r <- raster::raster(S2ts, i)
  raster::writeRaster(r, 
                      filename = paste0("./NDVI_SL/NDVI_S2_Mosaic_GapFill_BV_", formatC(i, width = 2, flag = "0")), format="GTiff",NAflag=-99999,overwrite=TRUE)
  rm(r)
}


stopCluster(cl)   

T2<-Sys.time()
XT<-difftime(T2,T1)
print(XT)


